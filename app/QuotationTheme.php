<?php

namespace App;

use App\Observers\QuotationThemeObserver;
use App\Scopes\CompanyScope;

class QuotationTheme extends BaseModel
{
    protected $table = 'quotation_themes';

    protected $fillable = ['name','allow_purchase','description','category_id','sub_category_id','contract_theme_id'];
    
    protected static function boot()
    {
        parent::boot();

        static::observe(QuotationThemeObserver::class);

        static::addGlobalScope(new CompanyScope);
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }

    public function subcategory()
    {
        return $this->belongsTo(ProductSubCategory::class, 'sub_category_id');
    }

}
