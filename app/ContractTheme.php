<?php

namespace App;

use App\Observers\ContractThemeObserver;
use App\Scopes\CompanyScope;

class ContractTheme extends BaseModel
{
    protected $table = 'contract_themes';

    protected $fillable = ['name','allow_purchase','description','category_id','sub_category_id'];
    
    protected static function boot()
    {
        parent::boot();

        static::observe(ContractThemeObserver::class);

        static::addGlobalScope(new CompanyScope);
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }

    public function subcategory()
    {
        return $this->belongsTo(ProductSubCategory::class, 'sub_category_id');
    }

}
