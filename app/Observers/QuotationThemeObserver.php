<?php

namespace App\Observers;

use App\QuotationTheme;

class QuotationThemeObserver
{

    public function saving(QuotationTheme $quotation_themes)
    {
        // Cannot put in creating, because saving is fired before creating. And we need company id for check bellow
        if (company()) {
            $quotation_themes->company_id = company()->id;
        }
    }

}
