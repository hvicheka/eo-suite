<?php

namespace App\Listeners;

use App\Events\NewProjectEvent;
use App\Notifications\NewProject;

use App\Scopes\CompanyScope;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class NewProjectListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * NewExpenseRecurringListener constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @param NewProjectEvent $event
     */
    public function handle(NewProjectEvent $event)
    {
//
//        if ($event->project->notify_admin) {
//            $email = 'hvicheka.it@gmail.com';
//
//            $user = User::query()
//                ->where('email', $email)
//                ->firstOrFail();
//            Notification::send($user, new NewProject($event->project));
//        }


        if (($event->project->client_id != null)) {
            $clientId = $event->project->client_id;
            // Notify client
            $notifyUser = User::withoutGlobalScopes([CompanyScope::class, 'active'])->findOrFail($clientId);
            if ($notifyUser) {
                Notification::send($notifyUser, new NewProject($event->project));
            }
        }

        if ($event->project->notify_admin) {
            $admins = User::allAdmins();
            foreach ($admins as $admin) {
                Notification::send($admin, new NewProject($event->project));
            }
        }
    }


}
