<?php

namespace App\Http\Controllers\Admin;

use App\Tax;
use App\Product;
use App\Helper\Reply;
use App\ContractTheme;
use App\InvoiceSetting;
use App\QuotationTheme;
use App\ProductCategory;
use App\ProductSubCategory;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\DataTables\Admin\QuotationThemeDataTable;
use App\Http\Requests\Product\StoreProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Http\Requests\QuotationTheme\StoreQuotationThemeRequest;
use App\Http\Requests\QuotationTheme\UpdateQuotationThemeRequest;

class AdminQuotationThemeController extends AdminBaseController
{


    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.quotation_theme';
        $this->pageIcon = 'icon-printer fa-fw';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(QuotationThemeDataTable $dataTable)
    {
        $this->totalQuotationTheme    = QuotationTheme::count();
        $this->categories       = ProductCategory::all();
        $this->subCategories    = ProductSubCategory::all();
        return $dataTable->render('admin.quotation-themes.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->contract_themes = ContractTheme::all();
        $this->categories = ProductCategory::all();
        $this->subCategories = ProductSubCategory::all();
        $this->invoiceSetting = InvoiceSetting::first();
        return view('admin.quotation-themes.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreQuotationThemeRequest $request)
    {
        
        $quotation_themes                   = new QuotationTheme();
        $quotation_themes->name             = $request->name;
        $quotation_themes->contract_theme_id             = $request->contract_theme_id;
        $quotation_themes->description      = $request->description;
        $quotation_themes->allow_purchase   = ($request->purchase_allow == 'no') ? true : false;
        $quotation_themes->category_id      = ($request->category_id) ? $request->category_id : null;
        $quotation_themes->sub_category_id  = ($request->sub_category_id) ? $request->sub_category_id : null;
        $quotation_themes->save();
      
        return Reply::redirect(route('admin.quotation-theme.index'), __('messages.quotationThemeAdded'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->quotation_theme = QuotationTheme::find($id);
       
        $this->invoiceSetting = InvoiceSetting::first();
        return view('admin.quotation-themes.show', $this->data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->quotation_theme  = QuotationTheme::find($id);
        $this->contract_themes = ContractTheme::all();
        $this->categories       = ProductCategory::all();
        $this->subCategories    = ProductSubCategory::all();
        $this->invoiceSetting   = InvoiceSetting::first();

        return view('admin.quotation-themes.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateQuotationThemeRequest $request, $id)
    {
        
        $quotation_themes = QuotationTheme::find($id);
       
        $quotation_themes->name = $request->name;
        $quotation_themes->contract_theme_id             = $request->contract_theme_id;
        $quotation_themes->description = $request->description;
        $quotation_themes->allow_purchase = ($request->purchase_allow == 'no') ? true : false;
        $quotation_themes->category_id = ($request->category_id) ? $request->category_id : null;
        $quotation_themes->sub_category_id = ($request->sub_category_id) ? $request->sub_category_id : null;
        $quotation_themes->save();

        return Reply::redirect(route('admin.quotation-theme.index'), __('messages.quotationThemeUpdated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        QuotationTheme::destroy($id);
        return Reply::success(__('messages.quotationThemeDeleted'));
    }
   
    public function export()
    {
        $attributes = [];
        $quotation_themes = QuotationTheme::select('id', 'name')
            ->get()->makeHidden($attributes);

            // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($quotation_themes as $row) {
            $rowArrayData = $row->toArray();
            $exportArray[] = $rowArrayData;
        }

        // Generate and return the spreadsheet
        Excel::create('Quotation Theme', function($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Quotation Theme');
            $excel->setCreator('Worksuite')->setCompany($this->companyName);
            $excel->setDescription('Quotation Theme file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       => true
                    ));

                });

            });



        })->download('xlsx');
    }

    public function getSubcategory(Request $request)
    {
        $this->subcategories = ProductSubCategory::where('category_id', $request->cat_id)->get();

        return Reply::dataOnly(['subcategory' => $this->subcategories]);
    }

}
