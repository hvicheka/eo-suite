<?php

namespace App\Http\Controllers\Admin;

use App\Tax;
use App\Product;
use App\Helper\Reply;
use App\ContractTheme;
use App\InvoiceSetting;
use App\QuotationTheme;
use App\ProductCategory;
use App\ProductSubCategory;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\DataTables\Admin\ContractThemeDataTable;
use App\Http\Requests\Product\StoreProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Http\Requests\ContractTheme\StoreContractThemeRequest;
use App\Http\Requests\ContractTheme\UpdateContractThemeRequest;

class AdminContractThemeController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.contract_theme';
        $this->pageIcon = 'icon-printer fa-fw';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ContractThemeDataTable $dataTable)
    {
        $this->totalContractTheme    = ContractTheme::count();
        $this->categories       = ProductCategory::all();
        $this->subCategories    = ProductSubCategory::all();
        return $dataTable->render('admin.contract-themes.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->categories = ProductCategory::all();
        $this->subCategories = ProductSubCategory::all();
        $this->invoiceSetting = InvoiceSetting::first();
        return view('admin.contract-themes.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContractThemeRequest $request)
    {
       
        $contract_themes                   = new ContractTheme();
        $contract_themes->name             = $request->name;
        $contract_themes->description      = $request->description;
        $contract_themes->allow_purchase   = ($request->allow_purchase == 'no') ? true : false;
        $contract_themes->category_id      = ($request->category_id) ? $request->category_id : null;
        $contract_themes->sub_category_id  = ($request->sub_category_id) ? $request->sub_category_id : null;
        $contract_themes->save();
      
        return Reply::redirect(route('admin.contract-theme.index'), __('messages.contractThemeAdded'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->quotation_theme = QuotationTheme::find($id);
       
        $this->invoiceSetting = InvoiceSetting::first();
        return view('admin.quotation-themes.show', $this->data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->contract_theme  = ContractTheme::find($id);
        $this->categories       = ProductCategory::all();
        $this->subCategories    = ProductSubCategory::all();
        $this->invoiceSetting   = InvoiceSetting::first();

        return view('admin.contract-themes.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateContractThemeRequest $request, $id)
    {
      
        try{
            $contract_themes = ContractTheme::find($id);
            $contract_themes->name = $request->name;
            $contract_themes->description = $request->description;
            $contract_themes->allow_purchase = ($request->allow_purchase == 'no') ? true : false;
            $contract_themes->category_id = ($request->category_id) ? $request->category_id : null;
            $contract_themes->sub_category_id = ($request->sub_category_id) ? $request->sub_category_id : null;
            $contract_themes->save();
        }catch (\Exception $e) {
            return $e->getMessage();
        }
        
        return Reply::redirect(route('admin.contract-theme.index'), __('messages.contractThemeUpdated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ContractTheme::destroy($id);
        return Reply::success(__('messages.contractThemeDeleted'));
    }
   
    public function export()
    {
        $attributes = [];
        $contract_themes = ContractTheme::select('id', 'name')
            ->get()->makeHidden($attributes);

            // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($contract_themes as $row) {
            $rowArrayData = $row->toArray();
            $exportArray[] = $rowArrayData;
        }

        // Generate and return the spreadsheet
        Excel::create('Contract Theme', function($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Contract Theme');
            $excel->setCreator('Worksuite')->setCompany($this->companyName);
            $excel->setDescription('Contract Theme file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       => true
                    ));

                });

            });



        })->download('xlsx');
    }

    public function getSubcategory(Request $request)
    {
        $this->subcategories = ProductSubCategory::where('category_id', $request->cat_id)->get();

        return Reply::dataOnly(['subcategory' => $this->subcategories]);
    }

}
