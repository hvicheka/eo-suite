<?php

namespace App\Http\Controllers;

use App\Events\NewProjectEvent;
use App\GoogleAccount;
use App\Notifications\TestEmail;
use App\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TestingController extends Controller
{

    public function login()
    {
        $id = \request('id', '2');
        $user = User::query()
            ->where('id', $id)
            ->firstOrFail();
        auth()->loginUsingId($user->id);
        return redirect()->to('/login');
    }

    public function send_mail()
    {
        try {
            $email = \request('email', 'hvicheka.it@gmail.com');
            $user = User::query()->where('email', $email)->firstOrFail();
            // Notify User
            $user->notify(new TestEmail());
            dd('KK');
        } catch (\Exception $exception) {
            dd($exception);
        }

    }

    public function add_project_send_mail(Request $request)
    {

        try {
            $project = Project::query()
                ->latest()
                ->first();

            event(new NewProjectEvent($project));

        } catch (\Exception $exception) {
            dd($exception);
        }

    }


    public function get_google_account(Request $request)
    {
        $googleAccount = GoogleAccount::query()->where('user_id', auth()->id())->first();
        dd($googleAccount);
    }

    public function attendance()
    {
        $dt = Carbon::now()->timezone('Asia/Phnom_Penh');
        return response()->json($dt);
    }

    public function role()
    {
        $url = null;
        if (auth()->user()->hasRole('admin')) {
            $url = route('admin.profile-settings.index');
        } elseif (auth()->user()->hasRole('employee')) {
            $url = route('member.profile.index');
        } elseif (auth()->user()->hasRole('client')) {
            $url = route('client.profile.index');
        }

        dd('kkk', $url);
    }
}
