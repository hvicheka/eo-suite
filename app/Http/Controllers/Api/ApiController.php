<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Lead;


class ApiController extends Controller
{
    public function storeLeadFromWebsite(Request $request)
    {
        $lead = new Lead();
        $lead->company_name = $request->company_name;
        $lead->client_email = $request->email;
        $lead->mobile = $request->input('phone_code').' '.$request->input('mobile');
        $lead->currency_id = company()->currency_id;
        $lead->save();
        if($lead->id){
            $data = true;
        }else{
            $data = false;
        }
        return response()->json($data);
    }
}
