<?php

namespace App\Http\Controllers\Member;

use App\EmployeeDetails;
use App\GoogleCalendarModules;
use App\Helper\Files;
use App\Helper\Reply;
use App\Http\Requests\User\UpdateProfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class MemberProfileController extends MemberBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.profileSettings';
        $this->pageIcon = 'icon-user';
    }

    public function index()
    {
        $this->userDetail = auth()->user();
        $this->employeeDetail = EmployeeDetails::where('user_id', '=', $this->userDetail->id)->first();
        $google_account = DB::table('google_accounts')->where('user_id', auth()->id())->first();
        $google_calendar_module = GoogleCalendarModules::query()->where('user_id', auth()->id())->first();
        return view('member.profile.edit', $this->data, ['google_account' => $google_account]);
    }

    public function update(UpdateProfile $request, $id)
    {
        config(['filesystems.default' => 'local']);

        $user = User::withoutGlobalScope('active')->findOrFail($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->gender = $request->input('gender');
        if ($request->password != '') {
            $user->password = Hash::make($request->input('password'));
        }

        $user->mobile = $request->input('mobile.full', $request->mobile);
        $user->email_notifications = $request->email_notifications;

        if ($request->hasFile('image')) {
            Files::deleteFile($user->image, 'avatar');
            $user->image = Files::upload($request->image, 'avatar', 300);
        }

        $user->save();

        $validate = Validator::make(['address' => $request->address], [
            'address' => 'required'
        ]);

        if ($validate->fails()) {
            return Reply::formErrors($validate);
        }

        $employee = EmployeeDetails::where('user_id', '=', $user->id)->first();
        if (empty($employee)) {
            $employee = new EmployeeDetails();
            $employee->user_id = $user->id;
        }
        $employee->address = $request->address;
        $employee->save();

        // Setting for google calendar setting

        GoogleCalendarModules::query()->updateOrCreate([
            'user_id' => auth()->id()
        ], [
            'lead_status' => isset($request->lead_status),
            'leave_status' => isset($request->leave_status),
            'invoice_status' => isset($request->invoice_status),
            'contract_status' => isset($request->contract_status),
            'task_status' => isset($request->task_status),
            'event_status' => isset($request->event_status),
            'holiday_status' => isset($request->holiday_status)
        ]);

        session()->forget('user');
        $this->logUserActivity($user->id, __('messages.updatedProfile'));
        return Reply::redirect(route('member.profile.index'), __('messages.profileUpdated'));
    }

    public function updateOneSignalId(Request $request)
    {
        $user = User::find($this->user->id);
        $user->onesignal_player_id = $request->userId;
        $user->save();
    }

    public function changeLanguage(Request $request)
    {
        $setting = User::findOrFail($this->user->id);
        $setting->locale = $request->input('lang');
        $setting->save();
        session()->forget('user');
        return Reply::success('Language changed successfully.');
    }

}
