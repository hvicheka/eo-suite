<?php

namespace App\Http\Controllers\Member;

use App\Tax;
use App\Lead;
use App\User;
use App\Invoice;
use App\Product;
use App\Contract;
use App\Currency;
use App\Proposal;
use Carbon\Carbon;
use App\ContractItem;
use App\ContractSign;
use App\ContractType;
use App\Helper\Files;
use App\Helper\Reply;
use App\ContractTheme;
use App\InvoiceSetting;
use App\ContractDiscussion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\DataTables\Member\ContractsDataTable;
use App\Http\Requests\Admin\Contract\SignRequest;
use App\Http\Requests\Admin\Contract\StoreRequest;
use App\Http\Requests\Admin\Contract\UpdateRequest;
use App\Http\Requests\Admin\Contract\StoreDiscussionRequest;
use App\Http\Requests\Admin\Contract\UpdateDiscussionRequest;

class MemberContractController extends MemberBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageIcon = 'fa fa-file';
        $this->pageTitle = 'app.menu.contracts';
        $this->middleware(function ($request, $next) {
            abort_if(!in_array('contracts', $this->user->modules), 403);
            return $next($request);
        });
        //** new update */
        $this->ContractStatus = ['waiting'=>'Waiting','draft' =>'Draft','sent'=>'Sent','expired'=>'Expired','declined'=>'Declined','accepted'=> 'Accepted'];
    }

    public function index(ContractsDataTable $dataTable)
    {
        abort_if(!$this->user->cans('view_contract'), 403);
        $this->clients = User::allClients();
        $this->contractType = ContractType::all();
        $this->contractCounts = Contract::count();
        $this->expiredCounts = Contract::where(DB::raw('DATE(`end_date`)'), '<', Carbon::now()->format('Y-m-d'))->count();
        $this->aboutToExpireCounts = Contract::where(DB::raw('DATE(`end_date`)'), '>', Carbon::now()->format('Y-m-d'))
            ->where(DB::raw('DATE(`end_date`)'), '<', Carbon::now()->timezone($this->global->timezone)->addDays(7)->format('Y-m-d'))
            ->count();
        return $dataTable->render('member.contracts.index', $this->data);
        
    }

    public function create()
    {
        abort_if(!$this->user->cans('add_contract'), 403);
        $this->clients = User::allClients();
        $this->contractType = ContractType::all();
        // *****new update****
        $this->leads = Lead::all();
        $this->contract_themes = ContractTheme::where('allow_purchase',1)->select('id','name')->get();
        $this->currencies = Currency::all();
        $this->employees = User::
        withoutGlobalScope('active')
        ->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->where('roles.name', '<>', 'client')
        ->select('users.id', 'users.name')->get();
        $this->taxes = Tax::all();
        $this->products = Product::where('allow_purchase',1)->select('id','name','price','description')->get();
        
        return view('member.contracts.create', $this->data);
    }

    public function store(StoreRequest $request)
    {
        $contract = new Contract();
        $contract = $this->storeUpdate($request, $contract);
        return Reply::redirect(route('member.contracts.edit', $contract->id), __('messages.contractAdded'));
    }

    public function edit($id)
    {
        abort_if(!$this->user->cans('edit_contract'), 403);
        $this->clients = User::allClients();
        $this->contractType = ContractType::all();
        $this->contract = Contract::with('signature', 'renew_history', 'renew_history.renewedBy')->find($id);

        // **new update**
        $this->leads = Lead::all();
        $this->contract_themes = ContractTheme::where('allow_purchase',1)->select('id','name')->get();
        $this->employees = User::
            withoutGlobalScope('active')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', '<>', 'client')
            ->select('users.id', 'users.name')->get();
        $this->products = Product::where('allow_purchase',1)->select('id','name','price','description')->get();
        $this->currencies = Currency::all();
        $this->taxes = Tax::all();
        return view('member.contracts.edit', $this->data);
    }

    public function update(UpdateRequest $request, $id)
    {
        $contract = Contract::findOrFail($id);
        $this->storeUpdate($request, $contract);

        return Reply::redirect(route('member.contracts.index'), __('messages.contractUpdated'));
    }

    public function show($id)
    {
        abort_if(!$this->user->cans('view_contract'), 403);
        $this->contract = Contract::whereRaw('md5(id) = ?', $id)
            ->with('client', 'contract_type', 'signature', 'discussion', 'discussion.user')
            ->firstOrFail();

        return view('member.contracts.show', $this->data);
    }

    private function storeUpdate($request, $contract)
    {
        // ***New update***
        $items                  = $request->item_name;
        $cost_per_item          = $request->cost_per_item;
        $line_discount_value    = $request->line_discount_value;
        $line_discount_type     = $request->line_discount_type;
        $quantity               = $request->quantity;
        $item_id                = $request->item_id;
        $hsnSacCode             = $request->hsn_sac_code;
        $amount                 = $request->amount;
        $itemsSummary = $request->input('item_summary');
        $tax = $request->input('taxes');
        $type = $request->type;

        if($items) {
            foreach ($items as $index => $item) {
                if (!is_null($item)) {
                    if (!is_numeric($quantity[$index]) && (intval($quantity[$index]) < 1)) {
                        return Reply::error(__('messages.quantityNumber'));
                    }
                    if (!is_numeric($cost_per_item[$index])) {
                        return Reply::error(__('messages.unitPriceNumber'));
                    }
                    if (!is_numeric($amount[$index])) {
                        return Reply::error(__('messages.amountNumber'));
                    }
                }

                if ($index > 0 && is_null($item)) {
                    return Reply::error(__('messages.itemBlank'));
                }

            }
        }
        // *********************************************
        
        $contract->client_id = $request->client;
        $contract->subject = $request->subject;
        // $contract->amount = $request->amount;
        // $contract->original_amount = $request->amount;
        $contract->amount               = $request->total;
        $contract->original_amount      =  $request->total;
        $contract->contract_name = $request->contract_name;
        $contract->alternate_address = $request->alternate_address;
        $contract->mobile = $request->mobile;
        $contract->office_phone = $request->office_phone;
        $contract->city = $request->city;
        $contract->state = $request->state;
        $contract->country = $request->country;
        $contract->postal_code = $request->postal_code;
        $contract->contract_type_id = $request->contract_type;
        $contract->start_date = Carbon::createFromFormat($this->global->date_format, $request->start_date)->format('Y-m-d');
        $contract->original_start_date = Carbon::createFromFormat($this->global->date_format, $request->start_date)->format('Y-m-d');
        $contract->end_date = $request->end_date == null ? $request->end_date : Carbon::createFromFormat($this->global->date_format, $request->end_date)->format('Y-m-d');
        $contract->original_end_date = $request->end_date == null ? $request->end_date : Carbon::createFromFormat($this->global->date_format, $request->end_date)->format('Y-m-d');
        $contract->description = $request->description;
        if ($request->hasFile('company_logo')) {
            Files::deleteFile($contract->company_logo, 'avatar');
            $contract->company_logo = Files::upload($request->company_logo, 'avatar', 300);
        }

        if ($request->contract_detail) {
            $contract->contract_detail = $request->contract_detail;
        }

        if ($request->proposal_id) {
            $proposal = Proposal::findOrFail($request->proposal_id);
            $proposal->contract_convert = 1;
            $proposal->save();
        }

        // ******************new update********************
        $contract->relate_to          = $request->relate_to;
        $contract->lead_id          = $request->lead_id;
        $contract->contract_theme_id = $request->contract_theme_id;
        $contract->status             = $request->quotation_status;
        $contract->name               = $request->name;
        $contract->company_name       = $request->company_name;
        $contract->position           = $request->position;
        $contract->email              = $request->email;
        $contract->assist_to_id       = $request->assist_to_id;
        $contract->sale_by_id         = $request->sale_by_id;
        $contract->sale_by_id         = $request->sale_by_id;
        $contract->sub_total          = $request->sub_total;
        $contract->discount             = round($request->discount_value, 2);
        $contract->discount_type        = $request->discount_type;
        $contract->currency_id          = $request->currency_id;

        $contract->save();

        ContractItem::where('contract_id', $contract->id)->delete();

        if($items){
            foreach ($items as $key => $item) :
                if (!is_null($item)) {
                    try{
                        ContractItem::create(
                            [
                                'contract_id'   => $contract->id,
                                'item_name'     => $item,
                                'item_id'       => $item_id[$key],
                                'item_summary'  => $itemsSummary[$key],
                                'hsn_sac_code'  => (isset($hsnSacCode[$key]) && !is_null($hsnSacCode[$key])) ? $hsnSacCode[$key] : null,
                                'type'          => 'item',
                                'quantity'      => $quantity[$key],
                                'unit_price'    => round($cost_per_item[$key], 2),
                                'discount'      => $line_discount_value[$key]?$line_discount_value[$key]:0,
                                'discount_type' => $line_discount_type[$key],
                                'amount'        => round($amount[$key], 2),
                                'taxes'         => $tax ? array_key_exists($key, $tax) ? json_encode($tax[$key]) : null : null
                            ]
                        );
                    } catch (\Exception $e) {
                           dd($e);
                    }
                }
            endforeach;
        }

        return $contract;
    }

    public function destroy($id)
    {
        abort_if(!$this->user->cans('delete_contract'), 403);
        Contract::destroy($id);

        return Reply::success(__('messages.contactDeleted'));
    }

    // public function download($id)
    // {
    //     $this->contract = Contract::findOrFail($id);
    //     $pdf = app('dompdf.wrapper');

    //     $pdf->getDomPDF()->set_option('enable_php', true);
    //     $pdf->loadView('member.contracts.contract-pdf', $this->data);

    //     $dom_pdf = $pdf->getDomPDF();
    //     $canvas = $dom_pdf->get_canvas();
    //     $canvas->page_text(530, 820, 'Page {PAGE_NUM} of {PAGE_COUNT}', null, 10, array(0, 0, 0));


    //     $filename = 'contract-' . $this->contract->id;

    //     return $pdf->download($filename . '.pdf');
    // }

    public function download($id)
    {
        $this->contract = Contract::findOrFail($id);
        $this->contract_theme = ContractTheme::findOrFail($this->contract->contract_theme_id);
        if ($this->contract->discount > 0) {
            if ($this->contract->discount_type == 'percent') {
                $this->discount = (($this->contract->discount / 100) * $this->contract->sub_total);
            } else {
                $this->discount = $this->contract->discount;
            }
        } else {
            $this->discount = 0;
        }
        $this->taxes = ContractItem::where('type', 'tax')
        ->where('contract_id', $this->contract->id)
        ->get();
        
        $items = ContractItem::whereNotNull('taxes')
        ->where('contract_id', $this->contract->id)
        ->get();
        
        $taxList = array();
        
        foreach ($items as $item) {
            if ($this->contract->discount > 0 && $this->contract->discount_type == 'percent') {
                $item->amount = $item->amount - (($this->contract->discount / 100) * $item->amount);
            }
            foreach (json_decode($item->taxes) as $tax) {
                $this->tax = ContractItem::taxbyid($tax)->first();
                if ($this->tax) {
                    if (!isset($taxList[$this->tax->tax_name . ': ' . $this->tax->rate_percent . '%'])) {
                        $taxList[$this->tax->tax_name . ': ' . $this->tax->rate_percent . '%'] = ($this->tax->rate_percent / 100) * $item->amount;
                    } else {
                        $taxList[$this->tax->tax_name . ': ' . $this->tax->rate_percent . '%'] = $taxList[$this->tax->tax_name . ': ' . $this->tax->rate_percent . '%'] + (($this->tax->rate_percent / 100) * $item->amount);
                    }
                }
            }
        }

        $this->taxes = $taxList;
        $productLists = array();
        foreach($this->contract->items as $item){
            $productLists[] = Product::findOrFail($item->item_id);
        }

        $this->products = $productLists;

    //    return $this->settings = $this->company;

        
        $pdf = app('dompdf.wrapper');
        $pdf->getDomPDF()->set_option('enable_php', true);
        \App::setLocale($this->invoiceSetting->locale);
        Carbon::setLocale($this->invoiceSetting->locale);
          
        return $this->covert_to_html($this->data);

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($this->covert_to_html($this->data));
        $pdf->setPaper('A4', 'portrait');

        return $pdf->stream();

    }

    public function covert_to_html($data){
        return view('member.contracts.contract-pdf',$data);
    }

    public function addDiscussion(StoreDiscussionRequest $request, $id)
    {
        $contractDiscussion = new ContractDiscussion();
        $contractDiscussion->from = $this->user->id;
        $contractDiscussion->message = $request->message;
        $contractDiscussion->contract_id = $id;
        $contractDiscussion->save();

        return Reply::redirect(route('member.contracts.show', md5($id) . '#discussion'), __('messages.addDiscussion'));
    }

    public function signModal($id)
    {
        $this->contract = Contract::find($id);
        return view('member.contracts.accept', $this->data);
    }

    public function sign(SignRequest $request, $id)
    {
        $this->contract = Contract::whereRaw('md5(id) = ?', $id)->firstOrFail();

        if (!$this->contract) {
            return Reply::error('you are not authorized to access this.');
        }

        $sign = new ContractSign();
        $sign->full_name = $request->first_name . ' ' . $request->last_name;
        $sign->contract_id = $this->contract->id;
        $sign->email = $request->email;

        $image = $request->signature;  // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(32) . '.' . 'jpg';

        if (!\File::exists(public_path('user-uploads/' . 'contract/sign'))) {
            $result = \File::makeDirectory(public_path('user-uploads/contract/sign'), 0775, true);
        }

        \File::put(public_path() . '/user-uploads/contract/sign/' . $imageName, base64_decode($image));

        $sign->signature = $imageName;
        $sign->save();

        return Reply::redirect(route('member.contracts.show', md5($this->contract->id)));
    }

    public function editDiscussion($id)
    {
        $this->discussion = ContractDiscussion::find($id);
        return view('member.contracts.edit-discussion', $this->data);
    }

    public function updateDiscussion(UpdateDiscussionRequest $request, $id)
    {
        $this->discussion = ContractDiscussion::find($id);
        $this->discussion->message = $request->messages;
        $this->discussion->save();

        return Reply::success(__('modules.contracts.discussionUpdated'));
    }

    public function removeDiscussion($id)
    {
        ContractDiscussion::destroy($id);

        return Reply::success(__('modules.contracts.discussionDeleted'));
    }

    public function copy($id)
    {
        $this->clients = User::allClients();
        $this->contractType = ContractType::all();
        $this->contract = Contract::with('signature', 'renew_history', 'renew_history.renewedBy')->find($id);
        return view('member.contracts.copy', $this->data);
    }

    public function copySubmit(StoreRequest $request)
    {
        $contract  = new Contract();
        $contract->client_id = $request->client;
        $contract->subject = $request->subject;
        $contract->amount = $request->amount;
        $contract->original_amount = $request->amount;
        $contract->contract_name = $request->contract_name;
        $contract->alternate_address = $request->alternate_address;
        $contract->mobile = $request->mobile;
        $contract->office_phone = $request->office_phone;
        $contract->city = $request->city;
        $contract->state = $request->state;
        $contract->country = $request->country;
        $contract->postal_code = $request->postal_code;
        $contract->contract_type_id = $request->contract_type;
        $contract->start_date = Carbon::parse($request->start_date)->format('Y-m-d');
        $contract->original_start_date = Carbon::parse($request->start_date)->format('Y-m-d');
        $contract->end_date = $request->end_date == null ? $request->end_date : Carbon::parse($request->end_date)->format('Y-m-d');
        $contract->original_end_date = $request->end_date == null ? $request->end_date : Carbon::parse($request->end_date)->format('Y-m-d');
        $contract->description = $request->description;
        if ($request->hasFile('company_logo')) {
            Files::deleteFile($contract->company_logo, 'avatar');
            $contract->company_logo = Files::upload($request->company_logo, 'avatar', 300);
        }
        if ($request->contract_detail) {
            $contract->contract_detail = $request->contract_detail;
        }

        $contract->save();

        return Reply::redirect(route('member.contracts.edit', $contract->id), __('messages.contractAdded'));
    }

    public function convertContractToInvoice($id)
    {
    
        $this->contractId       = $id;
        $this->invoice          = Contract::with('items', 'lead', 'lead.client')->findOrFail($id);
        $this->lastInvoice      = Invoice::lastInvoiceNumber() + 1;
        $this->invoiceSetting   = InvoiceSetting::first();
        $this->currencies       = Currency::all();
        $this->taxes            = Tax::all();
        $this->clients          = User::allClients();
        $this->zero = '';

        if (!is_null($this->invoice->client_id)) {
            $this->clientDetail = User::findOrFail($this->invoice->client_id);
        }

        if (strlen($this->lastInvoice) < $this->invoiceSetting->invoice_digit) {
            for ($i = 0; $i < $this->invoiceSetting->invoice_digit - strlen($this->lastInvoice); $i++) {
                $this->zero = '0' . $this->zero;
            }
        }
        //        foreach ($this->invoice->items as $items)

        $discount = $this->invoice->items->filter(function ($value, $key) {
            return $value->type == 'discount';
        });

        $tax = $this->invoice->items->filter(function ($value, $key) {
            return $value->type == 'tax';
        });

        $this->totalTax = $tax->sum('amount');
        $this->totalDiscount = $discount->sum('amount');

        return view('member.contracts.convert_contract_to_invoice', $this->data);
        
    }

}
