<?php
return [
    'projectName' => 'បញ្ចូលចំណងជើងនៃគម្រោង',
    'projectCategorySettings' => 'ជ្រើសរើសប្រភេទគម្រោង។',
    'startDate' => 'ជ្រើសរើសកាលបរិច្ឆេទចាប់ផ្តើមនៃគម្រោង។',
];
