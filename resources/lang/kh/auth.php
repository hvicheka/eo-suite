<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'លិខិតសម្គាល់ទាំងនេះមិនត្រូវគ្នានឹងកំណត់ត្រារបស់យើងទេ។',
    'throttle' => 'ការព្យាយាមចូលច្រើនពេក។ សូមព្យាយាមម្តងទៀតក្នុងរយៈពេល : វិនាទីវិនាទី។',
    'recaptchaFailed' => 'Recaptcha មិនមានសុពលភាពទេ។',
    'companyStatusInactive' => 'គណនីក្រុមហ៊ុនអសកម្ម។ សូមទាក់ទងអ្នកគ្រប់គ្រង។',
    'sociaLoginFail' => 'គណនីរបស់អ្នកមិនមានទេ។ សូមចុះឈ្មោះ',
];
