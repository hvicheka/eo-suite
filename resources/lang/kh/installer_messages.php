<?php

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Laravel Installer',
    'next' => 'Next Step',
    'finish' => 'Install',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title'   => 'សូមស្វាគមន៍មកកាន់កម្មវិធីដំឡើង',
        'message' => 'សូមស្វាគមន៍មកកាន់អ្នកជំនួយការដំឡើង។',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'តម្រូវការម៉ាស៊ីនមេ',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'ការអនុញ្ញាត',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'ការកំណត់រចនាសម្ព័ន្ធមូលដ្ឋានទិន្នន័យ',
        'save' => 'រក្សាទុក .env',
        'success' => 'របស់អ្នក .env ការកំណត់ឯកសារត្រូវបានរក្សាទុក។',
        'errors' => 'មិនអាចរក្សាទុកបានទេ .env file, សូមបង្កើតវាដោយដៃ។',
    ],
    
    'install' => 'Install',


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'ចប់',
        'finished' => 'កម្មវិធីត្រូវបានដំឡើងដោយជោគជ័យ។',
        'exit' => 'ចុចទីនេះដើម្បីចាកចេញ',
    ],
    'checkPermissionAgain' => 'ពិនិត្យការអនុញ្ញាតម្តងទៀត'
];
