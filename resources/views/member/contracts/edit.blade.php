@extends('layouts.member-app')
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">

    <style>
        .img-width {
            width: 185px;
        }
        .tabs-style-line nav a {
            box-shadow: unset !important;
        }
        .steamline .sl-left {
            margin-left: -7px !important;
        }
        .history-remove {
            display: none;
        }
        .sl-item:hover .history-remove {
            display: block;
        }
    </style>
@endpush
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> @lang($pageTitle)</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.contracts.index') }}">@lang($pageTitle)</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@section('content')

{!! Form::open(['id'=>'createContract','class'=>'ajax-form','method'=>'PUT']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">{{ $contract->subject }}
                    <a href="{{ route('member.contracts.show', md5($contract->id)) }}" target="_blank" class="btn btn-sm btn-default pull-right">@lang('app.view') @lang('app.menu.contract')</a>
                </h3>

                <div class="sttabs tabs-style-line" id="invoice_container">
                    <nav>
                        <ul class="customtab" role="tablist" id="myTab">
                            <li class="nav-item active"><a class="nav-link" href="#summery" data-toggle="tab" role="tab"><span><i class="glyphicon glyphicon-file"></i> @lang('app.menu.contract')</span></a>
                            </li>

                            <li class="nav-item"><a class="nav-link" href="#renewval" data-toggle="tab" role="tab"><span><i class="fa fa-history"></i> @lang('modules.contracts.contractRenewalHistory')</span></a></li>
                        </ul>
                    </nav>
                    <div class="tab-content tabcontent-border">
                        <div class="tab-pane active" id="summery" role="tabpanel">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p class="text-muted m-b-30 font-13"></p>
                                    <div class="form-group">
                                <textarea name="contract_detail" id="contract_detail"
                                        class="summernote">{{ $contract->contract_detail }}</textarea>
                                    </div>
                                    @if($contract->signature)
                                        <div class="text-right" id="signature-box">
                                            <h2 class="box-title">Signature (Customer)</h2>
                                            <img src="{{ $contract->signature->signature }}" class="img-width">
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="renewval" role="tabpanel">
                            <div class="row">
                                <div class="col-xs-12">
                                    <button type="button" class="btn btn-primary" onclick="renewContract();return false;"><i class="fa fa-refresh"></i> @lang('modules.contracts.renewContract')</button>
                                </div>
                            </div>
                            <hr>
                            <div class="row  slimscrolltab">
                                <div class="col-xs-12">
                                    <div class="steamline">
                                        @foreach($contract->renew_history as $history)
                                            <div class="sl-item">
                                                <div class="sl-left"><i class="fa fa-circle text-info"></i>
                                                </div>
                                                <div class="sl-right">
                                                    <div class="pull-right history-remove">
                                                        <button class="btn btn-inverse btn-outline btn-sm" onclick="removeHistory('{{ $history->id }}')"><i class="fa fa-trash"></i></button>
                                                    </div>
                                                    <div><h6><a href="javascript:;" class="text-danger">{{ $history->renewedBy->name }}
                                                                :</a> @lang('modules.contracts.renewedThisContract'): ({{ $history->created_at->timezone($global->timezone)->format($global->date_format) }} {{ $history->created_at->timezone($global->timezone)->format($global->time_format) }})</h6>
                                                        <span class="sl-date">@lang('modules.contracts.newStartDate'): {{ $history->start_date->timezone($global->timezone)->format($global->date_format) }}</span><br>
                                                        <span class="sl-date">@lang('modules.contracts.newEndDate') : {{ $history->end_date->timezone($global->timezone)->format($global->date_format) }}</span><br>
                                                        <span class="sl-date">@lang('modules.contracts.newAmount') : {{ $history->amount }}</span><br>
                                                    </div>
                                                </div>

                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">@lang('app.edit') @lang('app.menu.contract')</h3>

                <p class="text-muted m-b-30 font-13"></p>

                <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label required" for="relate_to">Relate To</label>
                            <select class="select2 form-control" name="relate_to" id="relate_to">
                                <option value="lead" {{ $contract->relate_to == 'lead'?'selected':'' }}>Lead</option>
                                <option value="customer" {{ $contract->relate_to == 'customer'?'selected':'' }}>Customer</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label required" for="lead_id">Lead/Customer</label>
                            <select class="select2 form-control" name="lead_id" id="lead_id">
                                <option value="">Select Option</option>
                                @foreach ($leads as $item)
                                    <option value="{{$item->id}}" {{ $contract->lead_id == $item->id?'selected':'' }}>{{$item->client_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">@lang('modules.contracts.contract_templete')</label>
                            <select class="form-control select2" name="contract_theme_id" id="contract_theme_id">
                                @foreach($contract_themes as $theme)
                                    <option value="{{ $theme->id }}" {{ $contract->contract_theme_id == $item->id?'selected':'' }}>{{$theme->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">@lang('modules.invoices.currency')</label>
                            <select class="form-control" name="currency_id" id="currency_id">
                                @foreach($currencies as $currency)
                                    <option
                                            @if($contract->currency_id == $currency->id) selected
                                            @endif
                                            value="{{ $currency->id }}">{{ $currency->currency_symbol.' ('.$currency->currency_code.')' }}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">@lang('modules.quotation.status')</label>
                            <select class="form-control select2" name="quotation_status" id="quotation_status">
                                @foreach($ContractStatus as $key =>  $value)
                                    <option value="{{$key}}" {{$key == $contract->status?'selected':''}}>{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">@lang('modules.quotation.company_name') <span class="text-danger">*</span></label> 
                            <div class="input-icon">
                                <input type="text" class="form-control" name="company_name" id="company_name" value="{{ $contract->company_name }}" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">@lang('modules.quotation.name') <span class="text-danger">*</span></label> 
                            <div class="input-icon">
                                <input type="text" class="form-control" name="name" id="name" value="{{ $contract->name }}" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">@lang('modules.quotation.position') <span class="text-danger">*</span></label> 
                            <div class="input-icon">
                                <input type="text" class="form-control" name="position" id="position" value="{{ $contract->position }}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">@lang('modules.quotation.email') <span class="text-danger">*</span> </label>
                            <div class="input-icon">
                                <input type="email" class="form-control" name="email" id="email" value="{{ $contract->email }}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>@lang('modules.lead.mobile')</label>
                            <input type="tel" name="mobile" value="{{ $contract->mobile ?? '' }}" id="mobile" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>@lang('modules.clients.officePhoneNumber')</label>
                            <input type="text" name="office_phone" id="office_phone"  value="{{ $contract->office_phone ?? '' }}"  class="form-control">
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>@lang('modules.stripeCustomerAddress.city')</label>
                            <input type="text" name="city" id="city"  value="{{ $contract->city ?? '' }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>@lang('modules.stripeCustomerAddress.state')</label>
                            <input type="text" name="state" id="state"   value="{{ $contract->state ?? '' }}" class="form-control">
                        </div>
                    </div>
                
                   
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>@lang('modules.stripeCustomerAddress.country')</label>
                            <input type="text" name="country" id="country"  value="{{ $contract->country ?? '' }}"  class="form-control">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>@lang('modules.stripeCustomerAddress.postalCode')</label>
                            <input type="text" name="postal_code" id="postalCode"  value="{{ $contract->postal_code ?? '' }}"class="form-control">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="required">@lang('modules.timeLogs.startDate')</label>
                            <input id="start_date" name="start_date" type="text"
                                class="form-control"
                                value="{{ $contract->start_date->timezone($global->timezone)->format($global->date_format) }}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="required">@lang('modules.timeLogs.endDate')</label>
                            <input id="end_date" name="end_date" type="text"
                                class="form-control"
                                value="{{ $contract->end_date==null ? '' : $contract->end_date->timezone($global->timezone)->format($global->date_format)}}">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="company_name" class="control-label required">@lang('app.client')</label>
                            <div>
                                <select class="select2 form-control" data-placeholder="@lang('app.client')" name="client" id="clientID">
                                    @foreach($clients as $client)
                                        <option value="{{ $client->id }}" @if($client->id == $contract->client_id) selected @endif>{{ ucwords($client->name) }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="subject" class="control-label required">@lang('app.subject')</label>
                            <input type="text" class="form-control" id="subject" name="subject"  value="{{ $contract->subject ?? '' }}">
                        </div>
                    </div>

                    {{-- <div class="col-md-3">
                        <div class="form-group">
                            <label for="subject" class="control-label required">@lang('app.amount') ({{ $global->currency->currency_symbol }})</label>
                            <input type="number" class="form-control" id="amount" name="amount" value="{{ $contract->amount ?? '' }}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group" >
                            <div class="checkbox checkbox-info">
                                <input name="no_amount" id="check_amount"
                                    type="checkbox" onclick="setAmount()">
                                    <label for="check_amount">@lang('modules.contracts.noAmount')</label>
                            </div>
                       </div>
                    </div> --}}
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label required">@lang('modules.contracts.contractType')
                                <a href="javascript:;"
                                   id="createContractType"
                                   class="btn btn-xs btn-outline btn-success">
                                    <i class="fa fa-plus"></i> 
                                </a>
                            </label>
                            <div>
                                <select class="select2 form-control" data-placeholder="@lang('app.client')" id="contractType" name="contract_type">
                                    @foreach($contractType as $type)
                                        <option
                                                value="{{ $type->id }}">{{ ucwords($type->name) }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                        <label>@lang('modules.contracts.contractName')</label>
                            <input name="contract_name" type="text"
                                    value="{{$contract->contract_name ??''}}"   class="form-control">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">@lang('modules.quotation.assistTo') <span class="text-danger">*</span> </label>
                            <select class="form-control select2" name="assist_to_id" id="assist_to_id">
                                @foreach($employees as $value)
                                    <option value="{{ $value->id }} {{$value->id == $contract->assist_to_id?'selected':''}}">{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">@lang('modules.quotation.saleBy') <span class="text-danger">*</span> </label>
                            <select class="form-control select2" name="sale_by_id" id="sale_by_id">
                                @foreach($employees as $value)
                                    <option value="{{ $value->id }}" {{$value->id == $contract->sale_by_id?'selected':''}}>{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label>@lang('modules.contracts.notes')</label>
                            <textarea class="form-control summernote" id="description" name="description" rows="4">{{ $contract->description ?? '' }}</textarea>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12 text-center">
                        <label>@lang('modules.contracts.companyLogo')</label>
                        <div class="form-group">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="{{$contract->image_url}}" alt=""/>

                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"
                                    style="max-width: 200px; max-height: 150px;">
                                </div>
                                <div>
                                    <span class="btn btn-info btn-file">
                                        <span class="fileinput-new"> @lang('app.selectImage') </span>
                                        <span class="fileinput-exists"> @lang('app.change') </span>
                                        <input type="file" name="company_logo" id="company_logo"> 
                                    </span>
                                    <a href="javascript:;" class="btn btn-danger fileinput-exists" data-dismiss="fileinput"> @lang('app.remove') </a>
                                </div>
                            
                            </div>

                        </div>                            
                    </div> 
                </div>

                <hr>
                <div class="col-xs-12 m-t-5">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">@lang('modules.quotation.product')</label>
                            <select class="form-control select2" name="product_id" id="product_id">
                                @foreach($products as $value)
                                    <option value="{{ $value->id }}">{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <br>
                        <button type="button" class="btn btn-info" id="add-item"><i class="fa fa-plus"></i>
                            @lang('modules.invoices.addItem')
                        </button>
                    </div>
                </div>
        
                <div class="row">
        
                    <div class="col-xs-12  visible-md visible-lg">
        
                        <div class="@if($invoiceSetting->hsn_sac_code_show) col-md-3 @else col-md-4 @endif font-bold" style="padding: 8px 15px">
                            @lang('modules.invoices.item')
                        </div>
        
                        {{-- @if($invoiceSetting->hsn_sac_code_show)
                            <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                @lang('modules.invoices.hsnSacCode')
                            </div>
                        @endif --}}
        
                        <div class="col-md-2 font-bold" style="padding: 8px 15px">
                            @lang('modules.invoices.qty')
                        </div>
        
                        <div class="col-md-1 font-bold" style="padding: 8px 15px">
                            @lang('modules.invoices.unitPrice')
                        </div>
                        
                        <div class="col-md-3 font-bold" style="padding: 8px 15px">
                            Discount
                        </div>

                        {{-- <div class="col-md-2 font-bold" style="padding: 8px 15px">
                            @lang('modules.invoices.tax') <a href="javascript:;" id="tax-settings" ><i class="ti-settings text-info"></i></a>
                        </div> --}}
        
                        <div class="col-md-2 text-center font-bold" style="padding: 8px 15px">
                            @lang('modules.invoices.amount')
                        </div>
        
                        <div class="col-md-1" style="padding: 8px 15px">
                            &nbsp;
                        </div>
        
                    </div>
        
                    <div id="sortable">
                        @foreach($contract->items as $key => $item)
                            <div class="col-xs-12 item-row margin-top-5" id="sortable_item" data-id="{{$item->item_id}}">
                                <div class="col-md-3">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.item')</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>
                                                <input type="text" class="form-control item_name" readonly name="item_name[]"
                                                        value="{{ $item->item_name }}">
                                                <input type="hidden" class="form-control item_id" name="item_id[]" value="{{ $item->item_id }}">
        
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <textarea name="item_summary[]" class="form-control item_summary" placeholder="@lang('app.description')" rows="2">{{ $item->item_summary }}</textarea>
        
                                        </div>
                                    </div>
        
                                </div>

                                {{-- @if($invoiceSetting->hsn_sac_code_show)
                                    <div class="col-md-1">
                                        <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.hsnSacCode')</label>
                                        <input type="text" class="form-control" value="{{ $item->hsn_sac_code }}"  name="hsn_sac_code[]" >
        
                                    </div>
                                @endif --}}

                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.qty')</label>
                                        <input type="number" min="1" class="form-control quantity"
                                                value="{{ $item->quantity }}" name="quantity[]"
                                        >
                                    </div>
                                </div>
        
                                <div class="col-md-1">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.unitPrice')</label>
                                            <input type="text" min="" class="form-control cost_per_item" readonly
                                                    name="cost_per_item[]" value="{{ $item->unit_price }}"
                                            >
                                        </div>
                                    </div>
        
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control line_discount_type" name="line_discount_type[]">
                                            <option value="percent" {{ $item->discount_type=='percent'?'selected':'' }}>%</option>
                                            <option value="fixed" {{ $item->discount_type=='fixed'?'selected':'' }}>@lang('modules.invoices.amount')</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1" >
                                    <div class="form-group">
                                        <input type="number" min="0" value="{{ $item->discount }}" class="form-control line_discount_value" name="line_discount_value[]">
                                    </div>
                                </div>
        
                                {{-- <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.type')</label>
                                        <select id="multiselect" name="taxes[{{ $key }}][]"  multiple="multiple" class="selectpicker form-control type">
                                            @foreach($taxes as $tax)
                                                <option data-rate="{{ $tax->rate_percent }}"
                                                        @if (isset($item->taxes) && $item->taxes != "null"  && array_search($tax->id, json_decode($item->taxes)) !== false)
                                                        selected
                                                        @endif
                                                        value="{{ $tax->id }}">{{ $tax->tax_name }}: {{ $tax->rate_percent }}%</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div> --}}
        
                                <div class="col-md-2 border-dark  text-center">
                                    <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.amount')</label>
                                    <p class="form-control-static"><span
                                                class="amount-html">{{ number_format((float)$item->amount, 2, '.', '') }}</span></p>
                                    <input type="hidden" value="{{ $item->amount }}" class="amount"
                                            name="amount[]">
                                </div>
        
                                <div class="col-md-1 text-right visible-md visible-lg">
                                    <button type="button" class="btn remove-item btn-circle btn-danger"><i
                                                class="fa fa-remove"></i></button>
                                </div>
                                <div class="col-md-1 hidden-md hidden-lg">
                                    <div class="row">
                                        <button type="button" class="btn btn-circle remove-item btn-danger"><i
                                                    class="fa fa-remove"></i> @lang('app.remove')
                                        </button>
                                    </div>
                                </div>
        
                            </div>
                        @endforeach
                    </div>
        
                    <div id="item-list">
        
                    </div>
        
                
                    <div class="col-xs-12 ">
                        <div class="row">
                            <div class="col-md-offset-9 col-xs-6 col-md-1 text-right p-t-10">@lang('modules.invoices.subTotal')</div>
        
                            <p class="form-control-static col-xs-6 col-md-2">
                                <span class="sub-total">{{ number_format((float)$contract->sub_total, 2, '.', '') }}</span>
                            </p>
        
        
                            <input type="hidden" class="sub-total-field" name="sub_total" value="{{ $contract->sub_total }}">
                        </div>
        
                        <div class="row">
                            <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                @lang('modules.invoices.discount')
                            </div>
                            <div class="form-group col-xs-6 col-md-1" >
                                <input type="number" min="0" value="{{ $contract->discount }}" name="discount_value" class="form-control discount_value" >
                            </div>
                            <div class="form-group col-xs-6 col-md-1" >
                                <select class="form-control" name="discount_type" id="discount_type">
                                    <option
                                            @if($contract->discount_type == 'percent') selected @endif
                                    value="percent">%</option>
                                    <option
                                            @if($contract->discount_type == 'fixed') selected @endif
                                    value="fixed">@lang('modules.invoices.amount')</option>
                                </select>
                            </div>
                        </div>
        
                        <div class="row m-t-5" id="invoice-taxes">
                            <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                @lang('modules.invoices.tax')
                            </div>
        
                            <p class="form-control-static col-xs-6 col-md-2" >
                                <span class="tax-percent">0</span>
                            </p>
                        </div>
        
                        <div class="row m-t-5 font-bold">
                            <div class="col-md-offset-9 col-md-1 col-xs-6 text-right p-t-10">@lang('modules.invoices.total')</div>
        
                            <p class="form-control-static col-xs-6 col-md-2">
                                <span class="total">{{ number_format((float)$contract->amount, 2, '.', '') }}</span>
                            </p>
        
        
                            <input type="hidden" class="total-field" name="total"
                                    value="{{ round($contract->amount, 2) }}">
                        </div>
        
                    </div>
        
                </div>
        
                <div class="col-md-12 text-left m-t-15 m-b-15">
                    <button type="submit" id="save-form" class="btn btn-success waves-effect waves-light m-r-10">
                        @lang('app.update')
                    </button>
                    <button type="reset" class="btn btn-inverse waves-effect waves-light">@lang('app.reset')</button>
                </div>

            </div>
        </div>

    </div>


{!! Form::close() !!}
 
{{--Ajax Modal--}}
<div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" id="modal-data-application">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                Loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->.
</div>
{{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script>
        $(document).ready(() => {
            $('#start_date').trigger('changeDate');
            let url = location.href.replace(/\/$/, "");

            if (location.hash) {
                const hash = url.split("#");
                $('#myTab a[href="#'+hash[1]+'"]').tab("show");
                url = location.href.replace(/\/#/, "#");
                history.replaceState(null, null, url);
                setTimeout(() => {
                    $(window).scrollTop(0);
                }, 400);
            }

            $('a[data-toggle="tab"]').on("click", function() {
                let newUrl;
                const hash = $(this).attr("href");
                if(hash == "#summery") {
                    newUrl = url.split("#")[0];
                } else {
                    newUrl = url.split("#")[0] + hash;
                }
                // newUrl += "/";
                history.replaceState(null, null, newUrl);
            });

            $('.slimscrolltab').slimScroll({
                height: '283px'
                , position: 'right'
                , size: "5px"
                , color: '#dcdcdc'
                , });
        });

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });

        $("#start_date").datepicker({
            format: '{{ $global->date_picker_format }}',
            todayHighlight: true,
            autoclose: true,
        }).on('changeDate', function (selected) {
            $('#end_date').datepicker({
                format: '{{ $global->date_picker_format }}',
                autoclose: true,
                todayHighlight: true
            });
            if(selected.date == undefined){
                var minDate = new Date("{{ $contract->start_date->timestamp }}");
                minDate += '000';
            }
            else{
                console.log('minDate'+selected.date.valueOf());
            }
           // $('#end_date').datepicker("update", minDate);
            $('#end_date').datepicker('setStartDate', minDate);
        });

        jQuery('#start_date, #end_date').datepicker({
            format: '{{ $global->date_picker_format }}',
            autoclose: true,
            todayHighlight: true,

        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('member.contracts.update', $contract->id)}}',
                container: '#createContract',
                type: "POST",
                file: true,
                redirect: true,
                data: $('#createContract').serialize()
            })
        });

        $('#createContractType').click(function(){
            var url = '{{ route('member.contract-type.create-contract-type')}}';
            $('#modelHeading').html("@lang('modules.contracts.manageContractType')");
            $.ajaxModal('#taskCategoryModal', url);
        })

        function  renewContract() {
            var url = '{{ route('member.contracts.renew', $contract->id)}}';
            $.ajaxModal('#taskCategoryModal',url);
        }

        function  removeHistory(id) {

            swal({
                title: "@lang('messages.sweetAlertTitle')",
                text: "@lang('messages.confirmation.recoverRenewal')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "@lang('messages.deleteConfirmation')",
                cancelButtonText: "@lang('messages.confirmNoArchive')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {

                    var url = '{{ route('member.contracts.renew-remove', ':id') }}';
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token},
                        success: function (response) {
                            if(response.status == 'success') {
                                window.location.reload();
                            }
                        }
                    });
                }
            });
        }


        //****update****
    $('#product_id').change(function () {
        var value = $(this).val();
        var products = {!! collect($products) !!};

        if($('div#sortable_item:last').attr('data-id') == value){

            // $('div#sortable_item').attr('data-id',value);
            // alert(1);
            
        }else{
            $('div#sortable_item:last').attr('data-id',value);
            

        };
        $.each(products, (i, v) => {
            if(v.id == value){
                $(`div#sortable_item[data-id="${value}"]:last`).find('.item_name').val(`${v.name}`);
                $(`div#sortable_item[data-id="${value}"]:last`).find('.item_summary').val(`${v.description}`);
                $(`div#sortable_item[data-id="${value}"]:last`).find('.item_id').val(`${v.id}`);
                $(`div#sortable_item[data-id="${value}"]:last`).find('.cost_per_item').val(`${v.price}`);

                var quantity = 1;

                var perItemCost = v.price;

                var amount = (quantity*perItemCost);

                $(`div#sortable_item[data-id="${value}"]:last`).find('.amount').val(decimalupto2(amount));
                $(`div#sortable_item[data-id="${value}"]:last`).find('.amount-html').html(decimalupto2(amount));
                
                calculateTotal();

            }
        });   
    })

    $('#add-item').click(function () {
        var i = $(document).find('.item_name').length;
        var item = '<div class="col-xs-12 item-row margin-top-5" id="sortable_item">'

            +'<div class="col-md-3">'
            +'<div class="row">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.item')</label>'
            +'<div class="input-group">'
            +'<div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>'
            +'<input type="text" class="form-control item_name" name="item_name[]" readonly >'
            +'<input type="hidden" class="item_id" name="item_id[]">'
            +'</div>'

            +'</div>'
            +'<div class="form-group">'
            +'<textarea name="item_summary[]" class="form-control item_summary" placeholder="@lang('app.description')" rows="2"></textarea>'
            +'</div>'
            +'</div>'

            +'</div>'
            // +'<div class="col-md-1">'

            // +'<div class="form-group">'
            // +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.hsnSacCode')</label>'
            // +'<input type="text"  class="form-control" name="hsn_sac_code[]" >'
            // +'</div>'
            // +'</div>'
            +'<div class="col-md-1">'

            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.qty')</label>'
            +'<input type="number" min="1" class="form-control quantity" value="1" name="quantity[]" >'
            +'</div>'


            +'</div>'

            +'<div class="col-md-1">'
            +'<div class="row">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.unitPrice')</label>'
            +'<input type="text" min="0" class="form-control cost_per_item" readonly value="0" name="cost_per_item[]">'
            +'</div>'
            +'</div>'

            +'</div>'

            +'<div class="col-md-1">'
            +'<div class="form-group">'
            +'<select class="form-control line_discount_type" name="line_discount_type[]">'
            +'<option value="percent">%</option>'
            +'<option value="fixed">@lang('modules.invoices.amount')</option>'
            +'</select>'
            +'</div>'
            +'</div>'

            +'<div class="col-md-2">'
            +'<div class="form-group">'
            +'<input type="number" min="0" value="0" name="line_discount_value[]" class="form-control line_discount_value">'
            +'</div>'
            +'</div>'

            // +'<div class="col-md-2">'

            // +'<div class="form-group">'
            // +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.tax')</label>'
            // +'<select id="multiselect'+i+'" name="taxes['+i+'][]"  multiple="multiple" class="selectpicker form-control type">'
            //     @foreach($taxes as $tax)
            // +'<option data-rate="{{ $tax->rate_percent }}" value="{{ $tax->id }}">{{ $tax->tax_name.': '.$tax->rate_percent }}%</option>'
            //     @endforeach
            // +'</select>'
            // +'</div>'


            // +'</div>'

            +'<div class="col-md-2 text-center">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.amount')</label>'
            +'<p class="form-control-static"><span class="amount-html">0.00</span></p>'
            +'<input type="hidden" class="amount" name="amount[]">'
            +'</div>'

            +'<div class="col-md-1 text-right visible-md visible-lg">'
            +'<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'
            +'</div>'

            +'<div class="col-md-1 hidden-md hidden-lg">'
            +'<div class="row">'
            +'<button type="button" class="btn remove-item btn-danger"><i class="fa fa-remove"></i> @lang('app.remove')</button>'
            +'</div>'
            +'</div>'

            +'</div>';

        $(item).hide().appendTo("#sortable").fadeIn(500);
        $('#multiselect'+i).selectpicker();
        hsnSacColumn();
    });

    hsnSacColumn();
    function hsnSacColumn(){
        @if($invoiceSetting->hsn_sac_code_show)
        $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').removeClass( "col-md-4");
        $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').addClass( "col-md-3");
        $('input[name="hsn_sac_code[]"]').parent("div").parent('div').show();
        @else
        $('input[name="hsn_sac_code[]"]').parent("div").parent('div').hide();
        $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').removeClass( "col-md-3");
        $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').addClass( "col-md-4");
        @endif
    }

    $('#createContract').on('click', '.remove-item', function () {
        $(this).closest('.item-row').fadeOut(300, function () {
            $(this).remove();
            calculateTotal();
        });
    });


    $('#createContract').on('keyup change','.quantity,.cost_per_item,.item_name, .discount_value, .line_discount_value', function () {
          
        var quantity = $(this).closest('.item-row').find('.quantity').val();

        var line_discount_value = $(this).closest('.item-row').find('.line_discount_value').val();

        var line_discount_type = $(this).closest('.item-row').find('.line_discount_type').val();

        var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

        if(line_discount_type == 'percent'){
            line_discounted_amount = parseFloat(perItemCost)*parseFloat(line_discount_value?line_discount_value:0) / 100;
        }
        else{
            line_discounted_amount = parseFloat(line_discount_value?line_discount_value:0);
        }

        var amount = (quantity* (perItemCost - line_discounted_amount));

        $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
        $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));

        calculateTotal();
        
    });

    $('#createContract').on('change','.line_discount_type', function () {
        var quantity = $(this).closest('.item-row').find('.quantity').val();

        var line_discount_value = $(this).closest('.item-row').find('.line_discount_value').val();

        var line_discount_type = $(this).closest('.item-row').find('.line_discount_type').val();

        var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

        if(line_discount_type == 'percent'){
            line_discounted_amount = parseFloat(perItemCost)*parseFloat(line_discount_value?line_discount_value:0) / 100;
        }
        else{
            line_discounted_amount = parseFloat(line_discount_value?line_discount_value:0);
        }

        var amount = (quantity* (perItemCost - line_discounted_amount));

        $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
        $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));

        calculateTotal();

    });


    $('#createContract').on('change','.type, #discount_type', function () {
        var quantity = $(this).closest('.item-row').find('.quantity').val();

        var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

        var amount = (quantity*perItemCost);

        $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
        $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));

        calculateTotal();


    });

    function calculateTotal()
    {
        var subtotal = 0;
        var discount = 0;
        var tax = '';
        var taxList = new Object();
        var taxTotal = 0;
        var discountType = $('#discount_type').val();
        var discountValue = $('.discount_value').val();

        $(".quantity").each(function (index, element) {
            var itemTax = [];
            var itemTaxName = [];
            var discountedAmount = 0;

            $(this).closest('.item-row').find('select.type option:selected').each(function (index) {
                itemTax[index] = $(this).data('rate');
                itemTaxName[index] = $(this).text();
            });
            var itemTaxId = $(this).closest('.item-row').find('select.type').val();

            var amount = parseFloat($(this).closest('.item-row').find('.amount').val());
            if(discountType == 'percent' && discountValue != ''){
                discountedAmount = parseFloat(amount - ((parseFloat(amount)/100)*parseFloat(discountValue)));
            }
            else{
                discountedAmount = parseFloat(amount - (parseFloat(discountValue)));
            }

            if(isNaN(amount)){ amount = 0; }

            subtotal = (parseFloat(subtotal)+parseFloat(amount)).toFixed(2);

            if(itemTaxId != ''){
                for(var i = 0; i<=itemTaxName.length; i++)
                {
                    if(typeof (taxList[itemTaxName[i]]) === 'undefined'){
                        if (discountedAmount > 0) {
                            taxList[itemTaxName[i]] = ((parseFloat(itemTax[i])/100)*parseFloat(discountedAmount));                         
                        } else {
                            taxList[itemTaxName[i]] = ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                        }
                    }
                    else{
                        if (discountedAmount > 0) {
                            taxList[itemTaxName[i]] = parseFloat(taxList[itemTaxName[i]]) + ((parseFloat(itemTax[i])/100)*parseFloat(discountedAmount));   
                            console.log(taxList[itemTaxName[i]]);
                         
                        } else {
                            taxList[itemTaxName[i]] = parseFloat(taxList[itemTaxName[i]]) + ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                        }
                    }
                }
            }
        });


        $.each( taxList, function( key, value ) {
            if(!isNaN(value)){
                tax = tax+'<div class="col-md-offset-8 col-md-2 text-right p-t-10">'
                    +key
                    +'</div>'
                    +'<p class="form-control-static col-xs-6 col-md-2" >'
                    +'<span class="tax-percent">'+(decimalupto2(value)).toFixed(2)+'</span>'
                    +'</p>';
                taxTotal = taxTotal+decimalupto2(value);
            }
        });

        if(isNaN(subtotal)){  subtotal = 0; }

        $('.sub-total').html(decimalupto2(subtotal).toFixed(2));
        $('.sub-total-field').val(decimalupto2(subtotal));

        

        if(discountValue != ''){
            if(discountType == 'percent'){
                discount = ((parseFloat(subtotal)/100)*parseFloat(discountValue));
            }
            else{
                discount = parseFloat(discountValue);
            }

        }

        $('#invoice-taxes').html(tax);

        var totalAfterDiscount = decimalupto2(subtotal-discount);

        totalAfterDiscount = (totalAfterDiscount < 0) ? 0 : totalAfterDiscount;

        var total = decimalupto2(totalAfterDiscount+taxTotal);

        $('.total').html(total.toFixed(2));
        $('.total-field').val(total.toFixed(2));

    }
    
    calculateTotal();

    $('#tax-settings').click(function () {
        var url = '{{ route('admin.taxes.create')}}';
        $('#modelHeading').html('Manage Project Category');
        $.ajaxModal('#taxModal', url);
    })

    function decimalupto2(num) {
        var amt =  Math.round(num * 100) / 100;
        return parseFloat(amt.toFixed(2));
    }
    </script>
@endpush

