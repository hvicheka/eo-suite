@extends('layouts.member-app')
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">

@endpush
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> @lang($pageTitle)</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.contracts.index') }}">@lang($pageTitle)</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('app.add') @lang('app.menu.contract')</div>

                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        @if($clients->isEmpty())
                        <div class="text-center">

                            <div class="empty-space" style="height: 200px;">
                                <div class="empty-space-inner">
                                    <div class="icon" style="font-size:30px"><i
                                                class="fa fa-user-secret"></i>
                                    </div>
                                    <div class="title m-b-15">@lang('messages.noClientFound')</div>
                                    <div class="subtitle">
                                        <a href="{{ route('member.clients.create') }}"
                                           class="btn btn-outline btn-success btn-sm">@lang('modules.client.addNewClient') <i class="fa fa-plus"
                                                                                                                              aria-hidden="true"></i></a>

                                    </div>
                                </div>
                            </div>

                        </div>
                        @else
                        {!! Form::open(['id'=>'createContract','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="row">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label required" for="relate_to">Relate To</label>
                                    <select class="select2 form-control" name="relate_to" id="relate_to">
                                        <option value="lead">Lead</option>
                                        <option value="customer">Customer</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label required" for="lead_id">Lead/Customer</label>
                                    <select class="select2 form-control" name="lead_id" id="lead_id">
                                        <option value="">Select Option</option>
                                        @foreach ($leads as $item)
                                            <option value="{{$item->id}}">{{$item->client_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.contracts.contract_templete')</label>
                                    <select class="form-control select2" name="contract_theme_id" id="contract_theme_id">
                                        @foreach($contract_themes as $theme)
                                            <option value="{{ $theme->id }}">{{$theme->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.invoices.currency')</label>
                                    <select class="form-control" name="currency_id" id="currency_id">
                                        @foreach($currencies as $currency)
                                            <option value="{{ $currency->id }}" @if($global->currency_id == $currency->id) selected @endif>{{ $currency->currency_symbol.' ('.$currency->currency_code.')' }}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.quotation.status')</label>
                                    <select class="form-control select2" name="quotation_status" id="quotation_status">
                                        @foreach($ContractStatus as $key =>  $value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.quotation.company_name') <span class="text-danger">*</span></label> 
                                    <div class="input-icon">
                                        <input type="text" class="form-control" name="company_name" id="company_name" required>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.quotation.name') <span class="text-danger">*</span></label> 
                                    <div class="input-icon">
                                        <input type="text" class="form-control" name="name" id="name" required>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.quotation.position') <span class="text-danger">*</span></label> 
                                    <div class="input-icon">
                                        <input type="text" class="form-control" name="position" id="position">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>@lang('modules.lead.mobile')</label>
                                    <input type="tel" name="mobile" id="mobile" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3 ">
                                <div class="form-group">
                                    <label>@lang('modules.clients.officePhoneNumber')</label>
                                    <input type="text" name="office_phone" id="office_phone"   class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.quotation.email') <span class="text-danger">*</span> </label>
                                    <div class="input-icon">
                                        <input type="email" class="form-control" name="email" id="email">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 ">
                                <div class="form-group">
                                    <label>@lang('modules.stripeCustomerAddress.city')</label>
                                    <input type="text" name="city" id="city"  class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3 ">
                                <div class="form-group">
                                    <label>@lang('modules.stripeCustomerAddress.state')</label>
                                    <input type="text" name="state" id="state"   class="form-control">
                                </div>
                            </div>
                            
                            <div class="col-md-3 ">
                                <div class="form-group">
                                    <label>@lang('modules.stripeCustomerAddress.country')</label>
                                    <input type="text" name="country" id="country"  class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3 ">
                                <div class="form-group">
                                    <label>@lang('modules.stripeCustomerAddress.postalCode')</label>
                                    <input type="text" name="postal_code" id="postalCode"class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="required">@lang('modules.timeLogs.startDate')</label>
                                    <input id="start_date" name="start_date" type="text"
                                        class="form-control"
                                        value="{{ \Carbon\Carbon::today()->format($global->date_format) }}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="required">@lang('modules.timeLogs.endDate')</label>
                                    <input id="end_date" name="end_date" type="text"
                                        class="form-control"
                                        value="{{ \Carbon\Carbon::today()->format($global->date_format) }}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" style="padding-top: 19px; padding-left: 53px;">
                                    <div class="checkbox checkbox-info" onclick="setEndDate()">
                                        <input name="no_enddate" id="no_enddate" type="checkbox">
                                        <label for="no_enddate">@lang('modules.contracts.noEndDate')</label>
                                    </div>
                                </div>
                            </div>
                            

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="company_name" class="required">@lang('app.client')</label>
                                    <div>
                                        <select class="select2 form-control" data-placeholder="@lang('app.client')" name="client" id="clientID">
                                            @foreach($clients as $client)
                                                <option
                                                        value="{{ $client->id }}">{{ ucwords($client->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="required" for="subject">@lang('app.subject')</label>
                                    <input type="text" class="form-control" id="subject" name="subject">
                                </div>
                            </div>
                            {{-- <div class="col-md-3">
                                <div class="form-group">
                                    <label class="required" for="subject">@lang('app.amount') ({{ $global->currency->currency_symbol }})</label>
                                    <input type="number" min="0" class="form-control" id="amount" name="amount">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" style="padding-top: 19px; padding-left: 53px;">
                                    <div class="checkbox checkbox-info">
                                        <input name="no_amount" id="check_amount"
                                            type="checkbox" onclick="setAmount()">
                                        <label for="check_amount">@lang('modules.contracts.noAmount')</label>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="required" class="control-label">@lang('modules.contracts.contractType')
                                        <a href="javascript:;"
                                        id="createContractType"
                                        class="btn btn-xs btn-outline btn-success">
                                            <i class="fa fa-plus"></i> @lang('modules.contracts.addContractType')
                                        </a>
                                    </label>
                                    <div>
                                        <select class="select2 form-control" data-placeholder="@lang('app.client')" id="contractType" name="contract_type">
                                            @foreach($contractType as $type)
                                                <option
                                                        value="{{ $type->id }}">{{ ucwords($type->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>@lang('modules.contracts.contractName')</label>
                                    <input name="contract_name" type="text"
                                        class="form-control">
                                </div>
                            </div>
                            {{-- <div class="col-md-3">
                                <div class="form-group">
                                    <label>@lang('modules.contracts.alternateAddress')</label>
                                    <textarea class="form-control" name="alternate_address" 
                                        class="form-control"></textarea>
                                </div>
                            </div> --}}
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.quotation.assistTo') <span class="text-danger">*</span> </label>
                                    <select class="form-control select2" name="assist_to_id" id="assist_to_id">
                                        @foreach($employees as $value)
                                            <option value="{{ $value->id }}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.quotation.saleBy') <span class="text-danger">*</span> </label>
                                    <select class="form-control select2" name="sale_by_id" id="sale_by_id">
                                        @foreach($employees as $value)
                                            <option value="{{ $value->id }}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>@lang('modules.contracts.notes')</label>
                                    <textarea class="form-control summernote" id="description" name="description" rows="3"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.contracts.summary')</label>
                                    <textarea class="form-control summernote" id="contract_detail" name="contract_detail" rows="4"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <label>@lang('modules.contracts.companyLogo')</label>
                                <div class="form-group">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="http://via.placeholder.com/200x150.png?text=@lang('modules.contracts.companyLogo')"
                                                alt=""/>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                            style="max-width: 200px; max-height: 150px;"></div>
                                        <div>
                                        <span class="btn btn-info btn-file">
                                            <span class="fileinput-new"> @lang('app.selectImage') </span>
                                            <span class="fileinput-exists"> @lang('app.change') </span>
                                            <input type="file" id="company_logo" name="company_logo"> </span>
                                            <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                            data-dismiss="fileinput"> @lang('app.remove') </a>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <hr>
                        <div class="col-xs-12 m-t-5">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.quotation.product')</label>
                                    <select class="form-control select2" name="product_id" id="product_id">
                                        @foreach($products as $value)
                                            <option value="{{ $value->id }}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-t-5">
                                <br>
                                <button type="button" class="btn btn-info btn-sm" id="add-item"><i class="fa fa-plus"></i> @lang('modules.invoices.addItem')</button>
                            </div>
                        </div>
                        
                        <div class="row">

                            <div class="col-xs-12  visible-md visible-lg">

                                <div class="@if($invoiceSetting->hsn_sac_code_show) col-md-3 @else col-md-4 @endif font-bold" style="padding: 8px 15px">
                                    @lang('modules.invoices.item')
                                </div>

                                {{-- @if($invoiceSetting->hsn_sac_code_show)
                                    <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                        @lang('modules.invoices.hsnSacCode')
                                    </div>
                                @endif --}}

                                <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                    @lang('modules.invoices.qty')
                                </div>

                                <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                    @lang('modules.invoices.unitPrice')
                                </div>

                                <div class="col-md-3 font-bold" style="padding: 8px 15px">
                                    Discount
                                </div>

                                {{-- <div class="col-md-2 font-bold" style="padding: 8px 15px">
                                    @lang('modules.invoices.tax') <a href="javascript:;" id="tax-settings" ><i class="ti-settings text-info"></i></a>
                                </div> --}}

                                <div class="col-md-2 text-center font-bold" style="padding: 8px 15px">
                                    @lang('modules.invoices.amount')
                                </div>

                                <div class="col-md-1" style="padding: 8px 15px">
                                    &nbsp;
                                </div>

                            </div>

                            <div id="sortable">
                                <div class="col-xs-12 item-row margin-top-5" id="sortable_item">

                                    <div class="col-md-3">
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.item')</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>
                                                    <input type="text" class="form-control item_name" readonly name="item_name[]">
                                                    <input type="hidden" class="form-control item_id" name="item_id[]">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <textarea name="item_summary[]" class="form-control item_summary" placeholder="@lang('app.description')" cols="10" rows="2"></textarea>

                                            </div>
                                        </div>

                                    </div>

                                    {{-- @if($invoiceSetting->hsn_sac_code_show)
                                        <div class="col-md-1">
                                            <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.hsnSacCode')</label>
                                            <input type="text" class="form-control"   name="hsn_sac_code[]" >

                                        </div>
                                    @endif --}}                                           

                                    <div class="col-md-1">

                                        <div class="form-group">
                                            <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.qty')</label>
                                            <input type="text" min="0" class="form-control quantity" value="1" name="quantity[]" >
                                            
                                        </div>

                                    </div>

                                    <div class="col-md-1">
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.unitPrice')</label>
                                                <input type="text" min="" class="form-control cost_per_item" readonly name="cost_per_item[]" value="0" >
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <select class="form-control line_discount_type" name="line_discount_type[]">
                                                <option value="percent">%</option>
                                                <option value="fixed">@lang('modules.invoices.amount')</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1" >
                                        <div class="form-group">
                                            <input type="number" min="0" value="0" class="form-control line_discount_value" name="line_discount_value[]">
                                        </div>
                                    </div>
                                    

                                    {{-- <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.type')</label>
                                            <select id="multiselect" name="taxes[0][]"  multiple="multiple" class="selectpicker form-control type">
                                                @foreach($taxes as $tax)
                                                    <option data-rate="{{ $tax->rate_percent }}" value="{{ $tax->id }}">{{ $tax->tax_name }}: {{ $tax->rate_percent }}%</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> --}}

                                    <div class="col-md-2 border-dark  text-center">
                                        <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.amount')</label>

                                        <p class="form-control-static"><span class="amount-html">0.00</span></p>
                                        <input type="hidden" class="amount" name="amount[]">
                                    </div>

                                    <div class="col-md-1 text-right visible-md visible-lg">
                                        <button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>
                                    </div>
                                    <div class="col-md-1 hidden-md hidden-lg">
                                        <div class="row">
                                            <button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i> @lang('app.remove')</button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            
                            <div id="item-list">

                            </div>

                        
                            <div class="col-xs-12 ">
                                <div class="row">
                                    <div class="col-md-offset-9 col-xs-6 col-md-1 text-right p-t-10" >@lang('modules.invoices.subTotal')</div>

                                    <p class="form-control-static col-xs-6 col-md-2" >
                                        <span class="sub-total">0.00</span>
                                    </p>


                                    <input type="hidden" class="sub-total-field" name="sub_total" value="0">
                                </div>

                                <div class="row">
                                    <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                        @lang('modules.invoices.discount')
                                    </div>
                                    <div class="form-group col-xs-6 col-md-1" >
                                        <input type="number" min="0" value="0" name="discount_value" class="form-control discount_value">
                                    </div>
                                    <div class="form-group col-xs-6 col-md-1" >
                                        <select class="form-control" name="discount_type" id="discount_type">
                                            <option value="percent">%</option>
                                            <option value="fixed">@lang('modules.invoices.amount')</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row m-t-5" id="invoice-taxes">
                                    <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                        @lang('modules.invoices.tax')
                                    </div>

                                    <p class="form-control-static col-xs-6 col-md-2" >
                                        <span class="tax-percent">0.00</span>
                                    </p>
                                </div>

                                <div class="row m-t-5 font-bold">
                                    <div class="col-md-offset-9 col-md-1 col-xs-6 text-right p-t-10" >@lang('modules.invoices.total')</div>

                                    <p class="form-control-static col-xs-6 col-md-2" >
                                        <span class="total">0.00</span>
                                    </p>
                                    <input type="hidden" class="total-field" name="total" value="0">
                                </div>

                            </div>

                        </div> 
                        
                        <hr>

                        <button type="submit" id="save-form" class="btn btn-success waves-effect waves-light m-r-10">
                            @lang('app.save')
                        </button>
                        <button type="reset" class="btn btn-inverse waves-effect waves-light">@lang('app.reset')</button>
                         </div>

                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        $("#start_date").datepicker({
            format: '{{ $global->date_picker_format }}',
            todayHighlight: true,
            autoclose: true,
        }).on('changeDate', function (selected) {
            $('#end_date').datepicker({
                format: '{{ $global->date_picker_format }}',
                autoclose: true,
                todayHighlight: true
            });
            var minDate = new Date(selected.date.valueOf());
            $('#end_date').datepicker("update", minDate);
            $('#end_date').datepicker('setStartDate', minDate);
        });
        jQuery('#start_date, #end_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: '{{ $global->date_picker_format }}',
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('member.contracts.store')}}',
                container: '#createContract',
                type: "POST",
                file: (document.getElementById("company_logo").files.length == 0) ? false : true,
                redirect: true,
                data: $('#createContract').serialize()
            })
        });

        $('#createContractType').click(function(){
            var url = '{{ route('member.contract-type.create-contract-type')}}';
            $('#modelHeading').html("@lang('modules.contracts.manageContractType')");
            $.ajaxModal('#taskCategoryModal', url);
        })
        function setAmount() {
            let no_amount = document.getElementById("check_amount").checked;
            if(no_amount == true){
                document.getElementById("amount").value = "0";
            }else{
                document.getElementById("amount").value = "";
            }
        
        }
        function setEndDate() {
            let no_amount = document.getElementById("no_enddate").checked;
            if(no_amount == true){
                document.getElementById("end_date").value = "";
            }else{
                document.getElementById("end_date").value = "{{ \Carbon\Carbon::today()->format($global->date_format) }}";
            }
        
        }
        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });

        // *****Calculoate Item working
        $('#product_id').change(function () {
            var value = $(this).val();
            var products = {!! collect($products) !!};

            if($('div#sortable_item:last').attr('data-id') == value){

                // $('div#sortable_item').attr('data-id',value);
                
            }else{
                $('div#sortable_item:last').attr('data-id',value);
            };

            $.each(products, (i, v) => {
                if(v.id == value){
                    $(`div#sortable_item[data-id="${value}"]:last`).find('.item_name').val(`${v.name}`);
                    $(`div#sortable_item[data-id="${value}"]:last`).find('.item_summary').val(`${v.description}`);
                    $(`div#sortable_item[data-id="${value}"]:last`).find('.item_id').val(`${v.id}`);
                    $(`div#sortable_item[data-id="${value}"]:last`).find('.cost_per_item').val(`${v.price}`);

                    var quantity = 1;

                    var perItemCost = v.price;

                    var amount = (quantity*perItemCost);

                    $(`div#sortable_item[data-id="${value}"]:last`).find('.amount').val(decimalupto2(amount));
                    $(`div#sortable_item[data-id="${value}"]:last`).find('.amount-html').html(decimalupto2(amount));
                    
                    calculateTotal();

                }
            });
        })

        $('#add-item').click(function () {

            var i = $(document).find('.item_name').length;
            var item = '<div class="col-xs-12 item-row margin-top-5" id="sortable_item">'

                +'<div class="col-md-3">'
                +'<div class="row">'
                +'<div class="form-group">'
                +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.item')</label>'
                +'<div class="input-group">'
                +'<div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>'
                +'<input type="text" class="form-control item_name " readonly name="item_name[]" >'
                +'<input type="hidden" class="item_id" name="item_id[]">'
                +'</div>'
                +'</div>'

                +'<div class="form-group">'
                +'<textarea name="item_summary[]" class="form-control item_summary" placeholder="@lang('app.description')" rows="2"></textarea>'
                +'</div>'
                +'</div>'
                +'</div>'

                // +'<div class="col-md-1">'
                // +'<div class="form-group">'
                // +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.hsnSacCode')</label>'
                // +'<input type="text"  class="form-control" name="hsn_sac_code[]" >'
                // +'</div>'
                // +'</div>'
                +'<div class="col-md-1">'

                +'<div class="form-group">'
                +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.qty')</label>'
                +'<input type="number" min="1" class="form-control quantity" value="1" name="quantity[]" >'
                +'</div>'
                +'</div>'

                +'<div class="col-md-1">'
                +'<div class="row">'
                +'<div class="form-group">'
                +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.unitPrice')</label>'
                +'<input type="text" min="0" class="form-control cost_per_item" readonly value="0" name="cost_per_item[]">'
                +'</div>'
                +'</div>'

                +'</div>'

                +'<div class="col-md-2">'
                +'<div class="form-group">'
                +'<select class="form-control line_discount_type" name="line_discount_type[]">'
                +'<option value="percent">%</option>'
                +'<option value="fixed">@lang('modules.invoices.amount')</option>'
                +'</select>'
                +'</div>'
                +'</div>'

                +'<div class="col-md-1">'
                +'<div class="form-group">'
                +'<input type="number" min="0" value="0" name="line_discount_value[]" class="form-control line_discount_value">'
                +'</div>'
                +'</div>'

                // +'<div class="col-md-2">'
                // +'<div class="form-group">'
                // +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.tax')</label>'
                // +'<select id="multiselect'+i+'" name="taxes['+i+'][]"  multiple="multiple" class="selectpicker form-control type">'
                //     @foreach($taxes as $tax)
                // +'<option data-rate="{{ $tax->rate_percent }}" value="{{ $tax->id }}">{{ $tax->tax_name.': '.$tax->rate_percent }}%</option>'
                //     @endforeach
                // +'</select>'
                // +'</div>'
                // +'</div>'

                +'<div class="col-md-2 text-center">'
                +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.amount')</label>'
                +'<p class="form-control-static"><span class="amount-html">0.00</span></p>'
                +'<input type="hidden" class="amount" name="amount[]">'
                +'</div>'

                +'<div class="col-md-1 text-right visible-md visible-lg">'
                +'<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'
                +'</div>'

                +'<div class="col-md-1 hidden-md hidden-lg">'
                +'<div class="row">'
                +'<button type="button" class="btn remove-item btn-danger"><i class="fa fa-remove"></i> @lang('app.remove')</button>'
                +'</div>'
                +'</div>'

                +'</div>';

            $(item).hide().appendTo("#sortable").fadeIn(500);
            $('#multiselect'+i).selectpicker();
            hsnSacColumn();
        });

        hsnSacColumn();
        function hsnSacColumn(){
            @if($invoiceSetting->hsn_sac_code_show)
            $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').removeClass( "col-md-4");
            $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').addClass( "col-md-3");
            $('input[name="hsn_sac_code[]"]').parent("div").parent('div').show();
            @else
            $('input[name="hsn_sac_code[]"]').parent("div").parent('div').hide();
            $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').removeClass( "col-md-3");
            $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').addClass( "col-md-4");
            @endif
        }

        $('#createContract').on('click','.remove-item', function () {
            $(this).closest('.item-row').fadeOut(300, function() {
                $(this).remove();
                calculateTotal();
            });
        });

        $('#createContract').on('keyup change','.quantity,.cost_per_item,.item_name, .discount_value, .line_discount_value', function () {
          
            var quantity = $(this).closest('.item-row').find('.quantity').val();

            var line_discount_value = $(this).closest('.item-row').find('.line_discount_value').val();

            var line_discount_type = $(this).closest('.item-row').find('.line_discount_type').val();

            var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

            if(line_discount_type == 'percent'){
                line_discounted_amount = parseFloat(perItemCost)*parseFloat(line_discount_value?line_discount_value:0) / 100;
            }
            else{
                line_discounted_amount = parseFloat(line_discount_value?line_discount_value:0);
            }

            var amount = (quantity* (perItemCost - line_discounted_amount));

            $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
            $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));

            calculateTotal();
        });

        $('#createContract').on('change','.line_discount_type', function () {
            var quantity = $(this).closest('.item-row').find('.quantity').val();

            var line_discount_value = $(this).closest('.item-row').find('.line_discount_value').val();

            var line_discount_type = $(this).closest('.item-row').find('.line_discount_type').val();

            var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

            if(line_discount_type == 'percent'){
                line_discounted_amount = parseFloat(perItemCost)*parseFloat(line_discount_value?line_discount_value:0) / 100;
            }
            else{
                line_discounted_amount = parseFloat(line_discount_value?line_discount_value:0);
            }

            var amount = (quantity* (perItemCost - line_discounted_amount));

            $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
            $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));

            calculateTotal();

        });

        $('#createContract').on('change','.type, #discount_type', function () {

            var quantity = $(this).closest('.item-row').find('.quantity').val();

            var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

            var amount = (quantity*perItemCost);

            $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
            $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));

            calculateTotal();


        });

        function calculateTotal()
        {
            var subtotal = 0;
            var discount = 0;
            var tax = '';
            var taxList = new Object();
            var taxTotal = 0;
            var discountType = $('#discount_type').val();
            var discountValue = $('.discount_value').val();

            $(".quantity").each(function (index, element) {
                var itemTax = [];
                var itemTaxName = [];
                var discountedAmount = 0;

                $(this).closest('.item-row').find('select.type option:selected').each(function (index) {
                    itemTax[index] = $(this).data('rate');
                    itemTaxName[index] = $(this).text();
                });
                var itemTaxId = $(this).closest('.item-row').find('select.type').val();

                var amount = parseFloat($(this).closest('.item-row').find('.amount').val());

                if(discountType == 'percent' && discountValue != ''){
                    discountedAmount = parseFloat(amount - ((parseFloat(amount)/100)*parseFloat(discountValue)));
                }
                else{
                    discountedAmount = parseFloat(amount - (parseFloat(discountValue)));
                }

                if(isNaN(amount)){ amount = 0; }

                subtotal = (parseFloat(subtotal)+parseFloat(amount)).toFixed(2);

                if(itemTaxId != ''){
                    for(var i = 0; i<=itemTaxName.length; i++)
                    {
                        if(typeof (taxList[itemTaxName[i]]) === 'undefined'){
                            if (discountedAmount > 0) {
                                taxList[itemTaxName[i]] = ((parseFloat(itemTax[i])/100)*parseFloat(discountedAmount));                         
                            } else {
                                taxList[itemTaxName[i]] = ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                            }
                        }
                        else{
                            if (discountedAmount > 0) {
                                taxList[itemTaxName[i]] = parseFloat(taxList[itemTaxName[i]]) + ((parseFloat(itemTax[i])/100)*parseFloat(discountedAmount));   
                                console.log(taxList[itemTaxName[i]]);
                            
                            } else {
                                taxList[itemTaxName[i]] = parseFloat(taxList[itemTaxName[i]]) + ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                            }
                        }
                    }
                }
            });


            $.each( taxList, function( key, value ) {
                if(!isNaN(value)){
                    tax = tax+'<div class="col-md-offset-8 col-md-2 text-right p-t-10">'
                        +key
                        +'</div>'
                        +'<p class="form-control-static col-xs-6 col-md-2" >'
                        +'<span class="tax-percent">'+(decimalupto2(value)).toFixed(2)+'</span>'
                        +'</p>';
                    taxTotal = taxTotal+decimalupto2(value);
                }
            });

            if(isNaN(subtotal)){  subtotal = 0; }

            $('.sub-total').html(decimalupto2(subtotal).toFixed(2));
            $('.sub-total-field').val(decimalupto2(subtotal));

            

            if(discountValue != ''){
                if(discountType == 'percent'){
                    discount = ((parseFloat(subtotal)/100)*parseFloat(discountValue));
                }
                else{
                    discount = parseFloat(discountValue);
                }

            }

            $('#invoice-taxes').html(tax);

            var totalAfterDiscount = decimalupto2(subtotal-discount);

            totalAfterDiscount = (totalAfterDiscount < 0) ? 0 : totalAfterDiscount;

            var total = decimalupto2(totalAfterDiscount+taxTotal);

            $('.total').html(total.toFixed(2));
            $('.total-field').val(total.toFixed(2));

        }

        $('#tax-settings').click(function () {
            var url = '{{ route('admin.taxes.create')}}';
            $('#modelHeading').html('Manage Project Category');
            $.ajaxModal('#taxModal', url);
        })

        function decimalupto2(num) {
            var amt =  Math.round(num * 100) / 100;
            return parseFloat(amt.toFixed(2));
        }

    </script>
@endpush

