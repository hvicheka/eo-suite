@extends('layouts.member-app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.products.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.update') @lang('app.menu.products')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/html5-editor/bootstrap-wysihtml5.css') }}">

@endpush

@section('content')

<div class="row">
    <div class="col-xs-12">

        <div class="panel panel-inverse">
            <div class="panel-heading"> @lang('app.update') @lang('app.menu.products')</div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    {!! Form::open(['id'=>'updateProduct','class'=>'ajax-form']) !!}
                    <input name="_method" value="PUT" type="hidden">
                    <div class="form-body">
                        <h3 class="box-title">@lang('app.menu.products') @lang('app.details')</h3>
                        <hr>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.name')</label>
                                        <input type="text" id="name" name="name" class="form-control" value="{{ $product->name }}">
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.price')</label>
                                        <input type="number" min="0" id="price" name="price" class="form-control" value="{{ $product->price }}">
                                        <span class="help-block"> @lang('messages.productPrice')</span>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-xs-12 col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.productCategory.productCategory') <a href="javascript:;" id="addProjectCategory" class="text-info"><i class="ti-settings text-info"></i></a>
                                        </label>
                                        <select class="selectpicker form-control" name="category_id" id="category_id"
                                                data-style="form-control">
                                            <option value="">@lang('messages.pleaseSelectCategory')</option>
                                            @forelse($categories as $category)
                                                <option @if( $category->id == $product->category_id) selected @endif value="{{ $category->id }}">{{ ucwords($category->category_name) }}</option>
                                            @empty
                                                <option value="">@lang('messages.noProductCategory')</option>
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.productCategory.productSubCategory') <a href="javascript:;" id="addProductSubCategory" class="text-info"><i class="ti-settings text-info"></i></a>
                                        </label>
                                        <select class="select2 form-control" name="sub_category_id" id="sub_category_id"
                                                data-style="form-control">
                                            <option value="">@lang('messages.selectSubCategory')</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <br>
                                <div class="wrap-image text-center">
                                    <input type="file" id="image" name="image" style="display: none;"/>
                                    <a href="javascript:changeProfile()" id="upload" class="">
                                        <img class="preveiw_image"  id="preview" src="{{ $product->img_name?asset('user-uploads/product/'.$product->img_name):asset('img/default-image.png') }}" alt="" style="width: 176px;height: 180px;object-fit: cover;">
                                    </a>
                                    <div class="btn-remove">
                                        <a href="javascript:removeImage()">Remove</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--/row-->

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.invoices.tax') <a href="javascript:;" id="tax-settings" ><i class="ti-settings text-info"></i></a></label>
                                    <select id="multiselect" name="tax[]"  multiple="multiple" class="selectpicker form-control type">
                                        @foreach($taxes as $tax)
                                            <option @if (isset($product->taxes) && $product->taxes != "null"  && array_search($tax->id, json_decode($product->taxes)) !== false)
                                                    selected
                                                    @endif
                                                    value="{{ $tax->id }}">{{ $tax->tax_name }}: {{ $tax->rate_percent }}%</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @if($invoiceSetting->hsn_sac_code_show)
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.hsnSacCode')</label>
                                        <input type="text" id="hsn_sac_code" value="{{ $product->hsn_sac_code }}" name="hsn_sac_code" class="form-control" >
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">@lang('app.description')</label>
                                    <textarea name="description" id="" cols="30" rows="10" class="form-control summernote">{{ $product->description }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">@lang('app.feature')</label>
                                    <textarea name="feature" id="" cols="30" rows="20" class="form-control summernote">{{ $product->feature }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">

                                    <div class="checkbox checkbox-info">
                                        <input id="purchase_allow" name="purchase_allow" value="no"
                                               type="checkbox" @if($product->allow_purchase == 1) checked @endif>
                                        <label for="purchase_allow">@lang('app.purchaseAllow')</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-actions">
                        <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>

                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

{{--Ajax Modal--}}
<div class="modal fade bs-modal-md in" id="taxModal" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-md" id="modal-data-application">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                Loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
{{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/html5-editor/wysihtml5-0.3.0.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/html5-editor/bootstrap-wysihtml5.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/html5-editor/bootstrap-wysihtml5.js') }}"></script>
    <script>
        $('.textarea_editor').wysihtml5();
        $('.textarea_feature').wysihtml5();

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        function changeProfile() {
            $('#image').click();
        }
        
        $('#image').change(function () {
            var imgPath = this.value;
            var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
                readURL(this);
            else
                alert("Please select image file (jpg, jpeg, png).")
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.readAsDataURL(input.files[0]);
                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                    // $('#preview_a').attr('src', e.target.result);
                };
                $("#remove").val(0);
            }
        }
        function removeImage() {
            $('#preview').attr('src', '{{ asset('img/default-image.png') }}');
            $("#remove").val(1);
        }

        var subCategories = @json($subCategories);

        $('#category_id').change(function (e) {
            // get projects of selected users
            var opts = '';

            var subCategory = subCategories.filter(function (item) {
                return item.category_id == e.target.value
            });
            subCategory.forEach(project => {
                console.log(project);
                opts += `<option value='${project.id}'>${project.category_name}</option>`
            })

            $('#sub_category_id').html('<option value="0">Select Sub Category...</option>'+opts)
            $("#sub_category_id").select2({
                formatNoMatches: function () {
                    return "{{ __('messages.noRecordFound') }}";
                }
            });
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('member.products.store')}}',
                container: '#createProduct',
                type: "POST",
                redirect: true,
                file: true,
                data: $('#createProduct').serialize()
            })
        });

    </script>
@endpush

