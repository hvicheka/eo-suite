@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.proposals.show', $perposal->lead->id) }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.update')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
<style>
    .panel-black .panel-heading a, .panel-inverse .panel-heading a
    {
        color: #000000 !important;
    }
</style>
@endpush

@section('content')

    <div class="row">
        <div class="col-xs-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.proposal.updateProposal')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'updatePayments','class'=>'ajax-form','method'=>'PUT']) !!}

                        <div class="form-body">

                            <div class="row">

                                <div class="col-md-3">

                                    <div class="form-group" >
                                        <label class="control-label">@lang('app.lead')</label>
                                        <input disabled type="text" class="form-control" value="{{ $perposal->lead->client_name }}">
                                        {!! Form::hidden('lead_id', $perposal->lead->id) !!}
                                    </div>

                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.quotation.quotation_temple')</label>
                                        <select class="form-control select2" name="quotation_theme_id" id="quotation_theme_id">
                                            @foreach($quotation_themes as $theme)
                                                <option value="{{ $theme->id }}" {{$theme->id == $perposal->quotation_theme_id?'selected':''}}>{{$theme->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                {{-- <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.quotation.status')</label>
                                        <select class="form-control select2" name="quotation_status" id="quotation_status">
                                            @foreach($QuotationStatus as $key =>  $value)
                                                <option value="{{$key}}" {{$key == $perposal->status?'selected':''}}>{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div> --}}

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.status')</label>
                                        <select class="form-control" name="status" id="status">
                                            <option
                                                    @if($perposal->status == 'accepted') selected @endif
                                            value="accepted">@lang('modules.proposal.accepted')
                                            </option>
                                            <option
                                                    @if($perposal->status == 'waiting') selected @endif
                                            value="waiting">@lang('modules.proposal.waiting')
                                            </option>
                                            <option
                                                    @if($perposal->status == 'declined') selected @endif
                                            value="declined">@lang('modules.proposal.declined')
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.quotation.company_name') <span class="text-danger">*</span></label> 
                                        <div class="input-icon">
                                            <input type="text" class="form-control" name="company_name" id="company_name" value="{{$perposal->company_name}}" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.quotation.name') <span class="text-danger">*</span></label> 
                                        <div class="input-icon">
                                            <input type="text" class="form-control" name="name" id="name" value="{{$perposal->name}}" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.quotation.position') <span class="text-danger">*</span> </label>
                                        <div class="input-icon">
                                            <input type="text" class="form-control" name="position" value="{{$perposal->position}}" id="position">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.quotation.phone') <span class="text-danger">*</span> </label>
                                        <div class="input-icon">
                                            <input type="text" class="form-control" name="phone" value="{{$perposal->phone}}" id="phone">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.quotation.email') <span class="text-danger">*</span> </label>
                                        <div class="input-icon">
                                            <input type="email" class="form-control" name="email" value="{{$perposal->email}}" id="email">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.invoices.currency')</label>
                                        <select class="form-control" name="currency_id" id="currency_id">
                                            @foreach($currencies as $currency)
                                                <option
                                                        @if($perposal->currency_id == $currency->id) selected
                                                        @endif
                                                        value="{{ $currency->id }}">{{ $currency->currency_symbol.' ('.$currency->currency_code.')' }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.quotation.validFrom') <span class="text-danger">*</span></label>
                                        <div class="input-icon">
                                            <input type="text" class="form-control" name="valid_from" id="valid_from" value="{{date($global->date_format, strtotime($perposal->valid_from))}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.proposal.validTill')</label>

                                        <div class="input-icon">
                                            <input type="text" class="form-control" name="valid_till" id="valid_till"
                                                   value="{{ $perposal->valid_till->format($global->date_format) }}">
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.quotation.dueDate') <span class="text-danger">*</span> </label>
                                        <div class="input-icon">
                                            <input type="text" class="form-control" name="due_date" id="due_date" value="{{date($global->date_format, strtotime($perposal->due_date))}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.quotation.assistTo') <span class="text-danger">*</span> </label>
                                        <select class="form-control select2" name="assist_to_id" id="assist_to_id">
                                            @foreach($employees as $value)
                                                <option value="{{ $value->id }} {{$value->id == $perposal->assist_to_id?'selected':''}}">{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.quotation.saleBy') <span class="text-danger">*</span> </label>
                                        <select class="form-control select2" name="sale_by_id" id="sale_by_id">
                                            @foreach($employees as $value)
                                                <option value="{{ $value->id }}" {{$value->id == $perposal->sale_by_id?'selected':''}}>{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.quotation.sponsorBy') <span class="text-danger">*</span> </label>
                                        <select class="form-control select2" name="sponsor_by_id" id="sponsor_by_id">
                                            @foreach($employees as $value)
                                                <option value="{{ $value->id }}" {{$value->id == $perposal->sponsor_by_id?'selected':''}}>{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                            </div>

                            

                            {{-- <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.note')</label>
                                        <textarea name="note" class="form-control summernote" rows="5">{{ $perposal->note }}</textarea>
                                    </div>
                                </div>
                            </div>
                             --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.description')</label>
                                        <textarea name="description" class="form-control summernote" rows="5">{{ $perposal->description }}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-xs-6">
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info ">
                                            <input id="require_signature" name="require_signature" @if($perposal->signature_approval == 1) checked @endif value="true"
                                                   type="checkbox">
                                            <label for="require_signature" class="control-label">@lang('modules.proposal.requireSignature')</label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <hr>
                            <div class="col-xs-12 m-t-5">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.quotation.product')</label>
                                        <select class="form-control select2" name="product_id" id="product_id">
                                            @foreach($products as $value)
                                                <option value="{{ $value->id }}">{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <br>
                                    <button type="button" class="btn btn-info" id="add-item"><i class="fa fa-plus"></i>
                                        @lang('modules.invoices.addItem')
                                    </button>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-xs-12  visible-md visible-lg">

                                    <div class="@if($invoiceSetting->hsn_sac_code_show) col-md-3 @else col-md-4 @endif font-bold" style="padding: 8px 15px">
                                        @lang('modules.invoices.item')
                                    </div>

                                    {{-- @if($invoiceSetting->hsn_sac_code_show)
                                        <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                            @lang('modules.invoices.hsnSacCode')
                                        </div>
                                    @endif --}}

                                    <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                        @lang('modules.invoices.qty')
                                    </div>

                                    <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                        @lang('modules.invoices.unitPrice')
                                    </div>

                                    <div class="col-md-3 font-bold" style="padding: 8px 15px">
                                        @lang('modules.invoices.discount')
                                    </div>

                                    {{-- <div class="col-md-2 font-bold" style="padding: 8px 15px">
                                        @lang('modules.invoices.tax') <a href="javascript:;" id="tax-settings" ><i class="ti-settings text-info"></i></a>
                                    </div> --}}

                                    <div class="col-md-2 text-center font-bold" style="padding: 8px 15px">
                                        @lang('modules.invoices.amount')
                                    </div>

                                    <div class="col-md-1" style="padding: 8px 15px">
                                        &nbsp;
                                    </div>

                                </div>

                                <div id="sortable">
                                    @foreach($perposal->items as $key => $item)
                                        <div class="col-xs-12 item-row margin-top-5" id="sortable_item" data-id="{{$item->item_id}}">
                                            <div class="col-md-3">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.item')</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>
                                                            <input type="text" class="form-control item_name" readonly name="item_name[]"
                                                                   value="{{ $item->item_name }}">
                                                            <input type="hidden" class="form-control item_id" name="item_id[]" value="{{ $item->item_id }}">

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <textarea name="item_summary[]" class="form-control item_summary" placeholder="@lang('app.description')" rows="2">{{ $item->item_summary }}</textarea>
                                                    </div>
                                                </div>

                                            </div>
                                            {{-- @if($invoiceSetting->hsn_sac_code_show)
                                                <div class="col-md-1">
                                                    <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.hsnSacCode')</label>
                                                    <input type="text" class="form-control" value="{{ $item->hsn_sac_code }}"  name="hsn_sac_code[]" >

                                                </div>
                                            @endif --}}
                                            <div class="col-md-1">

                                                <div class="form-group">
                                                    <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.qty')</label>
                                                    <input type="number" min="1" class="form-control quantity"
                                                           value="{{ $item->quantity }}" name="quantity[]"
                                                    >
                                                </div>
                                            </div>

                                            <div class="col-md-1">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.unitPrice')</label>
                                                        <input type="text" min="" class="form-control cost_per_item" readonly
                                                               name="cost_per_item[]" value="{{ $item->unit_price }}"
                                                        >
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <select class="form-control line_discount_type" name="line_discount_type[]">
                                                        <option value="percent" {{ $item->discount_type=='percent'?'selected':'' }}>%</option>
                                                        <option value="fixed" {{ $item->discount_type=='fixed'?'selected':'' }}>@lang('modules.invoices.amount')</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-1" >
                                                <div class="form-group">
                                                    <input type="number" min="0" value="{{ $item->discount }}" class="form-control line_discount_value" name="line_discount_value[]">
                                                </div>
                                            </div>

                                            {{-- <div class="col-md-2">

                                                <div class="form-group">
                                                    <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.type')</label>
                                                    <select id="multiselect" name="taxes[{{ $key }}][]"  multiple="multiple" class="selectpicker form-control type">
                                                        @foreach($taxes as $tax)
                                                            <option data-rate="{{ $tax->rate_percent }}"
                                                                    @if (isset($item->taxes) && $item->taxes != "null"  && array_search($tax->id, json_decode($item->taxes)) !== false)
                                                                    selected
                                                                    @endif
                                                                    value="{{ $tax->id }}">{{ $tax->tax_name }}: {{ $tax->rate_percent }}%</option>
                                                        @endforeach
                                                    </select>
                                                </div>


                                            </div> --}}

                                            <div class="col-md-2 border-dark  text-center">
                                                <label class="control-label hidden-md hidden-lg">@lang('modules.invoices.amount')</label>
                                                <p class="form-control-static"><span
                                                            class="amount-html">{{ number_format((float)$item->amount, 2, '.', '') }}</span></p>
                                                <input type="hidden" value="{{ $item->amount }}" class="amount"
                                                       name="amount[]">
                                            </div>

                                            <div class="col-md-1 text-right visible-md visible-lg">
                                                <button type="button" class="btn remove-item btn-circle btn-danger"><i
                                                            class="fa fa-remove"></i></button>
                                            </div>
                                            <div class="col-md-1 hidden-md hidden-lg">
                                                <div class="row">
                                                    <button type="button" class="btn btn-circle remove-item btn-danger"><i
                                                                class="fa fa-remove"></i> @lang('app.remove')
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                    @endforeach
                                </div>

                                <div id="item-list">

                                </div>

                                <div class="col-xs-12 ">
                                    <div class="row">
                                        <div class="col-md-offset-9 col-xs-6 col-md-1 text-right p-t-10">@lang('modules.invoices.subTotal')</div>

                                        <p class="form-control-static col-xs-6 col-md-2">
                                            <span class="sub-total">{{ number_format((float)$perposal->sub_total, 2, '.', '') }}</span>
                                        </p>


                                        <input type="hidden" class="sub-total-field" name="sub_total" value="{{ $perposal->sub_total }}">
                                    </div>

                                    <div class="row">
                                        <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                            @lang('modules.invoices.discount')
                                        </div>
                                        <div class="form-group col-xs-6 col-md-1" >
                                            <input type="number" min="0" value="{{ $perposal->discount }}" name="discount_value" class="form-control discount_value" >
                                        </div>
                                        <div class="form-group col-xs-6 col-md-1" >
                                            <select class="form-control" name="discount_type" id="discount_type">
                                                <option
                                                        @if($perposal->discount_type == 'percent') selected @endif
                                                value="percent">%</option>
                                                <option
                                                        @if($perposal->discount_type == 'fixed') selected @endif
                                                value="fixed">@lang('modules.invoices.amount')</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row m-t-5" id="invoice-taxes">
                                        <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                            @lang('modules.invoices.tax')
                                        </div>

                                        <p class="form-control-static col-xs-6 col-md-2" >
                                            <span class="tax-percent">0</span>
                                        </p>
                                    </div>

                                    <div class="row m-t-5 font-bold">
                                        <div class="col-md-offset-9 col-md-1 col-xs-6 text-right p-t-10">@lang('modules.invoices.total')</div>

                                        <p class="form-control-static col-xs-6 col-md-2">
                                            <span class="total">{{ number_format((float)$perposal->total, 2, '.', '') }}</span>
                                        </p>


                                        <input type="hidden" class="total-field" name="total"
                                               value="{{ round($perposal->total, 2) }}">
                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="form-actions" style="margin-top: 70px">
                            <div class="row">
                                <div class="col-xs-12">
                                    <button type="button" id="save-form" class="btn btn-success"><i
                                                class="fa fa-check"></i> @lang('app.save')
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taxModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    @lang('app.loading')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">@lang('app.close')</button>
                    <button type="button" class="btn blue">@lang('app.save') @lang('changes')</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>


<script>
    $(function () {
        $( "#sortable" ).sortable();
    });

    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    jQuery('#valid_till').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        todayHighlight: true,
        format: '{{ $global->date_picker_format }}',
    });
    jQuery('#valid_from').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        todayHighlight: true,
        format: '{{ $global->date_picker_format }}',
    });
    jQuery('#due_date').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        todayHighlight: true,
        format: '{{ $global->date_picker_format }}',
    });

    $('#save-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.proposals.update', $perposal->id)}}',
            container: '#updatePayments',
            type: "POST",
            redirect: true,
            data: $('#updatePayments').serialize()
        })
    });

    $('#product_id').change(function () {
            var value = $(this).val();
            var products = {!! collect($products) !!};

            if($('div#sortable_item:last').attr('data-id') == value){

                // $('div#sortable_item').attr('data-id',value);
                // alert(1);
              
            }else{
                $('div#sortable_item:last').attr('data-id',value);
               

            };
            $.each(products, (i, v) => {
                if(v.id == value){
                    $(`div#sortable_item[data-id="${value}"]:last`).find('.item_name').val(`${v.name}`);
                    $(`div#sortable_item[data-id="${value}"]:last`).find('.item_summary').val(`${v.description}`);
                    $(`div#sortable_item[data-id="${value}"]:last`).find('.item_id').val(`${v.id}`);
                    $(`div#sortable_item[data-id="${value}"]:last`).find('.cost_per_item').val(`${v.price}`);

                    var quantity = 1;

                    var perItemCost = v.price;

                    var amount = (quantity*perItemCost);

                    $(`div#sortable_item[data-id="${value}"]:last`).find('.amount').val(decimalupto2(amount));
                    $(`div#sortable_item[data-id="${value}"]:last`).find('.amount-html').html(decimalupto2(amount));
                    
                    calculateTotal();

                }
            });

            
    })

    $('#add-item').click(function () {
        var i = $(document).find('.item_name').length;
        var item = '<div class="col-xs-12 item-row margin-top-5" id="sortable_item">'

            +'<div class="col-md-3">'
            +'<div class="row">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.item')</label>'
            +'<div class="input-group">'
            +'<div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>'
            +'<input type="text" class="form-control item_name" name="item_name[]" readonly >'
            +'<input type="hidden" class="item_id" name="item_id[]">'
            +'</div>'

            +'</div>'
            +'<div class="form-group">'
            +'<textarea name="item_summary[]" class="form-control item_summary" placeholder="@lang('app.description')" rows="2"></textarea>'
            +'</div>'
            +'</div>'
            +'</div>'

            // +'<div class="col-md-1">'
            // +'<div class="form-group">'
            // +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.hsnSacCode')</label>'
            // +'<input type="text"  class="form-control" name="hsn_sac_code[]" >'
            // +'</div>'
            // +'</div>'

            +'<div class="col-md-1">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.qty')</label>'
            +'<input type="number" min="1" class="form-control quantity" value="1" name="quantity[]" >'
            +'</div>'
            +'</div>'

            +'<div class="col-md-1">'
            +'<div class="row">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.unitPrice')</label>'
            +'<input type="text" min="0" class="form-control cost_per_item" readonly value="0" name="cost_per_item[]">'
            +'</div>'
            +'</div>'
            +'</div>'

            +'<div class="col-md-2">'
            +'<div class="form-group">'
            +'<select class="form-control line_discount_type" name="line_discount_type[]">'
            +'<option value="percent">%</option>'
            +'<option value="fixed">@lang('modules.invoices.amount')</option>'
            +'</select>'
            +'</div>'
            +'</div>'

            +'<div class="col-md-1">'
            +'<div class="form-group">'
            +'<input type="number" min="0" value="0" name="line_discount_value[]" class="form-control line_discount_value">'
            +'</div>'
            +'</div>'
            
            // +'<div class="col-md-2">'
            // +'<div class="form-group">'
            // +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.tax')</label>'
            // +'<select id="multiselect'+i+'" name="taxes['+i+'][]"  multiple="multiple" class="selectpicker form-control type">'
            //     @foreach($taxes as $tax)
            // +'<option data-rate="{{ $tax->rate_percent }}" value="{{ $tax->id }}">{{ $tax->tax_name.': '.$tax->rate_percent }}%</option>'
            //     @endforeach
            // +'</select>'
            // +'</div>'
            // +'</div>'

            +'<div class="col-md-2 text-center">'
            +'<label class="control-label hidden-md hidden-lg">@lang('modules.invoices.amount')</label>'
            +'<p class="form-control-static"><span class="amount-html">0.00</span></p>'
            +'<input type="hidden" class="amount" name="amount[]">'
            +'</div>'

            +'<div class="col-md-1 text-right visible-md visible-lg">'
            +'<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'
            +'</div>'

            +'<div class="col-md-1 hidden-md hidden-lg">'
            +'<div class="row">'
            +'<button type="button" class="btn remove-item btn-danger"><i class="fa fa-remove"></i> @lang('app.remove')</button>'
            +'</div>'
            +'</div>'

            +'</div>';

        $(item).hide().appendTo("#sortable").fadeIn(500);
        $('#multiselect'+i).selectpicker();
        hsnSacColumn();
    });

    hsnSacColumn();
    function hsnSacColumn(){
        @if($invoiceSetting->hsn_sac_code_show)
        $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').removeClass( "col-md-4");
        $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').addClass( "col-md-3");
        $('input[name="hsn_sac_code[]"]').parent("div").parent('div').show();
        @else
        $('input[name="hsn_sac_code[]"]').parent("div").parent('div').hide();
        $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').removeClass( "col-md-3");
        $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').addClass( "col-md-4");
        @endif
    }

    $('#updatePayments').on('click', '.remove-item', function () {
        $(this).closest('.item-row').fadeOut(300, function () {
            $(this).remove();
            calculateTotal();
        });
    });

    $('#updatePayments').on('keyup change','.quantity,.cost_per_item,.item_name, .discount_value, .line_discount_value', function () {
          
          var quantity = $(this).closest('.item-row').find('.quantity').val();
  
          var line_discount_value = $(this).closest('.item-row').find('.line_discount_value').val();
  
          var line_discount_type = $(this).closest('.item-row').find('.line_discount_type').val();
  
          var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();
  
          if(line_discount_type == 'percent'){
              line_discounted_amount = parseFloat(perItemCost)*parseFloat(line_discount_value?line_discount_value:0) / 100;
          }
          else{
              line_discounted_amount = parseFloat(line_discount_value?line_discount_value:0);
          }
  
          var amount = (quantity* (perItemCost - line_discounted_amount));
  
          $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
          $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));
  
          calculateTotal();
          
    });

    $('#updatePayments').on('change','.line_discount_type', function () {
        var quantity = $(this).closest('.item-row').find('.quantity').val();

        var line_discount_value = $(this).closest('.item-row').find('.line_discount_value').val();

        var line_discount_type = $(this).closest('.item-row').find('.line_discount_type').val();

        var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

        if(line_discount_type == 'percent'){
            line_discounted_amount = parseFloat(perItemCost)*parseFloat(line_discount_value?line_discount_value:0) / 100;
        }
        else{
            line_discounted_amount = parseFloat(line_discount_value?line_discount_value:0);
        }

        var amount = (quantity* (perItemCost - line_discounted_amount));

        $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
        $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));

        calculateTotal();

    });


    $('#updatePayments').on('change','.type, #discount_type', function () {
        var quantity = $(this).closest('.item-row').find('.quantity').val();

        var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

        var amount = (quantity*perItemCost);

        $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
        $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));

        calculateTotal();


    });

    function calculateTotal()
    {
        var subtotal = 0;
        var discount = 0;
        var tax = '';
        var taxList = new Object();
        var taxTotal = 0;
        var discountType = $('#discount_type').val();
        var discountValue = $('.discount_value').val();

        $(".quantity").each(function (index, element) {
            var itemTax = [];
            var itemTaxName = [];
            var discountedAmount = 0;

            $(this).closest('.item-row').find('select.type option:selected').each(function (index) {
                itemTax[index] = $(this).data('rate');
                itemTaxName[index] = $(this).text();
            });
            var itemTaxId = $(this).closest('.item-row').find('select.type').val();

            var amount = parseFloat($(this).closest('.item-row').find('.amount').val());
            if(discountType == 'percent' && discountValue != ''){
                discountedAmount = parseFloat(amount - ((parseFloat(amount)/100)*parseFloat(discountValue)));
            }
            else{
                discountedAmount = parseFloat(amount - (parseFloat(discountValue)));
            }

            if(isNaN(amount)){ amount = 0; }

            subtotal = (parseFloat(subtotal)+parseFloat(amount)).toFixed(2);

            if(itemTaxId != ''){
                for(var i = 0; i<=itemTaxName.length; i++)
                {
                    if(typeof (taxList[itemTaxName[i]]) === 'undefined'){
                        if (discountedAmount > 0) {
                            taxList[itemTaxName[i]] = ((parseFloat(itemTax[i])/100)*parseFloat(discountedAmount));                         
                        } else {
                            taxList[itemTaxName[i]] = ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                        }
                    }
                    else{
                        if (discountedAmount > 0) {
                            taxList[itemTaxName[i]] = parseFloat(taxList[itemTaxName[i]]) + ((parseFloat(itemTax[i])/100)*parseFloat(discountedAmount));   
                            console.log(taxList[itemTaxName[i]]);
                         
                        } else {
                            taxList[itemTaxName[i]] = parseFloat(taxList[itemTaxName[i]]) + ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                        }
                    }
                }
            }
        });


        $.each( taxList, function( key, value ) {
            if(!isNaN(value)){
                tax = tax+'<div class="col-md-offset-8 col-md-2 text-right p-t-10">'
                    +key
                    +'</div>'
                    +'<p class="form-control-static col-xs-6 col-md-2" >'
                    +'<span class="tax-percent">'+(decimalupto2(value)).toFixed(2)+'</span>'
                    +'</p>';
                taxTotal = taxTotal+decimalupto2(value);
            }
        });

        if(isNaN(subtotal)){  subtotal = 0; }

        $('.sub-total').html(decimalupto2(subtotal).toFixed(2));
        $('.sub-total-field').val(decimalupto2(subtotal));

        

        if(discountValue != ''){
            if(discountType == 'percent'){
                discount = ((parseFloat(subtotal)/100)*parseFloat(discountValue));
            }
            else{
                discount = parseFloat(discountValue);
            }

        }

        $('#invoice-taxes').html(tax);

        var totalAfterDiscount = decimalupto2(subtotal-discount);

        totalAfterDiscount = (totalAfterDiscount < 0) ? 0 : totalAfterDiscount;

        var total = decimalupto2(totalAfterDiscount+taxTotal);

        $('.total').html(total.toFixed(2));
        $('.total-field').val(total.toFixed(2));

    }

    calculateTotal();

    $('#tax-settings').click(function () {
        var url = '{{ route('admin.taxes.create')}}';
        $('#modelHeading').html('Manage Project Category');
        $.ajaxModal('#taxModal', url);
    })

    function decimalupto2(num) {
        var amt =  Math.round(num * 100) / 100;
        return parseFloat(amt.toFixed(2));
    }
    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });
</script>
@endpush