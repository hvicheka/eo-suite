<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Contract # {{ $contract->id }}</title>
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <style>
        body {
            margin: 0;
            font-family: 'Open Sans', sans-serif !important;
            font-weight: 'Light' 300;
        }

        .bg-grey {
            background-color: #F2F4F7;
        }

        .bg-white {
            background-color: #fff;
        }

       
        .text-black {
            color: #28313c;
        }

        .text-grey {
            color: #616e80;
        }

       

        .text-uppercase {
            text-transform: uppercase;
        }

        .text-capitalize {
            text-transform: capitalize;
        }

        /* .line-height {
            line-height: 24px;
        } */

        .mt-1 {
            margin-top: 1rem;
        }

        .mb-0 {
            margin-bottom: 0px;
        }

        .b-collapse {
            border-collapse: collapse;
            border: 1px solid #2b2a2a;
        }

        .heading-table-left {
            padding: 6px;
             border: 1px solid #2b2a2a;
            font-weight: bold;
            background-color: #f1f1f3;
            border-right: 0;
        }

        .heading-table-right {
            padding: 6px;
             border: 1px solid #2b2a2a;
            border-left: 0;
        }

        .unpaid {
            color: #000000;
            border: 1px solid #000000;
            position: relative;
            padding: 11px 22px;
            
            border-radius: 0.25rem;
            width: 100px;
            text-align: center;
        }

        .main-table-heading {
             border: 1px solid #2b2a2a;
            background-color: #f1f1f3;
            font-weight: 700;
            white-space: nowrap;
        }

        .main-table-heading td {
            padding: 11px 10px;
             border: 1px solid #2b2a2a;
            font-size: 12px !important;
            
        }

        .main-table-items td {
            padding: 11px 10px;
             border: 1px solid #2b2a2a;
            font-size: 12px !important;
        }

        .total-box {
             border: 1px solid #2b2a2a;
            padding: 0px;
            border-bottom: 0px;
            font-size: 12px !important;
        }

        .subtotal {
            padding: 9px 10px;
             border: 1px solid #2b2a2a;
            border-top: 0;
            border-left: 0;
            font-size: 12px !important;
        }

        .subtotal-amt {
            padding: 9px 10px;
             border: 1px solid #2b2a2a;
            border-top: 0;
            border-right: 0;
            font-size: 12px !important;
        }

        .total {
            padding: 9px 10px;
             border: 1px solid #2b2a2a;
            border-top: 0;
            font-weight: 700;
            border-left: 0;
            font-size: 12px !important;
        }

        .total-amt {
            padding: 9px 10px;
             border: 1px solid #2b2a2a;
            border-top: 0;
            border-right: 0;
            font-weight: 700;
            font-size: 12px !important;
        }

        .balance {
            
            font-weight: bold;
            background-color: #f1f1f3;
        }

        .balance-left {
            padding: 9px 10px;
             border: 1px solid #2b2a2a;
            border-top: 0;
            border-left: 0;
        }

        .balance-right {
            padding: 9px 10px;
             border: 1px solid #2b2a2a;
            border-top: 0;
            border-right: 0;
        }

        .centered {
            margin: 0 auto;
        }

        .rightaligned {
            margin-right: 0;
            margin-left: auto;
        }

        .leftaligned {
            margin-left: 0;
            margin-right: auto;
        }

        .page_break {
            page-break-before: always;
        }
        #logo {
            height: 60px;
        }
        #contract_sign{
            height: 60px;
        }
        .text-center{
            text-align: center;
        }
        .text-right{
            text-align: right;
        }
        hr{
            border: 0.1px solid gray;
        }
        
        div table {
            /* font-size: 8px !important; */
            border-collapse: collapse !important;
            width: 100%;
            font-family: 'Source Sans Pro', sans-serif;
            font-weight: normal !important;
            /* margin: 0 5px; */
        }
        div table tr td{
            font-size: 11px !important;
            padding: 5px !important;
        }
        ul li {
            font-size: 14px !important;
            font-family: 'Source Sans Pro', sans-serif;
            font-weight: normal !important;
            color: #404040;
            padding: 2px 0;
        }
        
    </style>

</head>

<body class="content-wrapper">
<br />
@php
    $colspan = ($invoiceSetting->hsn_sac_code_show) ? 4 : 3;
@endphp



@php
    if(invoice_setting()->logo_url) {
        $url =  invoice_setting()->logo_url;
    }else{
        $url =  '';
    }
    $url =  invoice_setting()->logo_url;
    $company_logo = "<img src='$url' alt='' id='logo'/>";
    $contract_sign = "<img src='$global->contract_sign_url' id='contract_sign'/>";
    if ($contract->discount > 0) {
        if ($contract->discount_type == 'percent') {
            $discount = number_format((float) $contract->discount, 2, '.', '').'%';
        } else {
            $discount = currency_formatter($contract->discount,$contract->currency->currency_symbol);
        }
    } else {
        $discount = 0;
    }
    $product_list = '<table width="100%" class="f-14 b-collapse">';
    $product_list .= '<tr class="main-table-heading">';
    $product_list .= '<td class="text-center">'.__("modules.invoices.no").'</td>';
    $product_list .= '<td class="text-center">'.__("modules.invoices.item").'</td>';
    $product_list .= '<td class="text-center">'.__("app.description").'</td>';
    $product_list .= '<td class="text-center">'.__("modules.invoices.qty").'</td>';
    $product_list .= '<td class="text-center">'.__("modules.invoices.unitPrice").'</td>';
    $product_list .= '<td class="text-center">'.__("modules.invoices.discount").'</td>';
    $product_list .= '<td class="text-center">'.__("modules.invoices.amount").'</td>';
    $product_list .= '<tbody>';
        
        foreach($contract->items as $key => $item){
            if($item->discount_type == 'percent'){
                $line_discount_amount = number_format((float) $item->discount, 0, '.', '').'%';
            }else{
                $line_discount_amount = currency_formatter($item->discount,$contract->currency->currency_symbol);
            }
            $product_list .= '<tr class="main-table-items" style="">';
            $product_list .= '<td>'.++$key.'</td>';
            $product_list .= '<td>'.$item->item_name.'</td>';
            $product_list .= '<td>'.$item->item_summary.'</td>';
            $product_list .= '<td class="text-center">'.$item->quantity.'</td>';
            $product_list .= '<td class="text-center">'.currency_formatter($item->unit_price,$contract->currency->currency_symbol).'</td>';
            $product_list .= '<td class="text-center">'.$line_discount_amount.'</td>';
            $product_list .= '<td class="text-center">'.currency_formatter($item->amount,$contract->currency->currency_symbol).'</td>';
            $product_list .= '</tr>';
        }

    $product_list .= '<tr align="right">
                    <td colspan="6" class="subtotal">'.__("modules.invoices.subTotal").'</td>
                    <td class="subtotal-amt">'.currency_formatter($contract->sub_total,$contract->currency->currency_symbol).'</td>
              </tr>';
          
    $product_list .= '<tr align="right">
                    <td colspan="6" class="subtotal">'.__("modules.invoices.discount").'</td>
                    <td class="subtotal-amt">'.
                        $discount
                        .'</td>
             </tr>';
       
    foreach ($taxes as $key => $tax){
        $product_list .='<tr align="right">
                    <td colspan="6" class="subtotal">'.strtoupper($key).'</td>
                    <td class="subtotal-amt">'.currency_formatter($tax,$contract->currency->currency_symbol).'</td>
                </tr>';
    }
    
    $product_list .= '<tr align="right">
                <td colspan="6" class="total">'.__("modules.invoices.total").'</td>
                <td class="total-amt f-15">'.currency_formatter($contract->amount,$contract->currency->currency_symbol).'</td>
             </tr>';

    $product_list .= '</tbody></table>';
    // *****************************************************
            
    $product_features = '';
    foreach ($products as $key => $product) {
        $product_features .= $product->feature;
    }
@endphp

@php
$content = $contract_theme->description;
$content = str_replace("{{company-logo}}",$company_logo,$content);
$content = str_replace("{{contract-number}}",$contract->id,$content);
$content = str_replace("{{created-date}}",date('d-M-Y',strtotime($contract->created_at)),$content);
$content = str_replace("{{company-name}}",ucwords($global->company_name),$content);
$content = str_replace("{{customer-company-name}}",$contract->company_name,$content);
$content = str_replace("{{valid-from}}",date('d-M-Y',strtotime($contract->start_date)),$content);
$content = str_replace("{{valid-to}}",date('d-M-Y',strtotime($contract->end_date)),$content);
$content = str_replace("{{product-list}}",$product_list,$content);
$content = str_replace("{{product-feature}}",$product_features,$content);
$content = str_replace("{{customer-full-name}}",$contract->name,$content);
$content = str_replace("{{customer-position}}",$contract->position,$content);
$content = str_replace("{{customer-company-name}}",$contract->company_name,$content);
$content = str_replace("{{customer-email}}",$contract->email,$content);
$content = str_replace("{{customer-phone}}",$contract->mobile,$content);
$content = str_replace("{{employee-signature}}",$contract_sign,$content);
@endphp
   
    {!! $content !!}
     
</body>
</html>
<script>

    window.print();
    window.onafterprint = function(){
        window.close();
    }

</script>
