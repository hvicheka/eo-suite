
@if(in_array('timelogs',$modules))

<div class="row">
<div class="col-md-12">
<span id="timer-section">
    @if(!is_null($timer))
        <div class="nav navbar-top-links navbar-right pull-right m-t-10 m-b-0">
            <a class="btn btn-rounded btn-default stop-timer-modal" href="javascript:;" data-timer-id="{{ $timer->id }}">
                <i class="ti-alarm-clock"></i>
                <span id="active-timer">{{ $timer->timer }}</span>
                <label class="label label-danger">@lang("app.stop")</label></a>
        </div>
    @else
        <div class="nav navbar-top-links navbar-right pull-right m-t-10 m-b-0">
            <a class="btn btn-outline btn-inverse timer-modal" href="javascript:;">@lang("modules.timeLogs.startTimer") <i class="fa fa-check-circle text-success"></i></a>
        </div>
    @endif
    </span>

    <span id="timer-section">
    
        <div class="nav navbar-top-links navbar-right pull-right m-t-10 m-b-0">
            <?php 
                $attendanceSettings = App\AttendanceSetting::first();
                //Getting Maximum Check-ins in a day
                $maxAttandenceInDay = $attendanceSettings->clockin_in_day;

                // Getting Current Clock-in if exist
                $currenntClockIn = App\Attendance::where(DB::raw('DATE(clock_in_time)'), Carbon\Carbon::today()->format('Y-m-d'))
                ->where('user_id', $user->id)->whereNull('clock_out_time')->first();

                 // Keamsan added
                $currenntClockOut = App\Attendance::where('user_id', $user->id)->orderBy('id','DESC')->whereNull('clock_out_time')->first();
                $ch_date = null;
              
                if( $currenntClockOut){
                    $currenntClockIn = $currenntClockOut;
                    //Check days
                    
                    $ch_date = $currenntClockIn->clock_in_time->diff(now())->days;
                        
                }
               
    
                // End Keamsan added

                // Getting Today's Total Check-ins
                $todayTotalClockin = App\Attendance::where(DB::raw('DATE(clock_in_time)'), Carbon\Carbon::today()->format('Y-m-d'))
                    ->where('user_id', $user->id)->where(DB::raw('DATE(clock_out_time)'), Carbon\Carbon::today()->format('Y-m-d'))->count();
              
                $currentDate = Carbon\Carbon::now()->format('Y-m-d');

                // Check Holiday by date
                $checkTodayHoliday = App\Holiday::where('date', $currentDate)->first();


                //check office time passed
                $officeEndTime = Carbon\Carbon::createFromFormat('H:i:s', $attendanceSettings->office_end_time, $global->timezone)->timestamp;
                $currentTime = Carbon\Carbon::now()->timezone($global->timezone)->timestamp;
                if ($officeEndTime < $currentTime) {
                    $noClockIn = true;
                }
                
                //check office start time
                $officeStartTime = Carbon\Carbon::createFromFormat('H:i:s', $attendanceSettings->office_start_time, $global->timezone)->timestamp;
                if ($currentTime > $officeStartTime) {
                    $onTimeClockIn = true;
                }
                
               
                
            ?>
            
        <!--{{@$currenntClockIn->clock_in_time?$currenntClockIn->clock_in_time->timezone($global->timezone)->format($global->date_format . ' ' .$global->time_format):''}}-->
            
            
            @if (!isset($noClockIn))
                @if(!$checkTodayHoliday)
                    @if($todayTotalClockin < $maxAttandenceInDay)
                        <div class="">
                            <label class="">&nbsp;</label>
                            @if(is_null($currenntClockIn))
                            
                                <button class="btn btn-success btn-sm" id="clock-in">@lang('modules.attendance.clock_in')</button>

                            @endif
                            @if(!is_null($currenntClockIn) && is_null($currenntClockIn->clock_out_time))
                                <input type="hidden" id="start_time" value="{{@$currenntClockIn->clock_in_time?$currenntClockIn->clock_in_time->timezone($global->timezone)->format($global->date_format . ' ' .$global->time_format):''}}">
                    
                                <button class="btn btn-danger btn-sm">
                                    <span id="working_time"></span>
                                </button>
                             
                                @if ($ch_date)
                                   <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-checkout">@lang('modules.attendance.clock_out')</button>
                                @else
                                  <button class="btn btn-danger btn-sm" id="clock-out">@lang('modules.attendance.clock_out')</button>
                                @endif
                            @endif
                        </div>
                        
                    @else
                        <div class="col-xs-12">
                            <div class="btn btn-outline btn-info">@lang('modules.attendance.maxColckIn')</div>
                        </div>
                    @endif
                @else
                    <div class="col-xs-12">
                        <div class="alert alert-info alert-dismissable">
                            <b>@lang('modules.dashboard.holidayCheck') {{ ucwords($checkTodayHoliday->occassion) }}.</b> </div>
                    </div>
                @endif
            @else
                <div class="col-xs-12 text-center">
                    <h6><i class="ti-alert text-danger"></i></h6>
                    <h6>@lang('messages.officeTimeOver')</h6>
                </div>
            @endif
        </div>
        
    </span>

   

    @if(isset($activeTimerCount) && $user->cans('view_timelogs'))
    <span id="timer-section">
        <div class="nav navbar-top-links navbar-right m-t-10 m-r-10">
            <a class="btn btn-rounded btn-default active-timer-modal" href="javascript:;">@lang("modules.projects.activeTimers")
                <span class="label label-danger" id="activeCurrentTimerCount">@if($activeTimerCount > 0) {{ $activeTimerCount }} @else 0 @endif</span>
            </a>
        </div>
    </span>
    @endif
</div>
</div>
@endif
<div class="modal fade bs-modal-md in"  id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" id="modal-data-application">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="subTaskModelHeading">Clockin</span>
            </div>
            <div class="modal-body">
                You are not clock in, Please click clock in to start your work.
                Thanks you.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
             
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->.
</div>
@if ($ch_date)
    <div class="modal fade bs-modal-md in"  id="modal-checkout" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="subTaskModelHeading">@lang('modules.attendance.clockout')</span>
                </div>
                <div class="modal-body">
                    @lang('modules.attendance.reason_message')
                    <div class="m-t-20">
                        <label for="">@lang('modules.attendance.reason')</label>
                        <textarea name="reason" id="reason" cols="10" rows="5" class="form-control" style="resize: vertical;height:80px;1px solid #ccc"></textarea>
                        <div style="color:red;display: none" class="error-message">@lang('modules.attendance.input_reason_message')</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="clock-out" data-ch-date="{{ $ch_date }}">@lang('modules.attendance.clockout')</button>
                    <button type="button" class="btn default" data-dismiss="modal">@lang('modules.attendance.close')</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
@endif

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script>
    @if (!isset($noClockIn))
        @if(isset($onTimeClockIn))
            @if(!$checkTodayHoliday)
                @if($todayTotalClockin < $maxAttandenceInDay)
                
                        @if(is_null($currenntClockIn))
                        setInterval(function(){
                            $('#myModal').modal('show');
                        }, 10000);
                            
                        @endif
                @endif
            @endif
        @endif
    @endif
    
    @if(!is_null($currenntClockIn) && is_null($currenntClockIn->clock_out_time))
    
         const date = new Date($('#start_time').val());
        const dateInMillisecond = date.getTime();
        setInterval(function(){
            const now = new Date();
            const nowInMillisecond = now.getTime();
    
            const interval = nowInMillisecond - dateInMillisecond;
    
            const seconds = (interval / 1000) % 60 ;
            const minutes = parseInt(((interval / (1000*60)) % 60));
            const hours   = parseInt(((interval / (1000*60*60)) % 24));
    
            // console.log('hour:'+ hours +'minute:'+minutes);
            var _final_duration = hours + "Hour " + minutes + "Minutes";
              $('#working_time').text(_final_duration);
    
        }, 1000);
    @endif

    @if(!is_null($currenntClockIn))

$('#reason').on('input',function(){
    if($(this).val().trim()){
        $('#reason').css({
                border: '1px solid #ccc',
            });
        $('#reason').parent().find(`.error-message`).hide();
    }else{
        $('#reason').css({
                border: '1px solid red',
            });
        $('#reason').parent().find(`.error-message`).show();
    }
});
$('#clock-out').click(function () {
    var ch_date = $(this).data('ch-date');
    var reason = null;
    if(ch_date){
         reason = $('#reason').val();
         if(!reason.trim()){
            // alert('Please input reason');
            $('#reason').css({
                border: '1px solid red',
            });
            $('#reason').parent().find(`.error-message`).show();
            return;
        }else{
            $('#reason').css({
                border: '1px solid #ccc',
            });
            $('#reason').parent().find(`.error-message`).hide();
        }
       
    }
    var token = "{{ csrf_token() }}";
    var currentLatitude = document.getElementById("current-latitude").value;
    var currentLongitude = document.getElementById("current-longitude").value;

    $.easyAjax({
        url: '{{route('member.attendances.update', $currenntClockIn->id)}}',
        type: "PUT",
        data: {
            currentLatitude: currentLatitude,
            currentLongitude: currentLongitude,
            reason : reason,
            _token: token
        },
        success: function (response) {
            if(response.status == 'success'){
                window.location.reload();
            }
        }
    })
})
@endif

</script>

