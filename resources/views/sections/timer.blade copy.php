@if(in_array('timelogs',$modules))
<div class="row">
<div class="col-md-12">
<span id="timer-section">
    @if(!is_null($timer))
        <div class="nav navbar-top-links navbar-right pull-right m-t-10 m-b-0">
            <a class="btn btn-rounded btn-default stop-timer-modal" href="javascript:;" data-timer-id="{{ $timer->id }}">
                <i class="ti-alarm-clock"></i>
                <span id="active-timer">{{ $timer->timer }}</span>
                <label class="label label-danger">@lang("app.stop")</label></a>
        </div>
    @else
        <div class="nav navbar-top-links navbar-right pull-right m-t-10 m-b-0">
            <a class="btn btn-outline btn-inverse timer-modal" href="javascript:;">@lang("modules.timeLogs.startTimer") <i class="fa fa-check-circle text-success"></i></a>
        </div>
    @endif
    </span>

    <span id="timer-section">
    
        <div class="nav navbar-top-links navbar-right pull-right m-t-10 m-b-0">
            <?php 
                $attendanceSettings = App\AttendanceSetting::first();
                //Getting Maximum Check-ins in a day
                $maxAttandenceInDay = $attendanceSettings->clockin_in_day;

                // Getting Current Clock-in if exist
                $currenntClockIn = App\Attendance::where(DB::raw('DATE(clock_in_time)'), Carbon\Carbon::today()->format('Y-m-d'))
                ->where('user_id', $user->id)->whereNull('clock_out_time')->first();
                // Getting Today's Total Check-ins
                $todayTotalClockin = App\Attendance::where(DB::raw('DATE(clock_in_time)'), Carbon\Carbon::today()->format('Y-m-d'))
                    ->where('user_id', $user->id)->where(DB::raw('DATE(clock_out_time)'), Carbon\Carbon::today()->format('Y-m-d'))->count();

                $currentDate = Carbon\Carbon::now()->format('Y-m-d');

                // Check Holiday by date
                $checkTodayHoliday = App\Holiday::where('date', $currentDate)->first();


                //check office time passed
                $officeEndTime = Carbon\Carbon::createFromFormat('H:i:s', $attendanceSettings->office_end_time, $global->timezone)->timestamp;
                $currentTime = Carbon\Carbon::now()->timezone($global->timezone)->timestamp;
                if ($officeEndTime < $currentTime) {
                    $noClockIn = true;
                }
            ?>
            
            @if (!isset($noClockIn))
                @if(!$checkTodayHoliday)
                    @if($todayTotalClockin < $maxAttandenceInDay)
                        <div class="">
                            <label class="">&nbsp;</label>
                            @if(is_null($currenntClockIn))
                            
                                <button class="btn btn-success btn-sm" id="clock-in">@lang('modules.attendance.clock_in')</button>

                            @endif
                            @if(!is_null($currenntClockIn) && is_null($currenntClockIn->clock_out_time))
                                <input type="hidden" id="start_time" value="{{ @$currenntClockIn->clock_in_time?$currenntClockIn->clock_in_time->format($global->time_format):'' }}">
                    
                                <button class="btn btn-danger btn-sm">
                                    <span id="working_time"></span>
                                </button>
                                <button class="btn btn-danger btn-sm" id="clock-out">@lang('modules.attendance.clock_out')</button>
                            @endif
                        </div>
                        
                    @else
                        <div class="col-xs-12">
                            <div class="alert alert-info ">@lang('modules.attendance.maxColckIn')</div>
                        </div>
                    @endif
                @else
                    <div class="col-xs-12">
                        <div class="alert alert-info alert-dismissable">
                            <b>@lang('modules.dashboard.holidayCheck') {{ ucwords($checkTodayHoliday->occassion) }}.</b> </div>
                    </div>
                @endif
            @else
                <div class="col-xs-12 text-center">
                    <h6><i class="ti-alert text-danger"></i></h6>
                    <h6>@lang('messages.officeTimeOver')</h6>
                </div>
            @endif
        </div>
        
    </span>

   

    @if(isset($activeTimerCount) && $user->cans('view_timelogs'))
    <span id="timer-section">
        <div class="nav navbar-top-links navbar-right m-t-10 m-r-10">
            <a class="btn btn-rounded btn-default active-timer-modal" href="javascript:;">@lang("modules.projects.activeTimers")
                <span class="label label-danger" id="activeCurrentTimerCount">@if($activeTimerCount > 0) {{ $activeTimerCount }} @else 0 @endif</span>
            </a>
        </div>
    </span>
    @endif
</div>
</div>
@endif
<div class="modal fade bs-modal-md in"  id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" id="modal-data-application">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="subTaskModelHeading">Clockin</span>
            </div>
            <div class="modal-body">
                You are not clock in, Please click clock in to start your work.
                Thanks you.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
             
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->.
</div>


<script>
    @if (!isset($noClockIn))
        @if(!$checkTodayHoliday)
            @if($todayTotalClockin < $maxAttandenceInDay)
            
                    @if(is_null($currenntClockIn))
                    setInterval(function(){
                        $('#myModal').modal('show');
                    }, 10000);
                        
                    @endif
            @endif
        @endif
    @endif
    
    @if(!is_null($currenntClockIn) && is_null($currenntClockIn->clock_out_time))
        setInterval(function(){
            var end_time = new Date().toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
            duration(end_time);
        }, 1000);
    @endif

    
    function duration(end_time) {
        var _starttime = $('#start_time').val();
        var _endtime = end_time;
  
        //divide AM/PM from time
        var _split_starttime = _starttime.split(" ");
        var _split_endtime = _endtime.split(" ");
  
        //check if start time is bigger than end time
        if (_split_starttime[1] == "PM") {
          alert("start time can't be bigger than end time.");
          return false;
        }
        else {
          var _start_timenumber = _split_starttime[0].split(":");
          var _end_timenumber = _split_endtime[0].split(":");
  
          //start time hour & minute
          var _start_hour = parseInt(_start_timenumber[0]); 
          var _start_min = parseInt(_start_timenumber[1]); 
  
          //end time hour & minute
          var _end_hour = parseInt(_end_timenumber[0]); 
          var _end_min = parseInt(_end_timenumber[1]); 
  
          //get hour duration
          var _hour_format = 0;
          if (_split_endtime[1] == "PM") {
            if(_end_hour == 12){
              _end_hour = 0;
            }
            _hour_format = _end_hour + 12;
          }
          else{
            _hour_format = _end_hour;
          }
          var _duration_hour = _hour_format - _start_hour;
  
          //calculate minutes
          var _remained_min = Math.abs(_start_min - _end_min);;
  
          //final result
          var _final_duration = _duration_hour + "Hour " + _remained_min + "Minutes";
          $('#working_time').text(_final_duration);
        }
      }

</script>