<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">


    <title>@lang('app.login') | {{ ucwords($setting->company_name)}}</title>
    <!-- Favicons -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ $global->favicon_url }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ $global->favicon_url }}">
    <meta name="theme-color" content="#ffffff">

    <!-- Bootstrap CSS -->
    <link type="text/css" rel="stylesheet" media="all"
          href="{{ asset('saas/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link type="text/css" rel="stylesheet" media="all" href="{{ asset('saas/vendor/animate-css/animate.min.css') }}">
    <link type="text/css" rel="stylesheet" media="all" href="{{ asset('saas/vendor/slick/slick.css') }}">
    <link type="text/css" rel="stylesheet" media="all" href="{{ asset('saas/vendor/slick/slick-theme.css') }}">
    <link type="text/css" rel="stylesheet" media="all" href="{{ asset('saas/fonts/flaticon/flaticon.css') }}">
    <link href="{{ asset('front/plugin/froiden-helper/helper.css') }}" rel="stylesheet">
    <!-- Template CSS -->
    <link type="text/css" rel="stylesheet" media="all" href="{{ asset('saas/css/main.css') }}">
    <!-- Template Font Family  -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700&family=Rubik:wght@400;500&display=swap"
          rel="stylesheet">
    <link type="text/css" rel="stylesheet" media="all"
          href="{{ asset('saas/vendor/material-design-iconic-font/css/material-design-iconic-font.min.css') }}">

    @if($global->google_recaptcha_status  && $global->google_captcha_version=="v3")
        <script src="https://www.google.com/recaptcha/api.js?render={{ $global->google_recaptcha_key }}"></script>

        <script>
            setInterval(function () {

                grecaptcha.ready(function () {
                    grecaptcha.execute('{{ $global->google_recaptcha_key }}', {action: 'submit'}).then(function (token) {
                        document.getElementById("recaptcha_token").value = token;
                    });
                });

            }, 3000);

        </script>
    @endif

    @if(isset($socialAuthSettings) && ($socialAuthSettings->facebook_status == 'enable' || $socialAuthSettings->google_status == 'enable' || $socialAuthSettings->twitter_status == 'enable' || $socialAuthSettings->linkedin_status == 'enable'))
        <style>
            .login-box {
                max-width: 900px
            }
        </style>
    @endif
    <style>
        {!! $setting->auth_css_theme_two !!}

        :root {
            --main-color: {{ $frontDetail->primary_color }};
        }

        .help-block {
            color: #8a1f11 !important;
        }

        @media (max-width: 767px) {
            .login-box form {
                padding: 10px;
            }

            .input-group-text {
                font-size: 13px;
            }

            .login-box h5 {
                padding: 35px 15px 15px;
                font-size: 21px;
                text-align: center;
                font-weight: 600;
            }
        }

    </style>

    @if(isset($socialAuthSettings) && ($socialAuthSettings->facebook_status == 'enable' || $socialAuthSettings->google_status == 'enable' || $socialAuthSettings->twitter_status == 'enable' || $socialAuthSettings->linkedin_status == 'enable'))
        @php
            $socialLogin = true;
        @endphp
    @else
        @php
            $socialLogin = false;
        @endphp
    @endif

    @if($socialLogin)
        <style>
            .login-box {
                max-width: 900px
            }
        </style>
    @endif
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>

    <style>
        .iti--allow-dropdown {
            width: 100%;
        }

        .form-horizontal .form-group {
            overflow: initial;
        }
        .d-none{
            display: none;
        }
    </style>

</head>

<body id="home">


<!-- Topbar -->
@include('sections.saas.saas_header')
<!-- END Topbar -->

<!-- Header -->
<!-- END Header -->


<section class="sp-100 login-section" id="section-contact">
    <div class="container">
        <div class="login-box mt-5 shadow bg-white form-section row justify-content-center">

            <div class="col-lg-6" id="form-box">
                <h4 class="mb-0">
                    @lang('app.login')
                </h4>

                <form class="form-horizontal form-material" id="loginform" action="{{ route('login') }}" method="POST">
                    {{ csrf_field() }}


                    @if (session('message'))
                        <div class="alert alert-danger m-t-10">
                            {{ session('message') }}
                        </div>
                    @endif
                
                        <div class="form-group">
                            <div class="btn-group w-100 btn-group-toggle">
                                <label  class="btn btn-info {{ old('login_type','email') == 'email'? 'active': null }}" role="button">
                                    <input type="radio" name="login_type" value="email" {{ old('login_type','email') == 'email'? 'checked': null }}>@lang('app.email')
                                </label>
                                <label class="btn btn-info {{ old('login_type','email')== 'mobile'? 'active': null }}" role="button">
                                    <input type="radio" name="login_type" value="mobile" {{ old('login_type','email')== 'mobile'? 'checked': null }}>@lang('app.phone')
                                </label>
                            </div>
                        </div>

                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }} {{ old('login_type','email')== 'email'? null: 'd-none' }}" id="input-email">
                        <div class="col-xs-12">
                            <input class="form-control" id="email" type="email" name="email" value="{{ old('email') }}"
                                autofocus required="" placeholder="@lang('app.email')">
                            @if ($errors->has('email'))
                                <div class="help-block with-errors">{{ $errors->first('email') }}</div>
                            @endif
            
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('mobiles') ? 'has-error' : '' }} {{ old('login_type')== 'mobile'? null: 'd-none' }}"" id="input-mobile">
                        <div class="col-xs-12">
                            <input class="form-control" id="mobile" type="tel" name="mobiles[main]" value="{{ old('mobiles.full') }}"
                                placeholder="@lang('app.mobile')">
                            @if ($errors->has('mobiles'))
                                <div class="help-block with-errors">{{ $errors->first('mobiles') }}</div>
                            @endif
            
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" id="password" type="password" name="password" required=""
                                   placeholder="@lang('modules.client.password')">
                            @if ($errors->has('password'))
                                <div class="help-block with-errors">{{ $errors->first('password') }}</div>
                            @endif
                        </div>
                    </div>
                    @if($global->google_recaptcha_status  && $global->google_captcha_version=="v2")
                        <div class="form-group {{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }}">
                            <div class="col-xs-12">
                                <div class="g-recaptcha"
                                     data-sitekey="{{ $global->google_recaptcha_key }}">
                                </div>
                                @if ($errors->has('g-recaptcha-response'))
                                    <div class="help-block with-errors">{{ $errors->first('g-recaptcha-response') }}</div>
                                @endif
                            </div>
                        </div>
                    @endif
                    <input type='hidden' name='recaptcha_token' id='recaptcha_token'>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="checkbox checkbox-primary float-left p-t-0">
                                <input id="checkbox-signup" type="checkbox"
                                       name="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label for="checkbox-signup" class="text-dark"> @lang('app.rememberMe') </label>
                            </div>
                            <a href="{{ route('password.request') }}" class="text-dark float-right"><i
                                        class="fa fa-lock m-r-5"></i> @lang('app.forgotPassword')?</a></div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light"
                                    type="submit">@lang('app.login')</button>
                        </div>
                    </div>
                    @if($setting->enable_register == true)
                        <div class="form-group m-b-0">
                            <div class="col-sm-12 text-center">
                                <p>@lang('messages.dontHaveAccount')<br><a href="{{ route('front.signup.index') }}"
                                                                           class="text-primary m-l-5"><b>@lang('app.signup')</b></a>
                                </p>
                            </div>
                            @endif
                        </div>
                </form>

                @if(isset($socialAuthSettings) && ($socialAuthSettings->facebook_status == 'enable' || $socialAuthSettings->google_status == 'enable' || $socialAuthSettings->twitter_status == 'enable' || $socialAuthSettings->linkedin_status == 'enable'))
              
                    <div class="row ">

                        <div class="col-xs-12 col-sm-12 m-t-10 text-center mb-2">
                            @if($socialAuthSettings->facebook_status == 'enable')
                                <a href="javascript:;" class="btn btn-primary btn-facebook" data-toggle="tooltip"
                                   title="@lang('app.loginWithFacebook')" onclick="window.location.href = facebook;"
                                   data-original-title="@lang('app.loginWithFacebook')">@lang('app.loginWithFacebook')
                                    <i aria-hidden="true" class="zmdi zmdi-facebook"></i> </a>
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-12 m-t-10 text-center mb-2">
                            @if($socialAuthSettings->google_status == 'enable')
                                <a href="javascript:;" class="btn btn-primary btn-google" data-toggle="tooltip"
                                   title="@lang('app.loginWithGoogle')" onclick="window.location.href = google;"
                                   data-original-title="@lang('app.loginWithGoogle')">@lang('app.loginWithGoogle') <i
                                            aria-hidden="true" class="zmdi zmdi-google"></i> </a>
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-12  m-t-10 text-center mb-2">
                            @if($socialAuthSettings->twitter_status == 'enable')
                                <a href="javascript:;" class="btn btn-primary btn-twitter" data-toggle="tooltip"
                                   title="@lang('app.loginWithTwitter')" onclick="window.location.href = twitter;"
                                   data-original-title="@lang('app.loginWithTwitter')">@lang('app.loginWithTwitter') <i
                                            aria-hidden="true" class="zmdi zmdi-twitter"></i> </a>
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-12 m-t-10 text-center">
                            @if($socialAuthSettings->linkedin_status == 'enable')
                                <a href="javascript:;" class="btn btn-primary btn-linkedin" data-toggle="tooltip"
                                   title="@lang('app.loginWithLinkedin')" onclick="window.location.href = linkedin;"
                                   data-original-title="@lang('app.loginWithLinkedin')">@lang('app.loginWithLinkedin')
                                    <i aria-hidden="true" class="zmdi zmdi-linkedin"></i> </a>
                            @endif
                        </div>
                    </div>

            @endif
            </div>

            
        </div>
    </div>

    <script>
        var facebook = "{{ route('social.login', 'facebook') }}";
        var google = "{{ route('social.login', 'google') }}";
        var twitter = "{{ route('social.login', 'twitter') }}";
        var linkedin = "{{ route('social.login', 'linkedin') }}";
    </script>
</section>

<!-- END Main container -->

<!-- Cta -->
{{--@include('saas.sections.cta')--}}
<!-- End Cta -->

<!-- Footer -->
@include('sections.saas.saas_footer')
<!-- END Footer -->


<!-- Scripts -->
<script src="{{ asset('saas/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('saas/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('saas/vendor/slick/slick.min.js') }}"></script>
<script src="{{ asset('saas/vendor/wowjs/wow.min.js') }}"></script>
<script src="{{ asset('front/plugin/froiden-helper/helper.js') }}"></script>
<script src="{{ asset('saas/js/main.js') }}"></script>
<script src="{{ asset('front/plugin/froiden-helper/helper.js') }}"></script>
<!-- Global Required JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <script>
     $(document).ready(() => {
           $(`[name="login_type"]`).on('change', function() {
               var login_type = $(this).val();
            $(`.btn-group-toggle label`).removeClass('active');
            $(this).parent().addClass('active');
               switch (login_type) {
                   case 'email':
                       $(`#input-email`).removeClass('d-none').find(`input`).prop('required',true);
                       $(`#input-mobile`).addClass('d-none').find(`input`).prop('required',false);
                       break;
                   case 'mobile':
                       $(`#input-email`).addClass('d-none').find(`input`).prop('required',false);
                       $(`#input-mobile`).removeClass('d-none').find(`input`).prop('required',true);
                       break;

                   default:
                       break;
               }
           });
       });
     

       const phoneInputField = document.querySelector("#mobile");
       const phoneInput = window.intlTelInput(phoneInputField, {
           separateDialCode: true,
           preferredCountries: ['kh', 'th'],
           hiddenInput: "full",
           initialCountry: "auto",
           geoIpLookup: function(success) {
               $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                   var countryCode = (resp && resp.country) ? resp.country : "kh";
                   success(countryCode);
               }).fail(() => {
                   success("kh");
               });
           },
           utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
       });

       $(phoneInputField).on("input", function() {
           $(`[name="mobile[full]"]`).val(window.intlTelInputGlobals.getInstance(phoneInputField).getNumber());
       });

      
   </script>
<script>
    $('#save-form').click(function () {


        $.easyAjax({
            url: '{{route('front.signup.store')}}',
            container: '.form-section',
            type: "POST",
            data: $('#register').serialize(),
            messagePosition: "inline",
            success: function (response) {
                if (response.status == 'success') {
                    $('#form-box').remove();
                } else if (response.status == 'fail') {
                    @if($global->google_recaptcha_status)
                    grecaptcha.reset();
                    @endif

                }
            }
        })
    });
</script>

</body>
</html>
