@extends('layouts.auth')

@push('head-script')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>

    <style>
        .iti--allow-dropdown {
            width: 100%;
        }

        .form-horizontal .form-group {
            overflow: initial;
        }
        .d-none{
            display: none;
        }
    </style>
@endpush
@section('content')
   
    <form class="form-horizontal form-material" id="loginform" action="{{ route('login') }}" method="POST">
        {{ csrf_field() }}

       
        @if (session('message'))
            <div class="alert alert-danger m-t-10">
                {{ session('message') }}
            </div>
        @endif
        
        <div class="form-group col-lg-12">
            <div class="btn-group btn-group-toggle form-control" data-toggle="buttons">
                <label class="btn {{ old('login_type','email') == 'email'? 'active': null }}" role="button">
                    <input type="radio" name="login_type" value="email" {{ old('login_type','email') == 'email'? 'checked': null }}>@lang('app.email')
                </label>
                <label class="btn {{ old('login_type','email')== 'mobile'? 'active': null }}" role="button">
                    <input type="radio" name="login_type" value="mobile" {{ old('login_type','email')== 'mobile'? 'checked': null }}>@lang('app.phone')
                </label>
            </div>

        </div>

        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }} {{ old('login_type','email')== 'email'? null: 'd-none' }}" id="input-email">
            <div class="col-xs-12">
                <input class="form-control" id="email" type="email" name="email" value="{{ old('email') }}"
                    autofocus required="" placeholder="@lang('app.email')">
                @if ($errors->has('email'))
                    <div class="help-block with-errors">{{ $errors->first('email') }}</div>
                @endif

            </div>
        </div>
        <div class="form-group {{ $errors->has('mobiles') ? 'has-error' : '' }} {{ old('login_type')== 'mobile'? null: 'd-none' }}"" id="input-mobile">
            <div class="col-xs-12">
                <input class="form-control" id="mobile" type="tel" name="mobiles[main]" value="{{ old('mobiles.full') }}"
                    placeholder="@lang('app.mobile')">
                @if ($errors->has('mobiles'))
                    <div class="help-block with-errors">{{ $errors->first('mobiles') }}</div>
                @endif

            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <input class="form-control" id="password" type="password" name="password" required=""
                    placeholder="@lang('modules.client.password')">
                @if ($errors->has('password'))
                    <div class="help-block with-errors">{{ $errors->first('password') }}</div>
                @endif
            </div>
        </div>
        {{-- @php dd($global); @endphp --}}
        @if ($global->google_recaptcha_status && $global->google_captcha_version == 'v2')
            <div class="form-group {{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }}">
                <div class="col-xs-12">
                    <div class="g-recaptcha" data-sitekey="{{ $global->google_recaptcha_key }}">
                    </div>
                    @if ($errors->has('g-recaptcha-response'))
                        <div class="help-block with-errors">{{ $errors->first('g-recaptcha-response') }}</div>
                    @endif
                </div>
            </div>
        @endif
        <input type='hidden' name='recaptcha_token' id='recaptcha_token'>

        <div class="form-group">
            <div class="col-xs-12">
                <div class="checkbox checkbox-primary pull-left p-t-0">
                    <input id="checkbox-signup" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label for="checkbox-signup" class="text-dark"> @lang('app.rememberMe') </label>
                </div>
                <a href="{{ route('password.request') }}" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i>
                    @lang('app.forgotPassword')?</a>
            </div>
        </div>
        <div class="form-group text-center m-t-20">
            <div class="col-xs-12">
                <button class="btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light"
                    type="submit">@lang('app.login')</button>
            </div>
        </div>

    </form>
    <div class="col-md-12">
        <div class="row">

            @if (isset($socialAuthSettings) && !module_enabled('Subdomain'))
                @if ($socialAuthSettings && $socialAuthSettings->facebook_status == 'enable')
                    <div class="col-xs-12 col-sm-6 col-md-6 m-t-10 mb-16" style="padding: 0 7px">
                        <a href="javascript:;" class="btn btn-primary btn-block btn-facebook" data-toggle="tooltip"
                            title="@lang('app.loginWithFacebook')" onclick="window.location.href = facebook;"
                            data-original-title="@lang('app.loginWithFacebook')"><i aria-hidden="true" class="fa fa-facebook-f"></i>
                            &nbsp;@lang('app.loginWithFacebook')</a>
                    </div>
                @endif

                @if ($socialAuthSettings->google_status == 'enable')
                    <div class="col-xs-12 col-sm-6 col-md-6 m-t-10 mb-16 text-right" style="padding: 0 7px">
                        <a href="javascript:;" class="btn btn-primary btn-block btn-google" data-toggle="tooltip"
                            title="@lang('app.loginWithGoogle')" onclick="window.location.href = google;"
                            data-original-title="@lang('app.loginWithGoogle')"><i aria-hidden="true" class="fa fa-google-plus"></i>
                            &nbsp;@lang('app.loginWithGoogle')</a>
                    </div>
                @endif
                @if ($socialAuthSettings->twitter_status == 'enable')
                    <div class="col-xs-12 col-sm-12 col-md-4 m-t-10 text-center mb-16">
                        <a href="javascript:;" class="btn btn-primary btn-block btn-twitter" data-toggle="tooltip"
                            title="@lang('app.loginWithTwitter')" onclick="window.location.href = twitter;"
                            data-original-title="@lang('app.loginWithTwitter')"><i aria-hidden="true" class="fa fa-twitter"></i>
                            &nbsp;@lang('app.loginWithTwitter')</a>
                    </div>
                @endif
                @if ($socialAuthSettings->linkedin_status == 'enable')
                    <div class="col-xs-12 col-sm-12 col-md-4 m-t-10 text-center mb-16">
                        <a href="javascript:;" class="btn btn-primary btn-block btn-linkedin" data-toggle="tooltip"
                            title="@lang('app.loginWithLinkedin')" onclick="window.location.href = linkedin;"
                            data-original-title="@lang('app.loginWithLinkedin')"><i aria-hidden="true" class="fa fa-linkedin"></i>
                            &nbsp;@lang('app.loginWithLinkedin')</a>
                    </div>
                @endif
            @endif
        </div>
        @if (!module_enabled('Subdomain'))
            @if ($setting->enable_register == true)
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <p>@lang('messages.dontHaveAccount') <a href="{{ route('front.signup.index') }}"
                                class="text-primary m-l-5"><b>@lang('app.signup')</b></a>
                        </p>
                    </div>
                </div>
            @endif

            @if (!$setting->frontend_disable)
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <p>@lang('messages.goToWebsite') <a href="{{ route('front.home') }}"
                                class="text-primary m-l-5"><b>@lang('app.home')</b></a></p>
                    </div>
                </div>
            @endif
        @endif
    </div>
    <script>
        var facebook = "{{ route('social.login', 'facebook') }}";
        var google = "{{ route('social.login', 'google') }}";
        var twitter = "{{ route('social.login', 'twitter') }}";
        var linkedin = "{{ route('social.login', 'linkedin') }}";
    </script>

    <!-- jQuery -->
    <script src="{{ asset('plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script>
          $(document).ready(() => {
            $(`[name="login_type"]`).on('change', function() {
                var login_type = $(this).val();
                switch (login_type) {
                    case 'email':
                        $(`#input-email`).removeClass('d-none').find(`input`).prop('required',true);
                        $(`#input-mobile`).addClass('d-none').find(`input`).prop('required',false);
                        break;
                    case 'mobile':
                        $(`#input-email`).addClass('d-none').find(`input`).prop('required',false);
                        $(`#input-mobile`).removeClass('d-none').find(`input`).prop('required',true);
                        break;

                    default:
                        break;
                }
            });
        });
        const phoneInputField = document.querySelector("#mobile");
        const phoneInput = window.intlTelInput(phoneInputField, {
            separateDialCode: true,
            preferredCountries: ['kh', 'th'],
            hiddenInput: "full",
            initialCountry: "auto",
            geoIpLookup: function(success) {
                $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "kh";
                    success(countryCode);
                }).fail(() => {
                    success("kh");
                });
            },
            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
        });

        $(phoneInputField).on("input", function() {
            $(`[name="mobile[full]"]`).val(window.intlTelInputGlobals.getInstance(phoneInputField).getNumber());
        });

      
    </script>
@endsection
@if ($global->google_recaptcha_status && $global->google_captcha_version == 'v3')
    <script src="https://www.google.com/recaptcha/api.js?render={{ $global->google_recaptcha_key }}"></script>

    <script>
        setInterval(function() {
            grecaptcha.ready(function() {
                grecaptcha.execute('{{ $global->google_recaptcha_key }}', {
                    action: 'submit'
                }).then(function(token) {
                    document.getElementById("recaptcha_token").value = token;
                });
            });

        }, 3000);
    </script>
@endif
