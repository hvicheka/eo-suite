<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title"><?php echo app('translator')->get('app.leadAgent'); ?></h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th><?php echo app('translator')->get('app.leadAgent'); ?></th>
                    <th><?php echo app('translator')->get('app.action'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php $__empty_1 = true; $__currentLoopData = $agentData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$empAgent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <tr id="cat-<?php echo e($empAgent->id); ?>">
                        <td><?php echo e($key+1); ?></td>
                        <td><?php echo e(ucwords($empAgent->user->name). ' ['.$empAgent->user->email.']'); ?></td>
                        <td><a href="javascript:;" data-cat-id="<?php echo e($empAgent->id); ?>" class="btn btn-sm btn-danger btn-rounded delete-category"><?php echo app('translator')->get("app.remove"); ?></a></td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <tr>
                        <td colspan="3"><?php echo app('translator')->get('messages.noLeadAgent'); ?></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>

        <?php echo Form::open(['id'=>'createProjectCategory','class'=>'ajax-form','method'=>'POST']); ?>

        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label for=""><?php echo app('translator')->get('modules.tickets.chooseAgents'); ?></label>
                        <select class="select2 form-control" data-placeholder="<?php echo app('translator')->get('modules.tickets.chooseAgents'); ?>" id="agent_name" name="agent_name">
                            <option value=""><?php echo app('translator')->get('modules.tickets.chooseAgents'); ?></option>
                            <?php $__currentLoopData = $employeeData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $empData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option id="employeeList<?php echo e($empData->id); ?>" value="<?php echo e($empData->id); ?>"><?php echo e(ucwords($empData->name)); ?> <?php if($empData->id == $user->id): ?>
                                        (YOU) <?php endif; ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> <?php echo app('translator')->get('app.save'); ?></button>
        </div>
        <?php echo Form::close(); ?>

    </div>
</div>

<script>
    $("#agent_name").select2({
        formatNoMatches: function () {
            return "<?php echo e(__('messages.noRecordFound')); ?>";
        }
    });
    $('body').on('click', '.delete-category', function() {
        var id = $(this).data('cat-id');
        var url = "<?php echo e(route('admin.lead-agent-settings.destroy',':id')); ?>";
        url = url.replace(':id', id);

        var token = "<?php echo e(csrf_token()); ?>";

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token, '_method': 'DELETE'},
            success: function (response) {
                if (response.status == "success") {
                    $.unblockUI();
                    $('#cat-'+id).fadeOut();
                    var options = [];
                    var empOptions = [];
                    var rData = [];
                    var empData = [];
                    rData = response.data;
                    empData = response.empData;

                    $.each(rData, function( index, value ) {
                        var selectData = '';
                        selectData = '<option value="'+value.id+'">'+value.name+'</option>';
                        options.push(selectData);
                    });

                    $('#agent_id').html(options);
                    $("#agent_name").select2();

                    var you = '';
                    $.each(empData, function( empIndex, empValue ) {
                        var selectEmpData = '';
                        var userID = <?php echo e($user->id); ?>;
                        if(empValue.id == userID){
                            you = '(YOU)';
                        }
                        else{
                            you = '';
                        }
                        selectEmpData = '<option  id="employeeList'+empValue.id+'" value="'+empValue.id+'">'+empValue.name+' '+you+'</option>';
                        empOptions.push(selectEmpData);
                    });

                    $('#agent_name').html(empOptions);
                    $("#agent_name").select2();
                }
            }
        });
    });

    $('#save-category').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.lead-agent-settings.create-agent')); ?>',
            container: '#createProjectCategory',
            type: "POST",
            data: $('#createProjectCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    if(response.status == 'success'){
                        console.log(response.data);
                        var options = [];
                        var rData = [];
                        rData = response.data;
                        console.log(rData);
                        $.each(rData, function( index, value ) {
                            var selectData = '';
                            selectData = '<option value="'+value.id+'">'+value.name+'</option>';
                            options.push(selectData);
                        });

                        $('#agent_id').html(options);
                        $("#agent_name").select2();
                        $('#projectCategoryModal').modal('hide');
                    }
                }
            }
        })
    });
</script><?php /**PATH D:\WAMP-SERVER\www\eOsuite\resources\views/admin/lead/create-agent.blade.php ENDPATH**/ ?>