<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('member.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li class="active"><?php echo e(__($pageTitle)); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css')); ?>">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('plugins/daterange-picker/daterangepicker.css')); ?>" />

<?php $__env->stopPush(); ?>
<?php $__env->startSection('filter-section'); ?>
    <div class="row" id="ticket-filters">
        <form action="" id="filter-form">
            <div class="col-xs-12">
                <div class="form-group">
                    <h5 ><?php echo app('translator')->get('app.selectDateRange'); ?></h5>
                    <div class="form-group">
                        <div id="reportrange" class="form-control reportrange">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="fa fa-caret-down pull-right"></i>
                        </div>

                        <input type="hidden" class="form-control" id="start-date" placeholder="<?php echo app('translator')->get('app.startDate'); ?>"
                               value=""/>
                        <input type="hidden" class="form-control" id="end-date" placeholder="<?php echo app('translator')->get('app.endDate'); ?>"
                               value=""/>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="control-label"><?php echo app('translator')->get('app.status'); ?></label>
                    <select class="form-control selectpicker" name="status" id="status" data-style="form-control">
                        <option value="all"><?php echo app('translator')->get('modules.tickets.nofilter'); ?></option>
                        <option selected value="open"><?php echo app('translator')->get('modules.tickets.totalOpenTickets'); ?></option>
                        <option value="pending"><?php echo app('translator')->get('modules.tickets.totalPendingTickets'); ?></option>
                        <option value="resolved"><?php echo app('translator')->get('modules.tickets.totalResolvedTickets'); ?></option>
                        <option value="closed"><?php echo app('translator')->get('modules.tickets.totalClosedTickets'); ?></option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="control-label"><?php echo app('translator')->get('modules.tasks.priority'); ?></label>
                    <select class="form-control selectpicker" name="priority" id="priority" data-style="form-control">
                        <option value="all"><?php echo app('translator')->get('modules.tickets.nofilter'); ?></option>
                        <option value="low"><?php echo app('translator')->get('modules.tasks.low'); ?></option>
                        <option value="medium"><?php echo app('translator')->get('modules.tasks.medium'); ?></option>
                        <option value="high"><?php echo app('translator')->get('modules.tasks.high'); ?></option>
                        <option value="urgent"><?php echo app('translator')->get('modules.tickets.urgent'); ?></option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="control-label col-xs-12">&nbsp;</label>
                    <button type="button" id="apply-filters" class="btn btn-success col-md-6"><i class="fa fa-check"></i> <?php echo app('translator')->get('app.apply'); ?></button>
                    <button type="button" id="reset-filters" class="btn btn-inverse col-md-5 col-md-offset-1"><i class="fa fa-refresh"></i> <?php echo app('translator')->get('app.reset'); ?></button>
                </div>
            </div>
        </form>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="white-box">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <a href="<?php echo e(route('member.tickets.create')); ?>"
                               class="btn btn-info btn-sm"><?php echo app('translator')->get('modules.tickets.requestTicket'); ?> <i class="fa fa-plus"
                                                                                                 aria-hidden="true"></i></a>
                            <?php if($isAgent): ?>
                                <a href="<?php echo e(route('member.ticket-agent.index')); ?>"
                               class="btn btn-inverse btn-sm"><?php echo app('translator')->get('modules.tickets.goToAgentDashboard'); ?> <i class="fa fa-arrow-right"
                                                                                                 aria-hidden="true"></i></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="tickets-table">
                        <thead>
                        <tr>
                            <th><?php echo app('translator')->get('modules.tickets.ticket'); ?> #</th>
                            <th><?php echo app('translator')->get('modules.tickets.ticketSubject'); ?></th>
                            <th><?php echo app('translator')->get('modules.tickets.agent'); ?></th>
                            <th><?php echo app('translator')->get('modules.tickets.requestedOn'); ?></th>
                            <th><?php echo app('translator')->get('modules.sticky.lastUpdated'); ?></th>
                            <th><?php echo app('translator')->get('app.status'); ?></th>
                            <th><?php echo app('translator')->get('app.action'); ?></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/moment/moment.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="<?php echo e(asset('plugins/bower_components/moment/moment.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('plugins/daterange-picker/daterangepicker.js')); ?>"></script>

<script>
    $(function() {
        var dateformat = '<?php echo e($global->moment_format); ?>';

        var start = '';
        var end = '';

        function cb(start, end) {
            if(start){
                $('#start-date').val(start.format(dateformat));
                $('#end-date').val(end.format(dateformat));
                $('#reportrange span').html(start.format(dateformat) + ' - ' + end.format(dateformat));
            }

        }
        moment.locale('<?php echo e($global->locale); ?>');
        $('#reportrange').daterangepicker({
            // startDate: start,
            // endDate: end,
            locale: {
                language: '<?php echo e($global->locale); ?>',
                format: '<?php echo e($global->moment_format); ?>',
            },
            linkedCalendars: false,
            ranges: dateRangePickerCustom
        }, cb);

        cb(start, end);

    });
    showTable();

    $('.toggle-filter').click(function () {
        $('#ticket-filters').toggle('slide');
    })
    var table;

    $('#apply-filters').click(function () {
        showTable();
    });

    $('#reset-filters').click(function () {
        $('#filter-form')[0].reset();
        $('#filter-form').find('select').selectpicker('render');
        $('#start-date').val('');
        $('#end-date').val('');
        $('#reportrange span').html('');
        showTable();
    })
    function showTable() {

        var startDate = $('#start-date').val();

        if (startDate == '') {
            startDate = null;
        }

        var endDate = $('#end-date').val();

        if (endDate == '') {
            endDate = null;
        }

        var status = $('#status').val();
        if (status == "") {
            status = 0;
        }

        var priority = $('#priority').val();
        if (priority == "") {
            priority = 0;
        }

        table = $('#tickets-table').dataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            destroy: true,
            ajax: {
                "url": '<?php echo route('member.tickets.data'); ?>',
                "type": "POST",
                data:function (d) {d.startDate = startDate,
                    d.endDate = endDate,
                    d.status = status,
                    d.priority = priority,
                    d._token = '<?php echo e(csrf_token()); ?>' },

            },
            deferRender: true,
            language: {
                "url": "<?php echo __("app.datatable") ?>"
            },
            "fnDrawCallback": function( oSettings ) {
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            "order": [[0, "desc"]],
            columns: [
                {data: 'id', name: 'id'},
                {data: 'subject', name: 'subject', width: '25%'},
                {data: 'agent_id', name: 'agent_id', width: '20%'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', "searchable": false}
            ]
        });
    }

    $(function() {
        $('body').on('click', '.sa-params', function(){
            var id = $(this).data('user-id');
            swal({
                title: "<?php echo app('translator')->get('messages.sweetAlertTitle'); ?>",
                text: "<?php echo app('translator')->get('messages.confirmation.recoverProjectTemplate'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo app('translator')->get('messages.deleteConfirmation'); ?>",
                cancelButtonText: "<?php echo app('translator')->get('messages.confirmNoArchive'); ?>",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {

                    var url = "<?php echo e(route('member.projects.destroy',':id')); ?>";
                    url = url.replace(':id', id);

                    var token = "<?php echo e(csrf_token()); ?>";

                    $.easyAjax({
                        type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                table._fnDraw();
                                $('#totalTickets').html(response.data.totalTickets);
                                $('#closedTickets').html(response.data.closed);
                                $('#openTickets').html(response.data.open);
                                $('#pendingTickets').html(response.data.pending);
                                $('#resolvedTickets').html(response.data.resolved);
                            }
                        }
                    });
                }
            });
        });

    });

</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.member-app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eOsuite\resources\views/member/tickets/index.blade.php ENDPATH**/ ?>