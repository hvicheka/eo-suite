<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.payments.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.addNew'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/datetime-picker/datetimepicker.css')); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> <?php echo app('translator')->get('modules.payments.addPayment'); ?></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <?php echo Form::open(['id'=>'createPayment','class'=>'ajax-form','method'=>'POST']); ?>

                        <div class="form-body">
                            <?php echo Form::hidden('invoice_id', $invoice->id); ?>

                            <div class="row">

                                <div class="col-xs-12">
                                   <h2><?php echo e($invoice->invoice_number); ?></h2>
                                </div>
                                <?php if($invoice->project_id): ?>
                                    <div class="col-xs-12">
                                        <div class="col-md-4 form-group">
                                            <label><?php echo app('translator')->get('app.project'); ?></label>
                                            <h3 class="form-control-static"><?php echo e($invoice->project->project_name); ?></h3>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="col-xs-12">
                                    <div class="col-md-4 form-group">
                                        <label><?php echo app('translator')->get('modules.invoices.amount'); ?></label>
                                        <h3 class="form-control-static"><?php echo e($invoice->currency->currency_symbol.' '.$invoice->total); ?></h3>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label><?php echo app('translator')->get('modules.invoices.paid'); ?></label>
                                        <h3 class="form-control-static"><?php echo e($invoice->currency->currency_symbol); ?> <?php echo e($paidAmount); ?></h3>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label><?php echo app('translator')->get('modules.invoices.due'); ?></label>
                                        <h3 class="form-control-static"><?php echo e($invoice->currency->currency_symbol); ?> <?php echo e(max(($invoice->total-$paidAmount),0)); ?></h3>
                                    </div>
                                </div>
                                <!--/span-->

                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->get('modules.payments.amount'); ?></label>
                                        <input type="number" name="amount" id="amount" step=".01"  value="<?php echo e(max(($invoice->total-$paidAmount),0)); ?>" class="form-control">
                                        <span class="help-block"> </span>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->get('modules.payments.paymentGateway'); ?></label>
                                        <input type="text" name="gateway" id="gateway" class="form-control">
                                        <span class="help-block"> Paypal, Authorize.net, Stripe, Bank Transfer, Cash or others.</span>
                                    </div>
                                </div>
                                <!--/span-->

                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->get('modules.payments.transactionId'); ?></label>
                                        <input type="text" name="transaction_id" id="transaction_id" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->

                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.payments.paidOn'); ?></label>
                                        <input type="text" class="form-control" name="paid_on" id="paid_on" value="<?php echo e(Carbon\Carbon::now($global->timezone)->format('d/m/Y H:i')); ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.payments.remark'); ?></label>
                                        <textarea class="form-control" name="remarks" id="remark"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form-2" class="btn btn-success"><i class="fa fa-check"></i>
                                <?php echo app('translator')->get('app.save'); ?>
                            </button>

                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->
<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/moment/moment.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/datetime-picker/datetimepicker.js')); ?>"></script>

<script>

    // Switchery
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function() {
        new Switchery($(this)[0], $(this).data());

    });

    $(".select2").select2({
        formatNoMatches: function () {
            return "<?php echo e(__('messages.noRecordFound')); ?>";
        }
    });

    var timeFormat = '';
    var dateformat = '<?php echo e($global->moment_format); ?>';
    <?php if($global->time_format == 'h:i A'): ?>
        timeFormat = 'hh:mm A';
    <?php elseif($global->time_format == 'h:i a'): ?>
        timeFormat = 'hh:mm a';
    <?php else: ?>
        timeFormat = 'HH:mm';
    <?php endif; ?>
    var dateTimeFormat = dateformat+' '+timeFormat;
    jQuery('#paid_on').datetimepicker({
        format: dateTimeFormat
    });

    $('#save-form-2').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.payments.store')); ?>',
            container: '#createPayment',
            type: "POST",
            redirect: true,
            data: $('#createPayment').serialize()
        })
    });
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/payments/pay-invoice.blade.php ENDPATH**/ ?>