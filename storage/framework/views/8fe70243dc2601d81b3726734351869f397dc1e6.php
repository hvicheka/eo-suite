<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?> #<?php echo e($project->id); ?> - <span
                        class="font-bold"><?php echo e(ucwords($project->project_name)); ?></span></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12 text-right bg-title-right">
            <a href="javascript:;" class="btn btn-sm btn-success btn-outline" id="discussion-category"><i class="fa fa-gear"></i> <?php echo app('translator')->get('modules.discussions.discussionCategory'); ?></a>
            <a href="<?php echo e(route('admin.projects.edit', $project->id)); ?>" class="btn btn-sm btn-info btn-outline" ><i class="icon-note"></i> <?php echo app('translator')->get('app.edit'); ?></a>
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.projects.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->get('modules.projects.discussion'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css')); ?>">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
<style>
    #projects-table_wrapper .dt-buttons, #projects-table thead{
        display: none !important;
    }

    #projects-table tbody td{
        /* border: none; */
        padding: 15px 8px;
    }

    #projects-table.table-hover>tbody>tr:hover {
        background-color: #f7fafc!important;
    }

    .action-div {
        visibility: hidden;
    }
    .discussion-btn{
        margin-top: 7px;
    }
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-12">

            <section>
                <div class="sttabs tabs-style-line">

                    <?php echo $__env->make('admin.projects.show_project_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                    <div class="content-wrap">
                        <section class="show" id="discussion">
                            <div class="white-box">
                                <div class="row">
                                    <div class="col-md-3 ">
                                        <div class="row" id="button-area">
                                            <div class="col-xs-12">
                                                <button class="btn btn-info discussion-btn" type="button" id="add-discussion"><i class="fa fa-plus"></i> <?php echo app('translator')->get('app.new'); ?> <?php echo app('translator')->get('modules.projects.discussion'); ?></button>
                                            </div>
                                        </div>

                                        <div class="row" id="categories-area">
                                            <div class="col-xs-12">
                                                <ul class="discussion-categories">
                                                    <li class="active">
                                                        <a href="javascript:;" class="text-dark" data-category-id="">
                                                            <i class="fa fa-circle-o"></i> 
                                                            <?php echo app('translator')->get('app.all'); ?> <?php echo app('translator')->get('modules.projects.discussion'); ?>
                                                        </a>
                                                    </li>
                                                    <?php $__currentLoopData = $discussionCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <li>
                                                            <a href="javascript:;" data-category-id="<?php echo e($item->id); ?>" style="color: <?php echo e($item->color); ?>"><i class="fa fa-circle-o"></i> <?php echo e(ucwords($item->name)); ?></a>
                                                        </li>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-9">
                                      

                                        <div class="row">
                                            <div class="col-xs-12 discussion-btn">
                                                    <input type="hidden" id="project_id" value="<?php echo e($project->id); ?>">
                                                    <input type="hidden" id="category_id">
                                    
                                                    <div class="table-responsive">
                                                        <?php echo $dataTable->table(['class' => 'table table-bordered table-hover toggle-circle default footable-loaded footable']); ?>

                                                    </div>
                                            </div>
                                        </div>
                                        <!-- .row -->

                                    </div>
                                </div>
                            </div>
                        </section>

                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>


    </div>
    <!-- .row -->

    
    <div class="modal fade bs-modal-md in" id="editTimeLogModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

<script src="<?php echo e(asset('plugins/bower_components/waypoints/lib/jquery.waypoints.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/counterup/jquery.counterup.min.js')); ?>"></script>
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="<?php echo e(asset('js/datatables/buttons.server-side.js')); ?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


<?php echo $dataTable->scripts(); ?>


<script>


    function showData() {
        $('#projects-table').on('preXhr.dt', function (e, settings, data) {
            var projectId = $('#project_id').val();
            var categoryId = $('#category_id').val();

            data['project_id'] = projectId;
            data['category_id'] = categoryId;
        });

        window.LaravelDataTables["projects-table"].draw();
    }

    $('body').on('click', '.delete-discussion', function () {
        var id = $(this).data('discussion-id');
        swal({
            title: "<?php echo app('translator')->get('messages.sweetAlertTitle'); ?>",
            text: "<?php echo app('translator')->get('messages.confirmation.recoverRecord'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<?php echo app('translator')->get('messages.deleteConfirmation'); ?>",
            cancelButtonText: "<?php echo app('translator')->get('messages.confirmNoArchive'); ?>",
            closeOnConfirm: true,
            closeOnCancel: true
        }).then(function (isConfirm) {
            if (isConfirm) {

                var url = "<?php echo e(route('admin.discussion.destroy',':id')); ?>";
                url = url.replace(':id', id);

                var token = "<?php echo e(csrf_token()); ?>";

                $.easyAjax({
                    type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                    success: function (response) {
                        if (response.status == "success") {
                            $.unblockUI();
                            showData();
                        }
                    }
                });
            }
        });
    });

    $('body').on('click', '#discussion-category', function () {
        var url = '<?php echo e(route('admin.discussion-category.create')); ?>';

        $('#modelHeading').html('<?php echo e(__('app.edit')); ?> <?php echo e(__('modules.projects.milestones')); ?>');
        $.ajaxModal('#editTimeLogModal', url);

    });

    $('body').on('click', '#add-discussion', function () {
        var url = '<?php echo e(route('admin.discussion.create', ['id' => $project->id])); ?>';

        $('#modelHeading').html('<?php echo e(__('app.edit')); ?> <?php echo e(__('modules.projects.milestones')); ?>');
        $.ajaxModal('#editTimeLogModal', url);

    });

    $('body').on('click', '.edit-category', function () {
        var categoryId = $(this).data('category-id');
        var url = '<?php echo e(route('admin.discussion-category.edit', ':id')); ?>';
        url = url.replace(':id', categoryId);

        $('#modelHeading').html('<?php echo e(__('app.edit')); ?> <?php echo e(__('modules.projects.discussion')); ?>');
        $.ajaxModal('#editTimeLogModal', url);
    });

    $('.discussion-categories li a').click(function () {
        $('.discussion-categories li').removeClass('active');
        $(this).closest('li').addClass('active');

        var categoryId = $(this).data('category-id');
        $('#category_id').val(categoryId);
        showData();
    });

    $('body').on('mouseover', '#projects-table tr', function () {
        $(this).find('.action-div').css('visibility', 'visible');
    });

    $('body').on('mouseout', '#projects-table tr', function () {
        $('.action-div').css('visibility', 'hidden');
    });
   
    $('ul.showProjectTabs .discussion').addClass('tab-current');

    $('body').on('click', '.file-delete', function () {
        var id = $(this).data('file-id');
        swal({
            title: "<?php echo app('translator')->get('messages.sweetAlertTitle'); ?>",
            text: "<?php echo app('translator')->get('messages.confirmation.deleteFile'); ?>",
            dangerMode: true,
            icon: 'warning',
            buttons: {
                cancel: "<?php echo app('translator')->get('messages.confirmNoArchive'); ?>",
                confirm: {
                    text: "<?php echo app('translator')->get('messages.deleteConfirmation'); ?>!",
                    value: true,
                    visible: true,
                    className: "danger",
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {

                var url = "<?php echo e(route('admin.discussion-files.destroy',':id')); ?>";
                url = url.replace(':id', id);

                var token = "<?php echo e(csrf_token()); ?>";

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token': token, '_method': 'DELETE'},
                    success: function (response) {
                        if (response.status == "success") {
                            showData();
                        }
                    }
                });
            }
        });
    });
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/projects/discussion/show.blade.php ENDPATH**/ ?>