<?php $__empty_1 = true; $__currentLoopData = $chatDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $chatDetail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>

    <li class="<?php if($chatDetail->from == $user->id): ?> odd <?php else: ?>  <?php endif; ?>">
        <div class="chat-image">
            <?php if(is_null($chatDetail->fromUser->image)): ?>
                <img src="<?php echo e(asset('img/default-profile-3.png')); ?>" alt="user-img"
                     class="img-circle" style="height:30px; width:30px;">
            <?php else: ?>
                <img src="<?php echo e(asset_url('avatar/' . $chatDetail->fromUser->image)); ?>" alt="user-img"
                     class="img-circle" style="height:30px; width:30px;">
            <?php endif; ?>
        </div>
        <div class="chat-body">
            <div class="chat-text">
                <?php if(($chatDetail->from == $user->id)): ?>
                    <div class="messageDelete <?php if($chatDetail->from == $user->id): ?> left <?php else: ?> right <?php endif; ?>" onclick="deleteMessage('<?php echo e($chatDetail->id); ?>')"><i class="fa fa-trash"></i></div>
                <?php endif; ?>
                <h4><?php if($chatDetail->from == $user->id): ?> you <?php else: ?> <?php echo e($chatDetail->fromUser->name); ?> <?php endif; ?></h4>
                <p><?php echo e($chatDetail->message); ?> </p>
                    <?php $__currentLoopData = $chatDetail->files; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-2 m-b-10">
                            <div class="card">
                                <div class="file-bg">
                                    <div class="overlay-file-box">
                                        <div class="user-content">
                                            <a target="_blank" href="<?php echo e($file->file_url); ?>">
                                                <?php if($file->icon == 'images'): ?>
                                                    <img class="card-img-top img-responsive" src="<?php echo e($file->file_url); ?>" alt="Card image cap">
                                                <?php else: ?>
                                                    <i class="fa <?php echo e($file->icon); ?> card-img-top img-responsive" style="font-size: -webkit-xxx-large; padding-top: 65px;"></i>
                                                <?php endif; ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <h6 class="card-title thumbnail-img"> </h6>

                                    <a target="_blank" href="<?php echo e($file->file_url); ?>"
                                       data-toggle="tooltip" data-original-title="View"
                                       class="btn btn-info btn-circle"><i
                                                class="fa fa-search"></i></a>

                                        <a href="<?php echo e(route('admin.user-chat-files.download', $file->id)); ?>"
                                           data-toggle="tooltip" data-original-title="Download"
                                           class="btn btn-default btn-circle"><i
                                                    class="fa fa-download"></i></a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <b><?php echo e($chatDetail->created_at->timezone($global->timezone)->format($global->date_format.' '. $global->time_format)); ?></b>
            </div>
        </div>
    </li>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
    <li><div class="message"><?php echo app('translator')->get('messages.noMessage'); ?></div></li>
<?php endif; ?>
<?php /**PATH D:\WAMP-SERVER\www\eOsuite\resources\views/admin/user-chat/ajax-chat-list.blade.php ENDPATH**/ ?>