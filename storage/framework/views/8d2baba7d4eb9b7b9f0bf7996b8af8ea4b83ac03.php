<?php $__empty_1 = true; $__currentLoopData = $timelogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
    <div class="col-xs-12 item-row margin-top-5">
        <?php if(!is_null($item->task_id)): ?>
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group">
                        <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.item'); ?></label>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>
                            <input type="text" value="<?php echo e($item->task->heading); ?>" readonly class="form-control item_name" name="item_name[]">
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea name="item_summary[]" class="form-control" placeholder="<?php echo app('translator')->get('app.description'); ?>" rows="2"></textarea>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group">
                        <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.item'); ?></label>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>
                            <input type="text" value="<?php echo e(__('app.others')); ?>" readonly class="form-control item_name" name="item_name[]">
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea name="item_summary[]" class="form-control" placeholder="<?php echo app('translator')->get('app.description'); ?>" rows="2"></textarea>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="col-md-1">
            <div class="form-group">
                <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.qty'); ?></label>
                <input type="number" min="1" class="form-control quantity" value="1" name="quantity[]" >
            </div>
        </div>
        <div class="col-md-2">
            <div class="row">
                <div class="form-group">
                    <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.unitPrice'); ?></label>
                    <input type="text"  class="form-control cost_per_item" name="cost_per_item[]" value="<?php echo e($item->sum); ?>" >
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.type'); ?></label>
                <select id="multiselect" name="taxes[0][]"  multiple="multiple" class="selectpicker customSequence form-control type">
                    <?php $__currentLoopData = $taxes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tax): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option data-rate="<?php echo e($tax->rate_percent); ?>" value="<?php echo e($tax->id); ?>"><?php echo e($tax->tax_name); ?>: <?php echo e($tax->rate_percent); ?>%</option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>
        </div>
        <div class="col-md-2 border-dark  text-center">
            <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.amount'); ?></label>
            <p class="form-control-static"><span class="amount-html"><?php echo e($item->sum); ?></span></p>
            <input type="hidden" class="amount" name="amount[]" value="<?php echo e($item->sum); ?>">
        </div>
        <div class="col-md-1 text-right visible-md visible-lg">
            <button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>
        </div>
        <div class="col-md-1 hidden-md hidden-lg">
            <div class="row">
                <button type="button" class="btn remove-item btn-danger"><i class="fa fa-remove"></i> <?php echo app('translator')->get('app.remove'); ?></button>
            </div>
        </div>
    </div>    
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
    <div class="col-xs-12 item-row margin-top-5">
        <div class="alert alert-danger text-center"><?php echo app('translator')->get('messages.noLogTimeFound'); ?></div>
    </div>
<?php endif; ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/invoices/timelog-item.blade.php ENDPATH**/ ?>