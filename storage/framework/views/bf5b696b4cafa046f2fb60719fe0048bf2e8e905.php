<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.products.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.update'); ?> <?php echo app('translator')->get('app.menu.products'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/html5-editor/bootstrap-wysihtml5.css')); ?>">

<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-xs-12">

        <section>
            <div class="sttabs tabs-style-line">
                
                <div class="content-wrap">
                    <section id="section-line-3" class="show">
                        <div class="row">
                            <div class="col-xs-12" id="files-list-panel">
                                <div class="white-box">
                                    <h3 class="box-title"><?php echo app('translator')->get('app.menu.products'); ?> <?php echo app('translator')->get('app.details'); ?></h3>

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-xs-6 b-r"> <strong><?php echo app('translator')->get('app.name'); ?></strong> <br>
                                                        <p class="text-muted"><?php echo e((!is_null($product)) ? $product->name : '--'); ?></p>
                                                    </div>
                                                    <div class="col-xs-6"> <strong><?php echo app('translator')->get('app.price'); ?></strong> <br>
                                                        <p class="text-muted"><?php echo e((!is_null($product)) ? $product->price : '--'); ?></p>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-xs-6 b-r"> <strong><?php echo app('translator')->get('modules.productCategory.productCategory'); ?></strong> <br>
                                                        <p class="text-muted"><?php echo e(($product->category != '') ? ucwords($product->category->category_name) :'--'); ?></p>
                                                    </div>
                                                    <div class="col-xs-6 b-r"> <strong><?php echo app('translator')->get('modules.productCategory.productSubCategory'); ?></strong> <br>
                                                        <p class="text-muted"><?php echo e(($product->subcategory != '') ? ucwords($product->subcategory->category_name) :'--'); ?></p>
                                                    </div>
        
                                                </div>
                                                <hr>
        
                                                <?php if(@isset($product->tax)): ?>
                                                    
                                                <div class="row">
                                                    <div class="col-xs-6 b-r"> <strong><?php echo app('translator')->get('modules.invoices.tax'); ?></strong> <br>
                                                        <p class="text-muted"><?php echo e($product->tax->name); ?>: <?php echo e($product->tax->rate_percent); ?>%</p>
                                                    </div>
                                                </div>
                                                <hr>
                                                <?php endif; ?>

                                            </div>
                                            <div class="col-md-4 text-center">
                                                <img class="img-thumbnail"  id="preview" src="<?php echo e($product->img_name?asset('user-uploads/product/'.$product->img_name):asset('img/default-image.png')); ?>" alt="" style="width: 176px;height: 180px;object-fit: cover;">
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-xs-12"> <strong><?php echo app('translator')->get('app.description'); ?></strong> <br>
                                                <p class="text-muted"><?php echo e($product->description); ?> </p>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-12"> <strong><?php echo app('translator')->get('app.feature'); ?></strong> <br>
                                                <p class="text-muted"><?php echo $product->feature; ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </section>

                </div><!-- /content -->
            </div><!-- /tabs -->
        </section>
    </div>


</div>

    

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
    <script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/html5-editor/wysihtml5-0.3.0.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/html5-editor/bootstrap-wysihtml5.js')); ?>"></script>
   
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/products/show.blade.php ENDPATH**/ ?>