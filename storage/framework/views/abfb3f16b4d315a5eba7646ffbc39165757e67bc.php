<?php $__currentLoopData = $boardColumns; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="panel col-xs-3 board-column p-0" data-column-id="<?php echo e($column->id); ?>" >
        <div class="panel-heading p-t-5 p-b-5" >
            <div class="panel-title">
                <h6 style="color: <?php echo e($column->label_color); ?>"><?php echo e(ucwords($column->type)); ?> (<span id="statusTotal<?php echo e($column->id); ?>"><?php echo e($column->leads->count()); ?></span>)

                    <div style="position: relative;" class="dropdown pull-right fullscreen-hide">
                        <a href="javascript:;"  data-toggle="dropdown"  class="dropdown-toggle "><i class="ti-settings font-normal"></i></a>
                        <ul role="menu" class="dropdown-menu">
                            <li><a href="javascript:;" data-type-id="<?php echo e($column->id); ?>" class="edit-type" ><?php echo app('translator')->get('app.edit'); ?></a>
                            </li>
                            <?php if(!$column->default): ?>
                                <li><a href="javascript:;" data-column-id="<?php echo e($column->id); ?>" class="delete-column"  ><?php echo app('translator')->get('app.delete'); ?></a></li>
                            <?php endif; ?>
                        </ul>

                    </div>
                </h6>
            </div>
        </div>
        <div class="panel-body" id="taskBox_<?php echo e($column->id); ?>" style="height: 90vh; overflow-y: auto">
            <div class="row">
                <div class="col-xs-12" style="height: 400px !important;">
                    <?php $__currentLoopData = $column->leads; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lead): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="panel panel-default lobipanel view-task" data-task-id="<?php echo e($lead->id); ?>" data-sortable="true">
                            <div class="panel-body">
                                <div class="p-10 font-12 font-semi-bold"><?php echo e(ucfirst($lead->client_name)); ?> <?php if(!is_null($lead->currency_id)): ?> (<?php echo e($lead->currency->currency_symbol); ?><span id="leadValue<?php echo e($lead->id); ?>"><?php echo e($lead->value); ?></span>) <?php endif; ?></div>
                                <div class="p-10 p-t-0 text-muted"><small><i class="fa fa-building-o"></i> <?php echo e(ucfirst($lead->company_name)); ?></small></div>

                                <?php if(!is_null($lead->agent_id)): ?>
                                    <div class="p-t-10 p-b-10 p-10">
                                        <img src="<?php echo e($lead->lead_agent->user->image_url); ?>" data-toggle="tooltip" data-original-title="<?php echo e(ucwords($lead->lead_agent->user->name)); ?> " data-placement="right"
                                             alt="user" class="img-circle" width="25" height="25">
                                    </div>
                                <?php endif; ?>

                                <div class="p-t-0 p-10 font-12 col-xs-12">
                                    <?php if($lead->next_follow_up_date != null && $lead->next_follow_up_date != ''): ?>
                                        <i class="icon-calender"></i> <?php echo e(\Carbon\Carbon::parse($lead->next_follow_up_date)->format($global->date_format)); ?>

                                    <?php else: ?>
                                        <?php echo app('translator')->get('modules.followup.followUpNotFound'); ?>
                                    <?php endif; ?>
                                </div>


                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <div class="panel panel-default lobipanel"  data-sortable="true"></div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<script>
    $(function () {

        var data ='<?php echo app('translator')->get("app.menu.leads"); ?> <?php echo app('translator')->get("app.from"); ?><strong> <?php echo e(\Carbon\Carbon::parse($startDate)->format($global->date_format)); ?> </strong> to <strong><?php echo e(\Carbon\Carbon::parse($endDate)->format($global->date_format)); ?></strong>';
        $('#filter-result').html(data);

        // <?php $__currentLoopData = $boardColumns; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        // $('#taskBox_<?php echo e($column->id); ?>').slimScroll({
        //     height: '70vh'
        // });
        // <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        let draggingTaskId = 0;
        let draggedTaskId = 0;

        $('.lobipanel').on('dragged.lobiPanel', function (ev, lobiPanel) {
            var body = lobiPanel.$el.find('.view-task');
            var $parent = $(this).parent(),
                $children = $parent.children();

            var boardColumnIds = [];
            var taskIds = [];
            var prioritys = [];

            $children.each(function (ind, el) {
                boardColumnIds.push($(el).closest('.board-column').data('column-id'));
                boardColumnIds.push($(el).closest('.board-column').data('column-id'));
                taskIds.push($(el).data('task-id'));
                prioritys.push($(el).index());
            });
            var startDate = '<?php echo e($startDate); ?>';
            var endDate = '<?php echo e($endDate); ?>';
            var assignedTo = '<?php echo e($assignedTo); ?>';
            // update values for all tasks
            $.easyAjax({
                url: '<?php echo e(route("admin.leads.updateIndex")); ?>',
                type: 'POST',
                async: false,
                data:{boardColumnIds: boardColumnIds, taskIds: taskIds, prioritys: prioritys,'_token':'<?php echo e(csrf_token()); ?>', draggingTaskId: draggingTaskId, draggedTaskId: draggedTaskId,
                    startDate: startDate, endDate: endDate, assignedTo: assignedTo},
                success: function (response) {
                    draggedTaskId = draggingTaskId;
                    draggingTaskId = 0;

                    if(response.columnData){
                        response.columnData.forEach(function (ind, el) {
                            console.log([ind['columnId'], ind['value']]);
                            $('#statusTotal'+ind['columnId']).html(ind['value']);
                        });
                    }
                }
            });

            $("body").tooltip({
                selector: '[data-toggle="tooltip"]'
            });

            $('.board-column').each(function () {
                let lobipanelItems = $(this).find('.view-task').length;
                // console.log(lobipanelItems);
                if (lobipanelItems == 1) {
                    $(this).find('.lobipanel:first').addClass('m-b-0');
                }
            })

        }).lobiPanel({
            sortable: true,
            reload: false,
            editTitle: false,
            close: false,
            minimize: false,
            unpin: false,
            expand: false

        });

        var isDragging = 0;
        $('.lobipanel-parent-sortable').on('sortactivate', function(){
            // console.log("activate event handle");
            $('.board-column > .panel-body').css('overflow-y', 'unset');
            isDragging = 1;
        });
        $('.lobipanel-parent-sortable').on('sortstop', function(e){
            // console.log("stop event handle");
            $('.board-column > .panel-body').css('overflow-y', 'auto');
            isDragging = 0;
        });

        $('.view-task').click(function () {
            var id = $(this).data('task-id');
            var url = "<?php echo e(route('admin.leads.show',':id')); ?>";
            url = url.replace(':id', id);

            draggingTaskId = id;

            if (isDragging == 0) {
                window.open(url, '_blank');
            }
        })

        $('.delete-column').click(function () {
            var id = $(this).data('column-id');

            var url = "<?php echo e(route('admin.lead-status-settings.destroy',':id')); ?>";
            url = url.replace(':id', id);

            var token = "<?php echo e(csrf_token()); ?>";

            swal({
                title: "<?php echo app('translator')->get('messages.sweetAlertTitle'); ?>",
                text: "<?php echo app('translator')->get('messages.confirmation.recoveColumn'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo app('translator')->get('messages.deleteConfirmation'); ?>",
                cancelButtonText: "<?php echo app('translator')->get('messages.confirmNoArchive'); ?>",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
                                window.location.reload();
                            }
                        }
                    });

                }
            });

        })

    });
</script>
<?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/lead/board_data.blade.php ENDPATH**/ ?>