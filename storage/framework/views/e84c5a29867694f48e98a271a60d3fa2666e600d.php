<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/dropzone-master/dist/dropzone.css')); ?>">

<style>
    #editTimeLogModal{
        overflow-x: hidden;
         overflow-y: auto;
    }
    </style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title"><?php echo app('translator')->get('app.new'); ?> <?php echo app('translator')->get('modules.projects.discussion'); ?></h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        <?php echo Form::open(['id'=>'createProjectCategory','class'=>'ajax-form','method'=>'POST']); ?>

        <div class="form-body">
            <?php echo Form::hidden('project_id', $projectId); ?>

            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label class="control-label"><?php echo app('translator')->get('app.category'); ?></label>
                        <select class="select2 form-control" data-placeholder="<?php echo app('translator')->get("app.category"); ?>" id="discussion_category_id" name="discussion_category_id">
                            <option value="">--</option>
                            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($item->id); ?>" ><?php echo e(ucwords($item->name)); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="form-group">
                        <label class="required"><?php echo app('translator')->get('app.title'); ?></label>
                        <input type="text" name="title" id="title" class="form-control">
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="form-group">
                        <label class="control-label"><?php echo app('translator')->get('app.description'); ?></label>
                        <textarea id="description" name="description" class="form-control summernote"></textarea>
                    </div>
                </div>
                <div class="row m-b-20">
                    <div class="col-xs-12">
                        <?php if($upload): ?>
                            <button type="button"
                                    class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button"
                                    style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i>
                                File Select Or Upload
                            </button>
                            <div id="file-upload-box">
                                <div class="row" id="file-dropzone">
                                    <div class="col-xs-12">
                                        <div class="dropzone"
                                             id="file-upload-dropzone">
                                            <?php echo e(csrf_field()); ?>

                                            <div class="fallback">
                                                <input name="file" type="file" multiple/>
                                            </div>
                                            <input name="image_url" id="image_url" type="hidden"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="discussionID" id="discussionID">
                            <input type="hidden" name="type" id="discussion">
                        <?php else: ?>
                            <div class="alert alert-danger"><?php echo app('translator')->get('messages.storageLimitExceed', ['here' => '<a href='.route('admin.billing.packages'). '>Here</a>']); ?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> <?php echo app('translator')->get('app.save'); ?></button>
        </div>
        <?php echo Form::close(); ?>

    </div>
</div>

<script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/dropzone-master/dist/dropzone.js')); ?>"></script>

<script>
    <?php if($upload): ?>
        Dropzone.autoDiscover = false;
    //Dropzone class
    myDropzone = new Dropzone("div#file-upload-dropzone", {
        url: "<?php echo e(route('admin.discussion-files.store')); ?>",
        headers: {'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'},
        paramName: "file",
        maxFilesize: 10,
        maxFiles: 10,
        acceptedFiles: "image/*,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/docx,application/pdf,text/plain,application/msword,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        autoProcessQueue: false,
        uploadMultiple: true,
        addRemoveLinks: true,
        parallelUploads: 10,
        dictDefaultMessage: "<?php echo app('translator')->get('modules.projects.dropFile'); ?>",
        init: function () {
            myDropzone = this;
            this.on("success", function (file, response) {
                if(response.status == 'fail') {
                    $.showToastr(response.message, 'error');
                    return;
                }
            })
        }
    });

    myDropzone.on('sending', function (file, xhr, formData) {
        console.log(myDropzone.getAddedFiles().length, 'sending');
        var ids = $('#discussionID').val();
        formData.append('discussion_id', ids);
        formData.append('type', 'discussion');
    });

    myDropzone.on('completemultiple', function () {
        var msgs = "<?php echo app('translator')->get('messages.taskCreatedSuccessfully'); ?>";
        $.showToastr(msgs, 'success');
        window.location.href = '<?php echo e(route('admin.projects.discussion', $projectId)); ?>'

    });
    <?php endif; ?>
    $("#discussion_category_id").select2({
        formatNoMatches: function () {
            return "<?php echo e(__('messages.noRecordFound')); ?>";
        }
    });
    
    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        dialogsInBody: true,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['picture']],
            ["view", ["fullscreen"]],
        ]
    });
var discussionID;
    $('#save-category').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.discussion.store')); ?>',
            container: '#createProjectCategory',
            type: "POST",
            data: $('#createProjectCategory').serialize(),
            success: function (data) {
                    var dropzone = 0;
                    <?php if($upload): ?>
                        dropzone = myDropzone.getQueuedFiles().length;
                    <?php endif; ?>

                    if(dropzone > 0){
                        discussionID = data.discussionID;
                        $('#discussionID').val(data.discussionID);
                        myDropzone.processQueue();
                    } else {
                        var msgs = "<?php echo app('translator')->get('messages.taskCreatedSuccessfully'); ?>";
                        $.showToastr(msgs, 'success');
                        window.location.href = '<?php echo e(route('admin.projects.discussion', $projectId)); ?>'
                    }

            }
        })
    });
</script><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/discussion/create.blade.php ENDPATH**/ ?>