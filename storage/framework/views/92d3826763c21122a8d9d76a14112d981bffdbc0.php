<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
    <style>
        .d-none {
            display: none;
        }
        

    </style>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.clients.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <?php echo $__env->make('admin.clients.client_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="col-xs-12">
            <section>
                <div class="sttabs tabs-style-line">
                    <?php echo $__env->make('admin.clients.tabs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <div class="content-wrap">
                        <section id="section-line-3" class="show">
                            <div class="row">
                                <div class="col-xs-12" id="issues-list-panel">
                                    <div class="white-box">
                                        <h2><?php echo app('translator')->get('app.menu.notes'); ?></h2>
                                        <div class="row m-b-10">
                                            <div class="col-xs-12">
                                                <a href="javascript:;" id="show-add-form"
                                                   class="btn btn-success btn-outline"> <?php echo app('translator')->get('modules.notes.addNotes'); ?></a>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <?php echo Form::open(['id'=>'addNotes','class'=>'ajax-form hide','method'=>'POST']); ?>

                                                <?php echo Form::hidden('client_id', $client->id); ?>

                                                <div class="form-body" id ="addContact">
                                                    <div class="row m-t-30">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label><?php echo app('translator')->get('modules.notes.notesTitle'); ?></label>
                                                                <input type="text" name="notes_title" id="notes_title"
                                                                    class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label><?php echo app('translator')->get('modules.notes.notesType'); ?></label>
                                                                <div class="radio-list">
                                                                    <label class="radio-inline p-0">
                                                                        <div class="radio radio-info">
                                                                            <input type="radio" name="notes_type" id="public"
                                                                                checked="" value="0">
                                                                            <label><?php echo app('translator')->get('modules.notes.public'); ?></label>
                                                                        </div>
                                                                    </label>
                                                                    <label class="radio-inline">
                                                                        <div class="radio radio-info">
                                                                            <input type="radio" name="notes_type" id="private"
                                                                                value="1">
                                                                            <label><?php echo app('translator')->get('modules.notes.private'); ?></label>
                                                                        </div>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                   

                                                </div>
                                                <div class="col-xs-12 " id="type_private">
                                                    <div class="form-group">
                                                        <div class="row m-t-30">
                                                            <div class="col-md-4 ">
                                                                <div class="form-group">
                                                                    <label><?php echo app('translator')->get('modules.notes.selectMember'); ?></label>
                                                                    <select class="select2 m-b-10 select2-multiple" multiple="multiple" name="user_id[]" id="user_id">
                                                                        <option value=""> -- </option>
                                                                        <?php $__empty_1 = true; $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                                      
                                                                        <option  value="<?php echo e($employee->id); ?>"><?php echo e(ucwords($employee->name)); ?></option>
                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                                        <option value=""><?php echo app('translator')->get('messages.noProjectCategoryAdded'); ?></option>
                                                                    <?php endif; ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 ">
                                                                <div class="form-group" style="padding-top: 19px;">
                                                                    <div class="checkbox checkbox-info">
                                                                        <input name="is_client_show" value = "1"
                                                                            type="checkbox" >
                                                                        <label for="check_amount"><?php echo app('translator')->get('modules.notes.showClient'); ?></label>
                                                                    </div>
                                                                    
                                                                </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 ">
                                                                <div class="form-group" style="padding-top: 19px;">
                                                                    <div class="checkbox checkbox-info">
                                                                        <input name="ask_password" value = "1"
                                                                            type="checkbox" >
                                                                        <label for="check_amount"><?php echo app('translator')->get('modules.notes.askReenterPassword'); ?></label>
                                                                    </div>
                                                                    
                                                                </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="form-group">
                                                            <label class="control-label"><?php echo app('translator')->get('modules.notes.notesDetails'); ?></label>
                                                            <textarea name="note_details"  id="note_details"  rows="5"  class="form-control"><?php echo e($leadDetail->address ?? ''); ?></textarea>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                
                                                </div>
                                                <div class="form-actions m-t-30">
                                                    <button type="button" id="save-form" class="btn btn-success"> <i
                                                            class="fa fa-check"></i> <?php echo app('translator')->get('app.save'); ?></button>
                                                            <button type="button" id="close-form" class="btn btn-default"><i
                                                                class="fa fa-times"></i> <?php echo app('translator')->get('app.close'); ?></button>
                                                </div>
                                                <?php echo Form::close(); ?>


                                                <hr>
                                            </div>
                                        </div>

                                        <div class="table-responsive m-t-30">
                                            <table
                                                class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                                                id="contacts-table">
                                                <thead>
                                                    <tr>
                                                        <th><?php echo app('translator')->get('app.id'); ?></th>
                                                        <th><?php echo app('translator')->get('app.notesTitle'); ?></th>
                                                        <th><?php echo app('translator')->get('app.notesType'); ?></th>
                                                        <th><?php echo app('translator')->get('app.action'); ?></th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </section>

                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>
        
        <div class="modal fade bs-modal-md in" id="editContactModal" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" id="modal-data-application">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                    </div>
                    <div class="modal-body">
                        Loading...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn blue">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        

    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>

<script src="<?php echo e(asset('plugins/bower_components/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>


    <script>
        
        var table = $('#contacts-table').dataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: '<?php echo route('admin.notes.data', $client->id); ?>',
        deferRender: true,
        language: {
            "url": "<?php echo __("app.datatable") ?>"
        },
        "fnDrawCallback": function( oSettings ) {
            $("body").tooltip({
                selector: '[data-toggle="tooltip"]'
            });
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'notes_title', name: 'notes_title' },
            { data: 'notes_type', name: 'notes_type' },
            { data: 'action', name: 'action' }
        ]
    });
             $('ul.showClientTabs .clientNotes').addClass('tab-current');
             if ($('input[name=notes_type]:checked').val() === "private") {
                    $('#type_private').removeClass('d-none').addClass('d-block');
                } else {
                    $('#type_private').removeClass('d-block').addClass('d-none');
                }
            $('.radio-list').click(function() {
                console.log($('input[name=notes_type]:checked').val());
                if ($('input[name=notes_type]:checked').val() === '1') {
                    $('#type_private').removeClass('d-none').addClass('d-block');
                } else {
                    $('#type_private').removeClass('d-block').addClass('d-none');
                }
            })
            
            $('#save-form').click(function () {
                $.easyAjax({
                    url: '<?php echo e(route('admin.notes.store')); ?>',
                    container: '#addNotes',
                    type: "POST",
                    redirect: true,
                    data: $('#addNotes').serialize(),
                    success: function (data) {
                    if(data.status == 'success'){
                        location.reload();

                     //   $('#addNotes').toggleClass('hide', 'show');
                        table._fnDraw();
                    }
            }
                })
             });
             $('body').on('click', '.edit-contact', function () {
                var id = $(this).data('contact-id');

                var url = '<?php echo e(route('admin.notes.edit', ':id')); ?>';
                url = url.replace(':id', id);

                $('#modelHeading').html('Update Contact');
                $.ajaxModal('#editContactModal',url);

            });
            $('body').on('click', '.view-contact', function () {
                var id = $(this).data('contact-id');

                var url = '<?php echo e(route('admin.notes.view', ':id')); ?>';
                url = url.replace(':id', id);

                $('#modelHeading').html('Update Contact');
                $.ajaxModal('#editContactModal',url);

            });

    $('body').on('click', '.sa-params', function(){
        var id = $(this).data('contact-id');
        swal({
            title: "<?php echo app('translator')->get('messages.sweetAlertTitle'); ?>",
            text: "<?php echo app('translator')->get('messages.confirmation.recoverNotes'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<?php echo app('translator')->get('messages.deleteConfirmation'); ?>",
            cancelButtonText: "<?php echo app('translator')->get('messages.confirmNoArchive'); ?>",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {
                var url = "<?php echo e(route('admin.notes.destroy',':id')); ?>";
                url = url.replace(':id', id);

                var token = "<?php echo e(csrf_token()); ?>";

                $.easyAjax({
                    type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
                                    table._fnDraw();
                                }
                            }
                });
            }
        });
    });
             $('#show-add-form,#close-form').click(function () {
                $('#addNotes').toggleClass('hide', 'show');
            });
            $(".select2").select2({
                formatNoMatches: function () {
                    return "<?php echo e(__('messages.noRecordFound')); ?>";
                }
            });

    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/notes/show.blade.php ENDPATH**/ ?>