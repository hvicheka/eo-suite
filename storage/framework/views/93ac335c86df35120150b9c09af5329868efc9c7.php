<div class="col-xs-12 item-row margin-top-5">
    <div class="col-md-3">
        <div class="row">
            <div class="form-group">
                <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.item'); ?></label>
                <div class="input-group">
                    <div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>
                    <input type="text" class="form-control item_name" name="item_name[]"
                           value="<?php echo e($items->name); ?>" >
                </div>
            </div>
            <div class="form-group">
                <textarea name="item_summary[]" class="form-control" placeholder="<?php echo app('translator')->get('app.description'); ?>" rows="2"><?php echo e($items->description); ?></textarea>
            </div>
        </div>
    </div>
    <?php if($invoiceSetting->hsn_sac_code_show): ?>
        <div class="col-md-1 ">
            <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.hsnSacCode'); ?></label>
            <input type="text" class="form-control"  data-item-id="<?php echo e($items->id); ?>"  name="hsn_sac_code[]" >

        </div>
    <?php endif; ?>
    <div class="col-md-1">
        <div class="form-group">
            <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.qty'); ?></label>
            <input type="number" min="1" class="form-control quantity" data-item-id="<?php echo e($items->id); ?>" value="1" name="quantity[]" >
        </div>
    </div>

    <div class="col-md-2">
        <div class="row">
            <div class="form-group">
                <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.unitPrice'); ?></label>
                <input type="text"  class="form-control cost_per_item" name="cost_per_item[]" data-item-id="<?php echo e($items->id); ?>" value="<?php echo e($items->price); ?>">
            </div>
        </div>
    </div>

    <div class="col-md-2">

        <div class="form-group">
            <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.type'); ?></label>
            <select id="" name=""  multiple="multiple" class="selectpicker form-control type">
                <?php $__currentLoopData = $taxes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tax): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option data-rate="<?php echo e($tax->rate_percent); ?>"
                            <?php if(isset($items->taxes) && $items->taxes != "null"  && array_search($tax->id, json_decode($items->taxes)) !== false): ?>
                            selected
                            <?php endif; ?>
                            value="<?php echo e($tax->id); ?>"><?php echo e($tax->tax_name); ?>: <?php echo e($tax->rate_percent); ?>%</option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
        </div>
    </div>

    <div class="col-md-2 border-dark  text-center">
        <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.amount'); ?></label>

        <p class="form-control-static"><span class="amount-html" data-item-id="<?php echo e($items->id); ?>">0</span></p>
        <input type="hidden" class="amount" name="amount[]" data-item-id="<?php echo e($items->id); ?>">
    </div>

    <div class="col-md-1 text-right visible-md visible-lg">
        <button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>
    </div>

    <div class="col-xs-12 text-center hidden-md hidden-lg">
        <div class="row">
            <button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i></button>
        </div>
    </div>

    <script>
        $(function () {
            var quantity = $('#sortable').find('.quantity[data-item-id="<?php echo e($items->id); ?>"]').val();
            var perItemCost = $('#sortable').find('.cost_per_item[data-item-id="<?php echo e($items->id); ?>"]').val();
            var amount = (quantity*perItemCost);
            $('#sortable').find('.amount[data-item-id="<?php echo e($items->id); ?>"]').val(amount);
            $('#sortable').find('.amount-html[data-item-id="<?php echo e($items->id); ?>"]').html(amount);

            calculateTotal();
        });
    </script>
</div>
<?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/member/invoices/add-item.blade.php ENDPATH**/ ?>