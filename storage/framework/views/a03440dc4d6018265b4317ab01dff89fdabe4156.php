<?php $__env->startPush('head-script'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.css')); ?>">

    <style>
        .img-width {
            width: 185px;
        }
        .tabs-style-line nav a {
            box-shadow: unset !important;
        }
        .steamline .sl-left {
            margin-left: -7px !important;
        }
        .history-remove {
            display: none;
        }
        .sl-item:hover .history-remove {
            display: block;
        }
    </style>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.contracts.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.addNew'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <?php echo Form::open(['id'=>'createContract','class'=>'ajax-form','method'=>'POST']); ?>

        <div class="col-md-6">
            <div class="white-box">
                <h3 class="box-title m-b-0"><?php echo e($contract->subject); ?>

                    <a href="<?php echo e(route('admin.contracts.show', md5($contract->id))); ?>" target="_blank" class="btn btn-sm btn-default pull-right">View Contract</a>
                </h3>

                <div class="sttabs tabs-style-line" id="invoice_container">
                    <nav>
                        <ul class="customtab" role="tablist" id="myTab">
                            <li class="nav-item active"><a class="nav-link" href="#summery" data-toggle="tab" role="tab"><span><i class="glyphicon glyphicon-file"></i> <?php echo app('translator')->get('app.menu.contract'); ?></span></a>
                            </li>
                        </ul>
                    </nav>
                    <div class="tab-content tabcontent-border">
                        <div class="tab-pane active" id="summery" role="tabpanel">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p class="text-muted m-b-30 font-13"></p>
                                    <div class="form-group">
                                    <textarea name="contract_detail" id="contract_detail"
                                          class="summernote"><?php echo e($contract->contract_detail); ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="white-box">
                <h3 class="box-title m-b-0"><?php echo app('translator')->get('app.edit'); ?> <?php echo app('translator')->get('app.menu.contract'); ?></h3>

                <p class="text-muted m-b-30 font-13"></p>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="company_name " class="required"><?php echo app('translator')->get('app.client'); ?></label>
                            <div>
                                <select class="select2 form-control" data-placeholder="<?php echo app('translator')->get('app.client'); ?>" name="client" id="clientID">
                                    <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($client->id); ?>" <?php if($client->id == $contract->client_id): ?> selected <?php endif; ?>><?php echo e(ucwords($client->name)); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="subject " class="required"><?php echo app('translator')->get('app.subject'); ?></label>
                            <input type="text" class="form-control" id="subject" name="subject"  value="<?php echo e($contract->subject ?? ''); ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="subject " class="required"><?php echo app('translator')->get('app.amount'); ?> (<?php echo e($global->currency->currency_symbol); ?>)</label>
                            <input type="number" class="form-control" id="amount" name="amount" value="<?php echo e($contract->amount ?? ''); ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label required"><?php echo app('translator')->get('modules.contracts.contractType'); ?>
                                <a href="javascript:;"
                                   id="createContractType"
                                   class="btn btn-sm btn-outline btn-success">
                                    <i class="fa fa-plus"></i> <?php echo app('translator')->get('modules.contracts.addContractType'); ?>
                                </a>
                            </label>
                            <div>
                                <select class="select2 form-control" data-placeholder="<?php echo app('translator')->get('app.client'); ?>" id="contractType" name="contract_type">
                                    <?php $__currentLoopData = $contractType; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option
                                                value="<?php echo e($type->id); ?>"><?php echo e(ucwords($type->name)); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required"><?php echo app('translator')->get('modules.timeLogs.startDate'); ?></label>
                            <input id="start_date" name="start_date" type="text"
                                   class="form-control"
                                   value="<?php echo e($contract->start_date->timezone($global->timezone)->format($global->date_format)); ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="required"><?php echo app('translator')->get('modules.timeLogs.endDate'); ?></label>
                            <input id="end_date" name="end_date" type="text"
                                   class="form-control"
                                   value="<?php echo e($contract->end_date == null ? $contract->end_date : $contract->end_date->timezone($global->timezone)->format($global->date_format) ?? ''); ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                           <label><?php echo app('translator')->get('modules.contracts.contractName'); ?></label>
                            <input name="contract_name" type="text"
                                     value="<?php echo e($contract->contract_name ??''); ?>"   class="form-control">
                         </div>
                     </div>
                    <div class="col-md-6">
                         <div class="form-group">
                          <label><?php echo app('translator')->get('modules.contracts.alternateAddress'); ?></label>
                             <textarea class="form-control" name="alternate_address" 
                               class="form-control"><?php echo e($contract->alternate_address ?? ''); ?></textarea>
                        </div>
                     </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><?php echo app('translator')->get('modules.lead.mobile'); ?></label>
                            <input type="tel" name="mobile" value="<?php echo e($contract->mobile ?? ''); ?>" id="mobile" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label><?php echo app('translator')->get('modules.clients.officePhoneNumber'); ?></label>
                            <input type="text" name="office_phone" id="office_phone"  value="<?php echo e($contract->office_phone ?? ''); ?>"  class="form-control">
                        </div>
                    </div>
                     <div class="col-md-6 ">
                        <div class="form-group">
                            <label><?php echo app('translator')->get('modules.stripeCustomerAddress.city'); ?></label>
                            <input type="text" name="city" id="city"  value="<?php echo e($contract->city ?? ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label><?php echo app('translator')->get('modules.stripeCustomerAddress.state'); ?></label>
                            <input type="text" name="state" id="state"   value="<?php echo e($contract->state ?? ''); ?>" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label><?php echo app('translator')->get('modules.stripeCustomerAddress.country'); ?></label>
                            <input type="text" name="country" id="country"  value="<?php echo e($contract->country ?? ''); ?>"  class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6 ">
                            <div class="form-group">
                                <label><?php echo app('translator')->get('modules.stripeCustomerAddress.postalCode'); ?></label>
                                <input type="text" name="postal_code" id="postalCode"  value="<?php echo e($contract->postal_code ?? ''); ?>"class="form-control">
                            </div>
                        </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="required"><?php echo app('translator')->get('modules.contracts.notes'); ?></label>
                            <textarea class="form-control summernote" id="description" name="description" rows="4"><?php echo e($contract->description ?? ''); ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6" >
                        <label><?php echo app('translator')->get('modules.contracts.companyLogo'); ?></label>
                        <div class="form-group">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="<?php echo e($contract->image_url); ?>" alt=""/>

                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"
                                    style="max-width: 200px; max-height: 150px;"></div>
                                    <div>
                                        <span class="btn btn-info btn-file">
                                            <span class="fileinput-new"> <?php echo app('translator')->get('app.selectImage'); ?> </span>
                                            <span class="fileinput-exists"> <?php echo app('translator')->get('app.change'); ?> </span>
                                            <input type="file" name="company_logo" id="company_logo"> </span>
                                            <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                data-dismiss="fileinput"> <?php echo app('translator')->get('app.remove'); ?> </a>
                                    </div>
                                </div>
                            </div>

                        </div>                            
                    </div> 
                </div>
            </div>
        </div>
        <div class="col-md-12 text-center m-t-15 m-b-15">
            <a href="<?php echo e(route('admin.contracts.index')); ?>" class="btn btn-inverse waves-effect waves-light m-r-10"><?php echo app('translator')->get('app.back'); ?></a>
            <button type="submit" id="save-form" class="btn btn-success waves-effect waves-light">
                <?php echo app('translator')->get('app.copy'); ?>
            </button>
        </div>
        <?php echo Form::close(); ?>

    </div>
    <!-- .row -->
    
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    
<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
    <script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.min.js')); ?>"></script>
    <script>
        $(document).ready(() => {
            $('.slimscrolltab').slimScroll({
                height: '283px'
                , position: 'right'
                , size: "5px"
                , color: '#dcdcdc'
                , });
        });

        $(".select2").select2({
            formatNoMatches: function () {
                return "<?php echo e(__('messages.noRecordFound')); ?>";
            }
        });

        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });

        jQuery('#start_date, #end_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: '<?php echo e($global->date_picker_format); ?>',
            weekStart:'<?php echo e($global->week_start); ?>',
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '<?php echo e(route('admin.contracts.copy-submit')); ?>',
                container: '#createContract',
                type: "POST",
                redirect: true,
                file:true,
                data: $('#createContract').serialize()
            })
        });

        $('#createContractType').click(function(){
            var url = '<?php echo e(route('admin.contract-type.create-contract-type')); ?>';
            $('#modelHeading').html("<?php echo app('translator')->get('modules.contracts.manageContractType'); ?>");
            $.ajaxModal('#taskCategoryModal', url);
        })
        $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });
    </script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/contracts/copy.blade.php ENDPATH**/ ?>