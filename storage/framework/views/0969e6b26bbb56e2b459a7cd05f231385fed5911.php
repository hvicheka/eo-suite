<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"> <i class="fa fa-search"></i>  <?php echo app('translator')->get('modules.payments.paymentDetails'); ?></h4>
</div>
<div class="modal-body">
    <div class="form-body">
        <div class="row">
            <div class="col-xs-12">
                <?php $__empty_1 = true; $__currentLoopData = $invoice->payment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div class="list-group-item edit-task">
                        <h5 class="list-group-item-heading sbold"><?php echo app('translator')->get('app.paymentOn'); ?>: <?php echo e($payment->paid_on->format($global->date_format)); ?></h5>
                        <p class="list-group-item-text">
                        <div class="row margin-top-5">
                            <div class="col-md-4">
                                <b><?php echo app('translator')->get('app.amount'); ?>:</b>  <br>
                            <?php echo e(currency_formatter($payment->amount,$invoice->currency->currency_symbol)); ?> 
                            </div>
                            <div class="col-md-4">
                                <b><?php echo app('translator')->get('app.gateway'); ?>:</b>  <br>
                                <?php echo e($payment->gateway); ?>

                            </div>
                            <div class="col-md-4">
                                <b><?php echo app('translator')->get('app.transactionId'); ?>:</b> <br>
                                <?php echo e($payment->transaction_id); ?>

                            </div>
                        </div>
                        <div class="row margin-top-10">
                            <div class="<?php if($payment->gateway == 'Offline'): ?> col-md-4 <?php else: ?> col-md-12 <?php endif; ?> ">
                                <b><?php echo app('translator')->get('app.remark'); ?>:</b>  <br>
                                <?php echo ($payment->remarks != '') ? ucfirst($payment->remarks) : "<span class='font-red'>--</span>"; ?>

                            </div>
                            <?php if($payment->gateway == 'Offline'): ?>
                                <div class="col-md-4">
                                    <b><?php echo app('translator')->get('app.paymentMethod'); ?>:</b>  <br>
                                    <?php echo ($payment->offlineMethod->name ); ?>

                                </div>
                                <div class="col-md-4">
                                    <b><?php echo app('translator')->get('app.receipt'); ?>:</b>  <br>
                                    <a href="<?php echo e($payment->invoice->approved_offline_invoice_payment->slip); ?>" data-toggle="tooltip" data-title="View File" target="_blank">
                                        <i class="fa fa-eye"></i> View
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>

                        </p>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <p><?php echo app('translator')->get('modules.payments.paymentDetailNotFound'); ?></p>
                <?php endif; ?>
            </div>
        </div>
        <!--/row-->
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-white waves-effect" data-dismiss="modal"><?php echo app('translator')->get('app.close'); ?></button>
</div>


<?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/invoices/payment-detail.blade.php ENDPATH**/ ?>