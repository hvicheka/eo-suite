<style>
    .signature-pad {
        position: absolute;
        left: 0;
        top: 0;
        width:100%;
        height: 150px;
        background-color: white;
    }
    .signature-pad {
        border: 1px solid #000000;
    }
</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <?php if($type == 'accept'): ?>
        <h4 class="modal-title"><?php echo app('translator')->get('modules.estimates.signatureAndConfirmation'); ?></h4>
    <?php else: ?>
        <h4 class="modal-title"><?php echo app('translator')->get('modules.proposal.rejectConfirm'); ?></h4>
    <?php endif; ?>
</div>
<div class="modal-body">
    <div class="portlet-body">
        <?php echo Form::open(['id'=>'acceptProposal','class'=>'ajax-form','method'=>'POST']); ?>

        <input type="hidden" name="type" value="<?php echo e($type); ?>" id="type">
        <div class="form-body">
            <div class="row ">
                <?php if($type == 'accept'): ?>
                    <div class="col-xs-12 m-b-10">
                        <div class="form-group">
                            <label class="col-xs-3"><?php echo app('translator')->get('app.fullName'); ?></label>
                            <div class="col-xs-9">
                                <input type="text" name="name" id="name" class="form-control" value="<?php echo e($proposal->lead->client_name); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 m-b-10">
                        <div class="form-group">
                            <label class="col-xs-3"><?php echo app('translator')->get('modules.lead.email'); ?></label>
                            <div class="col-xs-9">
                                <input type="email" name="email" id="email" class="form-control"  value="<?php echo e($proposal->lead->client_email); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 m-b-10" >
                        <div class="form-group">
                            <label class="col-xs-3"><?php echo app('translator')->get('app.signHere'); ?></label>
                        </div>
                    </div>
                    <div class="col-xs-12 m-b-10" style="height: 150px">
                        <div class="form-group">
                            <label><?php echo app('translator')->get('app.signHere'); ?></label>
                            <div class="wrapper form-control ">
                                <canvas id="signature-pad" class="signature-pad"></canvas>
                            </div>

                        </div>
                    </div>
                    <button id="undo" class="btn btn-default m-l-10"><?php echo app('translator')->get('modules.estimates.undo'); ?></button>
                    <button id="clear" class="btn btn-danger"><?php echo app('translator')->get('modules.estimates.clear'); ?></button>
                <?php else: ?>
                    <div class="col-xs-12 m-b-10">
                        <div class="form-group">
                            <label class="col-xs-3"><?php echo app('translator')->get('app.reason'); ?> (<?php echo app('translator')->get('app.optional'); ?>) :</label>
                            <div class="col-xs-9">
                                <textarea name="comment" id="comment" rows="5" class="form-control"> </textarea>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php echo Form::close(); ?>

    </div>
</div>
<div class="modal-footer">
    <div class="form-actions">
        <?php if($type == 'accept'): ?>
            <button type="button" id="accept-signature" class="btn btn-success save-action"> <i class="fa fa-check"></i> <?php echo app('translator')->get('app.accept'); ?></button>
        <?php else: ?>
            <button type="button" id="reject-signature" class="btn btn-danger save-action"> <i class="fa fa-check"></i> <?php echo app('translator')->get('app.reject'); ?></button>
        <?php endif; ?>
    </div>
</div>

<script>
    <?php if($type == 'accept'): ?>
        $(function () {
            var canvas = document.getElementById('signature-pad');

    // Adjust canvas coordinate space taking into account pixel ratio,
    // to make it look crisp on mobile devices.
    // This also causes canvas to be cleared.
            function resizeCanvas() {
                // When zoomed out to less than 100%, for some very strange reason,
                // some browsers report devicePixelRatio as less than 1
                // and only part of the canvas is cleared then.
                var ratio =  Math.max(window.devicePixelRatio || 1, 1);
                canvas.width = canvas.offsetWidth * ratio;
                canvas.height = canvas.offsetHeight * ratio;
                canvas.getContext("2d").scale(ratio, ratio);
            }

            window.onresize = resizeCanvas;
            resizeCanvas();

            signaturePad = new SignaturePad(canvas, {
                backgroundColor: 'rgb(255, 255, 255)' // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
            });

            document.getElementById('clear').addEventListener('click', function (e) {
                e.preventDefault();
                signaturePad.clear();
            });

            document.getElementById('undo').addEventListener('click', function (e) {
                e.preventDefault();
                var data = signaturePad.toData();
                if (data) {
                    data.pop(); // remove the last dot or line
                    signaturePad.fromData(data);
                }
            });
        });
    <?php endif; ?>

    $('.save-action').click(function () {
        var type = $('#type').val();
        if(type == 'accept'){
            var signature = signaturePad.toDataURL('image/png');

            if (signaturePad.isEmpty()) {
                return $.showToastr("Please provide a signature first.", 'error');
            }
            var name = $('#name').val();
            var email = $('#email').val();
            $.easyAjax({
                url: '<?php echo e(route('front.proposal-action-post', md5($proposal->id))); ?>',
                container: '#acceptEstimate',
                type: "POST",
                disableButton:true,
                data: {
                    type:type,
                    name:name,
                    email:email,
                    comment:comment,
                    signature:signature,
                    _token: '<?php echo e(csrf_token()); ?>'
                },
                success: function (response) {
                    window.location.reload();
                }
            })
        }
        else{
            var comment = $('#comment').val();
            $.easyAjax({
                url: '<?php echo e(route('front.proposal-action-post', md5($proposal->id))); ?>',
                container: '#acceptEstimate',
                type: "POST",
                disableButton:true,
                data: {
                    comment:comment,
                    type:type,
                    _token: '<?php echo e(csrf_token()); ?>'
                },
                success: function(data){
                    if(data.status == 'success'){
                        window.location.reload();
                    }
                }
            })
        }

    });
</script><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/proposal-front/accept.blade.php ENDPATH**/ ?>