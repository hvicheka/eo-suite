<?php if(in_array('timelogs',$modules)): ?>
<div class="row">
<div class="col-md-12">
<span id="timer-section">
    <?php if(!is_null($timer)): ?>
        <div class="nav navbar-top-links navbar-right pull-right m-t-10 m-b-0">
            <a class="btn btn-rounded btn-default stop-timer-modal" href="javascript:;" data-timer-id="<?php echo e($timer->id); ?>">
                <i class="ti-alarm-clock"></i>
                <span id="active-timer"><?php echo e($timer->timer); ?></span>
                <label class="label label-danger"><?php echo app('translator')->get("app.stop"); ?></label></a>
        </div>
    <?php else: ?>
        <div class="nav navbar-top-links navbar-right pull-right m-t-10 m-b-0">
            <a class="btn btn-outline btn-inverse timer-modal" href="javascript:;"><?php echo app('translator')->get("modules.timeLogs.startTimer"); ?> <i class="fa fa-check-circle text-success"></i></a>
        </div>
    <?php endif; ?>
</span>
<?php if(isset($activeTimerCount) && $user->cans('view_timelogs')): ?>
<span id="timer-section">
    <div class="nav navbar-top-links navbar-right m-t-10 m-r-10">
        <a class="btn btn-rounded btn-default active-timer-modal" href="javascript:;"><?php echo app('translator')->get("modules.projects.activeTimers"); ?>
            <span class="label label-danger" id="activeCurrentTimerCount"><?php if($activeTimerCount > 0): ?> <?php echo e($activeTimerCount); ?> <?php else: ?> 0 <?php endif; ?></span>
        </a>
    </div>
</span>
<?php endif; ?>
</div>
</div>
<?php endif; ?><?php /**PATH D:\WAMP-SERVER\www\eOsuite\resources\views/sections/timer.blade.php ENDPATH**/ ?>