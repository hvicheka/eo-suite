<form id="editSettings" class="ajax-form" data-language-id="<?php echo e($frontDetail->language_setting_id); ?>">
    <?php echo csrf_field(); ?>
    <div class="row">
        <div class="col-sm-12 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="price_title"><?php echo app('translator')->get('modules.frontCms.priceTitle'); ?></label>
                <input type="text" class="form-control" id="price_title" name="price_title"
                    value="<?php echo e($frontDetail->price_title); ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="price_description"><?php echo app('translator')->get('modules.frontCms.priceDescription'); ?></label>
                <textarea class="form-control" id="price_description" rows="5"
                        name="price_description"><?php echo e($frontDetail->price_description); ?></textarea>
            </div>
        </div>
    </div>

    <button type="button" id="save-form"
            class="btn btn-success waves-effect waves-light m-r-10">
        <?php echo app('translator')->get('app.update'); ?>
    </button>
</form><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/super-admin/price-settings/edit-form.blade.php ENDPATH**/ ?>