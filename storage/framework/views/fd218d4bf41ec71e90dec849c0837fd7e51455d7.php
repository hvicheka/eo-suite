<?php $__env->startSection('content'); ?>
    <!-- START Contact Section -->
    <section class="contact-section bg-white sp-100-70">
        <div class="container">
            <?php if(!is_null($frontDetail->contact_html)): ?>
                <div class="row">
                    <div class="col-md-10 mx-auto">
                        <?php echo $frontDetail->contact_html; ?>

                    </div>
                </div>
            <?php endif; ?>

            <div class="row">
                <div class="col-md-10 mx-auto">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="contact-info">
                                <div class="mobile-device"><span class="fa fa-home fa-fw style"></span><span class="heading-info"><?php echo app('translator')->get('app.address'); ?></span>
                                    <div class="address-content"><?php echo e($frontDetail->address); ?></div>
                                </div>

                                <div class="mobile-device"><span class="fa fa-envelope fa-fw style"></span><span class="heading-info"><?php echo app('translator')->get('app.email'); ?></span>
                                    <div class="address-content"><?php echo e($frontDetail->email); ?></div>
                                </div>
                                <?php if($frontDetail->phone): ?>
                                    <div class="mobile-device"><span class="fa fa-phone fa-fw style"></span><span class="heading-info"><?php echo app('translator')->get('app.phone'); ?></span>
                                        <div class="address-content"><?php echo e($frontDetail->phone); ?></div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            
                        </div>
                    </div>
                    <?php echo Form::open(['id'=>'contactUs', 'method'=>'POST']); ?>

                    <div class="row">
                        <div class="alert col-md-12 text-center" id="alert"></div>
                    </div>
                    <div class="row" id="contactUsBox">
                        <div class="form-group mb-4 col-lg-6 col-12">
                            <input type="text" name="name" class="form-control" placeholder="<?php echo app('translator')->get('modules.profile.yourName'); ?>"
                                   id="name">
                        </div>
                        <div class="form-group mb-4 col-lg-6 col-12">
                            <input type="email" class="form-control" placeholder="<?php echo app('translator')->get('modules.profile.yourEmail'); ?>"
                                   name="email" id="email">
                        </div>
                        <div class="form-group mb-4 col-12">
                            <textarea rows="6" name="message" class="form-control br-10"
                                      placeholder="<?php echo app('translator')->get('modules.messages.message'); ?>"
                                      id="message"></textarea>
                        </div>
                        <?php if($global->google_recaptcha_status  && $global->google_captcha_version=="v2"): ?>
                        <div class="form-group <?php echo e($errors->has('g-recaptcha-response') ? 'has-error' : ''); ?>">
                                <div class="g-recaptcha"
                                     data-sitekey="<?php echo e($global->google_recaptcha_key); ?>">
                                </div>
                                <?php if($errors->has('g-recaptcha-response')): ?>
                                    <div class="help-block with-errors"><?php echo e($errors->first('g-recaptcha-response')); ?></div>
                                <?php endif; ?>
                        </div>
                        <?php endif; ?>

                    <input type='hidden' name='recaptcha_token' id='recaptcha_token'>
                        <div class="col-12" style="margin-top: 12px;">
                            <button type="button" class="btn btn-lg btn-custom mt-1" id="contact-submit">
                                <?php echo e($frontMenu->contact_submit); ?>

                            </button>
                        </div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>

        </div>
    </section>
    <!-- END Contact Section -->
<?php $__env->stopSection(); ?>
<?php $__env->startPush('footer-script'); ?>
    <script>
        $('#contact-submit').click(function () {

            <?php if($global->google_recaptcha_status  && $global->google_recaptcha_status =="v2"): ?>
            if (grecaptcha.getResponse().length == 0) {
                alert('Please click the reCAPTCHA checkbox');
                return false;
            }
            <?php endif; ?>

            $.easyAjax({
                url: '<?php echo e(route('front.contact-us')); ?>',
                container: '#contactUs',
                type: "POST",
                data: $('#contactUs').serialize(),
                messagePosition: "inline",
                success: function (response) {
                    if (response.status === 'success') {
                        $('#contactUsBox').remove();
                    }
                }
            })
        });
    </script>
    <?php if($global->google_recaptcha_status  && $global->google_captcha_version=="v3"): ?>
    <script src="https://www.google.com/recaptcha/api.js?render=<?php echo e($global->google_recaptcha_key); ?>"></script>

    <script>
        setTimeout(function () {

            grecaptcha.ready(function () {
                grecaptcha.execute('<?php echo e($global->google_recaptcha_key); ?>', {action: 'submit'}).then(function (token) {
                    document.getElementById("recaptcha_token").value = token;
                });
            });

        }, 3000);

    </script>
<?php endif; ?>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.sass-app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eOsuite\resources\views/saas/contact.blade.php ENDPATH**/ ?>