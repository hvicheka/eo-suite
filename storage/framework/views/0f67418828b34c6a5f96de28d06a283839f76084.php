<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.clients.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.menu.invoices'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>

    <div class="row">


        <?php echo $__env->make('admin.clients.client_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


        <div class="col-xs-12">

            <section>
                <div class="sttabs tabs-style-line">
                    <?php echo $__env->make('admin.clients.tabs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                    <div class="content-wrap">
                        <section id="section-line-1" class="show">
                            <div class="row">


                                <div class="col-xs-12" >
                                    <div class="white-box">
                                        <h2><?php echo app('translator')->get('app.menu.invoices'); ?></h2>

                                        <ul class="list-group" id="invoices-list">
                                            <?php $__empty_1 = true; $__currentLoopData = $invoices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $invoice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                <li class="list-group-item">
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <?php echo app('translator')->get('app.invoice'); ?> # <?php echo e($invoice->invoice_number); ?>

                                                        </div>
                                                        <div class="col-md-2">
                                                            <?php echo e(currency_formatter($invoice->total ,$invoice->currency_symbol)); ?> 
                                                        </div>
                                                        <div class="col-md-3">
                                                            <a href="<?php echo e(route('admin.invoices.download', $invoice->id)); ?>" data-toggle="tooltip" data-original-title="Download" class="btn btn-default btn-circle"><i class="fa fa-download"></i></a>
                                                            <span class="m-l-10"><?php echo e($invoice->issue_date->format('d M, y')); ?></span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <?php if($loop->last): ?>
                                                    <li class="list-group-item">
                                                        <div class="row">
                                                            <div class="col-md-7 ">
                                                                <span class="pull-right"><?php echo app('translator')->get('modules.invoices.totalUnpaidInvoice'); ?></span>
                                                            </div>
                                                            <div class="col-md-2 text-danger">
                                                                <?php echo e(currency_formatter($invoices->sum('total')-$invoices->sum('paid_payment'),$invoice->currency_symbol)); ?> 
                                                            </div>
                                                            <div class="col-md-3">

                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                <li class="list-group-item">
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <?php echo app('translator')->get('modules.invoices.noInvoiceForClient'); ?>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                        </section>
                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>


    </div>
    <!-- .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
    <script>
        $('ul.showClientTabs .clientInvoices').addClass('tab-current');
    </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/clients/invoices.blade.php ENDPATH**/ ?>