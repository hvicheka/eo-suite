<form id="editSettings" class="ajax-form" data-language-id="<?php echo e($frontMenu->language_setting_id); ?>">
    <?php echo csrf_field(); ?>
    <div class="row">
        <div class="col-sm-12 col-md-3 col-xs-12">
            <div class="form-group">
                <label for="title"><?php echo app('translator')->get('app.home'); ?></label>
                <input type="text" class="form-control" id="home" name="home" value="<?php echo e($frontMenu->home); ?>">
            </div>
        </div>

        <div class="col-sm-12 col-md-3 col-xs-12">
            <div class="form-group">
                <label for="title"><?php echo app('translator')->get('app.price'); ?></label>
                <input type="text" class="form-control" id="price" name="price" value="<?php echo e($frontMenu->price); ?>">
            </div>
        </div>

        <div class="col-sm-12 col-md-3 col-xs-12">
            <div class="form-group">
                <label for="title"><?php echo app('translator')->get('app.contact'); ?></label>
                <input type="text" class="form-control" id="contact" name="contact" value="<?php echo e($frontMenu->contact); ?>">
            </div>
        </div>

        <div class="col-sm-12 col-md-3 col-xs-12">
            <div class="form-group">
                <label for="title"><?php echo app('translator')->get('app.feature'); ?></label>
                <input type="text" class="form-control" id="feature" name="feature" value="<?php echo e($frontMenu->feature); ?>">
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-12 col-md-3 col-xs-12">
            <div class="form-group">
                <label for="title"><?php echo app('translator')->get('app.get_start'); ?></label>
                <input type="text" class="form-control" id="get_start" name="get_start" value="<?php echo e($frontMenu->get_start); ?>">
            </div>
        </div>

        <div class="col-sm-12 col-md-3 col-xs-12">
            <div class="form-group">
                <label for="title"><?php echo app('translator')->get('app.login'); ?></label>
                <input type="text" class="form-control" id="login" name="login" value="<?php echo e($frontMenu->login); ?>">
            </div>
        </div>

        <div class="col-sm-12 col-md-3 col-xs-12">
            <div class="form-group">
                <label for="title"><?php echo app('translator')->get('app.contact_submit'); ?></label>
                <input type="text" class="form-control" id="contact_submit" name="contact_submit" value="<?php echo e($frontMenu->contact_submit); ?>">
            </div>
        </div>
    </div>

    <button type="button" id="save-form"
            class="btn btn-success waves-effect waves-light m-r-10">
        <?php echo app('translator')->get('app.update'); ?>
    </button>
</form><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/super-admin/front-menu-settings/edit-form.blade.php ENDPATH**/ ?>