<div id="event-detail">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="ti-eye"></i> <?php echo app('translator')->get('app.menu.leaves'); ?> <?php echo app('translator')->get('app.details'); ?> </h4>
    </div>
    <div class="modal-body">
        <?php echo Form::open(['id'=>'updateEvent','class'=>'ajax-form','method'=>'GET']); ?>

        <div class="form-body">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="form-group">
                        <label><?php echo app('translator')->get('modules.leaves.applicantName'); ?></label>
                        <p>
                            <?php echo e(ucwords($leave->user->name)); ?>

                        </p>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label><?php echo app('translator')->get('app.date'); ?></label>
                        <p><?php echo e($leave->leave_date->format($global->date_format)); ?>

                            <label class="label label-<?php echo e($leave->type->color); ?>"><?php echo e(ucwords($leave->type->type_name)); ?></label>
                            <?php if($leave->duration == 'half day'): ?>
                             <label class="label label-info"><?php echo e(ucwords($leave->duration)); ?></label>
                            <?php endif; ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 ">
                    <div class="form-group">
                        <label><?php echo app('translator')->get('modules.leaves.reason'); ?></label>
                        <p><?php echo $leave->reason; ?></p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label><?php echo app('translator')->get('app.status'); ?></label>
                        <p>
                            <?php if($leave->status == 'approved'): ?>
                                <strong class="text-success"><?php echo app('translator')->get('app.approved'); ?></strong>
                            <?php elseif($leave->status == 'pending'): ?>
                                <strong class="text-warning"><?php echo app('translator')->get('app.pending'); ?></strong>
                            <?php else: ?>
                                <strong class="text-danger"><?php echo app('translator')->get('app.rejected'); ?></strong>
                            <?php endif; ?>

                        </p>
                    </div>
                </div>

            </div>
        </div>
        <?php echo Form::close(); ?>


    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-white waves-effect" data-dismiss="modal"><?php echo app('translator')->get('app.close'); ?></button>
        <button type="button" class="btn btn-danger btn-outline delete-event waves-effect waves-light"><i class="fa fa-times"></i> <?php echo app('translator')->get('app.delete'); ?></button>
        <button type="button" class="btn btn-info save-event waves-effect waves-light"><i class="fa fa-edit"></i> <?php echo app('translator')->get('app.edit'); ?>
        </button>
    </div>

</div>

<script>

    $('.save-event').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.leaves.edit', $leave->id)); ?>',
            container: '#updateEvent',
            type: "GET",
            data: $('#updateEvent').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    $('#event-detail').html(response.view);
                }
            }
        })
    })

    $('.delete-event').click(function(){
        swal({
            title: "<?php echo app('translator')->get('messages.sweetAlertTitle'); ?>",
            text: "<?php echo app('translator')->get('messages.confirmation.recoverLeaveApplication'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<?php echo app('translator')->get('messages.deleteConfirmation'); ?>",
            cancelButtonText: "<?php echo app('translator')->get('messages.confirmNoArchive'); ?>",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {

                var url = "<?php echo e(route('admin.leaves.destroy', $leave->id)); ?>";

                var token = "<?php echo e(csrf_token()); ?>";

                $.easyAjax({
                    type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                    success: function (response) {
                        if (response.status == "success") {
                            window.location.reload();
                        }
                    }
                });
            }
        });
    });


</script><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/leaves/show.blade.php ENDPATH**/ ?>