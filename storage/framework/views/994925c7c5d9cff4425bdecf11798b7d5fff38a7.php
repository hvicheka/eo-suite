<div class="js-cookie-consent cookie-consent alert alert-info text-center mb-0 border-radius-0">

    <span class="cookie-consent__message">
        <?php echo app('translator')->get('modules.front.allowCookies'); ?>
        
    </span>

    <button class="js-cookie-consent-agree cookie-consent__agree btn btn-success">
        <?php echo app('translator')->get('modules.front.cookiesButton'); ?>

        
    </button>

</div>
<?php /**PATH D:\WAMP-SERVER\www\eOsuite\resources\views/vendor/cookieConsent/dialogContents.blade.php ENDPATH**/ ?>