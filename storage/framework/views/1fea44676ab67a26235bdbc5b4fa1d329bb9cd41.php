<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.clients.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.menu.invoices'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>

    <div class="row">


        <?php echo $__env->make('admin.clients.client_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


        <div class="col-xs-12">

            <section>
                <div class="sttabs tabs-style-line">
                    <?php echo $__env->make('admin.clients.tabs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                    <div class="content-wrap">
                        <section id="section-line-1" class="show">
                            <div class="row">


                                <div class="col-xs-12" >
                                    <div class="white-box">

                                        <div class="row">
                                            <div class="col-xs-12" >
                                                <button class="btn btn-sm btn-info addDocs m-t-10 m-b-10 " style="" onclick="showAdd()"><i
                                                            class="fa fa-plus"></i> <?php echo app('translator')->get('app.add'); ?></button>
                                                <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th width="70%"><?php echo app('translator')->get('app.name'); ?></th>
                                                        <th><?php echo app('translator')->get('app.action'); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="employeeDocsList">
                                                    <?php $__empty_1 = true; $__currentLoopData = $clientDocs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$clientDoc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                        <tr>
                                                            <td><?php echo e($key+1); ?></td>
                                                            <td width="70%"><?php echo e(ucwords($clientDoc->name)); ?></td>
                                                            <td>
                                                                <a href="<?php echo e(route('admin.client-docs.download', $clientDoc->id)); ?>"
                                                                   data-toggle="tooltip" data-original-title="Download"
                                                                   class="btn btn-default btn-circle"><i
                                                                            class="fa fa-download"></i></a>
                                                                <a target="_blank" href="<?php echo e($clientDoc->file_url); ?>"
                                                                   data-toggle="tooltip" data-original-title="View"
                                                                   class="btn btn-info btn-circle"><i
                                                                            class="fa fa-search"></i></a>
                                                                <a href="javascript:;" data-toggle="tooltip" data-original-title="Delete" data-file-id="<?php echo e($clientDoc->id); ?>"
                                                                   data-pk="list" class="btn btn-danger btn-circle sa-params"><i class="fa fa-times"></i></a>
                                                            </td>

                                                        </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                        <tr>
                                                            <td colspan="3" class="text-center p-30"><?php echo app('translator')->get('messages.noDocsFound'); ?></td>
                                                        </tr>
                                                    <?php endif; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </section>
                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>


    </div>
    <!-- .row -->
    
    <div class="modal fade bs-modal-md in" id="edit-column-form" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    
<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
    <script>
        function showAdd() {
            var url = "<?php echo e(route('admin.client-docs.quick-create', [$client->id])); ?>";
            $.ajaxModal('#edit-column-form', url);
        }

        $('body').on('click', '.sa-params', function () {
            var id = $(this).data('file-id');
            var deleteView = $(this).data('pk');
            swal({
                title: "<?php echo app('translator')->get('messages.sweetAlertTitle'); ?>",
                text: "<?php echo app('translator')->get('messages.confirmation.deleteFile'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo app('translator')->get('messages.deleteConfirmation'); ?>",
                cancelButtonText: "<?php echo app('translator')->get('messages.confirmNoArchive'); ?>",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {

                    var url = "<?php echo e(route('admin.client-docs.destroy',':id')); ?>";
                    url = url.replace(':id', id);

                    var token = "<?php echo e(csrf_token()); ?>";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE', 'view': deleteView},
                        success: function (response) {
                            console.log(response);
                            if (response.status == "success") {
                                $.unblockUI();
                                $('#employeeDocsList').html(response.html);
                            }
                        }
                    });
                }
            });
        });

        $('ul.showClientTabs .clientDocs').addClass('tab-current');
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/clients/docs.blade.php ENDPATH**/ ?>