<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.clients.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.menu.invoices'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>

    <div class="row">


        <?php echo $__env->make('admin.clients.client_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


        <div class="col-xs-12">

            <section>
                <div class="sttabs tabs-style-line">
                    <?php echo $__env->make('admin.clients.tabs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                    <div class="content-wrap">
                        <section id="section-line-1" class="show">
                            <div class="row">


                                <div class="col-xs-12" >
                                    <div class="white-box">

                                        <ul class="list-group" id="invoices-list">
                                            <li class="list-group-item">
                                                <div class="row font-semi-bold">
                                                    <div class="col-md-3">
                                                        <?php echo e(__('app.project')); ?>

                                                    </div>
                                                    <div class="col-md-3">
                                                        <?php echo e(__('app.invoice') . '#'); ?>

                                                    </div>
                                                    <div class="col-md-3">
                                                        <?php echo e(__('modules.invoices.amount')); ?>

                                                    </div>
                                                    <div class="col-md-3">
                                                        <?php echo e(__('modules.payments.paidOn')); ?>

                                                    </div>
                                                </div>
                                            </li>
                                            <?php $__empty_1 = true; $__currentLoopData = $payments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                <li class="list-group-item">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <?php if(!is_null($payment->project)): ?>
                                                                <a href="<?php echo e(route('admin.projects.show', $payment->project_id)); ?>"><?php echo e(ucfirst($payment->project->project_name)); ?></a>
                                                            <?php else: ?>
                                                                --
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <?php if(!is_null($payment->invoice)): ?>
                                                                <a href="<?php echo e(route('admin.all-invoices.show', $payment->invoice_id)); ?>"><?php echo e(ucfirst($payment->invoice->invoice_number)); ?></a>
                                                            <?php else: ?>
                                                                --
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <?php echo e(currency_formatter($payment->amount,'') .' ('.$payment->currency->currency_code . ')'); ?>

                                                            
                                                        </div>
                                                        
                                                        <div class="col-md-3">
                                                            <?php echo e($payment->paid_on->format($global->date_format . ' ' . $global->time_format)); ?>

                                                        </div>
                                                        
                                                    </div>
                                                </li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                <li class="list-group-item">
                                                    <div class="row">
                                                        <div class="col-md-12 text-center">
                                                            <div class="empty-space" style="height: 200px;">
                                                                        <div class="empty-space-inner">
                                                                            <div class="icon" style="font-size:30px"><i
                                                                                        class="icon-doc"></i>
                                                                            </div>
                                                                            <div class="title m-b-15"><?php echo app('translator')->get('messages.noPaymentFound'); ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                    </div>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                        </section>
                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>


    </div>
    <!-- .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
    <script>
        $('ul.showClientTabs .clientPayments').addClass('tab-current');
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/clients/payments.blade.php ENDPATH**/ ?>