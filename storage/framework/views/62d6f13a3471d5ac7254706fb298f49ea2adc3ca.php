<div class="white-box">
    <nav>
        <ul class="showProjectTabs">
            <li class="projects"><a href="<?php echo e(route('member.projects.show', $project->id)); ?>"><i class="icon-grid"></i> <span><?php echo app('translator')->get('modules.projects.overview'); ?></span></a>
            </li>

            <?php if(in_array('employees',$modules)): ?>
            <li class="projectMembers"><a href="<?php echo e(route('member.project-members.show', $project->id)); ?>"><i class="icon-people"></i> <span><?php echo app('translator')->get('modules.projects.members'); ?></span></a></li>
            <?php endif; ?>

            <?php if($user->cans('view_projects')): ?>
                <li class="projectMilestones">
                    <a href="<?php echo e(route('member.milestones.show', $project->id)); ?>"><i class="icon-flag"></i> <span><?php echo app('translator')->get('modules.projects.milestones'); ?></span></a>
                </li>
            <?php endif; ?>

            <?php if(in_array('tasks',$modules)): ?>
            <li class="projectTasks"><a href="<?php echo e(route('member.tasks.show', $project->id)); ?>"><i class="ti-check-box"></i> <span><?php echo app('translator')->get('app.menu.tasks'); ?></span></a></li>
            <?php endif; ?>

            <li class="projectFiles"><a href="<?php echo e(route('member.files.show', $project->id)); ?>"><i class="ti-files"></i> <span><?php echo app('translator')->get('modules.projects.files'); ?></span></a></li>

            <?php if(in_array('timelogs',$modules)): ?>
            <li class="projectTimelogs"><a href="<?php echo e(route('member.time-log.show-log', $project->id)); ?>"><i class="ti-alarm-clock"></i> <span><?php echo app('translator')->get('app.menu.timeLogs'); ?></span></a></li>
            <?php endif; ?>

            <li class="discussion">
                <a href="<?php echo e(route('member.projects.discussion', $project->id)); ?>"><i class="ti-comments"></i>
                    <span><?php echo app('translator')->get('modules.projects.discussion'); ?></span></a>
            </li>
            
            <li class="gantt">
                <a href="<?php echo e(route('member.projects.gantt', [$project->id])); ?>"><i class="fa fa-bar-chart"></i>
                    <span><?php echo app('translator')->get('modules.projects.viewGanttChart'); ?></span></a>
            </li>
            <?php if($project->visible_rating_employee): ?>
                <li class="projectRatings">
                    <a href="<?php echo e(route('member.project-ratings.show', $project->id)); ?>">
                        <i class="fa fa-star" aria-hidden="true"></i> <span><?php echo app('translator')->get('app.rating'); ?></span>
                    </a>
                </li>
            <?php endif; ?>
            <li class="notes">
                <a href="<?php echo e(route('member.project-notes.show', $project->id)); ?>"><i class="fa fa-sticky-note-o"></i>
                    <span><?php echo app('translator')->get('modules.projects.notes'); ?></span></a>
            </li> 
        </ul>
    </nav>
</div><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/member/projects/show_project_menu.blade.php ENDPATH**/ ?>