<div class="rpanel-title"> <?php echo app('translator')->get('app.company'); ?> <?php echo app('translator')->get('app.details'); ?>
    <span><i class="ti-close right-side-toggle"></i></span> 
</div>
<div class="r-panel-body p-t-0 h-scroll">

    <div class="row">
        <div class="col-xs-12 col-md-7 p-t-20 b-r ">
            <div class="row">
                <div class="col-xs-6 m-b-20">
                    <img src="<?php echo e($companyDetails->logo_url); ?>" alt="logo" height="30">
                </div>
                <div class="col-xs-6 m-b-20">
                    <a href="<?php echo e(route('super-admin.companies.edit',$companyDetails->id)); ?>" class="btn btn-outline btn-success btn-sm">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                        <?php echo app('translator')->get('app.edit'); ?>
                        <?php echo app('translator')->get('app.company'); ?> </a>
                    
                </div>

                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-6 b-r"> <span
                                class="font-semi-bold"><?php echo app('translator')->get('modules.accountSettings.companyName'); ?></span> <br>
                            <p class="text-muted"><?php echo e(ucwords($companyDetails->company_name)); ?></p>
                        </div>
                        <div class="col-xs-6"> <span
                                class="font-semi-bold"><?php echo app('translator')->get('modules.accountSettings.companyEmail'); ?></span> <br>
                            <p class="text-muted"><?php echo e($companyDetails->company_email); ?></p>
                        </div>
                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-xs-6 b-r"> <span
                                class="font-semi-bold"><?php echo app('translator')->get('modules.accountSettings.companyPhone'); ?></span> <br>
                            <p class="text-muted"><?php echo e(ucwords($companyDetails->company_phone)); ?></p>
                        </div>
                        <div class="col-xs-6"> <span
                                class="font-semi-bold"><?php echo app('translator')->get('modules.accountSettings.companyWebsite'); ?></span> <br>
                            <p class="text-muted"><?php echo e($companyDetails->website ?? "--"); ?></p>
                        </div>
                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-xs-12"> <span
                                class="font-semi-bold"><?php echo app('translator')->get('modules.accountSettings.companyAddress'); ?></span> <br>
                            <p class="text-muted"><?php echo $companyDetails->address; ?></p>
                        </div>
                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-xs-6 b-r"> <span
                                class="font-semi-bold"><?php echo app('translator')->get('modules.accountSettings.defaultCurrency'); ?></span> <br>
                            <p class="text-muted">
                                <?php echo e($companyDetails->currency->currency_symbol . ' (' .$companyDetails->currency->currency_code .')'); ?>

                            </p>
                        </div>
                        <div class="col-xs-6"> <span
                                class="font-semi-bold"><?php echo app('translator')->get('modules.accountSettings.defaultTimezone'); ?></span> <br>
                            <p class="text-muted"><?php echo e($companyDetails->timezone); ?></p>
                        </div>
                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-xs-6 b-r"> <span class="font-semi-bold"><?php echo app('translator')->get('app.status'); ?></span> <br>
                            <p class="text-muted">
                                <?php if($companyDetails->status == 'active'): ?>
                                <label class="label label-success"><?php echo app('translator')->get('app.active'); ?></label>
                                <?php else: ?>
                                <label class="label label-success"><?php echo app('translator')->get('app.active'); ?></label>
                                <?php endif; ?>
                            </p>
                        </div>
                        <div class="col-xs-6"> <span class="font-semi-bold"><?php echo app('translator')->get('app.lastActivity'); ?></span> <br>
                            <p class="text-muted">
                                <?php echo e(($companyDetails->last_login) ? $companyDetails->last_login->diffForHumans() : '--'); ?>

                            </p>
                        </div>
                    </div>
                    <hr>
                    <?php if(isset($fields) && count($fields) > 0): ?>
                        <h4 class="box-title m-t-20"><?php echo app('translator')->get('modules.projects.otherInfo'); ?></h4>
                        <div class="row">
                            <?php $__currentLoopData = $fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-md-3 b-r">
                                    <span class="font-semi-bold"><?php echo e(ucfirst($field->label)); ?></span> <br>
                                    <p class="text-muted">
                                        <?php if( $field->type == 'text'): ?>
                                            <?php echo e($companyDetails->custom_fields_data['field_'.$field->id] ?? '-'); ?>

                                        <?php elseif($field->type == 'password'): ?>
                                            <?php echo e($companyDetails->custom_fields_data['field_'.$field->id] ?? '-'); ?>

                                        <?php elseif($field->type == 'number'): ?>
                                            <?php echo e($companyDetails->custom_fields_data['field_'.$field->id] ?? '-'); ?>


                                        <?php elseif($field->type == 'textarea'): ?>
                                            <?php echo e($companyDetails->custom_fields_data['field_'.$field->id] ?? '-'); ?>


                                        <?php elseif($field->type == 'radio'): ?>
                                            <?php echo e(!is_null($companyDetails->custom_fields_data['field_'.$field->id]) ? $companyDetails->custom_fields_data['field_'.$field->id] : '-'); ?>

                                        <?php elseif($field->type == 'select'): ?>
                                            <?php echo e((!is_null($companyDetails->custom_fields_data['field_'.$field->id]) && $companyDetails->custom_fields_data['field_'.$field->id] != '') ? $field->values[$companyDetails->custom_fields_data['field_'.$field->id]] : '-'); ?>

                                        <?php elseif($field->type == 'checkbox'): ?>
                                            <?php echo e(!is_null($companyDetails->custom_fields_data['field_'.$field->id]) ? $field->values[$companyDetails->custom_fields_data['field_'.$field->id]] : '-'); ?>

                                        <?php elseif($field->type == 'date'): ?>
                                            <?php echo e(!is_null($companyDetails->custom_fields_data['field_'.$field->id]) ? \Carbon\Carbon::parse($companyDetails->custom_fields_data['field_'.$field->id])->format($global->date_format) : '--'); ?>

                                        <?php endif; ?>
                                    </p>

                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    <?php endif; ?>
                    <hr>

                </div>
            </div>


        </div>

        <div class="col-xs-12 col-md-5 p-t-20 ">

            <h4><?php echo app('translator')->get('app.package'); ?> <?php echo app('translator')->get('app.details'); ?></h4>
            <table class="table">
                <tr>
                    <td class="font-semi-bold"><?php echo app('translator')->get('app.package'); ?> <?php echo app('translator')->get('app.name'); ?></td>
                    <td><?php echo e($companyDetails->package->name); ?></td>
                </tr>
                <tr>
                    <td class="font-semi-bold"><?php echo app('translator')->get('app.menu.employees'); ?> <?php echo app('translator')->get('app.quota'); ?></td>
                    <td><?php echo e($companyDetails->employees->count()); ?> / <?php echo e($companyDetails->package->max_employees); ?></td>
                </tr>
                <tr>
                    <td class="font-semi-bold"><?php echo app('translator')->get('app.menu.storage'); ?></td>
                    <td>
                        <?php if($companyDetails->package->storage_unit == 'mb'): ?>
                        <?php echo e($companyDetails->file_storage->count() > 0 ? round($companyDetails->file_storage->sum('size')/(1000*1024), 4). ' MB' : 'Not used'); ?>

                        <?php else: ?>
                        <?php echo e($companyDetails->file_storage->count() > 0 ? round($companyDetails->file_storage->sum('size')/(1000*1024*1024), 4). ' MB' : 'Not Used'); ?>

                        <?php endif; ?>
                        /
                        <?php if($companyDetails->package->max_storage_size == -1): ?>
                        <?php echo app('translator')->get('app.unlimited'); ?>
                        <?php else: ?>
                        <?php echo e($companyDetails->package->max_storage_size); ?>

                        (<?php echo e(strtoupper($companyDetails->package->storage_unit)); ?>)
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td class="font-semi-bold"><?php echo app('translator')->get('app.price'); ?></td>
                    <td>
                        <p>
                            <?php echo app('translator')->get('app.monthly'); ?> <?php echo app('translator')->get('app.price'); ?>:
                            <?php echo e($companyDetails->package->currency->currency_symbol . $companyDetails->package->monthly_price); ?>

                        </p>
                        <p>
                            <?php echo app('translator')->get('app.annual'); ?> <?php echo app('translator')->get('app.price'); ?>:
                            <?php echo e($companyDetails->package->currency->currency_symbol . $companyDetails->package->annual_price); ?>

                        </p>
                    </td>
                </tr>
                <tr>
                    <td class="font-semi-bold"><?php echo app('translator')->get('modules.package.licenseExpiresOn'); ?></td>
                    <td><?php echo e(($companyDetails->licence_expire_on) ? $companyDetails->licence_expire_on->toFormattedDateString() . ' ('. $companyDetails->licence_expire_on->diffForHumans() .')' : '--'); ?>

                    </td>
                </tr>
            </table>

            <a href="javascript:;" class="btn btn-info btn-outline package-update-button"
                data-company-id="<?php echo e($companyDetails->id); ?>"><i class="fa fa-pencil"></i> <?php echo app('translator')->get('app.change'); ?>
                <?php echo app('translator')->get('app.package'); ?></a>

            <a href="javascript:;" class="btn btn-success btn-outline" data-company-id="<?php echo e($companyDetails->id); ?>"
                id="login-as-company"><i class="fa fa-sign-in"></i> <?php echo app('translator')->get('modules.superadmin.loginAsCompany'); ?></a>


        </div>



    </div>

</div><?php /**PATH D:\WAMP-SERVER\www\eOsuite\resources\views/super-admin/companies/show.blade.php ENDPATH**/ ?>