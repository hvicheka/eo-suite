<div class="white-box">
    <nav>
        <ul class="showClientTabs">
            <li class="clientProfile"><a href="<?php echo e(route('admin.clients.show', $client->id)); ?>"><i class="icon-user"></i> <span><?php echo app('translator')->get('modules.employees.profile'); ?></span></a>
            </li>
            <li class="clientProjects"><a href="<?php echo e(route('admin.clients.projects', $client->id)); ?>"><i class="icon-layers"></i> <span><?php echo app('translator')->get('app.menu.projects'); ?></span></a>
            </li>
            <li class="clientInvoices"><a href="<?php echo e(route('admin.clients.invoices', $client->id)); ?>"><i class="icon-doc"></i> <span><?php echo app('translator')->get('app.menu.invoices'); ?></span></a>
            </li>
            <li class="clientContacts"><a href="<?php echo e(route('admin.contacts.show', $client->id)); ?>"><i class="icon-people"></i> <span><?php echo app('translator')->get('app.menu.contacts'); ?></span></a>
            </li>
            <li class="clientPayments"><a href="<?php echo e(route('admin.clients.payments', $client->id)); ?>"><i class="ti-receipt"></i> <span><?php echo app('translator')->get('app.menu.payments'); ?></span></a>
            </li>
            <li class="clientNotes"><a href="<?php echo e(route('admin.notes.show', $client->id)); ?>"><i class="fa fa-sticky-note-o"></i> <span><?php echo app('translator')->get('app.menu.notes'); ?></span></a>
            <li class="clientDocs"><a href="<?php echo e(route('admin.client-docs.show', $client->id)); ?>"><i class="icon-docs"></i> <span><?php echo app('translator')->get('app.menu.documents'); ?></span></a>
            </li>
            <?php if($gdpr->enable_gdpr): ?>
            <li class="clientGdpr"><a href="<?php echo e(route('admin.clients.gdpr', $client->id)); ?>"><i class="icon-lock"></i> <span><?php echo app('translator')->get('modules.gdpr.gdpr'); ?></span></a>
            </li>
            <?php endif; ?>
        </ul>
    </nav>
</div><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/clients/tabs.blade.php ENDPATH**/ ?>