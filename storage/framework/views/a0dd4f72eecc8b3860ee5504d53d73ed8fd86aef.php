<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.invoice-recurring.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.update'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/select2/select2.min.css')); ?>">

<style>
    .dropdown-content {
        width: 250px;
        max-height: 250px;
        overflow-y: scroll;
        overflow-x: hidden;
    }
     .recurringPayment {
         display: none;
     }
    .label-font{
        font-weight: 500 !important;
    }
    #product-box .select2-results__option--highlighted[aria-selected] {
        background-color: #ffffff !important;
        color: #000000 !important;
    }
    #product-box .select2-results__option[aria-selected=true] {
        background-color: #ffffff !important;
        color: #000000 !important;
    }
    #product-box .select2-results__option[aria-selected] {
        cursor:default !important;
    }
    #selectProduct {
        width: 200px !important;
    }
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> <?php echo app('translator')->get('app.update'); ?> <?php echo app('translator')->get('app.invoiceRecurring'); ?></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <?php echo Form::open(['id'=>'updatePayments','class'=>'ajax-form','method'=>'PUT']); ?>

                        <div class="form-body">

                            <div class="row">
                                <?php if(in_array('projects', $modules)): ?>
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('app.project'); ?></label>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <select class="select2 form-control" onchange="getCompanyName()" data-placeholder="Choose Project"
                                                            name="project_id" id="project_id">
                                                        <option value="">--</option>
                                                        <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option
                                                                    <?php if($invoice->project_id == $project->id): ?> selected
                                                                    <?php endif; ?>
                                                                    value="<?php echo e($project->id); ?>"><?php echo e(ucwords($project->project_name)); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                <?php endif; ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" id="companyClientName"> <?php if($invoice->project_id == ''): ?> <?php echo app('translator')->get('app.client_name'); ?> <?php else: ?> <?php echo app('translator')->get('app.company_name'); ?> <?php endif; ?></label>
                                        <div class="row">
                                            <div class="col-xs-12" id="client_company_div">
                                                <?php if($invoice->project_id == ''): ?>
                                                    <select class="form-control select2" name="client_id" id="client_id" data-style="form-control">
                                                        <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($client->id); ?>" <?php if($client->id == $invoice->client_id): ?> selected <?php endif; ?>><?php echo e(ucwords($client->name)); ?>

                                                                <?php if($client->company_name != ''): ?> <?php echo e('('.$client->company_name.')'); ?> <?php endif; ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                <?php else: ?>
                                                    <div class="input-icon">
                                                        <input type="text" readonly class="form-control" name="" id="company_name" value="<?php echo e($companyName); ?>">
                                                    </div>
                                                <?php endif; ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('modules.invoices.currency'); ?></label>
                                            <select class="form-control" name="currency_id" id="currency_id">
                                                <?php $__currentLoopData = $currencies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $currency): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option
                                                            <?php if($invoice->currency_id == $currency->id): ?> selected
                                                            <?php endif; ?>
                                                            value="<?php echo e($currency->id); ?>"><?php echo e($currency->currency_symbol.' ('.$currency->currency_code.')'); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>

                                    </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.invoices.invoiceDate'); ?></label>

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="input-icon">
                                                    <input type="text" class="form-control" name="issue_date"
                                                           id="invoice_date"
                                                           value="<?php echo e($invoice->issue_date->format($global->date_format)); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('app.dueDate'); ?></label>

                                        <div class="input-icon">
                                            <input type="text" class="form-control" name="due_date" id="due_date"
                                                   value="<?php echo e($invoice->due_date->format($global->date_format)); ?>">
                                        </div>
                                    </div>

                                </div>



                            </div>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.invoices.billingFrequency'); ?></label>
                                        <select class="form-control" onchange="changeRotation(this.value);" name="rotation" id="rotation">
                                            <option value="daily"><?php echo app('translator')->get('app.daily'); ?></option>
                                            <option value="weekly"><?php echo app('translator')->get('app.weekly'); ?></option>
                                            <option value="bi-weekly"><?php echo app('translator')->get('app.bi-weekly'); ?></option>
                                            <option value="monthly"><?php echo app('translator')->get('app.monthly'); ?></option>
                                            <option value="quarterly"><?php echo app('translator')->get('app.quarterly'); ?></option>
                                            <option value="half-yearly"><?php echo app('translator')->get('app.half-yearly'); ?></option>
                                            <option value="annually"><?php echo app('translator')->get('app.annually'); ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 dayOfWeek">
                                    <div class="form-group">
                                        <label class="control-label required"><?php echo app('translator')->get('modules.expensesRecurring.dayOfWeek'); ?> </label>
                                        <select class="form-control"  name="day_of_week" id="dayOfWeek">
                                            <option value="1"><?php echo app('translator')->get('app.sunday'); ?></option>
                                            <option value="2"><?php echo app('translator')->get('app.monday'); ?></option>
                                            <option value="3"><?php echo app('translator')->get('app.tuesday'); ?></option>
                                            <option value="4"><?php echo app('translator')->get('app.wednesday'); ?></option>
                                            <option value="5"><?php echo app('translator')->get('app.thursday'); ?></option>
                                            <option value="6"><?php echo app('translator')->get('app.friday'); ?></option>
                                            <option value="7"><?php echo app('translator')->get('app.saturday'); ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 dayOfMonth">
                                    <div class="form-group">
                                        <label class="control-label required"><?php echo app('translator')->get('modules.expensesRecurring.dayOfMonth'); ?></label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select class="form-control"  name="day_of_month" id="dayOfMonth">
                                                    <?php for($m=1; $m<=31; ++$m): ?>
                                                        <option value="<?php echo e($m); ?>"><?php echo e($m); ?></option>
                                                    <?php endfor; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 billingInterval">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.invoices.billingCycle'); ?> </label>
                                        <div class="input-icon">
                                            <input type="number" class="form-control" name="billing_cycle" id="billing_cycle"
                                                   <?php if($invoice->unlimited_recurring == 1): ?>
                                                   value="-1"
                                                   <?php else: ?>
                                                   value="<?php echo e($invoice->billing_cycle); ?>"
                                                    <?php endif; ?>
                                            >
                                        </div>
                                        <p class="text-bold"><?php echo app('translator')->get('messages.setForInfinite'); ?></p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="usd_price"><?php echo app('translator')->get('app.status'); ?> </label>
                                        <select class="form-control" name="status">
                                            <option <?php if($invoice->status == 'active'): ?> selected <?php endif; ?> value="active"><?php echo app('translator')->get('app.active'); ?></option>
                                            <option  <?php if($invoice->status == 'inactive'): ?> selected <?php endif; ?>  value="inactive"><?php echo app('translator')->get('app.inactive'); ?></option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.invoices.showShippingAddress'); ?>
                                            <a class="mytooltip" href="javascript:void(0)">
                                                <i class="fa fa-info-circle"></i>
                                                <span class="tooltip-content5">
                                                        <span class="tooltip-text3">
                                                            <span class="tooltip-inner2">
                                                                <?php echo app('translator')->get('modules.invoices.showShippingAddressInfo'); ?>
                                                            </span>
                                                        </span>
                                                    </span>
                                            </a>
                                        </label>
                                        <div class="switchery-demo">
                                            <input type="checkbox" id="show_shipping_address" name="show_shipping_address"
                                                   <?php if($global->show_shipping_address == 'yes'): ?> checked
                                                   <?php endif; ?> class="js-switch " data-color="#00c292"
                                                   data-secondary-color="#f96262"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div id="shippingAddress">

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group" style="padding-top: 25px;">
                                        <div class="checkbox checkbox-info">
                                            <input id="client_can_stop" name="client_can_stop" <?php if($invoice->client_can_stop == 1): ?> checked <?php endif; ?> value="true"
                                                   type="checkbox">
                                            <label for="client_can_stop" class="control-label"><?php echo app('translator')->get('modules.recurringInvoice.allowToClient'); ?></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>

                            

                            <div class="col-xs-12">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.quotation.product'); ?></label>
                                        <select class="form-control select2" name="product_id" id="product_id">
                                            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="form-group  m-t-5">
                                    <button type="button" class="btn btn-info btn-sm" id="add-item"><i class="fa fa-plus"></i>
                                        <?php echo app('translator')->get('modules.invoices.addItem'); ?>
                                    </button>
                                </div>
                            </div>


                            <div class="row">

                                <div class="col-xs-12  visible-md visible-lg">

                                    <div class="<?php if($invoiceSetting->hsn_sac_code_show): ?> col-md-3 <?php else: ?> col-md-4 <?php endif; ?> font-bold" style="padding: 8px 15px">
                                        <?php echo app('translator')->get('modules.invoices.item'); ?>
                                    </div>
                                    
                                    <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                        <?php echo app('translator')->get('modules.invoices.qty'); ?>
                                    </div>

                                    <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                        <?php echo app('translator')->get('modules.invoices.unitPrice'); ?>
                                    </div>

                                    <div class="col-md-3 font-bold" style="padding: 8px 15px">
                                        Discount
                                    </div>

                                    

                                    <div class="col-md-2 text-center font-bold" style="padding: 8px 15px">
                                        <?php echo app('translator')->get('modules.invoices.amount'); ?>
                                    </div>

                                    <div class="col-md-1" style="padding: 8px 15px">
                                        &nbsp;
                                    </div>

                                </div>

                                <div id="sortable">
                                    <?php $__currentLoopData = $invoice->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-xs-12 item-row margin-top-5" id="sortable_item" data-id="<?php echo e($item->item_id); ?>">
                                            <div class="col-md-3">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.item'); ?></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>
                                                            <input type="text" class="form-control item_name" name="item_name[]" value="<?php echo e($item->item_name); ?>" readonly>
                                                            <input type="hidden" class="form-control item_id" name="item_id[]" value="<?php echo e($item->item_id); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <textarea name="item_summary[]" class="form-control item_summary" placeholder="<?php echo app('translator')->get('app.description'); ?>" rows="2"><?php echo e($item->item_summary); ?></textarea>
                                                    </div>
                                                </div>

                                            </div>
                                            
                                            <div class="col-md-1">

                                                <div class="form-group">
                                                    <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.qty'); ?></label>
                                                    <input type="number" min="1" class="form-control quantity" value="<?php echo e($item->quantity); ?>" name="quantity[]">
                                                </div>


                                            </div>

                                            <div class="col-md-1">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.unitPrice'); ?></label>
                                                        <input type="text" min="" class="form-control cost_per_item" name="cost_per_item[]" value="<?php echo e($item->unit_price); ?>" readonly>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <select class="form-control line_discount_type" name="line_discount_type[]">
                                                        <option value="percent" <?php echo e($item->discount_type=='percent'?'selected':''); ?>>%</option>
                                                        <option value="fixed" <?php echo e($item->discount_type=='fixed'?'selected':''); ?>><?php echo app('translator')->get('modules.invoices.amount'); ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-1" >
                                                <div class="form-group">
                                                    <input type="number" min="0" value="<?php echo e($item->discount); ?>" class="form-control line_discount_value" name="line_discount_value[]">
                                                </div>
                                            </div>

                                            

                                            <div class="col-md-2 border-dark  text-center">
                                                <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.amount'); ?></label>
                                                <p class="form-control-static"><span
                                                            class="amount-html"><?php echo e(number_format((float)$item->amount, 2, '.', '')); ?></span></p>
                                                <input type="hidden" value="<?php echo e($item->amount); ?>" class="amount"
                                                       name="amount[]">
                                            </div>

                                            <div class="col-md-1 text-right visible-md visible-lg">
                                                <button type="button" class="btn remove-item btn-circle btn-danger"><i
                                                            class="fa fa-remove"></i></button>
                                            </div>
                                            <div class="col-md-1 hidden-md hidden-lg">
                                                <div class="row">
                                                    <button type="button" class="btn btn-circle remove-item btn-danger"><i
                                                                class="fa fa-remove"></i> <?php echo app('translator')->get('app.remove'); ?>
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>

                                

                                <div class="col-xs-12 ">


                                    <div class="row">
                                        <div class="col-md-offset-9 col-xs-6 col-md-1 text-right p-t-10"><?php echo app('translator')->get('modules.invoices.subTotal'); ?></div>

                                        <p class="form-control-static col-xs-6 col-md-2">
                                            <span class="sub-total"><?php echo e(number_format((float)$invoice->sub_total, 2, '.', '')); ?></span>
                                        </p>


                                        <input type="hidden" class="sub-total-field" name="sub_total" value="<?php echo e($invoice->sub_total); ?>">
                                    </div>

                                    <div class="row">
                                        <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                            <?php echo app('translator')->get('modules.invoices.discount'); ?>
                                        </div>
                                        <div class="form-group col-xs-6 col-md-1" >
                                            <input type="number" min="0" value="<?php echo e($invoice->discount); ?>" name="discount_value" class="form-control discount_value" >
                                        </div>
                                        <div class="form-group col-xs-6 col-md-1" >
                                            <select class="form-control" name="discount_type" id="discount_type">
                                                <option
                                                        <?php if($invoice->discount_type == 'percent'): ?> selected <?php endif; ?>
                                                        value="percent">%</option>
                                                <option
                                                        <?php if($invoice->discount_type == 'fixed'): ?> selected <?php endif; ?>
                                                value="fixed"><?php echo app('translator')->get('modules.invoices.amount'); ?></option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row m-t-5" id="invoice-taxes">
                                        <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                            <?php echo app('translator')->get('modules.invoices.tax'); ?>
                                        </div>

                                        <p class="form-control-static col-xs-6 col-md-2" >
                                            <span class="tax-percent">0</span>
                                        </p>
                                    </div>

                                    <div class="row m-t-5 font-bold">
                                        <div class="col-md-offset-9 col-md-1 col-xs-6 text-right p-t-10"><?php echo app('translator')->get('modules.invoices.total'); ?></div>

                                        <p class="form-control-static col-xs-6 col-md-2">
                                            <span class="total"><?php echo e(number_format((float)$invoice->total, 2, '.', '')); ?></span>
                                        </p>


                                        <input type="hidden" class="total-field" name="total"
                                               value="<?php echo e(round($invoice->total, 2)); ?>">
                                    </div>

                                </div>

                            </div>

                            <div class="col-xs-12">

                                <div class="form-group" >
                                    <label class="control-label"><?php echo app('translator')->get('app.note'); ?></label>
                                    <textarea class="form-control" name="note" id="note" rows="5"><?php echo e($invoice->note); ?></textarea>
                                </div>

                            </div>


                        </div>
                        <div class="form-actions" style="margin-top: 70px">
                            <div class="row">
                                <div class="col-xs-12">
                                    <button type="button" id="save-form" class="btn btn-success"><i
                                                class="fa fa-check"></i> <?php echo app('translator')->get('app.save'); ?>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

    
    <div class="modal fade bs-modal-md in" id="taxModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    <?php echo app('translator')->get('app.loading'); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal"><?php echo app('translator')->get('app.close'); ?></button>
                    <button type="button" class="btn blue"><?php echo app('translator')->get('app.save'); ?> <?php echo app('translator')->get('changes'); ?></button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    
<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>

<script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/select2/select2.min.js')); ?>"></script>

<script>
    var invoice = <?php echo json_encode($invoice, 15, 512) ?>;

    $('#rotation').val(invoice['rotation']);
    $('#dayOfWeek').val(invoice['day_of_week']);
    $('#dayOfMonth').val(invoice['day_of_month']);
    //    changeRotation(expense.rotation);

    $(function () {
        changeRotation(invoice.rotation);
    });

    $(document).ready(function(){
        var products = <?php echo json_encode($products); ?>

        var  selectedID = '';
        $("#selectProduct").select2({
            data: products,
            placeholder: "Select a Product",
            allowClear: true,
            escapeMarkup: function(markup) {
                return markup;
            },
            templateResult: function(data) {
                var htmlData = '<b>'+data.title+'</b> <a href="javascript:;" class="btn btn-success btn btn-outline btn-xs waves-effect pull-right"><?php echo app('translator')->get('app.add'); ?> <i class="fa fa-plus" aria-hidden="true"></i></a>';
                return htmlData;
            },
            templateSelection: function(data) {
                $('#select2-selectProduct-container').html('<?php echo app('translator')->get('app.add'); ?> <?php echo app('translator')->get('app.menu.products'); ?>');
                $("#selectProduct").val('');
                selectedID = data.id;
                return '';
            },
        }).on('change', function (e) {
            if(selectedID){
                addProduct(selectedID);
                $('#select2-selectProduct-container').html('<?php echo app('translator')->get('app.add'); ?> <?php echo app('translator')->get('app.menu.products'); ?>');
            }
            selectedID = '';
        }).on('select2:open', function (event) {
            $('span.select2-container--open').attr('id', 'product-box');
        });

    });

    function changeRotation (rotationValue){
        if(rotationValue == 'weekly' || rotationValue == 'bi-weekly'){
            $('.dayOfWeek').show().fadeIn(300);
            $('.dayOfMonth').hide().fadeOut(300);
        }
        else if(rotationValue == 'monthly' || rotationValue == 'quarterly' || rotationValue == 'half-yearly' || rotationValue == 'annually'){
            $('.dayOfWeek').hide().fadeOut(300);
            $('.dayOfMonth').show().fadeIn(300);
        }
        else{
            $('.dayOfWeek').hide().fadeOut(300);
            $('.dayOfMonth').hide().fadeOut(300);
        }
    }
    $('#infinite-expenses').change(function () {
        if($(this).is(':checked')){
            $('.billingInterval').hide();
        }
        else{
            $('.billingInterval').show();
        }
    });
    // Switchery
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    $('.js-switch').each(function () {
        new Switchery($(this)[0], $(this).data());
    });

    var showShippingSwitch = document.getElementById('show_shipping_address');

    <?php if($invoice->show_shipping_address === 'yes'): ?>
        showShippingSwitch.click();
    <?php elseif(!is_null($invoice->project_id)): ?>
        getCompanyName();
    <?php endif; ?>

    showShippingSwitch.onchange = function() {
        if (showShippingSwitch.checked) {
            checkShippingAddress();
        }
        else {
            $('#shippingAddress').html('');
        }
    }

    function getCompanyName(){
        var projectID = $('#project_id').val();
        var url = "<?php echo e(route('admin.all-invoices.get-client-company')); ?>";
        if(projectID != ''  && projectID !== undefined )
        {
            url = "<?php echo e(route('admin.all-invoices.get-client-company',':id')); ?>";
            url = url.replace(':id', projectID);
        }

        $.ajax({
            type: 'GET',
            url: url,
            success: function (data) {
                if(projectID != '')
                {
                    $('#companyClientName').text('<?php echo e(__('app.company_name')); ?>');
                } else {
                    $('#companyClientName').text('<?php echo e(__('app.client_name')); ?>');
                }
                $('#client_company_div').html(data.html);
                if ($('#show_shipping_address').prop('checked') === true) {
                    checkShippingAddress();
                }
                <?php if($invoice->project_id == ''): ?>
                    $('#client_id').val('<?php echo e($invoice->client_id); ?>').trigger('change');
                //        $('#client_id').select2();
                <?php endif; ?>
            }
        });
    }

    function checkShippingAddress() {
        var projectId = $('#project_id').val();
        var clientId = $('#client_company_id').length > 0 ? $('#client_company_id').val() : $('#client_id').val();
        var showShipping = $('#show_shipping_address').prop('checked') === true ? 'yes' : 'no';

        var url = `<?php echo e(route('admin.all-invoices.checkShippingAddress')); ?>?showShipping=${showShipping}`;
        if (clientId !== '') {
            url += `&clientId=${clientId}`;
        }

        $.ajax({
            type: 'GET',
            url: url,
            success: function (response) {
                if (response) {
                    if (response.switch === 'off') {
                        showShippingSwitch.click();
                    }
                    else {
                        if (response.show !== undefined) {
                            $('#shippingAddress').html('');
                        } else {
                            $('#shippingAddress').html(response.view);
                        }
                    }
                }
            }
        });
    }

    $(function () {
        recurringPayment();
        $( "#sortable" ).sortable();
    });

    $(".select2").select2({
        formatNoMatches: function () {
            return "<?php echo e(__('messages.noRecordFound')); ?>";
        }
    });

    jQuery('#invoice_date, #due_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'<?php echo e($global->week_start); ?>',
        format: '<?php echo e($global->date_picker_format); ?>',
    });

    $('#save-form').click(function () {

        var discount = $('.discount-amount').html();
        var total = $('.total-field').val();

        if(parseFloat(discount) > parseFloat(total)){
            $.toast({
                heading: 'Error',
                text: 'Discount cannot be more than total amount.',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
            return false;
        }

        $.easyAjax({
            url: '<?php echo e(route('admin.invoice-recurring.update', $invoice->id)); ?>',
            container: '#updatePayments',
            type: "POST",
            redirect: true,
            data: $('#updatePayments').serialize()
        })
    });
    $('#product_id').change(function () {
            var value = $(this).val();
            var products = <?php echo collect($products); ?>;

            if($('div#sortable_item:last').attr('data-id') == value){

                // $('div#sortable_item').attr('data-id',value);
                // alert(1);
              
            }else{
                $('div#sortable_item:last').attr('data-id',value);
               

            };
            $.each(products, (i, v) => {
                if(v.id == value){
                    $(`div#sortable_item[data-id="${value}"]:last`).find('.item_name').val(`${v.name}`);
                    $(`div#sortable_item[data-id="${value}"]:last`).find('.item_summary').val(`${v.description}`);
                    $(`div#sortable_item[data-id="${value}"]:last`).find('.item_id').val(`${v.id}`);
                    $(`div#sortable_item[data-id="${value}"]:last`).find('.cost_per_item').val(`${v.price}`);

                    var quantity = 1;

                    var perItemCost = v.price;

                    var amount = (quantity*perItemCost);

                    $(`div#sortable_item[data-id="${value}"]:last`).find('.amount').val(decimalupto2(amount));
                    $(`div#sortable_item[data-id="${value}"]:last`).find('.amount-html').html(decimalupto2(amount));
                    
                    calculateTotal();

                }
            });

            
    })
    $('#add-item').click(function () {
        var i = $(document).find('.item_name').length;
        var item = '<div class="col-xs-12 item-row margin-top-5" id="sortable_item">'

            +'<div class="col-md-3">'
            +'<div class="row">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.item'); ?></label>'
            +'<div class="input-group">'
            +'<div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>'
            +'<input type="text" class="form-control item_name" name="item_name[]" readonly>'
            +'<input type="hidden" class="item_id" name="item_id[]">'
            +'</div>'
            +'</div>'

            +'<div class="form-group">'
            +'<textarea name="item_summary[]" class="form-control item_summary" placeholder="<?php echo app('translator')->get('app.description'); ?>" rows="2"></textarea>'
            +'</div>'

            +'</div>'

            +'</div>'
            
            // +'<div class="col-md-1">'
            // +'<div class="form-group">'
            // +'<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.hsnSacCode'); ?></label>'
            // +'<input type="text"  class="form-control" name="hsn_sac_code[]" >'
            // +'</div>'
            // +'</div>'

            +'<div class="col-md-1">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.qty'); ?></label>'
            +'<input type="number" min="1" class="form-control quantity" value="1" name="quantity[]" >'
            +'</div>'


            +'</div>'

            +'<div class="col-md-1">'
            +'<div class="row">'
            +'<div class="form-group">'
            +'<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.unitPrice'); ?></label>'
            +'<input type="text" min="0" class="form-control cost_per_item" value="0" name="cost_per_item[]" readonly>'
            +'</div>'
            +'</div>'
            +'</div>'

            +'<div class="col-md-2">'
            +'<div class="form-group">'
            +'<select class="form-control line_discount_type" name="line_discount_type[]">'
            +'<option value="percent">%</option>'
            +'<option value="fixed"><?php echo app('translator')->get('modules.invoices.amount'); ?></option>'
            +'</select>'
            +'</div>'
            +'</div>'

            +'<div class="col-md-1">'
            +'<div class="form-group">'
            +'<input type="number" min="0" value="0" name="line_discount_value[]" class="form-control line_discount_value">'
            +'</div>'
            +'</div>'

            // +'<div class="col-md-2">'
            // +'<div class="form-group">'
            // +'<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.tax'); ?></label>'
            // +'<select id="multiselect'+i+'" name="taxes['+i+'][]" value="null"  multiple="multiple" class="selectpicker form-control type">'
            //     <?php $__currentLoopData = $taxes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tax): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            // +'<option data-rate="<?php echo e($tax->rate_percent); ?>" value="<?php echo e($tax->id); ?>"><?php echo e($tax->tax_name.': '.$tax->rate_percent); ?>%</option>'
            //     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            // +'</select>'
            // +'</div>'
            // +'</div>'

            +'<div class="col-md-2 text-center">'
            +'<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.amount'); ?></label>'
            +'<p class="form-control-static"><span class="amount-html">0.00</span></p>'
            +'<input type="hidden" class="amount" name="amount[]">'
            +'</div>'

            +'<div class="col-md-1 text-right visible-md visible-lg">'
            +'<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'
            +'</div>'

            +'<div class="col-md-1 hidden-md hidden-lg">'
            +'<div class="row">'
            +'<button type="button" class="btn remove-item btn-danger"><i class="fa fa-remove"></i> <?php echo app('translator')->get('app.remove'); ?></button>'
            +'</div>'
            +'</div>'

            +'</div>';

        $(item).hide().appendTo("#sortable").fadeIn(500);
        $('#multiselect'+i).selectpicker();
        hsnSacColumn();
    });

    hsnSacColumn();
    function hsnSacColumn(){
        <?php if($invoiceSetting->hsn_sac_code_show): ?>
        $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').removeClass( "col-md-4");
        $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').addClass( "col-md-3");
        $('input[name="hsn_sac_code[]"]').parent("div").parent('div').show();
        <?php else: ?>
        $('input[name="hsn_sac_code[]"]').parent("div").parent('div').hide();
        $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').removeClass( "col-md-3");
        $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').addClass( "col-md-4");
        <?php endif; ?>
    }

    $('#updatePayments').on('click', '.remove-item', function () {
        $(this).closest('.item-row').fadeOut(300, function () {
            $(this).remove();
            calculateTotal();
        });
    });

    // $('#updatePayments').on('keyup change', '.quantity,.cost_per_item,.item_name, .discount_value', function () {
    //     var quantity = $(this).closest('.item-row').find('.quantity').val();

    //     var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

    //     var amount = (quantity * perItemCost);

    //     $(this).closest('.item-row').find('.amount').val(decimalupto2(amount).toFixed(2));
    //     $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount).toFixed(2));

    //     calculateTotal();
    // });

    $('#updatePayments').on('keyup change','.quantity,.cost_per_item,.item_name, .discount_value, .line_discount_value', function () {
          
          var quantity = $(this).closest('.item-row').find('.quantity').val();
  
          var line_discount_value = $(this).closest('.item-row').find('.line_discount_value').val();
  
          var line_discount_type = $(this).closest('.item-row').find('.line_discount_type').val();
  
          var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();
  
          if(line_discount_type == 'percent'){
              line_discounted_amount = parseFloat(perItemCost)*parseFloat(line_discount_value?line_discount_value:0) / 100;
          }
          else{
              line_discounted_amount = parseFloat(line_discount_value?line_discount_value:0);
          }
  
          var amount = (quantity* (perItemCost - line_discounted_amount));
  
          $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
          $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));
  
          calculateTotal();
          
    });

    $('#updatePayments').on('change','.line_discount_type', function () {
        var quantity = $(this).closest('.item-row').find('.quantity').val();

        var line_discount_value = $(this).closest('.item-row').find('.line_discount_value').val();

        var line_discount_type = $(this).closest('.item-row').find('.line_discount_type').val();

        var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

        if(line_discount_type == 'percent'){
            line_discounted_amount = parseFloat(perItemCost)*parseFloat(line_discount_value?line_discount_value:0) / 100;
        }
        else{
            line_discounted_amount = parseFloat(line_discount_value?line_discount_value:0);
        }

        var amount = (quantity* (perItemCost - line_discounted_amount));

        $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
        $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));

        calculateTotal();

    });

    $('#updatePayments').on('change','.type, #discount_type', function () {
        var quantity = $(this).closest('.item-row').find('.quantity').val();

        var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

        var amount = (quantity*perItemCost);

        $(this).closest('.item-row').find('.amount').val(decimalupto2(amount).toFixed(2));
        $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount).toFixed(2));
        calculateTotal();
    });


    function calculateTotal()
    {
        var subtotal = 0;
        var discount = 0;
        var tax = '';
        var taxList = new Object();
        var taxTotal = 0;
        var discountType = $('#discount_type').val();
        var discountValue = $('.discount_value').val();

        $(".quantity").each(function (index, element) {
            var itemTax = [];
            var itemTaxName = [];
            var discountedAmount = 0;

            $(this).closest('.item-row').find('select.type option:selected').each(function (index) {
                itemTax[index] = $(this).data('rate');
                itemTaxName[index] = $(this).text();
            });
            var itemTaxId = $(this).closest('.item-row').find('select.type').val();

            var amount = parseFloat($(this).closest('.item-row').find('.amount').val());
            if(discountType == 'percent' && discountValue != ''){
                discountedAmount = parseFloat(amount - ((parseFloat(amount)/100)*parseFloat(discountValue)));
            }
            else{
                discountedAmount = parseFloat(amount - (parseFloat(discountValue)));
            }

            if(isNaN(amount)){ amount = 0; }

            subtotal = (parseFloat(subtotal)+parseFloat(amount)).toFixed(2);

            if(itemTaxId != ''){
                for(var i = 0; i<=itemTaxName.length; i++)
                {
                    if(typeof (taxList[itemTaxName[i]]) === 'undefined'){
                        if (discountedAmount > 0) {
                            taxList[itemTaxName[i]] = ((parseFloat(itemTax[i])/100)*parseFloat(discountedAmount));                         
                        } else {
                            taxList[itemTaxName[i]] = ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                        }
                    }
                    else{
                        if (discountedAmount > 0) {
                            taxList[itemTaxName[i]] = parseFloat(taxList[itemTaxName[i]]) + ((parseFloat(itemTax[i])/100)*parseFloat(discountedAmount));   
                            console.log(taxList[itemTaxName[i]]);
                         
                        } else {
                            taxList[itemTaxName[i]] = parseFloat(taxList[itemTaxName[i]]) + ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                        }
                    }
                }
            }
        });


        $.each( taxList, function( key, value ) {
            if(!isNaN(value)){
                tax = tax+'<div class="col-md-offset-8 col-md-2 text-right p-t-10">'
                    +key
                    +'</div>'
                    +'<p class="form-control-static col-xs-6 col-md-2" >'
                    +'<span class="tax-percent">'+(decimalupto2(value)).toFixed(2)+'</span>'
                    +'</p>';
                taxTotal = taxTotal+decimalupto2(value);
            }
        });

        if(isNaN(subtotal)){  subtotal = 0; }

        $('.sub-total').html(decimalupto2(subtotal).toFixed(2));
        $('.sub-total-field').val(decimalupto2(subtotal));

        

        if(discountValue != ''){
            if(discountType == 'percent'){
                discount = ((parseFloat(subtotal)/100)*parseFloat(discountValue));
            }
            else{
                discount = parseFloat(discountValue);
            }

        }

        $('#invoice-taxes').html(tax);

        var totalAfterDiscount = decimalupto2(subtotal-discount);

        totalAfterDiscount = (totalAfterDiscount < 0) ? 0 : totalAfterDiscount;

        var total = decimalupto2(totalAfterDiscount+taxTotal);

        $('.total').html(total.toFixed(2));
        $('.total-field').val(total.toFixed(2));

    }

    calculateTotal();

    function recurringPayment() {
        var recurring = $('#recurring_payment').val();

        if(recurring == 'yes')
        {
            $('.recurringPayment').show().fadeIn(300);
        } else {
            $('.recurringPayment').hide().fadeOut(300);
        }
    }

    function decimalupto2(num) {
        var amt =  Math.round(num * 100,2) / 100;
        return parseFloat(amt.toFixed(2));
    }

    function addProduct(id) {
        var currencyId = $('#currency_id').val();
        $.easyAjax({
            url:'<?php echo e(route('admin.all-invoices.update-item')); ?>',
            type: "GET",
            data: { id: id, currencyId: currencyId },
            success: function(response) {
                $(response.view).hide().appendTo("#sortable").fadeIn(500);
                var noOfRows = $(document).find('#sortable .item-row').length;
                var i = $(document).find('.item_name').length-1;
                var itemRow = $(document).find('#sortable .item-row:nth-child('+noOfRows+') select.type');
                itemRow.attr('id', 'multiselect'+i);
                itemRow.attr('name', 'taxes['+i+'][]');
                $(document).find('#multiselect'+i).selectpicker();
                calculateTotal();
      }
        });
    }

    $('#tax-settings').click(function () {
        var url = '<?php echo e(route('admin.taxes.create')); ?>';
        $('#modelHeading').html('Manage Project Category');
        $.ajaxModal('#taxModal', url);
    })

    function setClient() {
        <?php if($invoice->project_id == ''): ?>
            $('#client_company_id').val('<?php echo e($invoice->client_id); ?>').trigger('change');
        <?php endif; ?>
    }

</script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/invoice-recurring/edit.blade.php ENDPATH**/ ?>