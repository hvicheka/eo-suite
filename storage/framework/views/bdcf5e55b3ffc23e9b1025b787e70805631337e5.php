<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get("app.menu.home"); ?></a></li>
                <li class="active"><?php echo e(__($pageTitle)); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">

<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/morrisjs/morris.css')); ?>">


<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('plugins/daterange-picker/daterangepicker.css')); ?>" />

    <style>
        #all-time-logs-table_wrapper .dt-buttons{
            display: none !important;
        }
    </style>
<?php $__env->stopPush(); ?>


<?php $__env->startSection('filter-section'); ?>
<div class="row">
    <?php echo Form::open(['id'=>'filter-form','class'=>'ajax-form','method'=>'POST']); ?>

    <div class="col-xs-12">
        <div class="example">
            <h5 class="box-title"><?php echo app('translator')->get("app.selectDateRange"); ?></h5>

            <div class="form-group">
                <div id="reportrange" class="form-control reportrange">
                    <i class="fa fa-calendar"></i>&nbsp;
                    <span></span> <i class="fa fa-caret-down pull-right"></i>
                </div>

                <input type="hidden" class="form-control" id="start-date" placeholder="<?php echo app('translator')->get('app.startDate'); ?>"
                       value="<?php echo e($fromDate->format($global->date_format)); ?>"/>
                <input type="hidden" class="form-control" id="end-date" placeholder="<?php echo app('translator')->get('app.endDate'); ?>"
                       value="<?php echo e($toDate->format($global->date_format)); ?>"/>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <h5 class="box-title"><?php echo app('translator')->get('app.selectTask'); ?></h5>

        <div class="form-group">
            <div class="row">
                <div class="col-xs-12">
                    <select class="select2 form-control" data-placeholder="<?php echo app('translator')->get('app.selectTask'); ?>" id="task_id">
                        <option value=""><?php echo app('translator')->get('app.all'); ?></option>
                        <?php $__currentLoopData = $tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($task->id); ?>"><?php echo e(ucwords($task->heading)); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="form-group">
            <h5 class="box-title"><?php echo app('translator')->get('modules.employees.title'); ?></h5>
            <select class="form-control select2" name="employee" id="employee" data-style="form-control">
                <option value="all"><?php echo app('translator')->get('modules.client.all'); ?></option>
                <?php $__empty_1 = true; $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <option value="<?php echo e($employee->id); ?>"><?php echo e(ucfirst($employee->name)); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <?php endif; ?>
            </select>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="form-group">
            <label class="control-label col-xs-12">&nbsp;</label>
            <button type="button" id="filter-results" class="btn btn-success col-md-6"><i class="fa fa-check"></i> <?php echo app('translator')->get('app.apply'); ?></button>
            <button type="button" id="reset-filters" class="btn btn-inverse col-md-5 col-md-offset-1"><i class="fa fa-refresh"></i> <?php echo app('translator')->get('app.reset'); ?></button>
        </div>
    </div>
    <?php echo Form::close(); ?>


</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="white-box">
                <div id="morris-bar-chart"></div>
            </div>
        </div>

    </div>

    <div class="white-box">

        <div class="row">
            <div class="table-responsive m-t-30">
                <?php echo $dataTable->table(['class' => 'table table-bordered table-hover toggle-circle default footable-loaded footable']); ?>

            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>


<script src="<?php echo e(asset('plugins/bower_components/raphael/raphael-min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/morrisjs/morris.js')); ?>"></script>

<script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>

<script src="<?php echo e(asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>

<script src="<?php echo e(asset('plugins/bower_components/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="<?php echo e(asset('js/datatables/buttons.server-side.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/moment/moment.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('plugins/daterange-picker/daterangepicker.js')); ?>"></script>
<?php echo $dataTable->scripts(); ?>

<script>
    $(function() {
        var dateformat = '<?php echo e($global->moment_format); ?>';

        var startDate = '<?php echo e($fromDate->format($global->date_format)); ?>';
        var start = moment(startDate, dateformat);

        var endDate = '<?php echo e($toDate->format($global->date_format)); ?>';
        var end = moment(endDate, dateformat);

        function cb(start, end) {
            $('#start-date').val(start.format(dateformat));
            $('#end-date').val(end.format(dateformat));
            $('#reportrange span').html(start.format(dateformat) + ' - ' + end.format(dateformat));
        }
        moment.locale('<?php echo e($global->locale); ?>');
        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,

            locale: {
                language: '<?php echo e($global->locale); ?>',
                format: '<?php echo e($global->moment_format); ?>',
            },
            linkedCalendars: false,
            ranges: dateRangePickerCustom
        }, cb);

        cb(start, end);

    });
    $(".select2").select2({
        formatNoMatches: function () {
            return "<?php echo e(__('messages.noRecordFound')); ?>";
        }
    });


    $('#filter-results').click(function () {
        var token = '<?php echo e(csrf_token()); ?>';
        var url = '<?php echo e(route('admin.time-log-report.store')); ?>';

        var startDate = $('#start-date').val();

        if (startDate == '') {
            startDate = null;
        }

        var endDate = $('#end-date').val();

        if (endDate == '') {
            endDate = null;
        }

        var projectID = $('#task_id').val();
        var employeeID = $('#employee').val();

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {_token: token, startDate: startDate, endDate: endDate, task_id: projectID,employeeID: employeeID},
            success: function (response) {
                if(response.status == 'success'){
                    chartData = $.parseJSON(response.chartData);
                    $('#morris-bar-chart').html('');
                    $('#morris-bar-chart').empty();
                    barChart();
                    showTable();
                }
            }
        });
    })

    $('#reset-filters').click(function () {
        $('#filter-form')[0].reset();
        $('#status').val('all');
        $('.select2').val('all');
        $('#filter-form').find('select').select2();
        $('#start-date').val('<?php echo e($fromDate->format($global->date_format)); ?>');
        $('#end-date').val('<?php echo e($toDate->format($global->date_format)); ?>');
        $('#reportrange span').html('<?php echo e($fromDate->format($global->date_format)); ?>' + ' - ' + '<?php echo e($toDate->format($global->date_format)); ?>');
        $('#filter-results').trigger("click");
    })

    $('#all-time-logs-table').on('preXhr.dt', function (e, settings, data) {
        var startDate = $('#start-date').val();

        if(startDate == ''){
            startDate = null;
        }

        var endDate = $('#end-date').val();

        if(endDate == ''){
            endDate = null;
        }

        var projectID = $('#task_id').val();
        var employee = $('#employee').val();

        data['startDate'] = startDate;
        data['endDate'] = endDate;
        data['task_id'] = projectID;
        data['employee'] = employee;
    });

    function showTable() {
        window.LaravelDataTables["all-time-logs-table"].draw();
    }

</script>

<script>
    var chartData = <?php echo $chartData; ?>;
    function barChart() {
        if (chartData != '[]' && chartData != '') {
            Morris.Bar({
                element: 'morris-bar-chart',
                data: chartData,
                xkey: 'date',
                ykeys: ['total_hours'],
                labels: ['Hours Logged'],
                barColors:['#3594fa'],
                hideHover: 'auto',
                gridLineColor: '#ccccccc',
                resize: true
            });
        }

    }

    <?php if($chartData != '[]'): ?>
    barChart();
    <?php endif; ?>


</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eOsuite\resources\views/admin/reports/time-log/index.blade.php ENDPATH**/ ?>