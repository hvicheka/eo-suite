<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.payments.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.addNew'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/datetime-picker/datetimepicker.css')); ?>">

<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">

        <div class="col-xs-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> <?php echo app('translator')->get('modules.payments.addPayment'); ?></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <?php echo Form::open(['id'=>'createPayment','class'=>'ajax-form','method'=>'POST']); ?>

                        <div class="form-body">
                            <div class="row">
                                <?php if(isset($projectId)): ?>
                                    <input type="hidden" value="<?php echo e($projectId); ?>" name="project_id_direct">
                                <?php endif; ?>

                                <?php if(in_array('projects', $modules)): ?>
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <label><?php echo app('translator')->get('app.selectProject'); ?></label>
                                            <select class="select2 form-control" onchange="getInvoice(this.value)" data-placeholder="<?php echo app('translator')->get('app.selectProject'); ?> (<?php echo app('translator')->get('app.optional'); ?>)" name="project_id">
                                                <option value="">--</option>
                                                <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option
                                                        <?php if(isset($projectId) && $project->id == $projectId): ?>
                                                            selected
                                                        <?php endif; ?>
                                                            value="<?php echo e($project->id); ?>"><?php echo e($project->project_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if(in_array('invoices', $modules)): ?>
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <label><?php echo app('translator')->get('app.selectInvoice'); ?></label>
                                            <select class="select2 form-control" data-placeholder="<?php echo app('translator')->get('app.selectInvoice'); ?> (<?php echo app('translator')->get('app.optional'); ?>)" name="invoice_id" id="invoice_id">
                                                <option value="">--</option>
                                                <?php $__currentLoopData = $invoices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $invoice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($invoice->id); ?>"><?php echo e($invoice->invoice_number); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.payments.paidOn'); ?></label>
                                        <input type="text" class="form-control" name="paid_on" id="paid_on" value="<?php echo e(Carbon\Carbon::now()->timezone($global->timezone)->format($company->date_format.' '.$company->time_format)); ?>">
                                    </div>
                                </div>


                                <!--/span-->

                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->get('modules.invoices.currency'); ?></label>
                                        <select class="form-control" name="currency_id" id="currency_id">
                                            <option value=""><?php echo app('translator')->get('app.selectCurrency'); ?></option>
                                            <?php $__currentLoopData = $currencies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $currency): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($currency->id); ?>"><?php echo e($currency->currency_symbol.' ('.$currency->currency_code.')'); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->get('modules.invoices.amount'); ?></label>
                                        <input type="number" value="0" min="0" step=".01" name="amount" id="amount" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->


                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->get('modules.payments.paymentGateway'); ?></label>
                                        <input type="text" name="gateway" id="gateway" class="form-control">
                                        <span class="help-block"> Paypal, Authorize.net, Stripe, Bank Transfer, Cash or others.</span>
                                    </div>
                                </div>
                                <!--/span-->

                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->get('modules.payments.transactionId'); ?></label>
                                        <input type="text" name="transaction_id" id="transaction_id" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->

                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('app.receipt'); ?></label>
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput"> 
                                                <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span>
                                            </div>
                                            <span class="input-group-addon btn btn-default btn-file"> 
                                                <span class="fileinput-new"><?php echo app('translator')->get('app.selectFile'); ?></span> 
                                                <span class="fileinput-exists"><?php echo app('translator')->get('app.change'); ?></span>
                                                <input type="file" name="bill" id="bill">
                                            </span> 
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput"><?php echo app('translator')->get('app.remove'); ?></a> 
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('app.remark'); ?></label>
                                        <textarea id="remarks" name="remarks" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form-2" class="btn btn-success"><i class="fa fa-check"></i>
                                <?php echo app('translator')->get('app.save'); ?>
                            </button>

                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
    <script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/moment/moment.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/datetime-picker/datetimepicker.js')); ?>"></script>

    <script>

    $(".select2").select2({
        formatNoMatches: function () {
            return "<?php echo e(__('messages.noRecordFound')); ?>";
        }
    });
    function getInvoice(project_id){
            var url = "<?php echo e(route('admin.payments.getinvoice')); ?>";
            var token = "<?php echo e(csrf_token()); ?>";
            $.easyAjax({
                url: url,
                type: "GET",
                data: {project_id: project_id},
                success: function (data) {
                    $('#invoice_id').html(data.invoices);
                    $('#invoice_id').select2();
                }
            })
    }
        var timeFormat = '';
        var dateformat = '<?php echo e($global->moment_format); ?>';

        <?php if($global->time_format == 'h:i A'): ?>
            timeFormat = 'hh:mm A';
        <?php elseif($global->time_format == 'h:i a'): ?>
            timeFormat = 'hh:mm a';
        <?php else: ?>
            timeFormat = 'HH:mm';
        <?php endif; ?>
        var dateTimeFormat = dateformat+' '+timeFormat;
        jQuery('#paid_on').datetimepicker({
            format: dateTimeFormat
        });

    $('#save-form-2').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.payments.store')); ?>',
            container: '#createPayment',
            type: "POST",
            redirect: true,
            file: true,
            data: $('#createPayment').serialize()
        })
    });
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/payments/create.blade.php ENDPATH**/ ?>