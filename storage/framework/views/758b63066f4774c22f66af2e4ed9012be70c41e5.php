<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.clients.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.menu.projects'); ?></li>
            </ol>
        </div>
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12 text-right">

            <a href="<?php echo e(route('admin.clients.edit',$clientDetail->id)); ?>"
               class="btn btn-outline btn-success btn-sm"><?php echo app('translator')->get('modules.lead.edit'); ?>
                <i class="fa fa-edit" aria-hidden="true"></i></a>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>

    <div class="row">


        <?php echo $__env->make('admin.clients.client_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <div class="col-xs-12">

            <section>
                <div class="sttabs tabs-style-line">

                    <?php echo $__env->make('admin.clients.tabs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


                    <div class="content-wrap">
                        <section id="section-line-1" class="show">
                            <div class="row">


                                <div class="col-xs-12">
                                    <div class="white-box">
                                        <div class="row">
                                            <div class="col-md-4 col-xs-6 b-r"> <strong><?php echo app('translator')->get('modules.employees.fullName'); ?></strong> <br>
                                                <p class="text-muted"><?php echo e(ucwords($clientDetail->name)); ?></p>

                                            </div>
                                            <div class="col-md-4 col-xs-6 b-r"> <strong><?php echo app('translator')->get('app.email'); ?></strong> <br>
                                                <p class="text-muted"><?php echo e($clientDetail->email); ?></p>
                                            </div>
                                            <div class="col-md-4 col-xs-6"> <strong><?php echo app('translator')->get('app.mobile'); ?></strong> <br>
                                                <p class="text-muted"><?php echo e($clientDetail->mobile ?? '-'); ?></p>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-4 col-xs-6 b-r"> <strong><?php echo app('translator')->get('modules.client.companyName'); ?></strong> <br>
                                                <p class="text-muted"><?php echo e((!empty($clientDetail) ) ? ucwords($clientDetail->company_name) : '-'); ?></p>
                                            </div>
                                            <div class="col-md-4 col-xs-6 b-r"> <strong><?php echo app('translator')->get('modules.client.website'); ?></strong> <br>
                                                <p class="text-muted"><?php echo e($clientDetail->website ?? '-'); ?></p>
                                            </div>
                                            <div class="col-md-4 col-xs-6"> <strong><?php echo app('translator')->get('app.gstNumber'); ?></strong> <br>
                                                <p class="text-muted"><?php echo e($clientDetail->gst_number ?? '-'); ?></p>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6 b-r"> <strong><?php echo app('translator')->get('app.address'); ?></strong> <br>
                                                <p class="text-muted"><?php echo (!empty($clientDetail)) ? ucwords($clientDetail->address) : '-'; ?></p>
                                            </div>
                                            <div class="col-xs-6"> <strong><?php echo app('translator')->get('app.shippingAddress'); ?></strong> <br>
                                                <p class="text-muted"><?php echo e($clientDetail->shipping_address ?? '-'); ?></p>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <?php if($clientDetail->category_id != null): ?>
                                                <div class="col-xs-6 b-r"> <strong><?php echo app('translator')->get('modules.clients.clientCategory'); ?></strong> <br>
                                                <p class="text-muted"><?php echo e($clientDetail->clientCategory->category_name); ?></p>
                                                </div>
                                             <?php endif; ?>
                                            <?php if($clientDetail->sub_category_id != null): ?>
                                                <div class="col-xs-6"> <strong><?php echo app('translator')->get('modules.clients.clientSubCategory'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e($clientDetail->clientSubcategory->category_name); ?></p>
                                                </div>
                                            <?php endif; ?>
                                           
                                        </div>

                                        
                                        <?php if(isset($fields)): ?>
                                            <div class="row">
                                                <hr>
                                                <?php $__currentLoopData = $fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="col-md-4">
                                                        <strong><?php echo e(ucfirst($field->label)); ?></strong> <br>
                                                        <p class="text-muted">
                                                            <?php if( $field->type == 'text'): ?>
                                                                <?php echo e($clientDetail->custom_fields_data['field_'.$field->id] ?? '-'); ?>

                                                            <?php elseif($field->type == 'password'): ?>
                                                                <?php echo e($clientDetail->custom_fields_data['field_'.$field->id] ?? '-'); ?>

                                                            <?php elseif($field->type == 'number'): ?>
                                                                <?php echo e($clientDetail->custom_fields_data['field_'.$field->id] ?? '-'); ?>


                                                            <?php elseif($field->type == 'textarea'): ?>
                                                                <?php echo e($clientDetail->custom_fields_data['field_'.$field->id] ?? '-'); ?>


                                                            <?php elseif($field->type == 'radio'): ?>
                                                                <?php echo e(!is_null($clientDetail->custom_fields_data['field_'.$field->id]) ? $clientDetail->custom_fields_data['field_'.$field->id] : '-'); ?>

                                                            <?php elseif($field->type == 'select'): ?>
                                                                <?php echo e((!is_null($clientDetail->custom_fields_data['field_'.$field->id]) && $clientDetail->custom_fields_data['field_'.$field->id] != '') ? $field->values[$clientDetail->custom_fields_data['field_'.$field->id]] : '-'); ?>

                                                            <?php elseif($field->type == 'checkbox'): ?>
                                                            <ul>
                                                                <?php $__currentLoopData = $field->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php if($clientDetail->custom_fields_data['field_'.$field->id] != '' && in_array($value ,explode(', ', $clientDetail->custom_fields_data['field_'.$field->id]))): ?> <li><?php echo e($value); ?></li> <?php endif; ?>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </ul>
                                                            <?php elseif($field->type == 'date'): ?>
                                                                <?php echo e(\Carbon\Carbon::parse($clientDetail->custom_fields_data['field_'.$field->id])->format($global->date_format)); ?>

                                                            <?php endif; ?>
                                                        </p>

                                                    </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>
                                        <?php endif; ?>

                                        

                                        <div class="row">
                                            <div class="col-xs-12"> <strong><?php echo app('translator')->get('app.note'); ?></strong> <br>
                                                <p class="text-muted"><?php echo $clientDetail->note; ?></p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </section>
                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>


    </div>
    <!-- .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
    <script>
        $('ul.showClientTabs .clientProfile').addClass('tab-current');
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/clients/show.blade.php ENDPATH**/ ?>