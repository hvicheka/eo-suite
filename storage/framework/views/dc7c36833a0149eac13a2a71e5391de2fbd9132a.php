<style>
    .slimScrollDiv{
        overflow: initial !important;
    }
</style>
<div class="navbar-default sidebar" role="navigation">
    <div class="navbar-header">
        <!-- Toggle icon for mobile view -->
        <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse"
            data-target=".navbar-collapse"><i class="ti-menu"></i></a>

        <div class="top-left-part">
            <!-- Logo -->
            <a class="logo hidden-xs text-center" href="<?php echo e(route('admin.dashboard')); ?>">
                <span class="visible-md"><img src="<?php echo e($global->logo_url); ?>" alt="home" class=" admin-logo"/></span>
                <span class="visible-sm"><img src="<?php echo e($global->logo_url); ?>" alt="home" class=" admin-logo"/></span>
            </a>

        </div>
        <!-- /Logo -->

        <!-- This is the message dropdown -->
        <ul class="nav navbar-top-links navbar-right pull-right visible-xs">
            <?php if(isset($activeTimerCount)): ?>
            <li class="dropdown hidden-xs">
            <span id="timer-section">
                <div class="nav navbar-top-links navbar-right pull-right m-t-10">
                    <a class="btn btn-rounded btn-default timer-modal" href="javascript:;"><?php echo app('translator')->get("modules.projects.activeTimers"); ?>
                        <span class="label label-danger" id="activeCurrentTimerCount"><?php if($activeTimerCount > 0): ?> <?php echo e($activeTimerCount); ?> <?php else: ?> 0 <?php endif; ?></span>
                    </a>
                </div>
            </span>
            </li>
            <?php endif; ?>


            <li class="dropdown">
                <select class="selectpicker language-switcher" data-width="fit">
                    <?php if($global->timezone == "Europe/London"): ?>
                    <option value="en" <?php if($global->locale == "en"): ?> selected <?php endif; ?> data-content='<span class="flag-icon flag-icon-gb"></span>'>En</option>
                    <?php else: ?>
                    <option value="en" <?php if($global->locale == "en"): ?> selected <?php endif; ?> data-content='<span class="flag-icon flag-icon-us"></span>'>En</option>
                    <?php endif; ?>
                    <?php $__currentLoopData = $languageSettings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($language->language_code); ?>"
                                <?php if($user->locale == $language->language_code): ?> selected
                                <?php endif; ?>  data-content='<span class="flag-icon <?php if($language->language_code == 'zh-CN'): ?> flag-icon-cn <?php elseif($language->language_code == 'zh-TW'): ?> flag-icon-tw <?php else: ?> flag-icon-<?php echo e($language->language_code); ?> <?php endif; ?>"></span>'><?php echo e($language->language_code); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </li>
            <?php if($company_details->count() > 1): ?>
                <li class="dropdown">
                    <select class="selectpicker company-switcher" data-width="fit" name="companies" id="companies">
                        <?php $__currentLoopData = $company_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company_detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option <?php echo e($company_detail->company->id === $global->id ? 'selected' : ''); ?> value="<?php echo e($company_detail->company->id); ?>"><?php echo e(ucfirst($company_detail->company->company_name)); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </li>
            <?php endif; ?>

             <!-- .Task dropdown -->
             <li class="dropdown" id="top-notification-dropdown">
                <a class="dropdown-toggle waves-effect waves-light show-user-notifications" data-toggle="dropdown" href="#">
                    <i class="icon-bell"></i>
                    <?php if($unreadNotificationCount > 0): ?>
                        <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                    <?php endif; ?>
                </a>
                <ul class="dropdown-menu  dropdown-menu-right mailbox animated slideInDown">
                    <li>
                        <a href="javascript:;">...</a>
                    </li>
                    
                </ul>
            </li>
            <!-- /.Task dropdown -->


            <li class="dropdown">
                <a href="<?php echo e(route('logout')); ?>" title="Logout" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();"
                ><i class="fa fa-power-off"></i>
                </a>
            </li>

            <?php $__currentLoopData = $worksuitePlugins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if(in_array(strtolower($item), $modules) || in_array($item, $modules)): ?>
                    <?php if(View::exists(strtolower($item).'::sections.member_left_sidebar')): ?>
                        <?php echo $__env->make(strtolower($item).'::sections.member_left_sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



        </ul>

    </div>
    <!-- /.navbar-header -->

    <div class="top-left-part">
        <a class="logo hidden-xs hidden-sm text-center" href="<?php echo e(route('client.dashboard.index')); ?>">
            <img src="<?php echo e($global->logo_url); ?>" alt="home" class=" admin-logo"/>
        </a>
    </div>
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">

        <ul class="nav" id="side-menu">


            <li class="user-pro hidden-md  hidden-sm  hidden-lg">
                <?php if(is_null($user->image)): ?>
                    <a href="#" class="waves-effect"><img src="<?php echo e(asset('img/default-profile-3.png')); ?>" alt="user-img" class="img-circle"> <span class="hide-menu"><?php echo e((strlen($user->name) > 24) ? substr(ucwords($user->name), 0, 20).'..' : ucwords($user->name)); ?>

                            <span class="fa arrow"></span></span>
                    </a>
                <?php else: ?>
                    <a href="#" class="waves-effect"><img src="<?php echo e(asset_url('avatar/'.$user->image)); ?>" alt="user-img" class="img-circle"> <span class="hide-menu"><?php echo e(ucwords($user->name)); ?>

                            <span class="fa arrow"></span></span>
                    </a>
                <?php endif; ?>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo e(route('client.profile.index')); ?>"><i class="ti-user"></i> <?php echo app('translator')->get("app.menu.profileSettings"); ?></a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                        ><i class="fa fa-power-off"></i> <?php echo app('translator')->get('app.logout'); ?></a>
                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                            <?php echo e(csrf_field()); ?>

                        </form>
                    </li>
                </ul>
            </li>


            <li><a href="<?php echo e(route('client.dashboard.index')); ?>" class="waves-effect"><i class="icon-speedometer fa-fw"></i> <span class="hide-menu"><?php echo app('translator')->get('app.menu.dashboard'); ?> </span></a> </li>

            <?php if(in_array('projects',$modules)): ?>
                <li><a href="<?php echo e(route('client.projects.index')); ?>" class="waves-effect"><i class="icon-layers fa-fw"></i> <span class="hide-menu"><?php echo app('translator')->get('app.menu.projects'); ?> </span> <?php if($unreadProjectCount > 0): ?> <div class="notify notification-color"><span class="heartbit"></span><span class="point"></span></div><?php endif; ?></a> </li>
            <?php endif; ?>

            <?php if(in_array('products',$modules)): ?>
                <li><a href="<?php echo e(route('client.products.index')); ?>" class="waves-effect"><i class="icon-layers fa-fw"></i> <span class="hide-menu"><?php echo app('translator')->get('app.menu.products'); ?> </span> <?php if($unreadProjectCount > 0): ?> <div class="notify notification-color"><span class="heartbit"></span><span class="point"></span></div><?php endif; ?></a> </li>
            <?php endif; ?>

            <?php if(in_array('tickets',$modules)): ?>
                <li><a href="<?php echo e(route('client.tickets.index')); ?>" class="waves-effect"><i class="ti-ticket fa-fw"></i> <span class="hide-menu"><?php echo app('translator')->get("app.menu.tickets"); ?> </span></a> </li>
            <?php endif; ?>

            <?php if(in_array('invoices',$modules)): ?>
                <li><a href="<?php echo e(route('client.invoices.index')); ?>" class="waves-effect"><i class="ti-receipt fa-fw"></i> <span class="hide-menu"><?php echo app('translator')->get('app.menu.invoices'); ?> </span> <?php if($unreadInvoiceCount > 0): ?> <div class="notify notification-color"><span class="heartbit"></span><span class="point"></span></div><?php endif; ?></a> </li>
                <li><a href="<?php echo e(route('client.credit-notes.index')); ?>" class="waves-effect"><i class="ti-credit-card fa-fw"></i> <span class="hide-menu"><?php echo app('translator')->get('app.credit-note'); ?> </span><?php if($unreadCreditNoteCount > 0): ?> <div class="notify notification-color"><span class="heartbit"></span><span class="point"></span></div><?php endif; ?></a></a> </li>
            <?php endif; ?>

            <?php if(in_array('estimates',$modules)): ?>
                <li><a href="<?php echo e(route('client.estimates.index')); ?>" class="waves-effect"><i class="icon-doc fa-fw"></i> <span class="hide-menu"><?php echo app('translator')->get('app.menu.estimates'); ?> </span> <?php if($unreadEstimateCount > 0): ?> <div class="notify notification-color"><span class="heartbit"></span><span class="point"></span></div><?php endif; ?></a> </li>
            <?php endif; ?>

            <?php if(in_array('payments',$modules)): ?>
                <li><a href="<?php echo e(route('client.payments.index')); ?>" class="waves-effect"><i class="fa fa-money fa-fw"></i> <span class="hide-menu"><?php echo app('translator')->get('app.menu.payments'); ?> </span> <?php if($unreadPaymentCount > 0): ?> <div class="notify notification-color"><span class="heartbit"></span><span class="point"></span></div><?php endif; ?></a> </li>
            <?php endif; ?>

            <?php if(in_array('events',$modules)): ?>
                <li><a href="<?php echo e(route('client.events.index')); ?>" class="waves-effect"><i class="icon-calender fa-fw"></i> <span class="hide-menu"><?php echo app('translator')->get('app.menu.Events'); ?></span></a> </li>
            <?php endif; ?>

            <?php if(in_array('contracts',$modules)): ?>
                <li><a href="<?php echo e(route('client.contracts.index')); ?>" class="waves-effect"><i class="fa fa-file fa-fw"></i> <span class="hide-menu"><?php echo app('translator')->get('app.menu.contracts'); ?></span></a> </li>
            <?php endif; ?>

            <?php if($gdpr->enable_gdpr): ?>
                <li><a href="<?php echo e(route('client.gdpr.index')); ?>" class="waves-effect"><i class="icon-lock fa-fw"></i> <span class="hide-menu"><?php echo app('translator')->get('app.menu.gdpr'); ?></span></a> </li>
            <?php endif; ?>

            <?php if(in_array('notices',$modules)): ?>
                <li><a href="<?php echo e(route('client.notices.index')); ?>" class="waves-effect"><i class="ti-layout-media-overlay fa-fw"></i> <span class="hide-menu"><?php echo app('translator')->get("app.menu.noticeBoard"); ?> </span></a> </li>
            <?php endif; ?>

            <?php if(in_array('messages',$modules)): ?>
                <?php if($messageSetting->allow_client_admin == 'yes' || $messageSetting->allow_client_employee == 'yes'): ?>
                    <li><a href="<?php echo e(route('client.user-chat.index')); ?>" class="waves-effect"><i class="icon-envelope fa-fw"></i> <span class="hide-menu"><?php echo app('translator')->get('app.menu.messages'); ?> <?php if($unreadMessageCount > 0): ?><span class="label label-rouded label-custom pull-right"><?php echo e($unreadMessageCount); ?></span> <?php endif; ?></span></a> </li>
                <?php endif; ?>
            <?php endif; ?>
                 <li><a href="<?php echo e(route('client.notes.index')); ?>" class="waves-effect"><i class="fa fa-sticky-note-o"></i> <span class="hide-menu"><?php echo app('translator')->get('modules.projects.notes'); ?> </span></a> </li>

                 
           
                 <?php $__currentLoopData = $worksuitePlugins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if(in_array(strtolower($item), $modules) || in_array($item, $modules)): ?>
                    <?php if(View::exists(strtolower($item).'::sections.client_left_sidebar')): ?>
                        <?php echo $__env->make(strtolower($item).'::sections.client_left_sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </ul>





    </div>

    <div class="menu-footer">
        <div class="menu-user row">
            <div class="col-lg-6 m-b-5">
                <div class="btn-group dropup user-dropdown">
                    <?php if(is_null($user->image)): ?>
                        <img  aria-expanded="false" data-toggle="dropdown" src="<?php echo e(asset('img/default-profile-3.png')); ?>" alt="user-img" class="img-circle dropdown-toggle h-30 w-30">

                    <?php else: ?>
                        <img aria-expanded="false" data-toggle="dropdown" src="<?php echo e(asset_url('avatar/'.$user->image)); ?>" alt="user-img" class="img-circle dropdown-toggle h-30 w-30">

                    <?php endif; ?>
                    <ul role="menu" class="dropdown-menu">
                        <li><a class="bg-inverse"><strong class="text-info"><?php echo e(ucwords($user->name)); ?></strong></a></li>
                        <?php if($isAdmin): ?>
                        <li>
                            <a href="javascript:;" class="login-admin">
                                <i class="fa fa-sign-in"></i> <?php echo app('translator')->get('app.loginAsAdmin'); ?>
                            </a>
                        </li>                           
                        <?php endif; ?>
                        <li><a href="<?php echo e(route('client.profile.index')); ?>"><i class="ti-user"></i> <?php echo app('translator')->get("app.menu.profileSettings"); ?></a></li>

                        <li><a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();
                                                                document.getElementById('logout-form').submit();"
                            ><i class="fa fa-power-off"></i> <?php echo app('translator')->get('app.logout'); ?></a>

                        </li>

                    </ul>
                </div>
            </div>

            <div class="col-lg-6 text-center m-b-5">
                <div class="btn-group dropup notification-dropdown">
                    <a class="dropdown-toggle show-user-notifications" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell"></i>
                        <?php if($unreadNotificationCount > 0): ?>

                            <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                        <?php endif; ?>
                    </a>
                    <ul class="dropdown-menu mailbox ">
                        <li>
                            <a href="javascript:;">...</a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="menu-copy-right">
            <a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="ti-angle-double-right ti-angle-double-left"></i> <span class="collapse-sidebar-text"><?php echo app('translator')->get('app.collapseSidebar'); ?></span></a>
        </div>

    </div>


</div>
<?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/sections/client_left_sidebar.blade.php ENDPATH**/ ?>