<?php $__env->startSection('content'); ?>

<?php $__empty_1 = true; $__currentLoopData = $frontFeatures; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $frontFeature): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
    <!-- START Saas Features -->
    <section class="border-bottom bg-white sp-100 pb-3 overflow-hidden">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="sec-title mb-60">
                        <h3><?php echo e($frontFeature->title); ?></h3>
                        <p><?php echo $frontFeature->description; ?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php $__empty_2 = true; $__currentLoopData = $frontFeature->features; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $feature): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_2 = false; ?>
                    <div class="col-md-4 col-sm-6 col-12 mb-60">
                        <?php if($feature->type != 'image'): ?>
                        <div class="saas-f-box">
                            <div class="align-items-center icon justify-content-center">
                                <i class="<?php echo e($feature->icon); ?>"></i>
                            </div>
                            <h5><?php echo e($feature->title); ?></h5>
                            <p class="mb-0"><?php echo $feature->description; ?> </p>
                        </div>
                        <?php else: ?>
                        <div class="integrate-box shadow">
                            <img src="<?php echo e($feature->image_url); ?>"   alt="<?php echo e($feature->title); ?>">
                            <h5 class="mb-0"><?php echo e(ucfirst($feature->title)); ?> </h5>
                        </div>
                        <?php endif; ?>

                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_2): ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
<?php endif; ?>
    

    <!-- START Clients Section -->
    <?php echo $__env->make('saas.section.client', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <!-- END Clients Section -->

    <!-- START Integration Section -->
    <section class="sp-100-70 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="sec-title mb-60">
                        <h3><?php echo e($trFrontDetail->favourite_apps_title ?: $defaultTrFrontDetail->favourite_apps_title); ?></h3>
                        <p><?php echo e($trFrontDetail->favourite_apps_detail ?: $defaultTrFrontDetail->favourite_apps_detail); ?></p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <?php $__empty_1 = true; $__currentLoopData = $featureApps; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $featureApp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-30 wow fadeIn" data-wow-delay="0.4s">
                        <div class="integrate-box shadow">
                            <img src="<?php echo e($featureApp->image_url); ?>"   alt="<?php echo e($featureApp->title); ?>">
                            <h5 class="mb-0"><?php echo e(ucfirst($featureApp->title)); ?> </h5>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <!-- END Integration Section -->
<?php $__env->stopSection(); ?>
<?php $__env->startPush('footer-script'); ?>

<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.sass-app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eOsuite\resources\views/saas/feature.blade.php ENDPATH**/ ?>