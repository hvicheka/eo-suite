<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <div class="col-md-3 pull-right hidden-xs hidden-sm">
                <?php if($company_details->count() > 1): ?>
                    <select class="selectpicker company-switcher margin-right-auto" data-width="fit" name="companies" id="companies">
                        <?php $__currentLoopData = $company_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company_detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option <?php echo e($company_detail->company->id === $global->id ? 'selected' : ''); ?> value="<?php echo e($company_detail->company->id); ?>"><?php echo e(ucfirst($company_detail->company->company_name)); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                <?php endif; ?>
            </div>
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('member.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li class="active"><?php echo e(__($pageTitle)); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<?php $__env->stopPush(); ?>
<?php $__env->startSection('filter-section'); ?>
    <form action="" id="filter-form">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label"><?php echo app('translator')->get('modules.productCategory.category'); ?></label>
                    <select class="select2 form-control" name="category_id" id="category_id"
                            data-style="form-control">
                        <option selected value="all"><?php echo app('translator')->get('app.all'); ?></option>
                        <?php $__empty_1 = true; $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                            <option value="<?php echo e($category->id); ?>"><?php echo e(ucwords($category->category_name)); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                            <option value=""><?php echo app('translator')->get('messages.noProductCategory'); ?></option>
                        <?php endif; ?>
                    </select>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label"><?php echo app('translator')->get('modules.productCategory.subCategory'); ?></label>
                    <select class="select2 form-control" data-placeholder="<?php echo app('translator')->get('modules.productCategory.subCategory'); ?>" id="sub_category_id">
                        <option selected value="all"><?php echo app('translator')->get('app.all'); ?></option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group p-t-10">
                    <button type="button" class="btn btn-success" id="filter-results"><i class="fa fa-check"></i> <?php echo app('translator')->get('app.apply'); ?>
                    </button>
                    <button type="button" id="reset-filters"
                            class="btn btn-inverse"><i
                                class="fa fa-refresh"></i> <?php echo app('translator')->get('app.reset'); ?></button>
                </div>
            </div>

        </div>
    </form>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">
                        <div class="form-group pull-right">
                            <a href="<?php echo e(route('client.products.create')); ?>" class="btn btn-outline btn-success btn-sm cartButton"><i class="fa fa-shopping-cart"></i> <span class="badge badge-info right productCounter"><?php echo e(sizeof($productDetails)); ?></span></a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="products-table">
                        <thead>
                        <tr>
                            <th><?php echo app('translator')->get('app.id'); ?></th>
                            <th><?php echo app('translator')->get('app.name'); ?></th>
                            <th><?php echo app('translator')->get('app.price'); ?> (<?php echo app('translator')->get('app.inclusiveAllTaxes'); ?>)</th>
                            <th><?php echo app('translator')->get('app.category'); ?></th>
                            <th><?php echo app('translator')->get('app.subCategory'); ?> </th>
                            <th><?php echo app('translator')->get('app.action'); ?></th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">
                        <div class="form-group pull-right">
                            <a href="<?php echo e(route('client.products.create')); ?>" class="btn btn-outline btn-info btn-sm cartButton"> <?php echo app('translator')->get('app.goToCart'); ?> <span class="badge badge-info right productCounter"> <?php echo e(sizeof($productDetails)); ?></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->
    
    
    <div class="modal fade bs-modal-md in" id="productShow" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    
<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>

    <script src="<?php echo e(asset('plugins/bower_components/datatables/jquery.dataTables.min.js')); ?>"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script>

        showData();
        var table ;

        $(".select2").select2({
            formatNoMatches: function () {
                return "<?php echo e(__('messages.noRecordFound')); ?>";
            }
        });
        var subCategories = <?php echo json_encode($subCategories, 15, 512) ?>;

        $('#category_id').change(function (e) {
            // get projects of selected users
            var opts = '';

            var subCategory = subCategories.filter(function (item) {
                return item.category_id == e.target.value
            });
            subCategory.forEach(project => {
                opts += `<option value='${project.id}'>${project.category_name}</option>`
            })

            $('#sub_category_id').html('<option value="all">All</option>'+opts)
            $("#sub_category_id").select2({
                formatNoMatches: function () {
                    return "<?php echo e(__('messages.noRecordFound')); ?>";
                }
            });
        });

        function showData() {
            var categoryID = $('#category_id').val();
            var subCategoryID = $('#sub_category_id').val();

             table = $('#products-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                 destroy: true,
                // stateSave: true,
                ajax: '<?php echo route('client.products.data'); ?>?category_id='+categoryID+'&sub_category_id='+subCategoryID,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'price', name: 'price' },
                    { data: 'categoryname', name: 'categoryname' },
                    { data: 'sub_category_name', name: 'sub_category_name' },
                    { data: 'action', name: 'action' }
                ]
            });
        }
        $('body').on('click', '.view-product', function () {
            var id = $(this).data('product-id');
            var url = '<?php echo e(route('client.products.show', ':id')); ?>';
            url = url.replace(':id', id);

            $('#edit-column-form').html('<?php echo app('translator')->get('app.product'); ?>');
            $.ajaxModal('#productShow', url);
        })

        $('body').on('click', '.add-product', function () {
            let cartItems = [];
            var $this = $(this);
            cartItems  = cartItems.push($this.data('product-id'));

            if(cartItems === 0){
                swal('<?php echo app('translator')->get("modules.booking.addItemsToCart"); ?>');
                $('#cart-item-error').html('<?php echo app('translator')->get("modules.booking.addItemsToCart"); ?>');
                return false;
            }
            else {
                let url = '<?php echo e(route('client.products.add-cart-item')); ?>';

                $.easyAjax({
                    url: url,
                    container: '#products-table',
                    type: "GET",
                    data: {'productID': $this.data('product-id')},
                    success: function (response) {
                        cartItems = response.productItems;
                        $('.productCounter').html(cartItems.length);
                    }
                })
            }

        });

        $('#filter-results').on('click', function (event) {
            event.preventDefault();
            showData();
        });

        $('#reset-filters').click(function () {
            $("#category_id").val('all').trigger('change');
            $("#sub_category_id").val('all').trigger('change');
            showData();
        })


    </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.client-app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/client/products/index.blade.php ENDPATH**/ ?>