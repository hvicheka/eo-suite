<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo app('translator')->get('app.invoice'); ?></title>
    <style>
        /* Please don't remove this code it is useful in case of add new language in dompdf */

        /* @font-face {
            font-family: Hind;
            font-style: normal;
            font-weight: normal;
            src: url(<?php echo e(asset('fonts/hind-regular.ttf')); ?>) format('truetype');
        } */

         /* For hindi language  */

        /* * {
           font-family: Hind, DejaVu Sans, sans-serif;
        } */

         /* For japanese language */

        @font-face {
            font-family: 'THSarabun';
            font-style: normal;
            font-weight: normal;
            src: url("<?php echo e(asset('fonts/TH_Sarabun.ttf')); ?>") format('truetype');
        }
        @font-face {
            font-family: 'THSarabun';
            font-style: normal;
            font-weight: bold;
            src: url("<?php echo e(asset('fonts/TH_SarabunBold.ttf')); ?>") format('truetype');
        }
        @font-face {
            font-family: 'THSarabun';
            font-style: italic;
            font-weight: bold;
            src: url("<?php echo e(asset('fonts/TH_SarabunBoldItalic.ttf')); ?>") format('truetype');
        }
        @font-face {
            font-family: 'THSarabun';
            font-style: italic;
            font-weight: bold;
            src: url("<?php echo e(asset('fonts/TH_SarabunItalic.ttf')); ?>") format('truetype');
        }


        <?php if($invoiceSetting->is_chinese_lang): ?>
            @font-face {
                font-family: SimHei;
                /*font-style: normal;*/
                font-weight: bold;
                src: url('<?php echo e(asset('fonts/simhei.ttf')); ?>') format('truetype');
            }
        <?php endif; ?>

        <?php
            $font = '';
            if($invoiceSetting->locale == 'ja') {
                $font = 'ipag';
            } else if($invoiceSetting->locale == 'hi') {
                $font = 'hindi';
            } else if($invoiceSetting->locale == 'th') {
                $font = 'THSarabun';
            }else if($invoiceSetting->is_chinese_lang) {
                $font = 'SimHei';
            } else {
                $font = 'noto-sans';
            }
        ?>

        * {
            font-family: <?php echo e($font); ?>, DejaVu Sans , sans-serif;
        }
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #0087C3;
            text-decoration: none;
        }

        body {
            position: relative;
            width: 100%;
            height: auto;
            margin: 0 auto;
            color: #555555;
            background: #FFFFFF;
            font-size: 14px;
            font-family: Verdana, Arial, Helvetica, sans-serif;
        }

        h2 {
            font-weight:normal;
        }

        header {
            padding: 10px 0;
            margin-bottom: 20px;
            border-bottom: 1px solid #AAAAAA;
        }

        #logo {
            float: left;
            margin-top: 11px;
        }

        #logo img {
            height: 55px;
            margin-bottom: 15px;
        }

        #company {

        }

        #details {
            margin-bottom: 50px;
        }

        #client {
            padding-left: 6px;
            float: left;
        }

        #client .to {
            color: #777777;
        }

        h2.name, div.name {
            font-size: 1.2em;
            font-weight: normal;
            margin: 0;
        }

        #invoice {

        }

        #invoice h1 {
            color: #0087C3;
            font-size: 2.4em;
            line-height: 1em;
            font-weight: normal;
            margin: 0 0 10px 0;
        }

        #invoice .date {
            font-size: 1.1em;
            color: #777777;
        }

        table {
            width: 100%;
            border-spacing: 0;
            margin-bottom: 20px;
        }

        table th,
        table td {
            padding: 5px 10px 7px 10px;
            background: #EEEEEE;
            text-align: center;
            border-bottom: 1px solid #FFFFFF;
        }

        table th {
            white-space: nowrap;
            font-weight: normal;
        }

        table td {
            text-align: right;
        }

        table td.desc h3, table td.qty h3 {
            color: #57B223;
            font-size: 1em;
            font-weight: normal;
            margin: 0 0 0 0;
        }

        table .no {
            color: #FFFFFF;
            font-size: 1.6em;
            background: #57B223;
            width: 5%;
        }



        table .unit {
            background: #DDDDDD;
        }


        table .total {
            background: #57B223;
            color: #FFFFFF;
        }

        table td.unit,
        table td.qty,
        table td.total
        {
            font-size: 1em;
            text-align: center;
        }
        table td.text-center
        {
            text-align: center;
        }


        table td.unit{
            width: 40%;
        }
        <?php if($invoiceSetting->hsn_sac_code_show): ?>
            table td.desc{
                width: 10%;
            }
            table .desc {
                text-align: left;
                width: 35%;
            }
        <?php else: ?>
            table td.desc{
                width: 20%;
            }
            table .desc {
                text-align: left;
                width: 65%;
            }
        <?php endif; ?>

        table td.qty{
            width: 8%;
        }

        .status {
            margin-top: 15px;
            padding: 1px 8px 5px;
            font-size: 1.3em;
            width: 80px;
            color: #fff;
            float: right;
            text-align: center;
            display: inline-block;
        }

        .status.unpaid {
            background-color: #E7505A;
        }
        .status.paid {
            background-color: #26C281;
        }
        .status.cancelled {
            background-color: #95A5A6;
        }
        .status.error {
            background-color: #F4D03F;
        }

        table tr.tax .desc {
            text-align: right;
            color: #1BA39C;
        }
        table tr.discount .desc {
            text-align: right;
            color: #E43A45;
        }
        table tr.subtotal .desc {
            text-align: right;
            color: #1d0707;
        }
        table tbody tr:last-child td {
            border: none;
        }

        table tfoot td {
            padding: 10px 10px 20px 10px;
            background: #FFFFFF;
            border-bottom: none;
            font-size: 1.2em;
            white-space: nowrap;
            border-bottom: 1px solid #AAAAAA;
        }

        table tfoot tr:first-child td {
            border-top: none;
        }

        table tfoot tr td:first-child {
            border: none;
        }

        #thanks {
            font-size: 2em;
            margin-bottom: 50px;
        }

        #notices {
            padding-left: 6px;
            border-left: 6px solid #0087C3;
        }

        #notices .notice {
            font-size: 1.2em;
        }

        footer {
            color: #777777;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #AAAAAA;
            padding: 8px 0;
            text-align: center;
        }

        table.billing td {
            background-color: #fff;
        }

        table td div#invoiced_to {
            text-align: left;
        }

        #notes{
            color: #767676;
            font-size: 11px;
        }

        .item-summary{
            font-size: 12px
        }

        .mb-3{
            margin-bottom: 1rem;
        }
        .logo {
            text-align: right;
        }
        .logo img {
            max-height: 70px;
        }
        .page_break { page-break-before: always; }

        .h3-border {
            border-bottom: 1px solid #AAAAAA;
        }
        <?php if($invoiceSetting->is_chinese_lang): ?>
            body
        {
            font-weight: normal !important;
        }

        <?php endif; ?>
         .item-summary {
            font-size: 11px;
            text-align: left ;

        }
        .descItem {
            border-bottom: none !important;
        }
    </style>
</head>
<body>
<header class="clearfix">
    <?php
        $colspan = ($invoiceSetting->hsn_sac_code_show) ? 5 : 4;
    ?>
    <table cellpadding="0" cellspacing="0" class="billing">
        <tr>
            <td>
                <div id="invoiced_to">
                    <?php if(!is_null($invoice->project) && !is_null($invoice->project->client)): ?>
                        <small><?php echo app('translator')->get("modules.invoices.billedTo"); ?>:</small>
                        <div class="name">
                            <span class="bold"><?php echo e(ucwords($invoice->project->client->name)); ?></span>
                        </div>
                        <div><?php echo e(ucwords($invoice->project->client->company_name)); ?></div>
                        <div class="mb-3">
                            <span class="font-bold"><?php echo app('translator')->get('app.address'); ?> :</span>
                            <div><?php echo nl2br($invoice->project->clientdetails->address); ?></div>
                        </div>
                        <?php if($invoice->show_shipping_address === 'yes'): ?>
                            <div>
                                <b><?php echo app('translator')->get('app.shippingAddress'); ?> :</b>
                                <div><?php echo nl2br($invoice->project->clientdetails->shipping_address); ?></div>
                            </div>
                        <?php endif; ?>
                        <div>
                            <span><?php echo e($invoice->project->client->email); ?></span>
                        </div>
                        <?php if($invoiceSetting->show_gst == 'yes' && !is_null($invoice->project->client->gst_number)): ?>
                            <div> <?php echo app('translator')->get('app.gstIn'); ?>: <?php echo e($invoice->project->client->gst_number); ?> </div>
                        <?php endif; ?>
                    <?php elseif(!is_null($invoice->client_id) && !is_null($invoice->clientdetails)): ?>
                        <small><?php echo app('translator')->get("modules.invoices.billedTo"); ?>:</small>
                        <div class="name">
                            <span class="bold"><?php echo e(ucwords($invoice->clientdetails->name)); ?></span>
                        </div>
                        <div><?php echo e(ucwords($invoice->clientdetails->company_name)); ?></div>
                        <div class="mb-3">
                            <b><?php echo app('translator')->get('app.address'); ?> :</b>
                            <div><?php echo nl2br($invoice->clientdetails->address); ?></div>
                        </div>
                        <?php if($invoice->show_shipping_address === 'yes'): ?>
                            <div>
                                <b><?php echo app('translator')->get('app.shippingAddress'); ?> :</b>
                                <div><?php echo nl2br($invoice->clientdetails->shipping_address); ?></div>
                            </div>
                        <?php endif; ?>
                        <div>
                            <span><?php echo e($invoice->clientdetails->email); ?></span>
                        </div>
                        <?php if($invoiceSetting->show_gst == 'yes' && !is_null($invoice->clientdetails->gst_number)): ?>
                            <div> <?php echo app('translator')->get('app.gstIn'); ?>: <?php echo e($invoice->clientdetails->gst_number); ?> </div>
                        <?php endif; ?>
                    <?php endif; ?>

                </div>
            </td>
            <td>
                <div id="company">
                    <div class="logo">
                        <img src="<?php echo e($invoiceSetting->logo_url); ?>" alt="home" class="dark-logo" />
                    </div>
                    <div>
                        <small><?php echo app('translator')->get("modules.invoices.generatedBy"); ?>:</small>
                    </div>
                    <h3 class="name"><?php echo e(ucwords($company->company_name)); ?></h3>
                    <?php if(!is_null($settings)): ?>
                        <div><?php echo nl2br($company->address); ?></div>
                        <div><?php echo e($company->company_phone); ?></div>
                    <?php endif; ?>
                    <?php if($invoiceSetting->show_gst == 'yes' && !is_null($invoiceSetting->gst_number)): ?>
                        <div><?php echo app('translator')->get('app.gstIn'); ?>: <?php echo e($invoiceSetting->gst_number); ?></div>
                    <?php endif; ?>
                </div>
            </td>
        </tr>
    </table>
</header>
<main>
    <div id="details" class="clearfix">

        <div id="invoice">
            <h1><?php echo e($invoice->invoice_number); ?></h1>
            <?php if($creditNote): ?>
                <div class=""><?php echo app('translator')->get('app.credit-note'); ?>: <?php echo e($creditNote->cn_number); ?></div>
            <?php endif; ?>
            <div class="date"><?php echo app('translator')->get('app.menu.issues'); ?> <?php echo app('translator')->get('app.date'); ?>: <?php echo e($invoice->issue_date->format($company->date_format)); ?></div>
            <?php if($invoice->status == 'unpaid'): ?>
                <div class="date"><?php echo app('translator')->get('app.dueDate'); ?>: <?php echo e($invoice->due_date->format($company->date_format)); ?></div>
            <?php endif; ?>
            <div class=""><?php echo app('translator')->get('app.status'); ?>: <?php echo e(__('app.'.$invoice->status)); ?></div>
        </div>

    </div>
    <table border="0" cellspacing="0" cellpadding="0">
        <thead>
        <tr>
            <th class="no">#</th>
            <th class="desc"><?php echo app('translator')->get("modules.invoices.item"); ?></th>
            <?php if($invoiceSetting->hsn_sac_code_show): ?>
                <th class="desc"> <?php echo app('translator')->get('modules.invoices.hsnSacCode'); ?></th>
            <?php endif; ?>
            <th class="qty"><?php echo app('translator')->get("modules.invoices.qty"); ?></th>
            <th class="qty"><?php echo app('translator')->get("modules.invoices.unitPrice"); ?> (<?php echo htmlentities($invoice->currency->currency_code); ?>)</th>
            <th class="unit"><?php echo app('translator')->get("modules.invoices.price"); ?> (<?php echo htmlentities($invoice->currency->currency_code); ?>)</th>
        </tr>
        </thead>
        <tbody>
        <?php $count = 0; ?>
        <?php $__currentLoopData = $invoice->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <?php if($item->type == 'item'): ?>
                <tr style="page-break-inside: avoid;">
                    <td class="no <?php if(!is_null($item->item_summary)): ?> descItem <?php endif; ?>"><?php echo e(++$count); ?></td>
                    <td class="desc <?php if(!is_null($item->item_summary)): ?> descItem <?php endif; ?>"><h3><?php echo e(ucfirst($item->item_name)); ?></h3>
                    </td>
                    <?php if($invoiceSetting->hsn_sac_code_show): ?>
                        <td class="desc <?php if(!is_null($item->item_summary)): ?> descItem <?php endif; ?>"><?php echo e($item->hsn_sac_code ?? '--'); ?></td>
                    <?php endif; ?>
                    <td class="qty <?php if(!is_null($item->item_summary)): ?> descItem <?php endif; ?>"><h3><?php echo e($item->quantity); ?></h3></td>
                    <td class="qty <?php if(!is_null($item->item_summary)): ?> descItem <?php endif; ?>"><h3><?php echo e(currency_formatter($item->unit_price,'')); ?></h3></td>
                    <td class="unit <?php if(!is_null($item->item_summary)): ?> descItem <?php endif; ?>"><?php echo e(currency_formatter($item->amount,'')); ?></td>
                </tr>
                <?php if(!is_null($item->item_summary)): ?>
                    <tr>
                        <td class="no"></td>
                        <td class="item-summary" <?php if($invoiceSetting->hsn_sac_code_show): ?> colspan="4" <?php else: ?> colspan="3" <?php endif; ?>>
                            <?php echo ($item->item_summary); ?>

                        </td>
                        <td></td>
                    </tr>
                <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <tr style="page-break-inside: avoid;" class="subtotal">
            <td class="no"> </td>
            <?php if($invoiceSetting->hsn_sac_code_show): ?>
                <td class="qty"> </td>
            <?php endif; ?>
            <td class="qty"> </td>
            <td class="qty"> </td>
            <td class="desc"><?php echo app('translator')->get("modules.invoices.subTotal"); ?></td>
            <td class="unit"><?php echo e(number_format((float)$invoice->sub_total, 2, '.', '')); ?></td>
        </tr>
        <?php if($discount != 0 && $discount != ''): ?>
        <tr style="page-break-inside: avoid;" class="discount">
            <td class="no"> </td>
            <?php if($invoiceSetting->hsn_sac_code_show): ?>
                <td class="qty"> </td>
            <?php endif; ?>
            <td class="qty"> </td>
            <td class="qty"> </td>
            <td class="desc"><?php echo app('translator')->get("modules.invoices.discount"); ?></td>
            <td class="unit">-<?php echo e(number_format((float)$discount, 2, '.', '')); ?></td>
        </tr>
        <?php endif; ?>
        <?php $__currentLoopData = $taxes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$tax): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr style="page-break-inside: avoid;" class="tax">
                <td class="no"> </td>
                <?php if($invoiceSetting->hsn_sac_code_show): ?>
                    <td class="qty"> </td>
                <?php endif; ?>
                <td class="qty"> </td>
                <td class="qty"> </td>
                <td class="desc"><?php echo e(strtoupper($key)); ?></td>
                <td class="unit"><?php echo e(number_format((float)$tax, 2, '.', '')); ?></td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>

        <tfoot>
            <tr dontbreak="true">
                <td colspan="<?php echo e($colspan); ?>"><?php echo app('translator')->get("modules.invoices.total"); ?></td>
                <td style="text-align: center"><?php echo e(currency_formatter($invoice->total,'')); ?></td>
            </tr>
            <?php if($invoice->credit_notes()->count() > 0): ?>
                <tr dontbreak="true">
                    <td colspan="<?php echo e($colspan); ?>"><?php echo app('translator')->get('modules.invoices.appliedCredits'); ?></td>
                    <td style="text-align: center"><?php echo e(currency_formatter($invoice->appliedCredits(),'')); ?></td>
                </tr>
            <?php endif; ?>
            <tr dontbreak="true">
                <td colspan="<?php echo e($colspan); ?>"><?php echo app('translator')->get("modules.invoices.total"); ?> <?php echo app('translator')->get("modules.invoices.paid"); ?></td>
                <td style="text-align: center"><?php echo e(currency_formatter($invoice->amountPaid(),'')); ?></td>
            </tr>
            <tr dontbreak="true">
                <td colspan=" <?php echo e($colspan); ?>"><?php echo app('translator')->get("modules.invoices.total"); ?> <?php echo app('translator')->get("modules.invoices.due"); ?></td>

                <td style="text-align: center"><?php echo e(currency_formatter($invoice->amountDue(),'')); ?></td>
            </tr>
        </tfoot>
    </table>
    <p> </p>
    <hr>
    <p id="notes">
        <?php if(!is_null($invoice->note)): ?>
            <?php echo nl2br($invoice->note); ?><br>
        <?php endif; ?>
        <?php echo nl2br($invoiceSetting->invoice_terms); ?>


    </p>
    
    <?php if(isset($fields) && count($fields) > 0): ?>
        <div class="page_break"></div>
        <h3 class="box-title m-t-20 text-center h3-border"> <?php echo app('translator')->get('modules.projects.otherInfo'); ?></h3>
        <table  style="background: none" border="0" cellspacing="0" cellpadding="0" width="100%">
            <?php $__currentLoopData = $fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td style="text-align: left;background: none;" >
                        <div class="desc"><?php echo e(ucfirst($field->label)); ?></div>
                        <p id="notes">
                            <?php if( $field->type == 'text'): ?>
                                <?php echo e($invoice->custom_fields_data['field_'.$field->id] ?? '-'); ?>

                            <?php elseif($field->type == 'password'): ?>
                                <?php echo e($invoice->custom_fields_data['field_'.$field->id] ?? '-'); ?>

                            <?php elseif($field->type == 'number'): ?>
                                <?php echo e($invoice->custom_fields_data['field_'.$field->id] ?? '-'); ?>


                            <?php elseif($field->type == 'textarea'): ?>
                                <?php echo e($invoice->custom_fields_data['field_'.$field->id] ?? '-'); ?>


                            <?php elseif($field->type == 'radio'): ?>
                                <?php echo e(!is_null($invoice->custom_fields_data['field_'.$field->id]) ? $invoice->custom_fields_data['field_'.$field->id] : '-'); ?>

                            <?php elseif($field->type == 'select'): ?>
                                <?php echo e((!is_null($invoice->custom_fields_data['field_'.$field->id]) && $invoice->custom_fields_data['field_'.$field->id] != '') ? $field->values[$invoice->custom_fields_data['field_'.$field->id]] : '-'); ?>

                            <?php elseif($field->type == 'checkbox'): ?>
                                <?php echo e(!is_null($invoice->custom_fields_data['field_'.$field->id]) ? $field->values[$invoice->custom_fields_data['field_'.$field->id]] : '-'); ?>

                            <?php elseif($field->type == 'date'): ?>
                                <?php echo e(!is_null($invoice->custom_fields_data['field_'.$field->id]) ? \Carbon\Carbon::parse($invoice->custom_fields_data['field_'.$field->id])->format($global->date_format) : '--'); ?>

                            <?php endif; ?>
                        </p>
                    </td>


                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </table>
    <?php endif; ?>

    <?php if(!is_null($payments)): ?>
        <div class="page_break"></div>
        <div class="b-all m-t-20 m-b-20 text-center">
            <h3 class="box-title m-t-20 text-center h3-border"> <?php echo app('translator')->get('modules.invoices.recentPayments'); ?></h3>
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive m-t-40" style="clear: both;">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center"><?php echo app('translator')->get("modules.payments.amount"); ?></th>
                                <th class="text-center"><?php echo app('translator')->get("modules.invoices.paymentMethod"); ?></th>
                                <th class="text-center"><?php echo app('translator')->get("modules.invoices.paidOn"); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $count = 0; ?>
                            <?php $__empty_1 = true; $__currentLoopData = $payments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <tr>
                                    <td class="text-center"><?php echo e($key+1); ?></td>
                                    <td class="text-center"> <?php echo currency_formatter($payment->amount, $invoice->currency->currency_symbol); ?> </td>
                                    <td class="text-center">
                                        <?php ($method = (!is_null($payment->offline_method_id)?  $payment->offlineMethod->name : $payment->gateway)); ?>
                                        <?php echo e($method); ?>

                                    </td>
                                    <td class="text-center"> <?php echo e($payment->paid_on->format($global->date_format)); ?> </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</main>
</body>
</html><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/invoices/invoice-1.blade.php ENDPATH**/ ?>