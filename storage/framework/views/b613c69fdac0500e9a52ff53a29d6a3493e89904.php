<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?> #<?php echo e($project->id); ?> - <span class="font-bold"><?php echo e(ucwords($project->project_name)); ?></span></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12 text-right bg-title-right">
            <?php $pin = $project->pinned() ?>
            <a href="javascript:;" class="btn btn-sm btn-info <?php if(!$pin): ?> btn-outline <?php endif; ?>"  data-placement="bottom"  data-toggle="tooltip" data-original-title="<?php if($pin): ?> <?php echo app('translator')->get('app.unpin'); ?> <?php else: ?> <?php echo app('translator')->get('app.pin'); ?> <?php endif; ?>"   data-pinned="<?php if($pin): ?> pinned <?php else: ?> unpinned <?php endif; ?>" id="pinnedItem" >
                <i class="icon-pin icon-2 pin-icon  <?php if($pin): ?> pinned <?php else: ?> unpinned <?php endif; ?>" ></i>
            </a>

            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('member.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('member.projects.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->get('modules.projects.overview'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/icheck/skins/all.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/multiselect/css/multi-select.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">

<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-12">

            <section>
                <div class="sttabs tabs-style-line">
                    <?php echo $__env->make('member.projects.show_project_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    
                    <div class="content-wrap">
                        <section id="section-line-1" class="show">
                            <div class="white-box">
                                <div class="row">

                                    <div class="col-md-9">
                                    
                                        <div class="row">
                                            <div class="col-xs-12" style="max-height: 400px; overflow-y: auto;">
                                                <h5><?php echo app('translator')->get('app.project'); ?> <?php echo app('translator')->get('app.details'); ?></h5>
                                                <?php echo $project->project_summary; ?>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12" style="max-height: 400px; overflow-y: auto;">
                                                <h5><?php echo app('translator')->get('app.project'); ?> <?php echo app('translator')->get('app.note'); ?></h5>
                                                <?php echo $project->notes; ?>

                                            </div>
                                        </div>
            
                                        <div class="row m-t-25">
                                            <?php if($user->cans('view_clients')): ?>
                                            <div class="col-md-4">
                                                <div class="panel panel-inverse">
                                                    <div class="panel-heading"><?php echo app('translator')->get('modules.client.clientDetails'); ?> </div>
                                                    <div class="panel-wrapper collapse in">
                                                        <div class="panel-body">
                                                            <?php if(!is_null($project->client)): ?>
                                                            <dl>
                                                                <?php if(!is_null($project->client->client_details)): ?>
                                                                <dt><?php echo app('translator')->get('modules.client.companyName'); ?></dt>
                                                                <dd class="m-b-10"><?php echo e($project->client->client_details->company_name); ?></dd>
                                                                <?php endif; ?>
            
                                                                <dt><?php echo app('translator')->get('modules.client.clientName'); ?></dt>
                                                                <dd class="m-b-10"><?php echo e(ucwords($project->client->name)); ?></dd>
            
                                                                <dt><?php echo app('translator')->get('modules.client.clientEmail'); ?></dt>
                                                                <dd class="m-b-10"><?php echo e($project->client->email); ?></dd>
                                                            </dl>
                                                            <?php else: ?> <?php echo app('translator')->get('messages.noClientAddedToProject'); ?> <?php endif; ?>
                                                            <hr>
                                                                 <?php if(isset($fields)): ?>
                                                            <dl>
                                                                <?php $__currentLoopData = $fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <dt><?php echo e(ucfirst($field->label)); ?></dt>
                                                                <dd class="m-b-10">
                                                                    <?php if( $field->type == 'text'): ?> <?php echo e($project->custom_fields_data['field_'.$field->id] ?? '-'); ?> <?php elseif($field->type == 'password'): ?>
                                                                    <?php echo e($project->custom_fields_data['field_'.$field->id] ?? '-'); ?>

                                                                    <?php elseif($field->type == 'number'): ?> <?php echo e($project->custom_fields_data['field_'.$field->id]
                                                                    ?? '-'); ?> <?php elseif($field->type == 'textarea'): ?> <?php echo e($project->custom_fields_data['field_'.$field->id]
                                                                    ?? '-'); ?> <?php elseif($field->type == 'radio'): ?> <?php echo e(!is_null($project->custom_fields_data['field_'.$field->id])
                                                                    ? $project->custom_fields_data['field_'.$field->id] : '-'); ?>

                                                                    <?php elseif($field->type == 'select'): ?> <?php echo e((!is_null($project->custom_fields_data['field_'.$field->id])
                                                                    && $project->custom_fields_data['field_'.$field->id] != '') ?
                                                                    $field->values[$project->custom_fields_data['field_'.$field->id]]
                                                                    : '-'); ?> <?php elseif($field->type == 'checkbox'): ?> 
                                                                    <ul>
                                                                        <?php $__currentLoopData = $field->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <?php if($project->custom_fields_data['field_'.$field->id] != '' && in_array($value ,explode(', ', $project->custom_fields_data['field_'.$field->id]))): ?> <li><?php echo e($value); ?></li> <?php endif; ?>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                    </ul>  
                                                                    <?php elseif($field->type == 'date'): ?>
                                                                        <?php echo e(\Carbon\Carbon::parse($project->custom_fields_data['field_'.$field->id])->format($global->date_format)); ?>

                                                                    <?php endif; ?>
                                                                </dd>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </dl>
                                                            <?php endif; ?> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endif; ?>
            
                    
                                            <?php if($project->isProjectAdmin || $user->cans('edit_projects')): ?>
                                            <div class="col-md-8">
                                                <div class="panel panel-inverse">
                                                    <div class="panel-heading"><?php echo app('translator')->get('modules.projects.activeTimers'); ?></div>
                                                    <div class="panel-wrapper collapse in">
                                                        <div class="panel-body" id="timer-list">
                                                            
                                                            <?php $__empty_1 = true; $__currentLoopData = $activeTimers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$time): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                            <div class="row m-b-10">
                                                                <div class="col-xs-12 m-b-5">
                                                                    <?php echo e(ucwords($time->user->name)); ?>

                                                                </div>
                                                                <div class="col-xs-9 font-12">
                                                                    <?php echo e($time->duration); ?>

                                                                </div>
                                                                <div class="col-xs-3 text-right">
                                                                    <button type="button" data-time-id="<?php echo e($time->id); ?>" class="btn btn-danger btn-xs stop-timer"><?php echo app('translator')->get('app.stop'); ?></button>
                                                                </div>
                                                            </div>
                                                            
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                                <?php echo app('translator')->get('messages.noActiveTimer'); ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endif; ?>

                                            
            
                                        </div>
            
                                    </div>
            
                                    <div class="col-md-3">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="panel panel-inverse">
                                                    <div class="panel-heading"><?php echo app('translator')->get('modules.projects.members'); ?> 
                                                        <span class="label label-rouded label-custom pull-right"><?php echo e(count($project->members)); ?></span>    
                                                    </div>
                                                    <div class="panel-wrapper collapse in">
                                                        <div class="panel-body">
                                                            <?php $__empty_1 = true; $__currentLoopData = $project->members; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $member): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                                <img src="<?php echo e(asset($member->user->image_url)); ?>"
                                                                data-toggle="tooltip" data-original-title="<?php echo e(ucwords($member->user->name)); ?>"
            
                                                                alt="user" class="img-circle" width="25" height="25" height="25" height="25">
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?> 
                                                                <?php echo app('translator')->get('messages.noMemberAddedToProject'); ?> 
                                                            <?php endif; ?>
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
            
                                            <div class="col-xs-12">
                                                <div class="panel panel-inverse">
                                                    
                                                    <div class="panel-wrapper collapse in">
                                                        <div class="panel-body ">
                                                        <div class="row">
                                                            <div class="col-md-12 m-b-5 project-stats">
                                                                <div class="row">
                                                                    <div class="col-xs-6 text-right"><span class="text-danger font-semi-bold"><?php echo e(count($openTasks)); ?></span></div>
                                                                    <div class="col-xs-6"> <?php echo app('translator')->get('modules.projects.openTasks'); ?></div>
                                                                </div>
                                                                    
                                                            </div>
                                                            <div class="col-md-12 m-b-5 project-stats">
                                                                <div class="row">
                                                                    <div class="col-xs-6 text-right"><span class="text-info font-semi-bold"><?php echo e($daysLeft); ?></span></div>
                                                                    <div class="col-xs-6"> <?php echo app('translator')->get('modules.projects.daysLeft'); ?></div>
                                                                </div>

                                                            </div>
                                                            <div class="col-md-12 m-b-5 project-stats">
                                                                <div class="row">
                                                                    <div class="col-xs-6 text-right"><span class="text-success font-semi-bold"><?php echo e($hoursLogged); ?></span></div>
                                                                    <div class="col-xs-6"> <?php echo app('translator')->get('modules.projects.hoursLogged'); ?></div>
                                                                </div>
                                                                    
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
            
                                            <div class="col-xs-12"   id="project-timeline">
                                                <div class="panel panel-inverse">
                                                    <div class="panel-heading"><?php echo app('translator')->get('modules.projects.activityTimeline'); ?></div>
                                                    
                                                    <div class="panel-wrapper collapse in">
                                                        <div class="panel-body">
                                                            <div class="steamline">
                                                                <?php $__currentLoopData = $activities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $activ): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <div class="sl-item">
                                                                    <div class="sl-left"><i class="fa fa-circle text-primary"></i>
                                                                    </div>
                                                                    <div class="sl-right">
                                                                        <div>
                                                                            <h6><?php echo e($activ->activity); ?></h6> <span class="sl-date"><?php echo e($activ->created_at->diffForHumans()); ?></span></div>
                                                                    </div>
                                                                </div>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
            
                                        </div>
                                    </div>
            
                                </div>
                            </div>

                            
                        </section>
                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>


    </div>
    <!-- .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('js/cbpFWTabs.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/multiselect/js/jquery.multi-select.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<script type="text/javascript">
//    (function () {
//
//        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function (el) {
//            new CBPFWTabs(el);
//        });
//
//    })();

    $('#timer-list').on('click', '.stop-timer', function () {
       var id = $(this).data('time-id');
        var url = '<?php echo e(route('admin.time-logs.stopTimer', ':id')); ?>';
        url = url.replace(':id', id);
        var token = '<?php echo e(csrf_token()); ?>'
        $.easyAjax({
            url: url,
            type: "POST",
            data: {timeId: id, _token: token},
            success: function (data) {
                $('#timer-list').html(data.html);
            }
        })

    });
$('body').on('click', '#pinnedItem', function(){
    var type = $('#pinnedItem').attr('data-pinned');
    var id = <?php echo e($project->id); ?>;
    console.log(['type', type]);
    var dataPin = type.trim(type);
    if(dataPin == 'pinned'){
        swal({
            title: "<?php echo app('translator')->get('messages.sweetAlertTitle'); ?>",
            text: "<?php echo app('translator')->get('messages.confirmation.pinnedProject'); ?>!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<?php echo app('translator')->get('messages.deleteConfirmation'); ?>!",
            cancelButtonText: "<?php echo app('translator')->get('messages.confirmNoArchive'); ?>",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {

                var url = "<?php echo e(route('member.pinned.destroy',':id')); ?>";
                url = url.replace(':id', id);

                var token = "<?php echo e(csrf_token()); ?>";
                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token': token, '_method': 'DELETE'},
                    success: function (response) {
                        if (response.status == "success") {
                            $.unblockUI();
                            $('.pin-icon').removeClass('pinned');
                            $('.pin-icon').addClass('unpinned');
                            $('#pinnedItem').attr('data-pinned','unpinned');
                            $('#pinnedItem').attr('data-original-title','Pin');
                            $("#pinnedItem").tooltip("hide");
                        }
                    }
                });
            }
        });
    }
    else {

        swal({
            title: "<?php echo app('translator')->get('messages.sweetAlertTitle'); ?>",
            text: "<?php echo app('translator')->get('messages.confirmation.pinProject'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<?php echo app('translator')->get('messages.pinIt'); ?>",
            cancelButtonText: "<?php echo app('translator')->get('messages.confirmNoArchive'); ?>",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {

                var url = "<?php echo e(route('member.pinned.store')); ?>";

                var token = "<?php echo e(csrf_token()); ?>";
                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token': token,'project_id':id},
                    success: function (response) {
                        if (response.status == "success") {
                            $.unblockUI();
                            $('.pin-icon').removeClass('unpinned');
                            $('.pin-icon').addClass('pinned');
                            $('#pinnedItem').attr('data-pinned','pinned');
                            $('#pinnedItem').attr('data-original-title','Unpin');
                            $("#pinnedItem").tooltip("hide");
                        }
                    }
                });
            }
        });

    }
});
    $('ul.showProjectTabs .projects').addClass('tab-current');

</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.member-app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/member/projects/show.blade.php ENDPATH**/ ?>