<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Renew Contract</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        <?php echo Form::open(['id'=>'renewContract','class'=>'ajax-form','method'=>'POST']); ?>

        <div class="form-body">
            <div class="row ">
                <div class="col-xs-12 m-b-10">
                    <div class="form-group">
                        <label class="col-xs-3"><?php echo app('translator')->get('app.startDate'); ?></label>
                        <div class="col-xs-9">
                            <input type="text" name="start_date_1" id="start_date_1" class="form-control" value="<?php echo e($contract->start_date->format($global->date_format)); ?>">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 m-b-10">
                    <div class="form-group">
                        <label class="col-xs-3"><?php echo app('translator')->get('modules.contracts.endDate'); ?></label>
                        <div class="col-xs-9">
                            <input type="text" name="end_date_1" id="end_date_1" class="form-control" value="<?php if(!is_null($contract->end_date)): ?><?php echo e($contract->end_date->format($global->date_format)); ?><?php endif; ?>">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 m-b-10">
                    <div class="form-group">
                        <label class="col-xs-3"><?php echo app('translator')->get('app.amount'); ?></label>
                        <div class="col-xs-9">
                            <input type="number" name="amount_1" id="amount_1" class="form-control" value="<?php echo e($contract->amount); ?>">
                        </div>
                    </div>
                </div>
                <?php if($contract->signature): ?>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="checkbox checkbox-info">
                                <input id="keep_customer_signature" name="keep_customer_signature" value="true"
                                       type="checkbox">
                                <label for="without_deadline">Keep Customer Signature</label>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php echo Form::close(); ?>

    </div>
</div>
<div class="modal-footer">
    <div class="form-actions">
        <button type="button" id="renew-contract" class="btn btn-success"> <i class="fa fa-check"></i> <?php echo app('translator')->get('app.renew'); ?></button>
    </div>
</div>

<script>
    jQuery('#start_date_1, #end_date_1').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: '<?php echo e($global->date_picker_format); ?>',
        weekStart:'<?php echo e($global->week_start); ?>',
    });

    $('#renew-contract').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.contracts.renew-submit', $contract->id)); ?>',
            container: '#renewContract',
            type: "POST",
            data: $('#renewContract').serialize(),
            success: function (res) {
                if(res.status == "success")
                {
                    window.location.reload()
                }

            }
        })
    });
</script><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/contracts/renew/renew.blade.php ENDPATH**/ ?>