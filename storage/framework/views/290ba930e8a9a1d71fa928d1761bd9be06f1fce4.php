<?php $__env->startPush('head-script'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.css')); ?>">

<?php $__env->stopPush(); ?>
<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo app('translator')->get($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('member.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('member.contracts.index')); ?>"><?php echo app('translator')->get($pageTitle); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.addNew'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
        <div class="col-xs-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> <?php echo app('translator')->get('modules.estimates.convertEstimateTitle'); ?></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <?php echo Form::open(['id'=>'storePayments','class'=>'ajax-form','method'=>'POST']); ?>

                            <input type="hidden" name="contract_id" value="<?php echo e($contractId); ?>">
                            <div class="form-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo app('translator')->get('app.invoice'); ?> #</label>
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><span class="invoicePrefix" data-prefix="<?php echo e($invoiceSetting->invoice_prefix); ?>"><?php echo e($invoiceSetting->invoice_prefix); ?></span>#<span class="noOfZero" data-zero="<?php echo e($invoiceSetting->invoice_digit); ?>"><?php echo e($zero); ?></span></div>
                                                        <input type="text"  class="form-control" name="invoice_number" id="invoice_number" value="<?php if(is_null($lastInvoice)): ?>1 <?php else: ?><?php echo e(($lastInvoice)); ?><?php endif; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('app.client'); ?></label>
                                            <?php if(is_null($invoice->lead->client_id)): ?>
                                                <select class="select2 form-control" data-placeholder="Choose Project"
                                                        name="client_id">
                                                    <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option <?php if($invoice->lead->client_id == $client->id): ?> selected  <?php endif; ?>
                                                                value="<?php echo e($client->id); ?>"><?php echo e(ucwords($client->name)); ?>

                                                        </option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            <?php else: ?>
                                                <span class="form-control"><?php echo e(ucwords($invoice->lead->client->name)); ?></span>
                                                <input type="hidden" name="client_id" value="<?php echo e($invoice->lead->client_id); ?>">
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('modules.invoices.currency'); ?></label>
                                            <select class="form-control" name="currency_id" id="currency_id">
                                                <?php $__currentLoopData = $currencies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $currency): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option
                                                            <?php if($invoice->currency_id == $currency->id): ?> selected
                                                            <?php endif; ?>
                                                            value="<?php echo e($currency->id); ?>"><?php echo e($currency->currency_symbol.' ('.$currency->currency_code.')'); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('modules.invoices.invoiceDate'); ?></label>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" name="issue_date"
                                                            id="invoice_date"
                                                            value="<?php echo e(Carbon\Carbon::today()->format($global->date_format)); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('app.dueDate'); ?></label>

                                            <div class="input-icon">
                                                <input type="text" class="form-control" name="due_date" id="due_date"
                                                    value="<?php echo e($invoice->end_date->format($global->date_format)); ?>">

                                                <input type="hidden" name="recurring_payment" id="recurring_payment" value="no">
                                                <input type="hidden" name="project_id" id="project_id" value="">
                                            </div>
                                            
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('app.status'); ?></label>
                                            <select class="form-control" name="status" id="status">
                                                <option
                                                        <?php if($invoice->status == 'paid'): ?> selected <?php endif; ?>
                                                value="paid"><?php echo app('translator')->get('modules.invoices.paid'); ?>
                                                </option>
                                                <option
                                                        <?php if($invoice->status == 'unpaid'): ?> selected <?php endif; ?>
                                                value="unpaid"><?php echo app('translator')->get('modules.invoices.unpaid'); ?>
                                                </option>
                                            </select>
                                        </div>

                                    </div>

                                </div>
                                <hr>
                                <div class="row">

                                    <div class="col-xs-12  visible-md visible-lg">

                                        <div class="<?php if($invoiceSetting->hsn_sac_code_show): ?> col-md-3 <?php else: ?> col-md-4 <?php endif; ?> font-bold" style="padding: 8px 15px">
                                            <?php echo app('translator')->get('modules.invoices.item'); ?>
                                        </div>
                                        

                                        <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                            <?php echo app('translator')->get('modules.invoices.qty'); ?>
                                        </div>

                                        <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                            <?php echo app('translator')->get('modules.invoices.unitPrice'); ?>
                                        </div>

                                        <div class="col-md-3 font-bold" style="padding: 8px 15px">
                                            Discount
                                        </div>

                                        

                                        <div class="col-md-2 text-center font-bold" style="padding: 8px 15px">
                                            <?php echo app('translator')->get('modules.invoices.amount'); ?>
                                        </div>

                                        <div class="col-md-1" style="padding: 8px 15px">
                                            &nbsp;
                                        </div>

                                    </div>

                                    <div id="sortable">
                                        <?php $__currentLoopData = $invoice->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="col-xs-12 item-row margin-top-5" id="sortable_item" data-id="<?php echo e($item->item_id); ?>">
                                                <div class="col-md-3">
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.item'); ?></label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>
                                                                <input type="text" class="form-control item_name" name="item_name[]" value="<?php echo e($item->item_name); ?>" readonly>
                                                                <input type="hidden" class="form-control item_id" name="item_id[]" value="<?php echo e($item->item_id); ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea name="item_summary[]" class="form-control item_summary" placeholder="<?php echo app('translator')->get('app.description'); ?>" rows="2"><?php echo e($item->item_summary); ?></textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                                
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.qty'); ?></label>
                                                        <input type="number" min="1" class="form-control quantity" value="<?php echo e($item->quantity); ?>" name="quantity[]" readonly>
                                                    </div>


                                                </div>

                                                <div class="col-md-1">
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.unitPrice'); ?></label>
                                                            <input type="text" min="" class="form-control cost_per_item" name="cost_per_item[]" value="<?php echo e($item->unit_price); ?>">
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <select class="form-control line_discount_type" name="line_discount_type[]">
                                                            <option value="percent" <?php echo e($item->discount_type=='percent'?'selected':''); ?>>%</option>
                                                            <option value="fixed" <?php echo e($item->discount_type=='fixed'?'selected':''); ?>><?php echo app('translator')->get('modules.invoices.amount'); ?></option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2" >
                                                    <div class="form-group">
                                                        <input type="number" min="0" value="<?php echo e($item->discount); ?>" class="form-control line_discount_value" name="line_discount_value[]">
                                                    </div>
                                                </div>

                                                

                                                <div class="col-md-2 border-dark  text-center">
                                                    <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.amount'); ?></label>

                                                    <p class="form-control-static"><span
                                                                class="amount-html"><?php echo e(currency_formatter($item->amount, '')); ?></span></p>
                                                    <input type="hidden" value="<?php echo e($item->amount); ?>" class="amount" name="amount[]">
                                                </div>

                                                <div class="col-md-1 text-right visible-md visible-lg">
                                                    <button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>
                                                </div>
                                                <div class="col-xs-12 text-center hidden-md hidden-lg">
                                                    <div class="row">
                                                        <button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i>
                                                        </button>
                                                    </div>
                                                </div>

                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>


                                    

                                    <div class="col-xs-12 ">

                                            <div class="row">
                                                <div class="col-md-offset-9 col-xs-6 col-md-1 text-right p-t-10"><?php echo app('translator')->get('modules.invoices.subTotal'); ?></div>

                                                <p class="form-control-static col-xs-6 col-md-2">
                                                    <span class="sub-total"><?php echo e(currency_formatter($invoice->sub_total, '')); ?></span>
                                                </p>
                                                <input type="hidden" class="sub-total-field" name="sub_total" value="<?php echo e($invoice->sub_total); ?>">
                                            </div>

                                            <div class="row">
                                                <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                                    <?php echo app('translator')->get('modules.invoices.discount'); ?>
                                                </div>
                                                <div class="form-group col-xs-6 col-md-1" >
                                                    <input type="number" min="0" value="<?php echo e($invoice->discount); ?>" name="discount_value" class="form-control discount_value" >
                                                </div>
                                                <div class="form-group col-xs-6 col-md-1" >
                                                    <select class="form-control" name="discount_type" id="discount_type">
                                                        <option
                                                                <?php if($invoice->discount_type == 'percent'): ?> selected <?php endif; ?>
                                                                value="percent">%</option>
                                                        <option
                                                                <?php if($invoice->discount_type == 'fixed'): ?> selected <?php endif; ?>
                                                        value="fixed"><?php echo app('translator')->get('modules.invoices.amount'); ?></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row m-t-5" id="invoice-taxes">
                                                <div class="col-md-offset-9 col-xs-6 col-md-1 text-right p-t-10">
                                                    <?php echo app('translator')->get('modules.invoices.tax'); ?>
                                                </div>

                                                <p class="form-control-static col-xs-6 col-md-2" >
                                                    <span class="tax-percent">0</span>
                                                </p>
                                            </div>

                                            <div class="row m-t-5 font-bold">
                                                <div class="col-md-offset-9 col-md-1 col-xs-6 text-right p-t-10"><?php echo app('translator')->get('modules.invoices.total'); ?></div>

                                                <p class="form-control-static col-xs-6 col-md-2">
                                                    <span class="total"><?php echo e(currency_formatter($invoice->total, '')); ?></span>
                                                </p>


                                                <input type="hidden" class="total-field" name="total" value="<?php echo e(round($invoice->total, 2)); ?>">
                                            </div>

                                        </div>

                                    <?php if($invoice->signature): ?>
                                        <div>
                                            <h2 class="name" style="margin-bottom: 20px;"><?php echo app('translator')->get('modules.estimates.signature'); ?></h2>
                                            <img src="<?php echo e($invoice->signature->signature); ?>" style="width:250px">

                                            <p><?php echo e(ucwords($invoice->signature->full_name)); ?></p>
                                        </div>
                                    <?php endif; ?>

                                    <?php if($invoice->client_comment): ?>
                                        <hr>
                                        <div class="col-md-12">
                                            <h4 class="name" style="margin-bottom: 20px;"><?php echo app('translator')->get('app.comment'); ?></h4>
                                            <p> <?php echo e($invoice->client_comment); ?> </p>
                                        </div>
                                    <?php endif; ?>

                                </div>
                            </div>
                            <div class="form-actions" style="margin-top: 70px">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button type="button" id="save-form" class="btn btn-success"><i
                                                    class="fa fa-check"></i> <?php echo app('translator')->get('app.save'); ?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

    
    <div class="modal fade bs-modal-md in" id="taxModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    
<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
    <script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.min.js')); ?>"></script>
    <script>
        $(function () {
            $( "#sortable" ).sortable();
        });
    
        $(".select2").select2({
            formatNoMatches: function () {
                return "<?php echo e(__('messages.noRecordFound')); ?>";
            }
        });
    
        jQuery('#invoice_date, #due_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: '<?php echo e($global->date_picker_format); ?>',
        });
    
        $('#save-form').click(function(){
            calculateTotal();
    
            var discount = $('.discount-amount').html();
            var total = $('.total-field').val();
    
            if(parseFloat(discount) > parseFloat(total)){
                $.toast({
                    heading: 'Error',
                    text: 'Discount cannot be more than total amount.',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
                return false;
            }
    
            $.easyAjax({
                url:'<?php echo e(route('member.all-invoices.store')); ?>',
                container:'#storePayments',
                type: "POST",
                redirect: true,
                data:$('#storePayments').serialize()
            })
        });
    
        $('#add-item').click(function () {
            var i = $(document).find('.item_name').length;
            var item = '<div class="col-xs-12 item-row margin-top-5" id="sortable_item">'+
                        '<div class="col-md-3">'+
                        '<div class="row">'+
                        '<div class="form-group">'+
                        '<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.item'); ?></label>'+
                        '<div class="input-group">'+
                        '<div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>'+
                        '<input type="text" class="form-control item_name" name="item_name[]" readonly>'+
                        +'<input type="hidden" class="item_id" name="item_id[]">'
                        '</div>'+
                        '</div>'+
    
                        '<div class="form-group">'+
                        '<textarea name="item_summary[]" class="form-control item_summary" placeholder="<?php echo app('translator')->get('app.description'); ?>" rows="2"></textarea>'+
                        '</div>'+
    
                        '</div>'+
    
                        '</div>'+
    
                        // '<div class="col-md-1">'+
                        // '<div class="form-group">'+
                        // '<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.hsnSacCode'); ?></label>'+
                        // '<input type="text"  class="form-control" name="hsn_sac_code[]" >'+
                        // '</div>'+
                        // '</div>'+
    
                        '<div class="col-md-1">'+
    
                        '<div class="form-group">'+
                        '<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.qty'); ?></label>'+
                        '<input type="number" min="1" class="form-control quantity" value="1" name="quantity[]" >'+
                        '</div>'+
    
    
                        '</div>'+
    
                        '<div class="col-md-1">'+
                        '<div class="row">'+
                        '<div class="form-group">'+
                        '<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.unitPrice'); ?></label>'+
                        '<input type="text"  class="form-control cost_per_item" name="cost_per_item[]" value="0" readonly>'+
                        '</div>'+
                        '</div>'+
    
                        '</div>'+
    
                        +'<div class="col-md-1">'
                        +'<div class="form-group">'
                        +'<select class="form-control line_discount_type" name="line_discount_type[]">'
                        +'<option value="percent">%</option>'
                        +'<option value="fixed"><?php echo app('translator')->get('modules.invoices.amount'); ?></option>'
                        +'</select>'
                        +'</div>'
                        +'</div>'
    
                        +'<div class="col-md-2">'
                        +'<div class="form-group">'
                        +'<input type="number" min="0" value="0" name="line_discount_value[]" class="form-control line_discount_value">'
                        +'</div>'
                        +'</div>'
    
                        // '<div class="col-md-2">'+
                        // '<div class="form-group">'
                        // +'<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.tax'); ?></label>'
                        // +'<select id="multiselect'+i+'" name="taxes['+i+'][]"  multiple="multiple" class="selectpicker form-control type">'
                        //     <?php $__currentLoopData = $taxes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tax): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        // +'<option data-rate="<?php echo e($tax->rate_percent); ?>" value="<?php echo e($tax->id); ?>"><?php echo e($tax->tax_name.': '.$tax->rate_percent); ?>%</option>'
                        //     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        // +'</select>'
                        // +'</div>'+
                        // '</div>'+
    
                        '<div class="col-md-2 border-dark  text-center">'+
                            '<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.amount'); ?></label>'+
    
                            '<p class="form-control-static"><span class="amount-html">0.00</span></p>'+
                        '<input type="hidden" class="amount" name="amount[]">'+
                            '</div>'+
    
                            '<div class="col-md-1 text-right visible-md visible-lg">'+
                            '<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'+
                        '</div>'+
                        '<div class="col-xs-12 text-center hidden-md hidden-lg">'+
                            '<div class="row">'+
                            '<button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i></button>'+
                        '</div>'+
                        '</div>'+
    
                        '</div>';
    
            $(item).hide().appendTo("#sortable").fadeIn(500);
            $('#multiselect'+i).selectpicker();
            hsnSacColumn();
        });
    
        hsnSacColumn();
        function hsnSacColumn(){
            <?php if($invoiceSetting->hsn_sac_code_show): ?>
            $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').removeClass( "col-md-4");
            $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').addClass( "col-md-3");
            $('input[name="hsn_sac_code[]"]').parent("div").parent('div').show();
            <?php else: ?>
            $('input[name="hsn_sac_code[]"]').parent("div").parent('div').hide();
            $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').removeClass( "col-md-3");
            $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').addClass( "col-md-4");
            <?php endif; ?>
        }
    
        $('#storePayments').on('click','.remove-item', function () {
            $(this).closest('.item-row').fadeOut(300, function() {
                $(this).remove();
                calculateTotal();
            });
        });
    
        // $('#storePayments').on('keyup','.quantity, .cost_per_item, .item_name, .discount_value', function () {
        //     var quantity = $(this).closest('.item-row').find('.quantity').val();
    
        //     var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();
    
        //     var amount = (quantity*perItemCost);
    
        //     $(this).closest('.item-row').find('.amount').val(amount);
        //     $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));
    
        //     calculateTotal();
        // });
    
        $('#storePayments').on('keyup change','.quantity,.cost_per_item,.item_name, .discount_value, .line_discount_value', function () {
              
              var quantity = $(this).closest('.item-row').find('.quantity').val();
    
              var line_discount_value = $(this).closest('.item-row').find('.line_discount_value').val();
    
              var line_discount_type = $(this).closest('.item-row').find('.line_discount_type').val();
    
              var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();
    
              if(line_discount_type == 'percent'){
                  line_discounted_amount = parseFloat(perItemCost)*parseFloat(line_discount_value?line_discount_value:0) / 100;
              }
              else{
                  line_discounted_amount = parseFloat(line_discount_value?line_discount_value:0);
              }
    
              var amount = (quantity* (perItemCost - line_discounted_amount));
    
              $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
              $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));
    
              calculateTotal();
        });
    
        $('#storePayments').on('change','.line_discount_type', function () {
              var quantity = $(this).closest('.item-row').find('.quantity').val();
    
              var line_discount_value = $(this).closest('.item-row').find('.line_discount_value').val();
    
              var line_discount_type = $(this).closest('.item-row').find('.line_discount_type').val();
    
              var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();
    
              if(line_discount_type == 'percent'){
                  line_discounted_amount = parseFloat(perItemCost)*parseFloat(line_discount_value?line_discount_value:0) / 100;
              }
              else{
                  line_discounted_amount = parseFloat(line_discount_value?line_discount_value:0);
              }
    
              var amount = (quantity* (perItemCost - line_discounted_amount));
    
              $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
              $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));
    
              calculateTotal();
    
        });
    
    
        $('#storePayments').on('change','.type, #discount_type', function () {
            var quantity = $(this).closest('.item-row').find('.quantity').val();
    
            var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();
    
            var amount = (quantity*perItemCost);
    
            $(this).closest('.item-row').find('.amount').val(amount);
            $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));
    
            calculateTotal();
        });
    
        function calculateTotal()
        {
            var subtotal = 0;
            var discount = 0;
            var tax = '';
            var taxList = new Object();
            var taxTotal = 0;
            var discountType = $('#discount_type').val();
            var discountValue = $('.discount_value').val();
    
            $(".quantity").each(function (index, element) {
                var itemTax = [];
                var itemTaxName = [];
                var discountedAmount = 0;
    
                $(this).closest('.item-row').find('select.type option:selected').each(function (index) {
                    itemTax[index] = $(this).data('rate');
                    itemTaxName[index] = $(this).text();
                });
                var itemTaxId = $(this).closest('.item-row').find('select.type').val();
    
                var amount = parseFloat($(this).closest('.item-row').find('.amount').val());
                if(discountType == 'percent' && discountValue != ''){
                    discountedAmount = parseFloat(amount - ((parseFloat(amount)/100)*parseFloat(discountValue)));
                }
                else{
                    discountedAmount = parseFloat(amount - (parseFloat(discountValue)));
                }
    
                if(isNaN(amount)){ amount = 0; }
    
                subtotal = (parseFloat(subtotal)+parseFloat(amount)).toFixed(2);
    
                if(itemTaxId != ''){
                    for(var i = 0; i<=itemTaxName.length; i++)
                    {
                        if(typeof (taxList[itemTaxName[i]]) === 'undefined'){
                            if (discountedAmount > 0) {
                                taxList[itemTaxName[i]] = ((parseFloat(itemTax[i])/100)*parseFloat(discountedAmount));                         
                            } else {
                                taxList[itemTaxName[i]] = ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                            }
                        }
                        else{
                            if (discountedAmount > 0) {
                                taxList[itemTaxName[i]] = parseFloat(taxList[itemTaxName[i]]) + ((parseFloat(itemTax[i])/100)*parseFloat(discountedAmount));   
                                console.log(taxList[itemTaxName[i]]);
                            
                            } else {
                                taxList[itemTaxName[i]] = parseFloat(taxList[itemTaxName[i]]) + ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                            }
                        }
                    }
                }
            });
    
    
            $.each( taxList, function( key, value ) {
                if(!isNaN(value)){
                    tax = tax+'<div class="col-md-offset-8 col-md-2 text-right p-t-10">'
                        +key
                        +'</div>'
                        +'<p class="form-control-static col-xs-6 col-md-2" >'
                        +'<span class="tax-percent">'+(decimalupto2(value)).toFixed(2)+'</span>'
                        +'</p>';
                    taxTotal = taxTotal+decimalupto2(value);
                }
            });
    
            if(isNaN(subtotal)){  subtotal = 0; }
    
            $('.sub-total').html(decimalupto2(subtotal).toFixed(2));
            $('.sub-total-field').val(decimalupto2(subtotal));
    
            
    
            if(discountValue != ''){
                if(discountType == 'percent'){
                    discount = ((parseFloat(subtotal)/100)*parseFloat(discountValue));
                }
                else{
                    discount = parseFloat(discountValue);
                }
    
            }
    
            $('#invoice-taxes').html(tax);
    
            var totalAfterDiscount = decimalupto2(subtotal-discount);
    
            totalAfterDiscount = (totalAfterDiscount < 0) ? 0 : totalAfterDiscount;
    
            var total = decimalupto2(totalAfterDiscount+taxTotal);
    
            $('.total').html(total.toFixed(2));
            $('.total-field').val(total.toFixed(2));
    
        }
        calculateTotal();
    
        function recurringPayment() {
            var recurring = $('#recurring_payment').val();
    
            if(recurring == 'yes')
            {
                $('.recurringPayment').show().fadeIn(300);
            } else {
                $('.recurringPayment').hide().fadeOut(300);
            }
        }
    
        $('#tax-settings').click(function () {
            var url = '<?php echo e(route('admin.taxes.create')); ?>';
            $('#modelHeading').html('Manage Project Category');
            $.ajaxModal('#taxModal', url);
        });
    
        function decimalupto2(num) {
            var amt =  Math.round(num * 100) / 100;
            return parseFloat(amt.toFixed(2));
        }
    

    
    </script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.member-app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/member/contracts/convert_contract_to_invoice.blade.php ENDPATH**/ ?>