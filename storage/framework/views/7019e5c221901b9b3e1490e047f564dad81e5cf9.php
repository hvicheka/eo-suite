<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('super-admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li class="active"><?php echo e(__($pageTitle)); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.css')); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"><?php echo e(__($pageTitle)); ?></div>

                <div class="vtabs customvtab m-t-10">

                    <?php echo $__env->make('sections.super_admin_setting_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                    <div class="tab-content">
                        <div id="vhome3" class="tab-pane active">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12 ">
                                            <?php echo Form::open(['id'=>'updateSettings','class'=>'ajax-form','method'=>'PUT']); ?>

                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <h3 class="box-title text-success"><?php echo app('translator')->get('app.socialAuthSettings.google'); ?></h3>
                                                        <hr class="m-t-0 m-b-20">
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label><?php echo app('translator')->get('app.socialAuthSettings.googleClientId'); ?></label>
                                                            <input type="text" name="google_client_id" id="google_client_id"
                                                                   class="form-control" value="<?php echo e($credentials->google_client_id); ?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label><?php echo app('translator')->get('app.socialAuthSettings.googleSecret'); ?></label>
                                                            <input type="password" name="google_secret_id"
                                                                   id="google_secret_id"
                                                                   class="form-control"
                                                                   value="<?php echo e($credentials->google_secret_id); ?>">
                                                            <span class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                        </div>
                                                    </div>

                                                    <!--/span-->

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label" ><?php echo app('translator')->get('app.status'); ?></label>
                                                            <div class="switchery-demo">
                                                                <input
                                                                        type="checkbox"
                                                                        data-type-name="google"
                                                                        name="google_status"
                                                                        <?php if($credentials->google_status == 'enable'): ?> checked <?php endif; ?>
                                                                        class="js-switch special" id="googleButton"
                                                                        data-color="#00c292"
                                                                        data-secondary-color="#f96262"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="mail_from_name"><?php echo app('translator')->get('app.callback'); ?></label>
                                                            <p class="text-bold"><?php echo e(route('social.login-callback', 'google')); ?></p>
                                                            <p class="text-info">(<?php echo app('translator')->get('messages.addGoogleCallback'); ?>)</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 m-t-20">
                                                        <h3 class="box-title text-warning"><?php echo app('translator')->get('app.socialAuthSettings.facebook'); ?></h3>
                                                        <hr class="m-t-0 m-b-20">
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label><?php echo app('translator')->get('app.socialAuthSettings.facebookClientId'); ?></label>
                                                            <input type="text" name="facebook_client_id" id="facebook_client_id"
                                                                   class="form-control" value="<?php echo e($credentials->facebook_client_id); ?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label><?php echo app('translator')->get('app.socialAuthSettings.facebookSecret'); ?></label>
                                                            <input type="password" name="facebook_secret_id"
                                                                   id="facebook_secret_id"
                                                                   class="form-control"
                                                                   value="<?php echo e($credentials->facebook_secret_id); ?>">
                                                            <span class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                        </div>
                                                    </div>

                                                    <!--/span-->

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label" ><?php echo app('translator')->get('app.status'); ?></label>
                                                            <div class="switchery-demo">
                                                                <input
                                                                        type="checkbox"
                                                                        data-type-name="facebook"
                                                                        name="facebook_status"
                                                                        <?php if($credentials->facebook_status == 'enable'): ?> checked <?php endif; ?>
                                                                        class="js-switch special" id="facebookButton"
                                                                        data-color="#00c292"
                                                                        data-secondary-color="#f96262"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="mail_from_name"><?php echo app('translator')->get('app.callback'); ?></label>
                                                            <p class="text-bold"><?php echo e(route('social.login-callback', 'facebook')); ?></p>
                                                            <p class="text-info">(<?php echo app('translator')->get('messages.addFacebookCallback'); ?>)</p>
                                                        </div>
                                                    </div>
                                                    <!--/span-->

                                                    <div class="col-md-12 m-t-20">
                                                        <h3 class="box-title text-info"><?php echo app('translator')->get('app.socialAuthSettings.linkedin'); ?></h3>
                                                        <hr class="m-t-0 m-b-20">
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label><?php echo app('translator')->get('app.socialAuthSettings.linkedinClientId'); ?></label>
                                                            <input type="text" name="linkedin_client_id" id="linkedin_client_id"
                                                                   class="form-control" value="<?php echo e($credentials->linkedin_client_id); ?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label><?php echo app('translator')->get('app.socialAuthSettings.linkedinSecret'); ?></label>
                                                            <input type="password" name="linkedin_secret_id"
                                                                   id="linkedin_secret_id"
                                                                   class="form-control"
                                                                   value="<?php echo e($credentials->linkedin_secret_id); ?>">
                                                            <span class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                        </div>
                                                    </div>

                                                    <!--/span-->

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label" ><?php echo app('translator')->get('app.status'); ?></label>
                                                            <div class="switchery-demo">
                                                                <input
                                                                        type="checkbox"
                                                                        data-type-name="linkedin"
                                                                        name="linkedin_status"
                                                                        <?php if($credentials->linkedin_status == 'enable'): ?> checked <?php endif; ?>
                                                                        class="js-switch special" id="linkedinButton"
                                                                        data-color="#00c292"
                                                                        data-secondary-color="#f96262"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="mail_from_name"><?php echo app('translator')->get('app.callback'); ?></label>
                                                            <p class="text-bold"><?php echo e(route('social.login-callback', 'linkedin')); ?></p>
                                                            <p class="text-info">(<?php echo app('translator')->get('messages.addLinkedinCallback'); ?>)</p>
                                                        </div>
                                                    </div>
                                                    <!--/span-->

                                                    <div class="col-md-12 m-t-20">
                                                        <h3 class="box-title text-success"><?php echo app('translator')->get('app.socialAuthSettings.twitter'); ?></h3>
                                                        <hr class="m-t-0 m-b-20">
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label><?php echo app('translator')->get('app.socialAuthSettings.twitterClientId'); ?></label>
                                                            <input type="text" name="twitter_client_id" id="twitter_client_id"
                                                                   class="form-control" value="<?php echo e($credentials->twitter_client_id); ?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label><?php echo app('translator')->get('app.socialAuthSettings.twitterSecret'); ?></label>
                                                            <input type="password" name="twitter_secret_id"
                                                                   id="twitter_secret_id"
                                                                   class="form-control"
                                                                   value="<?php echo e($credentials->twitter_secret_id); ?>">
                                                            <span class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                        </div>
                                                    </div>

                                                    <!--/span-->

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label" ><?php echo app('translator')->get('app.status'); ?></label>
                                                            <div class="switchery-demo">
                                                                <input
                                                                        type="checkbox"
                                                                        data-type-name="twitter"
                                                                        name="twitter_status"
                                                                        <?php if($credentials->twitter_status == 'enable'): ?> checked <?php endif; ?>
                                                                        class="js-switch special" id="twitterButton"
                                                                        data-color="#00c292"
                                                                        data-secondary-color="#f96262"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="mail_from_name"><?php echo app('translator')->get('app.callback'); ?></label>
                                                            <p class="text-bold"><?php echo e(route('social.login-callback', 'twitter')); ?></p>
                                                            <p class="text-info">(<?php echo app('translator')->get('messages.addTwitterCallback'); ?>)</p>
                                                        </div>
                                                    </div>
                                                    <!--/span-->

                                                </div>

                                                <!--/row-->

                                            </div>
                                            <div class="form-actions m-t-20">
                                                <button type="submit" id="save-form" class="btn btn-success"><i class="fa fa-check"></i>
                                                    <?php echo app('translator')->get('app.save'); ?>
                                                </button>

                                            </div>
                                            <?php echo Form::close(); ?>

                                        </div>
                                    </div>

                                </div>
                                <!-- .row -->

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>
        <!-- .row -->


        <?php $__env->stopSection(); ?>

        <?php $__env->startPush('footer-script'); ?>
        <script src="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.js')); ?>"></script>
        <script>

            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });

            $('#save-form').click(function () {
                var url = '<?php echo e(route('super-admin.social-auth-settings.update', $credentials->id)); ?>';
                $('#method').val('PUT');
                $.easyAjax({
                    url: url,
                    type: "POST",
                    container: '#updateSettings',
                    data: $('#updateSettings').serialize()
                })
            });

        </script>
    <?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.super-admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eOsuite\resources\views/super-admin/social-login-settings/index.blade.php ENDPATH**/ ?>