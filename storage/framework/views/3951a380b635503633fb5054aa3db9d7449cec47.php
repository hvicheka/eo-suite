<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('super-admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('super-admin.companies.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.addNew'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> <?php echo app('translator')->get('app.add'); ?> <?php echo app('translator')->get('app.company'); ?></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">

                    <div class="panel-body">
                        <?php echo Form::open(['id'=>'createCompany','class'=>'ajax-form','method'=>'POST', 'enctype' => 'multipart/form-data']); ?>

                        <div class="form-body">
                            <h3 class="box-title"><?php echo app('translator')->get('app.company'); ?> <?php echo app('translator')->get('app.details'); ?></h3>
                            <hr>
                            <div class="row">
                                <?php if(module_enabled('Subdomain')): ?>

                                        <div class="col-md-<?php echo e($colClass); ?>">
                                            <div class="form-group">
                                                <label for="company_name" class="required">SubDomain</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="subdomain" name="sub_domain" id="sub_domain">
                                                    <span class = "input-group-addon">.<?php echo e(get_domain()); ?></span>
                                                </div>
                                            </div>
                                    </div>
                                <?php endif; ?>
                                    <div class="col-md-<?php echo e($colClass); ?>">
                                    <div class="form-group">
                                        <label for="company_name" class="required"><?php echo app('translator')->get('modules.accountSettings.companyName'); ?></label>
                                        <input type="text" class="form-control" id="company_name" name="company_name"
                                               value="">
                                    </div>
                                </div>
                                    <div class="col-md-<?php echo e($colClass); ?>">
                                    <div class="form-group">
                                        <label for="company_email" class="required"><?php echo app('translator')->get('modules.accountSettings.companyEmail'); ?></label>
                                        <input type="email" class="form-control" id="company_email" name="company_email"
                                               value="">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="company_phone" class="required"><?php echo app('translator')->get('modules.accountSettings.companyPhone'); ?></label>
                                        <input type="tel" class="form-control" id="company_phone" name="company_phone"
                                               value="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1"><?php echo app('translator')->get('modules.accountSettings.companyWebsite'); ?></label>
                                        <input type="text" class="form-control" id="website" name="website"
                                               value="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1"><?php echo app('translator')->get('modules.accountSettings.companyLogo'); ?></label>

                                        <div class="col-xs-12">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail"
                                                     style="width: 250px; height: 80px;">

                                                    <img src="<?php echo e($global->logo_url); ?>" alt=""/>
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail"
                                                     style="width: 250px; height: 80px;"></div>
                                                <div>
                                <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> <?php echo app('translator')->get('app.selectImage'); ?> </span>
                                    <span class="fileinput-exists"> <?php echo app('translator')->get('app.change'); ?> </span>
                                    <input type="file" name="logo" id="logo"> </span>
                                                    <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                       data-dismiss="fileinput"> <?php echo app('translator')->get('app.remove'); ?> </a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address" class="required"><?php echo app('translator')->get('modules.accountSettings.companyAddress'); ?></label>
                                        <textarea class="form-control" id="address" rows="5"
                                                  name="address"></textarea>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="address"><?php echo app('translator')->get('modules.accountSettings.defaultCurrency'); ?></label>
                                        <select name="currency_id" id="currency_id" class="form-control">
                                            <?php $__currentLoopData = $currencies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $currency): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($currency->id); ?>"><?php echo e($currency->currency_symbol.' ('.$currency->currency_code.')'); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="address"><?php echo app('translator')->get('modules.accountSettings.defaultTimezone'); ?></label>
                                        <select name="timezone" id="timezone" class="form-control select2">
                                            <?php $__currentLoopData = $timezones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tz): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option><?php echo e($tz); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="address"><?php echo app('translator')->get('modules.accountSettings.changeLanguage'); ?></label>
                                        <select name="locale" id="locale" class="form-control select2">
                                            <option value="en">English
                                            </option>
                                            <?php $__currentLoopData = $languageSettings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($language->language_code); ?>"><?php echo e($language->language_name ?? ''); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->get('app.status'); ?></label>
                                        <select name="status" id="status" class="form-control">
                                            <option value="active"><?php echo app('translator')->get('app.active'); ?></option>
                                            <option value="inactive"><?php echo app('translator')->get('app.inactive'); ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <h3 class="box-title"><?php echo app('translator')->get('app.admin'); ?> <?php echo app('translator')->get('modules.company.accountSetup'); ?></h3>
                            <hr>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email" class="required"><?php echo app('translator')->get('app.email'); ?></label>
                                        <input type="email" class="form-control" id="email" name="email"
                                               value="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email" class="required"><?php echo app('translator')->get('modules.employees.employeePassword'); ?></label>
                                        <input type="password" class="form-control" id="password" name="password"
                                               value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?php if(isset($fields)): ?>
                                    <?php $__currentLoopData = $fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label <?php if($field->required == 'yes'): ?> class="required" <?php endif; ?>><?php echo e(ucfirst($field->label)); ?></label>
                                                <?php if( $field->type == 'text'): ?>
                                                    <input type="text" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" placeholder="<?php echo e($field->label); ?>" value="<?php echo e($editUser->custom_fields_data['field_'.$field->id] ?? ''); ?>">
                                                <?php elseif($field->type == 'password'): ?>
                                                    <input type="password" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" placeholder="<?php echo e($field->label); ?>" value="<?php echo e($editUser->custom_fields_data['field_'.$field->id] ?? ''); ?>">
                                                <?php elseif($field->type == 'number'): ?>
                                                    <input type="number" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" placeholder="<?php echo e($field->label); ?>" value="<?php echo e($editUser->custom_fields_data['field_'.$field->id] ?? ''); ?>">

                                                <?php elseif($field->type == 'textarea'): ?>
                                                    <textarea name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" id="<?php echo e($field->name); ?>" cols="3"><?php echo e($editUser->custom_fields_data['field_'.$field->id] ?? ''); ?></textarea>

                                                <?php elseif($field->type == 'radio'): ?>
                                                    <div class="radio-list">
                                                        <?php $__currentLoopData = $field->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <label class="radio-inline <?php if($key == 0): ?> p-0 <?php endif; ?>">
                                                                <div class="radio radio-info">
                                                                    <input type="radio" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" id="optionsRadios<?php echo e($key.$field->id); ?>" value="<?php echo e($value); ?>" <?php if(isset($clientDetail) && $clientDetail->custom_fields_data['field_'.$field->id] == $value): ?> checked <?php elseif($key==0): ?> checked <?php endif; ?>>>
                                                                    <label for="optionsRadios<?php echo e($key.$field->id); ?>"><?php echo e($value); ?></label>
                                                                </div>
                                                            </label>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </div>
                                                <?php elseif($field->type == 'select'): ?>
                                                    <?php echo Form::select('custom_fields_data['.$field->name.'_'.$field->id.']',
                                                            $field->values,
                                                             isset($editUser)?$editUser->custom_fields_data['field_'.$field->id]:'',['class' => 'form-control gender']); ?>


                                                <?php elseif($field->type == 'checkbox'): ?>
                                                    <div class="mt-checkbox-inline custom-checkbox checkbox-<?php echo e($field->id); ?>">
                                                        <input type="hidden" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]"
                                                               id="<?php echo e($field->name.'_'.$field->id); ?>" value=" ">
                                                        <?php $__currentLoopData = $field->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <label class="mt-checkbox mt-checkbox-outline">
                                                                <input name="<?php echo e($field->name.'_'.$field->id); ?>[]"
                                                                       type="checkbox" onchange="checkboxChange('checkbox-<?php echo e($field->id); ?>', '<?php echo e($field->name.'_'.$field->id); ?>')" value="<?php echo e($value); ?>"> <?php echo e($value); ?>

                                                                <span></span>
                                                            </label>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </div>
                                                <?php elseif($field->type == 'date'): ?>
                                                    <input type="text" class="form-control form-control-inline date-picker" size="16" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]"
                                                           value="<?php echo e(isset($editUser->dob)?Carbon\Carbon::parse($editUser->dob)->format('Y-m-d'):Carbon\Carbon::now()->format($global->date_format)); ?>">
                                                <?php endif; ?>
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block"></span>

                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> <?php echo app('translator')->get('app.save'); ?></button>

                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>

    <script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "<?php echo e(__('messages.noRecordFound')); ?>";
            }
        });

        $('#save-form').click(function () {

            $.easyAjax({
                url: '<?php echo e(route('super-admin.companies.store')); ?>',
                container: '#createCompany',
                type: "POST",
                redirect: true,
                file: true,
            });
        });
    </script>

<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.super-admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eOsuite\resources\views/super-admin/companies/create.blade.php ENDPATH**/ ?>