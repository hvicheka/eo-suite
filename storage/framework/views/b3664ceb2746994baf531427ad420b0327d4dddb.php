<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="icon-pencil"></i> <?php echo app('translator')->get('app.edit'); ?> <?php echo app('translator')->get('app.menu.leaves'); ?></h4>
</div>
<div class="modal-body">
    <?php echo Form::open(['id'=>'createLeave','class'=>'ajax-form','method'=>'PUT']); ?>

    <div class="form-body">
        <div class="row">
            <div class="col-md-12 ">
                <div class="form-group">
                    <label><?php echo app('translator')->get('modules.messages.chooseMember'); ?></label>
                    <select class="select2 form-control" data-placeholder="<?php echo app('translator')->get('modules.messages.chooseMember'); ?>" id="user_id" name="user_id">
                        <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option
                                    <?php if($leave->user_id == $employee->id): ?> selected <?php endif; ?>
                            value="<?php echo e($employee->id); ?>"><?php echo e(ucwords($employee->name)); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
            </div>

            <!--/span-->
        </div>
        <div class="row">

            <div class="col-md-12 ">
                <div class="form-group">
                    <label class="control-label"><?php echo app('translator')->get('modules.leaves.leaveType'); ?></label>
                    <select class="form-control" name="leave_type_id" id="leave_type_id"
                            data-style="form-control">
                        <?php $__empty_1 = true; $__currentLoopData = $leaveTypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $leaveType): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                            <option
                                    <?php if($leave->leave_type_id == $leaveType->id): ?> selected <?php endif; ?>
                            value="<?php echo e($leaveType->id); ?>"><?php echo e(ucwords($leaveType->type_name)); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                            <option value=""><?php echo app('translator')->get('messages.noLeaveTypeAdded'); ?></option>
                        <?php endif; ?>
                    </select>
                </div>
            </div>

        </div>
        <!--/row-->

        <div class="row">
            <div class="col-md-6">
                <label><?php echo app('translator')->get('app.date'); ?></label>
                <div class="form-group">
                    <input type="text" class="form-control" name="leave_date" id="single_date" value="<?php echo e($leave->leave_date->format($global->date_format)); ?>">
                </div>
            </div>

        </div>
        <!--/span-->

        <div class="row">
            <div class="col-xs-12">
                <label><?php echo app('translator')->get('modules.leaves.reason'); ?></label>
                <div class="form-group">
                    <textarea name="reason" id="reason" class="form-control" cols="30" rows="5"><?php echo $leave->reason; ?></textarea>
                </div>
            </div>

            <div class="col-xs-12">
                <label><?php echo app('translator')->get('app.status'); ?></label>
                <div class="form-group">
                    <select class="form-control" data-style="form-control" name="status" id="status" >
                        <option
                                <?php if($leave->status == 'approved'): ?> selected <?php endif; ?>
                        value="approved">Approved</option>
                        <option
                                <?php if($leave->status == 'pending'): ?> selected <?php endif; ?>
                        value="pending">Pending</option>
                        <option
                                <?php if($leave->status == 'rejected'): ?> selected <?php endif; ?>
                        value="rejected">Rejected</option>
                    </select>
                </div>
            </div>

        </div>


    </div>
    <?php echo Form::close(); ?>


</div>
<div class="modal-footer">
    <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-success save-leave waves-effect waves-light"><?php echo app('translator')->get('app.update'); ?></button>
</div>

<script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<script>

    $("#createLeave .select2").select2({
        formatNoMatches: function () {
            return "<?php echo e(__('messages.noRecordFound')); ?>";
        }
    });

    jQuery('#single_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'<?php echo e($global->week_start); ?>',
        format: '<?php echo e($global->date_picker_format); ?>',
    });

    setMinDate($('#user_id').val());

    $('#user_id').on('change', function (e) {
        setMinDate(e.target.value)
    });

    function setMinDate(employeeID){
        var employees = <?php echo json_encode($employees, 15, 512) ?>;
        var employee = employees.filter(function (item) {
            return item.id == employeeID;
        });
        var minDate = new Date(employee[0].employee_detail.joining_date);

        $('#single_date').datepicker('setStartDate', minDate);
    }

    $('#createLeave').on('click', '#addLeaveType', function () {
        var url = '<?php echo e(route('admin.leaveType.create')); ?>';
        $('#modelHeading').html('Manage Leave Type');
        $.ajaxModal('#projectCategoryModal', url);
    })

    $('.save-leave').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.leaves.update', $leave->id)); ?>',
            container: '#createLeave',
            type: "POST",
            redirect: true,
            data: $('#createLeave').serialize()
        })
    });
</script><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/leaves/edit.blade.php ENDPATH**/ ?>