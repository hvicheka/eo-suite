<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.clients.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.edit'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.css')); ?>">

<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> <?php echo app('translator')->get('modules.client.updateTitle'); ?>
                    [ <?php echo e($clientDetail->name); ?> ]
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <?php echo Form::open(['id'=>'updateClient','class'=>'ajax-form','method'=>'PUT']); ?>

                        <div class="form-body">

                            <h3 class="box-title "><?php echo app('translator')->get('modules.client.clientDetails'); ?></h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->get('modules.client.clientName'); ?></label>
                                        <input type="text" name="name" id="name" class="form-control" value="<?php echo e($clientDetail->name); ?>">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->get('modules.client.clientEmail'); ?></label>
                                        <input type="email" name="email" id="email" class="form-control" value="<?php echo e($clientDetail->email); ?>">
                                        <span class="help-block"><?php echo app('translator')->get('modules.client.emailNote'); ?></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="required"><?php echo app('translator')->get('modules.employees.employeePassword'); ?></label>
                                        <input type="password" style="display: none">
                                        <input type="password" name="password" id="password" class="form-control" autocomplete="nope">
                                        <span class="help-block"> <?php echo app('translator')->get('modules.client.passwordUpdateNote'); ?></span>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                            <h3 class="box-title m-t-20"><?php echo app('translator')->get('modules.client.companyDetails'); ?></h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.client.companyName'); ?></label>
                                        <input type="text" id="company_name" name="company_name" class="form-control"  value="<?php echo e($clientDetail->company_name ?? ''); ?>">
                                    </div>
                                </div>
                                <!--/span-->
                                <?php
                                    $website = explode("://",$clientDetail->website);
                                ?>

                                <div class="col-md-1 ">
                                    <div class="form-group salutation" style="margin-top: 23px">
                                    <select name="hyper_text" id="hyper_text" class="form-control">
                                        <option value="">--</option>
                                        <option value="http://"  <?php if(isset($website) && $website[0] == 'http' ): ?> selected <?php endif; ?>  >http://</option>
                                        <option value="https://"<?php if(isset($website) && $website[0] == 'https' ): ?> selected <?php endif; ?>  >https://</option>
                                    </select>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.client.website'); ?></label>
                                        <input type="text" id="website" name="website" class="form-control" value="<?php echo e($clientWebsite ? $clientWebsite :''); ?>" >
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('app.address'); ?></label>
                                        <textarea name="address"  id="address"  rows="5" class="form-control"><?php echo e($clientDetail->address ?? ''); ?></textarea>
                                    </div>
                                </div>
                                <!--/span-->

                            </div>
                            <div class="row">
                                    <div class="col-md-3 ">
                                        <label><?php echo app('translator')->get('app.mobile'); ?></label>
                                        <?php $code = explode(" ",$clientDetail->mobile); ?>
                                        <div class="form-group">
                                           
                                        <select class="select2 phone_country_code form-control" name="phone_code">
                                            <option value="">--</option>
                                            <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option <?php if($code[0]!= "" && $item->phonecode == $code[0]): ?>
                                                    selected
                                                <?php endif; ?>
                                                value="<?php echo e($item->id); ?>">+<?php echo e($item->phonecode.' ('.$item->iso.')'); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <input type="tel" name="mobile" id="mobile" class="mobile" autocomplete="nope" value="<?php if(isset($code[1])): ?><?php echo e($code[1]); ?><?php endif; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3 ">
                                        <div class="form-group">
                                            <label><?php echo app('translator')->get('modules.clients.officePhoneNumber'); ?></label>
                                            <input type="text" name="office_phone" id="office_phone" value="<?php echo e($clientDetail->office_phone); ?>"  class="form-control">
                                        </div>
                                    </div>
                                <div class="col-md-3 ">
                                        <div class="form-group">
                                            <label><?php echo app('translator')->get('modules.stripeCustomerAddress.city'); ?></label>
                                            <input type="text" name="city" id="city" value="<?php echo e($clientDetail->city); ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3 ">
                                        <div class="form-group">
                                            <label><?php echo app('translator')->get('modules.stripeCustomerAddress.state'); ?></label>
                                            <input type="text" name="state" id="state" value="<?php echo e($clientDetail->state); ?>"  class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for=""><?php echo app('translator')->get('modules.clients.country'); ?></label>
                                            <select class="select2 form-control"  id="country_id" name="country_id">
                                                <?php $__empty_1 = true; $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                <option <?php if( $country->id == $clientDetail->country_id): ?> selected <?php endif; ?> value="<?php echo e($country->id); ?>"><?php echo e($country->nicename); ?></option>
                                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                <option value=""><?php echo app('translator')->get('messages.noCategoryAdded'); ?></option>
                                                 <?php endif; ?>   
                                            </select>
                                        </div>
                                    </div>
                                <div class="col-md-3 ">
                                        <div class="form-group">
                                            <label><?php echo app('translator')->get('modules.stripeCustomerAddress.postalCode'); ?></label>
                                            <input type="text" name="postal_code" id="postalCode"  value="<?php echo e($clientDetail->postal_code); ?>"  class="form-control">
                                        </div>
                                 </div>
                                <div class="col-md-3">
                                    <div class="form-group" style="margin-bottom: -3px;">
                                        <label for=""><?php echo app('translator')->get('modules.clients.clientCategory'); ?>
                                             <a href="javascript:;" id="addClientCategory" class="text-info">
                                             <i class="ti-settings text-info"></i> </a>
                                        </label>
                                        <select class="select2 form-control" data-placeholder="<?php echo app('translator')->get('modules.clients.clientCategory'); ?>"  id="category_id" name="category_id">
                                         <?php $__empty_1 = true; $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                         <option <?php if( $category->id == $clientDetail->category_id): ?> selected <?php endif; ?> value="<?php echo e($category->id); ?>"><?php echo e(ucwords($category->category_name)); ?></option>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                         <option value=""><?php echo app('translator')->get('messages.noCategoryAdded'); ?></option>
                                          <?php endif; ?>                
                                         </select>
                                     </div>
                                </div> 
                            <div class="col-md-3">
                                 <div class="form-group">
                                     <label for=""><?php echo app('translator')->get('modules.clients.clientSubCategory'); ?>
                                     <a href="javascript:;" id="addClientSubCategory" class="text-info">
                                     <i class="ti-settings text-info"></i></a>
                                    </label>
                                   
                                      <select class="select2 form-control" data-placeholder="<?php echo app('translator')->get('modules.client.clientSubCategory'); ?>"  id="sub_category_id" name="sub_category_id">
                                      <?php $__empty_1 = true; $__currentLoopData = $subcategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                     
                                       <option <?php if( $subcategory->id == $clientDetail->sub_category_id): ?> selected <?php endif; ?> value="<?php echo e($subcategory->id); ?>"><?php echo e(ucwords($subcategory->category_name)); ?></option>
                                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                         <option value=""><?php echo app('translator')->get('messages.noCategoryAdded'); ?></option>
                                        <?php endif; ?>                            
                                     </select>
                                   </div>
                              </div>
                                </div>

                            <!--/row-->
                            <h3 class="box-title m-t-20"><?php echo app('translator')->get('modules.client.clientOtherDetails'); ?></h3>
                            <hr>

                            <!--/row-->

                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Skype</label>
                                        <input type="text" name="skype" id="skype" class="form-control" value="<?php echo e($clientDetail->skype ?? ''); ?>">
                                    </div>
                                </div>
                                <!--/span-->

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Linkedin</label>
                                        <input type="text" name="linkedin" id="linkedin" class="form-control" value="<?php echo e($clientDetail->linkedin ?? ''); ?>">
                                    </div>
                                </div>
                                <!--/span-->

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Twitter</label>
                                        <input type="text" name="twitter" id="twitter" class="form-control" value="<?php echo e($clientDetail->twitter ?? ''); ?>">
                                    </div>
                                </div>
                                <!--/span-->

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Facebook</label>
                                        <input type="text" name="facebook" id="facebook" class="form-control" value="<?php echo e($clientDetail->facebook ?? ''); ?>">
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <!--row gst number-->
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="gst_number"><?php echo app('translator')->get('app.gstNumber'); ?></label>
                                        <input type="text" id="gst_number" name="gst_number" class="form-control" value="<?php echo e($clientDetail->gst_number ?? ''); ?>">
                                    </div>
                                </div>
                                
                                <!--/span-->

                             
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="m-b-10">
                                            <label class="control-label"><?php echo app('translator')->get('modules.emailSettings.emailNotifications'); ?></label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" 
                                            <?php if($clientDetail->email_notifications): ?>
                                                checked
                                            <?php endif; ?>
                                            name="email_notifications" id="email_notifications1" value="1">
                                            <label for="email_notifications1" class="">
                                                <?php echo app('translator')->get('app.enable'); ?> </label>
    
                                        </div>
                                        <div class="radio radio-inline ">
                                            <input type="radio" name="email_notifications"
                                            <?php if(!$clientDetail->email_notifications): ?>
                                                checked
                                            <?php endif; ?>
    
                                                   id="email_notifications2" value="0">
                                            <label for="email_notifications2" class="">
                                                <?php echo app('translator')->get('app.disable'); ?> </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="address"><?php echo app('translator')->get('modules.accountSettings.changeLanguage'); ?></label>
                                            <select name="locale" id="locale" class="form-control select2">
                                            <option value="en" <?php if($userDetail->locale == 'en'): ?> selected <?php endif; ?>>English</option>
                                                <?php $__currentLoopData = $languageSettings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($language->language_code); ?>" <?php if($userDetail->locale == $language->language_code): ?> selected <?php endif; ?> ><?php echo e($language->language_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                <label><?php echo app('translator')->get("modules.profile.profilePicture"); ?></label>
                                <div class="form-group">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <?php if(is_null($clientDetail->image)): ?>
                                                <img src="https://via.placeholder.com/200x150.png?text=<?php echo e(str_replace(' ', '+', __('modules.profile.uploadPicture'))); ?>"
                                                     alt=""/>
                                            <?php else: ?>
                                                <img src="<?php echo e(asset_url('avatar/'.$clientDetail->image)); ?>" alt=""/>
                                            <?php endif; ?>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 200px; max-height: 150px;"></div>
                                        <div>
                                <span class="btn btn-info btn-file">
                                    <span class="fileinput-new"> <?php echo app('translator')->get("app.selectImage"); ?> </span>
                                    <span class="fileinput-exists"> <?php echo app('translator')->get("app.change"); ?> </span>
                                    <input type="file" name="image" id="image"> </span>
                                            <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                               data-dismiss="fileinput"> <?php echo app('translator')->get("app.remove"); ?> </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            </div>
                            <div class="row">
                                <?php if(isset($fields)): ?>
                                    <?php $__currentLoopData = $fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-md-6">
                                            <label><?php echo e(ucfirst($field->label)); ?></label>
                                            <div class="form-group">
                                                <?php if( $field->type == 'text'): ?>
                                                    <input type="text" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" placeholder="<?php echo e($field->label); ?>" value="<?php echo e($clientDetail->custom_fields_data['field_'.$field->id] ?? ''); ?>">
                                                <?php elseif($field->type == 'password'): ?>
                                                    <input type="password" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" placeholder="<?php echo e($field->label); ?>" value="<?php echo e($clientDetail->custom_fields_data['field_'.$field->id] ?? ''); ?>">
                                                <?php elseif($field->type == 'number'): ?>
                                                    <input type="number" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" placeholder="<?php echo e($field->label); ?>" value="<?php echo e($clientDetail->custom_fields_data['field_'.$field->id] ?? ''); ?>">

                                                <?php elseif($field->type == 'textarea'): ?>
                                                    <textarea name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" id="<?php echo e($field->name); ?>" cols="3"><?php echo e($clientDetail->custom_fields_data['field_'.$field->id] ?? ''); ?></textarea>

                                                <?php elseif($field->type == 'radio'): ?>
                                                    <div class="radio-list">
                                                        <?php $__currentLoopData = $field->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <label class="radio-inline <?php if($key == 0): ?> p-0 <?php endif; ?>">
                                                                <div class="radio radio-info">
                                                                    <input type="radio" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" id="optionsRadios<?php echo e($key.$field->id); ?>" value="<?php echo e($value); ?>" <?php if(isset($clientDetail) && $clientDetail->custom_fields_data['field_'.$field->id] == $value): ?> checked <?php elseif($key==0): ?> checked <?php endif; ?>>>
                                                                    <label for="optionsRadios<?php echo e($key.$field->id); ?>"><?php echo e($value); ?></label>
                                                                </div>
                                                            </label>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </div>
                                                <?php elseif($field->type == 'select'): ?>
                                                    <?php echo Form::select('custom_fields_data['.$field->name.'_'.$field->id.']',
                                                            $field->values,
                                                             isset($clientDetail)?$clientDetail->custom_fields_data['field_'.$field->id]:'',['class' => 'form-control gender']); ?>


                                                <?php elseif($field->type == 'checkbox'): ?>
                                                <div class="mt-checkbox-inline custom-checkbox checkbox-<?php echo e($field->id); ?>">
                                                    <input type="hidden" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" 
                                                    id="<?php echo e($field->name.'_'.$field->id); ?>" value="<?php echo e($clientDetail->custom_fields_data['field_'.$field->id]); ?>">
                                                    <?php $__currentLoopData = $field->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <label class="mt-checkbox mt-checkbox-outline">
                                                            <input name="<?php echo e($field->name.'_'.$field->id); ?>[]" class="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]"
                                                                   type="checkbox" value="<?php echo e($value); ?>" onchange="checkboxChange('checkbox-<?php echo e($field->id); ?>', '<?php echo e($field->name.'_'.$field->id); ?>')"
                                                                   <?php if($clientDetail->custom_fields_data['field_'.$field->id] != '' && in_array($value ,explode(', ', $clientDetail->custom_fields_data['field_'.$field->id]))): ?> checked <?php endif; ?> > <?php echo e($value); ?>

                                                            <span></span>
                                                        </label>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </div>
                                                <?php elseif($field->type == 'date'): ?>
                                                    
                                                    <input type="text" class="form-control date-picker" size="16" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]"
                                                    value="<?php echo e(isset($clientDetail->custom_fields_data['field_'.$field->id]) ? \Carbon\Carbon::parse($clientDetail->custom_fields_data['field_'.$field->id])->format($global->date_format) : \Carbon\Carbon::now()->format($global->date_format)); ?>">
                                                <?php endif; ?>
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block"></span>

                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <label><?php echo app('translator')->get('app.shippingAddress'); ?></label>
                                    <div class="form-group">
                                        <textarea name="shipping_address" id="shipping_address" class="form-control" rows="4"><?php echo e($clientDetail->shipping_address ?? ''); ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <label><?php echo app('translator')->get('app.note'); ?></label>
                                    <div class="form-group">
                                        <textarea name="note" id="note" class="form-control summernote" rows="5"><?php echo e($clientDetail->note ?? ''); ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> <?php echo app('translator')->get('app.update'); ?></button>
                            <a href="<?php echo e(route('admin.clients.index')); ?>" class="btn btn-default"><?php echo app('translator')->get('app.back'); ?></a>
                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->
    
    <div class="modal fade bs-modal-md in" id="clientCategoryModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    
<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.min.js')); ?>"></script>

<script>
    function checkboxChange(parentClass, id){
        var checkedData = '';
        $('.'+parentClass).find("input[type= 'checkbox']:checked").each(function () {
            if(checkedData !== ''){
                checkedData = checkedData+', '+$(this).val();
            }
            else{
                checkedData = $(this).val();
            }
        });
        $('#'+id).val(checkedData);
    }

     var subCategories = <?php echo json_encode($subcategories, 15, 512) ?>;
        $('#category_id').change(function (e) {
            // get projects of selected users
            var opts = '';

            var subCategory = subCategories.filter(function (item) {
                return item.category_id == e.target.value
            });
            subCategory.forEach(project => {
                console.log(project);
            opts += `<option value='${project.id}'>${project.category_name}</option>`
        })

            $('#sub_category_id').html('<option value="">Select Sub Category...</option>'+opts)
            $("#sub_category_id").select2({
                formatNoMatches: function () {
                    return "<?php echo e(__('messages.noRecordFound')); ?>";
                }
            });
        });
    $(".date-picker").datepicker({
        todayHighlight: true,
        autoclose: true,
        weekStart:'<?php echo e($global->week_start); ?>',
        format: '<?php echo e($global->date_picker_format); ?>',
    });

     $(".select2").select2({
        formatNoMatches: function () {
            return "<?php echo e(__('messages.noRecordFound')); ?>";
        }
    });

    $('#save-form').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.clients.update', [$clientDetail->id])); ?>',
            container: '#updateClient',
            type: "POST",
            redirect: true,
            data: $('#updateClient').serialize()
        })
    });

    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });
    $('#addClientCategory').click(function () {
        var url = '<?php echo e(route('admin.clientCategory.create')); ?>';
        $('#modelHeading').html('...');
        $.ajaxModal('#clientCategoryModal', url);
    })
    $('#addClientSubCategory').click(function () {
        var url = '<?php echo e(route('admin.clientSubCategory.create')); ?>';
        $('#modelHeading').html('...');
        $.ajaxModal('#clientCategoryModal', url);
    })
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/clients/edit.blade.php ENDPATH**/ ?>