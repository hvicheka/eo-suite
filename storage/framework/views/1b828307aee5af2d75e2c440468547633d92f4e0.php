<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('super-admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li class="active"><?php echo e(__($pageTitle)); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.css')); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"><?php echo e(__($pageTitle)); ?></div>

                <div class="vtabs customvtab m-t-10">
                    <?php echo $__env->make('sections.super_admin_setting_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                    <div class="tab-content">
                        <div id="vhome3" class="tab-pane active">
                            <div class="row">
                                <div class="col-xs-12">








                                    <?php echo Form::open(['id'=>'editSlackSettings','class'=>'ajax-form','method'=>'PUT']); ?>


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label for="google_client_id" class="control-label"><?php echo app('translator')->get('app.clientId'); ?></label>
                                            <input type="text" class="form-control  form-control-lg"
                                                   id="google_client_id" name="google_client_id"
                                                   value="<?php echo e($calendarSetting->google_client_id); ?>">
                                        </div>

                                        <div class="form-group">
                                            <label for="google_client_secret" class="control-label"><?php echo app('translator')->get('app.clientSecret'); ?></label>
                                            <input type="password" class="form-control  form-control-lg"
                                                   id="google_client_secret" name="google_client_secret"
                                                   value="<?php echo e($calendarSetting->google_client_secret); ?>">
                                        </div>

                                        <div class="form-group">
                                            <label for="company_name"><?php echo app('translator')->get('app.status'); ?></label>
                                            <select name="google_calendar_status" class="form-control" id="">
                                                <option
                                                        <?php if($calendarSetting->google_calendar_status == 'inactive'): ?> selected <?php endif; ?>
                                                value="inactive"><?php echo app('translator')->get('app.inactive'); ?></option>
                                                <option
                                                        <?php if($calendarSetting->google_calendar_status == 'active'): ?> selected <?php endif; ?>
                                                value="active"><?php echo app('translator')->get('app.active'); ?></option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-actions m-t-20">
                                        <button type="submit" id="save-form"
                                                class="btn btn-success waves-effect waves-light m-r-10">
                                            <?php echo app('translator')->get('app.update'); ?>
                                        </button>
                                    </div>

                                    <?php echo Form::close(); ?>


                                </div>
                                <!-- .row -->

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>
        <!-- .row -->


        <?php $__env->stopSection(); ?>

        <?php $__env->startPush('footer-script'); ?>
        <script src="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.js')); ?>"></script>
        <script>
            $('#save-form').click(function () {
                $.easyAjax({
                    url: '<?php echo e(route('super-admin.google-calendar-settings.update', ['1'])); ?>',
                    container: '#editSlackSettings',
                    type: "POST",
                    data: $('#editSlackSettings').serialize(),
                })
            });
        </script>
    <?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.super-admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/super-admin/google-calendar-settings/index.blade.php ENDPATH**/ ?>