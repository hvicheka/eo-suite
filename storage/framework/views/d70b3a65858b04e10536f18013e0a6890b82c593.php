<div class="white-box">
    <nav>
        <ul class="showClientTabs">
            <li class="currencySetting"><a href="<?php echo e(route('admin.currency.index')); ?>"> <span><?php echo app('translator')->get('modules.currencySettings.currencySetting'); ?></span></a>
            </li>
            <li class="currencyFormatSetting"><a href="<?php echo e(route('admin.currency.currency-format')); ?>"> <span><?php echo app('translator')->get('modules.currencySettings.currenyFormatSetting'); ?></span></a>
            </li>
        </ul>
    </nav>
</div><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/currencies/tabs.blade.php ENDPATH**/ ?>