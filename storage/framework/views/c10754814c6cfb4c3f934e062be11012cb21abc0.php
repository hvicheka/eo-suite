<?php if($projectID == ''): ?>
    <select class="form-control select2" name="client_id" id="client_company_id" data-style="form-control">
        <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($client->id); ?>"><?php echo e(ucwords($client->name)); ?>

                <?php if($client->company_name != ''): ?> <?php echo e('('.$client->company_name.')'); ?> <?php endif; ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>

    
    <script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
    <script>
        $("#client_company_id").select2({
            formatNoMatches: function () {
                return "<?php echo e(__('messages.noRecordFound')); ?>";
            }
        });

        $('#client_company_id').change(function() {
            checkShippingAddress();
        });
    </script>
<?php else: ?>
    <div class="input-icon">
        <input type="text" readonly class="form-control" name="" id="company_name" value="<?php echo e($companyName); ?>">
        <input type="hidden" class="form-control" name="" id="client_id" value="<?php echo e($clientId); ?>">
    </div>
<?php endif; ?>
<?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/member/invoices/client_or_company_name.blade.php ENDPATH**/ ?>