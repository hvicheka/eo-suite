<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e($global->favicon_url); ?>">
    
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo e($global->favicon_url); ?>">
    <meta name="theme-color" content="#ffffff">

    <title><?php echo e($pageTitle); ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo e(asset('bootstrap/dist/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link rel='stylesheet prefetch'
          href='https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.5.0/css/flag-icon.min.css'>
    <link rel='stylesheet prefetch'
          href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css'>

    <!-- This is Sidebar menu CSS -->
    <link href="<?php echo e(asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css')); ?>" rel="stylesheet">

    <link href="<?php echo e(asset('plugins/bower_components/toast-master/css/jquery.toast.css')); ?>"   rel="stylesheet">
    <link href="<?php echo e(asset('plugins/bower_components/sweetalert/sweetalert.css')); ?>"   rel="stylesheet">

    <!-- This is a Animation CSS -->
    <link href="<?php echo e(asset('css/animate.css')); ?>" rel="stylesheet">

<?php echo $__env->yieldPushContent('head-script'); ?>

<!-- This is a Custom CSS -->
    <link href="<?php echo e(asset('css/style.css')); ?>" rel="stylesheet">
    <!-- color CSS you can use different color css from css/colors folder -->
    <!-- We have chosen the skin-blue (default.css) for this starter
       page. However, you can choose any other skin from folder css / colors .
       -->
    <link href="<?php echo e(asset('css/colors/default.css')); ?>" id="theme"  rel="stylesheet">
    <link href="<?php echo e(asset('plugins/froiden-helper/helper.css')); ?>"   rel="stylesheet">
    <link href="<?php echo e(asset('css/custom-new.css')); ?>"   rel="stylesheet">
    <link href="<?php echo e(asset('css/rounded.css')); ?>"   rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://js.stripe.com/v3/"></script>
    <style>
        .sidebar .notify  {
            margin: 0 !important;
        }
        .sidebar .notify .heartbit {
            top: -23px !important;
            right: -15px !important;
        }
        .sidebar .notify .point {
            top: -13px !important;
        }
        @media (max-width:991px){
            .tablet-margin{margin-top: 35px;}
        }

        .admin-logo {
            max-height: 40px;
        }

        .ribbon {
            top: 12px !important;
            left: 0px;
        }
        .signature-pad {
            position: absolute;
            left: 0;
            top: 0;
            width:100%;
            height: 100%;
            background-color: white;
        }
    </style>
</head>
<body class="fix-sidebar">
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>

<div id="wrapper" class="m-b-40">

    <!-- Left navbar-header end -->
    <!-- Page Content -->
    <div id="page-wrapper" style="margin-left: 0px !important;">
        <div class="container-fluid">

            <!-- .row -->
            <div class="row">
                <?php if($invoiceSetting->logo_url): ?>
                    <div class="col-md-offset-2 col-md-8 m-t-40 m-b-40 text-center">
                    <img src="<?php echo e($invoiceSetting->logo_url); ?>" alt="home" class="admin-logo"/>
                    </div>
                <?php endif; ?>

                <div class="col-md-offset-2 col-md-8 m-t-30 col-md-offset-2">
                    <div class="row m-b-20">
                        <div class="col-xs-12">
                            <a href="<?php echo e(route("front.download-proposal", md5($proposal->id))); ?>" class="btn btn-default pull-right m-r-10 actionButton"><i class="fa fa-file-pdf-o"></i> <?php echo app('translator')->get('app.download'); ?></a>
                            <?php if(!$proposal->signature && $proposal->status == 'waiting'): ?>
                                <button type="button" id="reject_action" class="btn btn-danger pull-right m-r-10 actionButton" onclick="sign('reject');return false;"><i class="fa fa-times"></i> <?php echo app('translator')->get('app.reject'); ?></button>

                                <button type="button" id="accept_action" class="btn btn-success pull-right m-r-10 actionButton" onclick="sign('accept');return false;"><i class="fa fa-check"></i> <?php echo app('translator')->get('app.accept'); ?></button>
                            <?php endif; ?>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="white-box printableArea ribbon-wrapper" style="background: #ffffff !important;">
                                <div class="ribbon-content p-20" id="invoice_container">
                                    <?php if($proposal->status == 'accepted'): ?>
                                        <div class="ribbon ribbon-bookmark ribbon-success"><?php echo app('translator')->get('modules.proposal.accepted'); ?></div>
                                    <?php elseif($proposal->status == 'waiting'): ?>
                                        <div class="ribbon ribbon-bookmark ribbon-warning"><?php echo app('translator')->get('app.pending'); ?></div>
                                    <?php elseif($proposal->status == 'draft'): ?>
                                        <div class="ribbon ribbon-bookmark ribbon-warning"><?php echo app('translator')->get('app.draft'); ?></div>
                                    <?php else: ?>
                                        <div class="ribbon ribbon-bookmark ribbon-danger"><?php echo app('translator')->get('app.rejected'); ?></div>
                                    <?php endif; ?>
                                    <h4 class="text-center"><b><?php echo app('translator')->get('app.proposal'); ?></b> </h4>
                                    <hr>
                                    <div class="row tablet-margin">
                                        <div class="row">
                                            <div class="col-xs-6 b-r">
                                                <strong class="clearfix"><?php echo app('translator')->get('app.name'); ?></strong> <br>
                                                <span class="text-muted"><?php echo e($proposal->lead->client_name); ?> </span> <br>
                                                <span class="text-muted"><?php echo e(ucwords($proposal->lead->company_name)); ?></span> <br>
                                                <span class="text-muted"><?php echo nl2br($proposal->lead->address); ?></span>
                                            </div>
                                            <div class="col-xs-6">
                                                <strong class="clearfix"><?php echo app('translator')->get('modules.proposal.validTill'); ?></strong> <br>
                                                <p class="text-muted"><?php echo e($proposal->valid_till->format($settings->date_format)); ?></p>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <?php if(!is_null($proposal->description)): ?>
                                                <div class="col-md-12 m-t-20" >
                                                    <p><strong><?php echo app('translator')->get('app.description'); ?></strong>: <?php echo nl2br($proposal->description); ?></p>
                                                </div>
                                              <?php endif; ?>
                                        </div>

                                        <div class="row">
                                            <?php if(count($proposal->items) > 0): ?>
                                            <div class="col-md-12">
                                                <div class="table-responsive m-t-40" style="clear: both;">
                                                    <table class="table table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th class="text-center">#</th>
                                                            <th><?php echo app('translator')->get("modules.invoices.item"); ?></th>
                                                            <?php if($invoiceSetting->hsn_sac_code_show): ?>
                                                                <th ><?php echo app('translator')->get('modules.invoices.hsnSacCode'); ?></th>
                                                            <?php endif; ?>
                                                            <th class="text-right"><?php echo app('translator')->get("modules.invoices.qty"); ?></th>
                                                            <th class="text-right"><?php echo app('translator')->get("modules.invoices.unitPrice"); ?></th>
                                                            <th class="text-right"><?php echo app('translator')->get("modules.invoices.price"); ?></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $count = 0; ?>
                                                        <?php $__currentLoopData = $proposal->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <tr>
                                                                <td class="text-center"><?php echo e(++$count); ?></td>
                                                                <td><?php echo e(ucfirst($item->item_name)); ?>

                                                                    <?php if(!is_null($item->item_summary)): ?>
                                                                        <p class="font-12"><?php echo e($item->item_summary); ?></p>
                                                                    <?php endif; ?>
                                                                </td>
                                                                <?php if($invoiceSetting->hsn_sac_code_show): ?>
                                                                    <td><?php echo e(($item->hsn_sac_code) ?? '--'); ?></td>
                                                                <?php endif; ?>
                                                                <td class="text-right"><?php echo e($item->quantity); ?></td>
                                                                <td class="text-right"> <?php echo e(currency_formatter($item->unit_price,$proposal->currency->currency_symbol)); ?> </td>
                                                                <td class="text-right"> <?php echo e(currency_formatter($item->amount,$proposal->currency->currency_symbol)); ?> </td>
                                                            </tr>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <?php endif; ?>
                                            <div class="col-md-12">
                                                <?php if(count($proposal->items) > 0): ?>
                                                <div class="pull-right m-t-30 text-right">
                                                    <p><?php echo app('translator')->get("modules.invoices.subTotal"); ?>
                                                        : <?php echo e(currency_formatter($proposal->sub_total,$proposal->currency->currency_symbol)); ?></p>

                                                    <?php if($discount > 0): ?>
                                                        <p><?php echo app('translator')->get("modules.invoices.discount"); ?>
                                                            : <?php echo e(currency_formatter($discount,$proposal->currency->currency_symbol)); ?> </p>
                                                    <?php endif; ?>
                                                    <?php $__currentLoopData = $taxes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$tax): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <p><?php echo e(strtoupper($key)); ?>

                                                            : <?php echo e(currency_formatter($tax,$proposal->currency->currency_symbol)); ?> </p>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <hr>
                                                    <h3><b><?php echo app('translator')->get("modules.invoices.total"); ?>
                                                            :</b> <?php echo e(currency_formatter($proposal->total,$proposal->currency->currency_symbol)); ?>

                                                    </h3>
                                                    <hr>
                                                </div>
                                                <?php endif; ?>
                                                <?php if(!is_null($proposal->note)): ?>
                                                    <div class="col-md-12">
                                                        <p><strong><?php echo app('translator')->get('app.note'); ?></strong>: <?php echo $proposal->note; ?> </p>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="clearfix"></div>
                                                <?php if($proposal->signature): ?>
                                                    <hr>
                                                    <div class="col-md-12" style="text-align: right;">
                                                        <h2 class="name" style="margin-bottom: 20px;"><?php echo app('translator')->get('modules.estimates.signature'); ?></h2>
                                                        <img src="<?php echo e($proposal->signature->signature); ?>" style="width:250px">

                                                        <p><?php echo e(ucwords($proposal->signature->full_name)); ?></p>
                                                    </div>
                                                <?php endif; ?>

                                                <?php if($proposal->client_comment): ?>
                                                    <hr>
                                                    <div class="col-md-12">
                                                        <h4 class="name" style="margin-bottom: 20px;"><?php echo app('translator')->get('app.comment'); ?></h4>
                                                        <p> <?php echo e($proposal->client_comment); ?> </p>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
        <!-- /.container-fluid -->

    </div>

    <div class="modal fade bs-modal-md in" id="estimateAccept" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    
                    
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<!-- jQuery -->
<script src="<?php echo e(asset('plugins/bower_components/jquery/dist/jquery.min.js')); ?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo e(asset('bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js'></script>

<!-- Sidebar menu plugin JavaScript -->
<script src="<?php echo e(asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js')); ?>"></script>
<!--Slimscroll JavaScript For custom scroll-->
<script src="<?php echo e(asset('js/jquery.slimscroll.js')); ?>"></script>
<!--Wave Effects -->
<script src="<?php echo e(asset('js/waves.js')); ?>"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo e(asset('plugins/bower_components/sweetalert/sweetalert.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/custom.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jasny-bootstrap.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/froiden-helper/helper.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/toast-master/js/jquery.toast.js')); ?>"></script>


<script src="<?php echo e(asset('js/cbpFWTabs.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/icheck/icheck.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/icheck/icheck.init.js')); ?>"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


<script>
    $(document).ready(() => {
       <?php if(!$proposal->signature && $proposal->status == 'waiting'): ?>
       <?php else: ?>
       <?php endif; ?>
        let url = location.href.replace(/\/$/, "");

        if (location.hash) {
            const hash = url.split("#");
            $('#myTab a[href="#'+hash[1]+'"]').tab("show");
            url = location.href.replace(/\/#/, "#");
            history.replaceState(null, null, url);
            setTimeout(() => {
                $(window).scrollTop(0);
            }, 400);
        }

        $('a[data-toggle="tab"]').on("click", function() {
            let newUrl;
            const hash = $(this).attr("href");
            if(hash == "#summery") {
                newUrl = url.split("#")[0];
            } else {
                newUrl = url.split("#")[0] + hash;
            }
            // newUrl += "/";
            history.replaceState(null, null, newUrl);
        });
    });

    //Accept proposal
    function sign(type) {
        $('.actionButton').prop("disabled", true);
        var allowSignature = false;
      <?php if($proposal->signature_approval == 1): ?> allowSignature = true; <?php endif; ?>
      if(allowSignature == true ||  type == 'reject')
      {
          var url = '<?php echo e(route('front.proposal-action', md5($proposal->id))); ?>?type='+type;
          $.ajaxModal('#estimateAccept', url);

      }else {
            swal({
                title: "<?php echo app('translator')->get('messages.sweetAlertTitle'); ?>",
                text: "<?php echo app('translator')->get('messages.confirmation.acceptApproval'); ?>",
                dangerMode: true,
                icon: 'warning',
                confirm: {
                    text: "<?php echo app('translator')->get('messages.approveIt'); ?>",
                    value: 'confirm',
                    visible: true,
                    className: "success",
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $.easyAjax({
                        url: '<?php echo e(route('front.proposal-action-post', md5($proposal->id))); ?>',
                        container: '#invoice_container',
                        type: "POST",
                        data: {
                            type:'accept',
                            _token: '<?php echo e(csrf_token()); ?>'
                        },
                        success: function(data){
                            if(data.status == 'success'){
                                window.location.reload();
                            }
                        }
                    })
                }
            });
        }

    }

</script>

</body>
</html>
<?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/proposal-front/proposal.blade.php ENDPATH**/ ?>