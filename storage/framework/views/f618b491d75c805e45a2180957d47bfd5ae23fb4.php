<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right bg-title-left">
            <a class="btn btn-default btn-outline btn-sm"
                href="<?php echo e(route('admin.all-invoices.download', $invoice->id)); ?>" target="__blank"> <span><i class="fa fa-file-pdf-o"></i> <?php echo app('translator')->get('modules.invoices.downloadPdf'); ?></span> </a>
            <?php if($invoice->status == 'review'): ?>
                <button type="button" data-invoice-id="<?php echo e($invoice->id); ?>" class="btn btn-primary btn-sm verify">
                    <i class="fa fa-check"></i> <?php echo app('translator')->get('app.verify'); ?>
                </button>
            <?php endif; ?>
            <button type="button" onclick="showPayments()" class="btn btn-info btn-sm">
                <?php echo app('translator')->get('app.view'); ?> <?php echo app('translator')->get('app.menu.payments'); ?>
            </button>
            <button type="button" data-clipboard-text="<?php echo e(route('front.invoice', [md5($invoice->id)])); ?>" class="btn btn-success btn-sm btn-copy">
                <i class="fa fa-copy"></i>
                <span id="copy_payment_text">
                    <?php echo app('translator')->get('modules.invoices.copyPaymentLink'); ?>
                </span>
            </button>
            <?php if($invoice->credit_notes->count() > 0): ?>
                <a href="javascript:;" onclick="showAppliedCredits('<?php echo e(route('admin.all-invoices.applied-credits', $invoice->id)); ?>')" class="btn btn-info  btn-sm">
                    <?php echo app('translator')->get('app.appliedCredits'); ?>
                </a>
            <?php endif; ?>

            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get("app.menu.home"); ?></a></li>
                <li><a href="<?php echo e(route('admin.all-invoices.index')); ?>"><?php echo app('translator')->get("app.menu.invoices"); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.invoice'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">

<style>
    .ribbon-wrapper {
        background: #ffffff !important;
    }
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12 m-t-20">
            <div class="white-box">
                <div class="col-md-4 text-center">
                    <h4><span class="text-dark"><?php echo e(currency_formatter($invoice->total,$invoice->currency->currency_symbol)); ?></span> <span class="font-12 text-muted m-l-5"> <?php echo app('translator')->get('modules.payments.totalAmount'); ?></span></h4>
                </div>

                <div class="col-md-4 text-center b-l">
                    <h4><span class="text-success"><?php echo e(currency_formatter($invoice->amountPaid(),$invoice->currency->currency_symbol)); ?></span> <span class="font-12 text-muted m-l-5"> <?php echo app('translator')->get('modules.payments.totalPaid'); ?></span></h4>
                </div>

                <div class="col-md-4 text-center b-l">
                    <h4><span class="text-danger"><?php echo e(currency_formatter($invoice->amountDue(),$invoice->currency->currency_symbol)); ?></span> <span class="font-12 text-muted m-l-5"> <?php echo app('translator')->get('modules.payments.totalDue'); ?></span></h4>
                </div>

            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12">
            <?php if($message = Session::get('success')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                   <i class="fa fa-check"></i> <?php echo $message; ?>

                </div>
                <?php Session::forget('success');?>
            <?php endif; ?>

            <?php if($message = Session::get('error')): ?>
                <div class="custom-alerts alert alert-danger fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <?php echo $message; ?>

                </div>
                <?php Session::forget('error');?>
            <?php endif; ?>


            <div class="white-box printableArea ribbon-wrapper">

                <div class="clearfix"></div>
                <div class="ribbon-content m-t-40 b-all p-20 ">
                    <?php if($invoice->credit_note): ?>
                        <div class="ribbon ribbon-bookmark ribbon-warning"><?php echo app('translator')->get('app.credit-note'); ?></div>
                    <?php else: ?>
                        <?php if($invoice->status == 'paid'): ?>
                            <div class="ribbon ribbon-bookmark ribbon-success"><?php echo app('translator')->get('modules.invoices.paid'); ?></div>
                        <?php elseif($invoice->status == 'partial'): ?>
                            <div class="ribbon ribbon-bookmark ribbon-info"><?php echo app('translator')->get('modules.invoices.partial'); ?></div>
                        <?php elseif($invoice->status == 'review'): ?>
                            <div class="ribbon ribbon-bookmark ribbon-warning"><?php echo app('translator')->get('modules.invoices.review'); ?></div>
                        <?php else: ?>
                            <div class="ribbon ribbon-bookmark ribbon-danger"><?php echo app('translator')->get('modules.invoices.unpaid'); ?></div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <h4 class="text-right"><b><?php echo app('translator')->get('app.invoice'); ?></b> <span class="pull-right m-l-10"><?php echo e($invoice->invoice_number); ?></span></h4>
                    <hr>
                    <div class="row">
                        <div class="col-xs-12">

                            <div class="pull-left">
                                <address>
                                    <h4> &nbsp;<b class="text-danger"><?php echo e(ucwords($global->company_name)); ?></b></h4>
                                    <?php if(!is_null($settings)): ?>
                                        <p class="text-muted m-l-5"><?php echo nl2br($global->address); ?></p>
                                    <?php endif; ?>
                                    <?php if($invoiceSetting->show_gst == 'yes' && !is_null($invoiceSetting->gst_number)): ?>
                                        <p class="text-muted m-l-5"><b><?php echo app('translator')->get('app.gstIn'); ?>
                                                :</b><?php echo e($invoiceSetting->gst_number); ?></p>
                                    <?php endif; ?>
                                </address>
                            </div>
                            <div class="pull-right text-right">
                                <address>
                                    <?php if(!is_null($invoice->project_id) && !is_null($invoice->project->client)): ?>
                                        <h3><?php echo app('translator')->get('modules.invoices.to'); ?></h3>
                                        <h4 class="font-bold"><?php echo e(ucwords($invoice->project->client->name)); ?></h4>
                                        <p class="m-l-30">
                                            <b><?php echo app('translator')->get('app.address'); ?> :</b>
                                            <span class="text-muted">
                                                <?php echo nl2br($invoice->project->client->address); ?>

                                            </span>
                                        </p>
                                        <?php if($invoice->show_shipping_address === 'yes'): ?>
                                            <p class="m-t-5">
                                                <b><?php echo app('translator')->get('app.shippingAddress'); ?> :</b>
                                                <span class="text-muted">
                                                    <?php echo nl2br($invoice->project->client->shipping_address); ?>

                                                </span>
                                            </p>
                                        <?php endif; ?>
                                        <?php if($invoiceSetting->show_gst == 'yes' && !is_null($invoice->project->client->gst_number)): ?>
                                            <p class="m-t-5"><b><?php echo app('translator')->get('app.gstIn'); ?>
                                                    :</b>  <?php echo e($invoice->project->client->gst_number); ?>

                                            </p>
                                        <?php endif; ?>
                                    <?php elseif(!is_null($invoice->client_id)): ?>
                                        <h3><?php echo app('translator')->get('modules.invoices.to'); ?>,</h3>
                                        <h4 class="font-bold"><?php echo e(ucwords($invoice->clientdetails->name)); ?></h4>
                                        <p class="m-l-30">
                                            <b><?php echo app('translator')->get('app.address'); ?> :</b>
                                            <span class="text-muted">
                                                <?php echo nl2br($invoice->clientdetails->address); ?>

                                            </span>
                                        </p>
                                        <?php if($invoice->show_shipping_address === 'yes'): ?>
                                            <p class="m-t-5">
                                                <b><?php echo app('translator')->get('app.shippingAddress'); ?> :</b>
                                                <span class="text-muted">
                                                    <?php echo nl2br($invoice->clientdetails->shipping_address); ?>

                                                </span>
                                            </p>
                                        <?php endif; ?>
                                        <?php if($invoiceSetting->show_gst == 'yes' && !is_null($invoice->clientdetails->gst_number)): ?>
                                            <p class="m-t-5"><b><?php echo app('translator')->get('app.gstIn'); ?>
                                                    :</b>  <?php echo e($invoice->clientdetails->gst_number); ?>

                                            </p>
                                        <?php endif; ?>
                                    <?php endif; ?>

                                    <p class="m-t-30"><b><?php echo app('translator')->get('app.invoice'); ?> <?php echo app('translator')->get('app.date'); ?> :</b> <i
                                                class="fa fa-calendar"></i> <?php echo e($invoice->issue_date->format($global->date_format)); ?>

                                    </p>

                                    <p><b><?php echo app('translator')->get('app.dueDate'); ?> :</b> <i
                                                class="fa fa-calendar"></i> <?php echo e($invoice->due_date->format($global->date_format)); ?>

                                    </p>
                                    <?php if($invoice->recurring == 'yes'): ?>
                                        <p><b class="text-danger"><?php echo app('translator')->get('modules.invoices.billingFrequency'); ?> : </b> <?php echo e($invoice->billing_interval . ' '. ucfirst($invoice->billing_frequency)); ?> (<?php echo e(ucfirst($invoice->billing_cycle)); ?> cycles)</p>
                                    <?php endif; ?>
                                </address>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="table-responsive m-t-40" style="clear: both;">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th><?php echo app('translator')->get("modules.invoices.item"); ?></th>
                                        
                                        <th class="text-right"><?php echo app('translator')->get("modules.invoices.qty"); ?></th>
                                        <th class="text-right"><?php echo app('translator')->get("modules.invoices.unitPrice"); ?></th>
                                        <th class="text-right">Discount</th>
                                        <th class="text-right"><?php echo app('translator')->get("modules.invoices.price"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $count = 0; ?>
                                    <?php $__currentLoopData = $invoice->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($item->type == 'item'): ?>
                                            <tr>
                                                <td class="text-center"><?php echo e(++$count); ?></td>
                                                <td><?php echo e(ucfirst($item->item_name)); ?>

                                                    <?php if(!is_null($item->item_summary)): ?>
                                                        <p class="font-12"><?php echo $item->item_summary; ?></p>
                                                    <?php endif; ?>
                                                </td>
                                                
                                                
                                                
                                                <td class="text-right"><?php echo e($item->quantity); ?></td>
                                                <td class="text-right"> <?php echo currency_formatter($item->unit_price, $invoice->currency->currency_symbol); ?> </td>
                                                
                                                <?php 
                                                    if($item->discount_type == 'percent'){
                                                        $line_discount_amount = number_format((float) $item->discount, 0, '.', '').'%';
                                                    }else{
                                                        $line_discount_amount = currency_formatter($item->discount,$invoice->currency->currency_symbol);
                                                    }
                                                ?>
                                                <td class="text-right"> <?php echo e($line_discount_amount); ?> </td>

                                                <td class="text-right"> <?php echo currency_formatter($item->amount, $invoice->currency->currency_symbol); ?> </td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="pull-right m-t-30 text-right">
                                <p><?php echo app('translator')->get("modules.invoices.subTotal"); ?>
                                    : <?php echo currency_formatter($invoice->sub_total,htmlentities($invoice->currency->currency_symbol)); ?></p>

                                <?php if($discount > 0): ?>
                                    <p><?php echo app('translator')->get("modules.invoices.discount"); ?>
                                    : <?php echo htmlentities($invoice->currency->currency_symbol); ?><?php echo e($discount); ?> </p>
                                <?php endif; ?>
                                <?php $__currentLoopData = $taxes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$tax): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <p><?php echo e(strtoupper($key)); ?>

                                        : <?php echo currency_formatter($tax, htmlentities($invoice->currency->currency_symbol)); ?> </p>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <hr>
                                <h3><b><?php echo app('translator')->get("modules.invoices.total"); ?>
                                        :</b> <?php echo currency_formatter($invoice->total, htmlentities($invoice->currency->currency_symbol)); ?>

                                </h3>
                                <hr>
                                <?php if($invoice->credit_notes()->count() > 0): ?>
                                    <p>
                                        <?php echo app('translator')->get('modules.invoices.appliedCredits'); ?>: <?php echo currency_position($invoice->appliedCredits(), htmlentities($invoice->currency->currency_symbol)); ?>

                                    </p>
                                <?php endif; ?>
                                <?php $__empty_1 = true; $__currentLoopData = $payments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                    <p>
                                        <?php ($method = (!is_null($payment->offline_method_id)?  $payment->offlineMethod->name : $payment->gateway)); ?>

                                        <?php echo app('translator')->get('messages.invoicePaymentBy', ['date'=> $payment->paid_on->format($global->date_format), 'method' => $method]); ?> : <?php echo e(currency_position($payment->amount, $invoice->currency->currency_symbol)); ?>

                                    </p>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                <?php endif; ?>
                                <p>
                                    <?php echo app('translator')->get('modules.invoices.amountPaid'); ?>: <?php echo e(currency_position($invoice->amountPaid(), $invoice->currency->currency_symbol)); ?>

                                </p>

                                <p class="<?php if($invoice->amountDue() > 0): ?> text-danger <?php endif; ?>">
                                    <?php echo app('translator')->get('modules.invoices.amountDue'); ?>: <?php echo e(currency_position($invoice->amountDue(), $invoice->currency->currency_symbol)); ?>

                                </p>
                            </div>

                            <?php if(!is_null($invoice->note)): ?>
                                <div class="col-xs-12">
                                    <p><strong><?php echo app('translator')->get('app.note'); ?></strong>: <?php echo e($invoice->note); ?></p>
                                </div>
                            <?php endif; ?>
                            <div class="clearfix"></div>

                        </div>
                    </div>
                </div>

                
                <?php if(isset($fields) && count($fields) > 0): ?>
                <h3 class="box-title m-t-20"><?php echo app('translator')->get('modules.projects.otherInfo'); ?></h3>
                <div class="row">
                    <?php $__currentLoopData = $fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-3">
                            <strong><?php echo e(ucfirst($field->label)); ?></strong> <br>
                            <p class="text-muted">
                                <?php if( $field->type == 'text'): ?>
                                    <?php echo e($invoice->custom_fields_data['field_'.$field->id] ?? '-'); ?>

                                <?php elseif($field->type == 'password'): ?>
                                    <?php echo e($invoice->custom_fields_data['field_'.$field->id] ?? '-'); ?>

                                <?php elseif($field->type == 'number'): ?>
                                    <?php echo e($invoice->custom_fields_data['field_'.$field->id] ?? '-'); ?>


                                <?php elseif($field->type == 'textarea'): ?>
                                    <?php echo e($invoice->custom_fields_data['field_'.$field->id] ?? '-'); ?>


                                <?php elseif($field->type == 'radio'): ?>
                                    <?php echo e(!is_null($invoice->custom_fields_data['field_'.$field->id]) ? $invoice->custom_fields_data['field_'.$field->id] : '-'); ?>

                                <?php elseif($field->type == 'select'): ?>
                                    <?php echo e((!is_null($invoice->custom_fields_data['field_'.$field->id]) && $invoice->custom_fields_data['field_'.$field->id] != '') ? $field->values[$invoice->custom_fields_data['field_'.$field->id]] : '-'); ?>

                                <?php elseif($field->type == 'checkbox'): ?>
                                <ul>
                                    <?php $__currentLoopData = $field->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($invoice->custom_fields_data['field_'.$field->id] != '' && in_array($value ,explode(', ', $invoice->custom_fields_data['field_'.$field->id]))): ?> <li><?php echo e($value); ?></li> <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                                <?php elseif($field->type == 'date'): ?>
                                    <?php echo e(!is_null($invoice->custom_fields_data['field_'.$field->id]) ? \Carbon\Carbon::parse($invoice->custom_fields_data['field_'.$field->id])->format($global->date_format) : '--'); ?>

                                <?php endif; ?>
                            </p>

                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <?php endif; ?>
                
                <?php if(!is_null($payments)): ?>
                    <div class="b-all m-t-20 m-b-20">
                    <h3 class="box-title m-t-20 text-center">Recent Payments</h3>
                    <hr>
                    <div class="row">
                        <div class="col-xs-12">
                        <div class="table-responsive m-t-40" style="clear: both;">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center"><?php echo app('translator')->get("modules.invoices.price"); ?></th>
                                    <th class="text-center"><?php echo app('translator')->get("modules.invoices.paymentMethod"); ?></th>
                                    <th class="text-center"><?php echo app('translator')->get("modules.invoices.paidOn"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $count = 0; ?>
                                <?php $__empty_1 = true; $__currentLoopData = $payments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                    <tr>
                                        <td class="text-center"><?php echo e($key+1); ?></td>
                                        <td class="text-center"> <?php echo currency_formatter($payment->amount, $invoice->currency->currency_symbol); ?> </td>
                                        <td class="text-center">
                                            <?php ($method = (!is_null($payment->offline_method_id)?  $payment->offlineMethod->name : $payment->gateway)); ?>
                                            <?php echo e($method); ?>

                                        </td>
                                        <td class="text-center"> <?php echo e($payment->paid_on->format($global->date_format)); ?> </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    
    <div class="modal fade bs-modal-lg in" id="paymentDetail" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
         <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    

    
    <div class="modal fade bs-modal-lg in" id="appliedCredits" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">

        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    <?php echo app('translator')->get('app.loading'); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal"><?php echo app('translator')->get('app.close'); ?></button>
                    <button type="button" class="btn blue"><?php echo app('translator')->get('app.save'); ?> <?php echo app('translator')->get('app.changes'); ?></button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    
    
    <div class="modal fade bs-modal-md in" id="offlinePaymentDetails" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/clipboard/clipboard.min.js')); ?>"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script>

    var clipboard = new ClipboardJS('.btn-copy');

    clipboard.on('success', function(e) {
        var copied = "<?php echo __("app.copied") ?>";
        // $('#copy_payment_text').html(copied);
        $.toast({
            heading: 'Success',
            text: copied,
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500
        });
    });

    function showAppliedCredits(url) {
        $.ajaxModal('#appliedCredits', url);
    }

    function deleteAppliedCredit(invoice_id, id) {
        let url = '<?php echo e(route('admin.all-invoices.delete-applied-credit', [':id'])); ?>';
        url = url.replace(':id', id);

        $.easyAjax({
            url: url,
            type: 'POST',
            data: { invoice_id: invoice_id, _token: '<?php echo e(csrf_token()); ?>'},
            success: function (response) {
                $('#appliedCredits .modal-content').html(response.view);
                $('#appliedCredits').on('hide.bs.modal', function (e) {
                    location.reload();
                })
            }
        })
    }

    $(function () {
        var table = $('#invoices-table').dataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: '<?php echo e(route('admin.all-invoices.create')); ?>',
            deferRender: true,
            "order": [[0, "desc"]],
            language: {
                "url": "<?php echo __("app.datatable") ?>"
            },
            "fnDrawCallback": function (oSettings) {
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'project_name', name: 'projects.project_name'},
                {data: 'invoice_number', name: 'invoice_number'},
                {data: 'currency_symbol', name: 'currencies.currency_symbol'},
                {data: 'total', name: 'total'},
                {data: 'issue_date', name: 'issue_date'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });

        $('body').on('click', '.verify', function() {
            var id = $(this).data('invoice-id');

            var url = '<?php echo e(route('admin.all-invoices.payment-verify', ':id')); ?>'
            url = url.replace(':id', id);

            $.ajaxModal('#offlinePaymentDetails', url);
        });
    });

    // Show Payment detail modal
    function showPayments() {
        var url = '<?php echo e(route('admin.all-invoices.payment-detail', $invoice->id)); ?>';
        $.ajaxModal('#paymentDetail', url);
    }

</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/invoices/show.blade.php ENDPATH**/ ?>