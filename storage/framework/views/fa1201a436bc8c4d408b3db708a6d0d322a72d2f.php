<?php $__env->startComponent('mail::message'); ?>
# <?php echo app('translator')->get('email.leave.applied'); ?>

<?php $__env->startComponent('mail::text', ['text' => $content]); ?>

<?php if (isset($__componentOriginalc89b362e347ac15eb429debed60656ada449567a)): ?>
<?php $component = $__componentOriginalc89b362e347ac15eb429debed60656ada449567a; ?>
<?php unset($__componentOriginalc89b362e347ac15eb429debed60656ada449567a); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>


<?php echo app('translator')->get('email.regards'); ?>,<br>
<?php echo e(config('app.name')); ?>

<?php if (isset($__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d)): ?>
<?php $component = $__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d; ?>
<?php unset($__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/mail/leaves/multiple.blade.php ENDPATH**/ ?>