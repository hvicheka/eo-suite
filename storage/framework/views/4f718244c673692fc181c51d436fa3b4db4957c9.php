<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?> #<?php echo e($lead->id); ?> - <span
                        class="font-bold"><?php echo e(ucwords($lead->company_name)); ?></span></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.leads.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->get('modules.projects.files'); ?></li>
            </ol>
        </div>
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12 text-right">
            <a href="<?php echo e(route('admin.leads.edit',$lead->id)); ?>"
               class="btn btn-outline btn-success btn-sm"><?php echo app('translator')->get('modules.lead.edit'); ?>
                <i class="fa fa-edit" aria-hidden="true"></i></a>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>

<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/dropzone-master/dist/dropzone.css')); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-12">

            <section>
                <div class="sttabs tabs-style-line">
                    <div class="white-box">
                        <nav>
                            <ul>
                                <li class="tab-current"><a href="<?php echo e(route('admin.leads.show', $lead->id)); ?>"><span><?php echo app('translator')->get('modules.lead.profile'); ?></span></a>
                                </li>
                                <li><a href="<?php echo e(route('admin.proposals.show', $lead->id)); ?>"><span><?php echo app('translator')->get('modules.lead.proposal'); ?></span></a></li>
                                <li ><a href="<?php echo e(route('admin.lead-files.show', $lead->id)); ?>"><span><?php echo app('translator')->get('modules.lead.file'); ?></span></a></li>
                                <li><a href="<?php echo e(route('admin.leads.followup', $lead->id)); ?>"><span><?php echo app('translator')->get('modules.lead.followUp'); ?></span></a></li>
                                <?php if($gdpr->enable_gdpr): ?>
                                    <li><a href="<?php echo e(route('admin.leads.gdpr', $lead->id)); ?>"><span><?php echo app('translator')->get('app.menu.gdpr'); ?></span></a></li>
                                <?php endif; ?>
                            </ul>
                        </nav>
                    </div>
                    <div class="content-wrap">
                        <section id="section-line-3" class="show">
                            <div class="row">
                                <div class="col-xs-12" id="files-list-panel">
                                    <div class="white-box">
                                        <h2><?php echo app('translator')->get('modules.lead.leadDetail'); ?></h2>

                                        <div class="white-box">
                                            <div class="row">
                                                <div class="col-xs-6 b-r"> <strong><?php echo app('translator')->get('modules.lead.companyName'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e(ucwords($lead->company_name)); ?></p>
                                                </div>
                                                <div class="col-xs-6"> <strong><?php echo app('translator')->get('modules.lead.website'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e($lead->website ?? '-'); ?></p>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-xs-6 b-r"> <strong><?php echo app('translator')->get('modules.lead.mobile'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e($lead->mobile ?? '-'); ?></p>
                                                </div>
                                                <div class="col-xs-6 b-r"> <strong><?php echo app('translator')->get('modules.clients.officePhoneNumber'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e($lead->office_phone ?? '-'); ?></p>
                                                </div>

                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-xs-6 b-r"> <strong><?php echo app('translator')->get('modules.lead.address'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e($lead->address ?? '-'); ?></p>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-xs-6 col-md-4 b-r" > <strong><?php echo app('translator')->get('app.name'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e($lead->client_name ?? '-'); ?></p>
                                                </div>
                                                <div class="col-xs-6 col-md-4 b-r"> <strong><?php echo app('translator')->get('app.email'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e($lead->client_email ?? '-'); ?></p>
                                                </div>
                                                <div class="col-xs-6 col-md-4"> <strong><?php echo app('translator')->get('app.lead'); ?> <?php echo app('translator')->get('app.value'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e(company()->currency->currency_symbol.$lead->value ?? '-'); ?></p>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <?php if($lead->agent_id != null): ?>
                                                <div class="col-md-4 b-r"> <strong><?php echo app('translator')->get('modules.lead.leadAgent'); ?></strong> <br>
                                                   <p>
                                                        <img src="<?php echo e($lead->lead_agent->user->image_url); ?>" alt="user" class="img-circle" width="25" height="25">
                                                        <?php echo e(ucwords($lead->lead_agent->user->name)); ?>

                                                   </p>
                                                    
                                                </div>
                                                <?php endif; ?>
                                                <?php if($lead->source_id != null): ?>
                                                <div class="col-xs-6 col-md-4 b-r"> <strong><?php echo app('translator')->get('modules.lead.source'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e($lead->lead_source->type ?? '-'); ?></p>
                                                </div>
                                                <?php endif; ?>
                                                <?php if($lead->status_id != null): ?>
                                                <div class="col-xs-6 col-md-4 b-r"> <strong><?php echo app('translator')->get('modules.lead.status'); ?></strong> <br>
                                                    <p class="text-muted"><?php echo e($lead->lead_status->type ?? '-'); ?></p>
                                                </div>
                                                <?php endif; ?>
                                                <?php if($lead->category_id != null): ?>
                                                <div class="col-md-4"> <strong><?php echo app('translator')->get('modules.lead.leadCategory'); ?></strong> <br>
                                                    <p class="text-muted"> <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($lead->category_id == $category->id): ?>
                                                    <?php echo e(ucwords($category->category_name)); ?>

                                                    <?php endif; ?>
                                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-xs-12"> <strong><?php echo app('translator')->get('app.note'); ?></strong> <br>
                                                    <p class="text-muted"><?php if($lead->note): ?> <?php echo $lead->note; ?> <?php else: ?> - <?php endif; ?> </p>
                                                </div>
                                            </div>

                                            
                        <?php if(isset($fields)): ?>
                        <div class="row">
                            <hr>
                            <?php $__currentLoopData = $fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-md-6">
                                    <strong><?php echo e(ucfirst($field->label)); ?></strong> <br>
                                    <p class="text-muted">
                                        <?php if( $field->type == 'text'): ?>
                                            <?php echo e($lead->custom_fields_data['field_'.$field->id] ?? ''); ?>

                                        <?php elseif($field->type == 'password'): ?>
                                            <?php echo e($lead->custom_fields_data['field_'.$field->id] ?? ''); ?>

                                        <?php elseif($field->type == 'number'): ?>
                                            <?php echo e($lead->custom_fields_data['field_'.$field->id] ?? ''); ?>


                                        <?php elseif($field->type == 'textarea'): ?>
                                        <?php echo e($lead->custom_fields_data['field_'.$field->id] ?? ''); ?>


                                        <?php elseif($field->type == 'radio'): ?>
                                        <?php echo e(!is_null($lead->custom_fields_data['field_'.$field->id]) ? $lead->custom_fields_data['field_'.$field->id] : '-'); ?>

                                        <?php elseif($field->type == 'select'): ?>
                                            <?php echo e((!is_null($lead->custom_fields_data['field_'.$field->id]) && $lead->custom_fields_data['field_'.$field->id] != '') ? $field->values[$lead->custom_fields_data['field_'.$field->id]] : '-'); ?>

                                        <?php elseif($field->type == 'checkbox'): ?>
                                        <ul>
                                            <?php $__currentLoopData = $field->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($lead->custom_fields_data['field_'.$field->id] != '' && in_array($value ,explode(', ', $lead->custom_fields_data['field_'.$field->id]))): ?> <li><?php echo e($value); ?></li> <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul> 
                                        <?php elseif($field->type == 'date'): ?>
                                            <?php echo e(!is_null($lead->custom_fields_data['field_'.$field->id]) ? \Carbon\Carbon::parse($lead->custom_fields_data['field_'.$field->id])->format($global->date_format) : '--'); ?>

                                        <?php endif; ?>
                                    </p>

                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <?php endif; ?>

                    
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </section>

                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>


    </div>
    <!-- .row -->

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/dropzone-master/dist/dropzone.js')); ?>"></script>
<script>
    $('#show-dropzone').click(function () {
        $('#file-dropzone').toggleClass('hide show');
    });

    $("body").tooltip({
        selector: '[data-toggle="tooltip"]'
    });

    // "myAwesomeDropzone" is the camelized version of the HTML element's ID
    Dropzone.options.fileUploadDropzone = {
        paramName: "file", // The name that will be used to transfer the file
//        maxFilesize: 2, // MB,
        dictDefaultMessage: "<?php echo app('translator')->get('modules.projects.dropFile'); ?>",
        accept: function (file, done) {
            done();
        },
        init: function () {
            this.on("success", function (file, response) {
                console.log(response);
                $('#files-list-panel ul.list-group').html(response.html);
            })
        }
    };

    $('body').on('click', '.sa-params', function () {
        var id = $(this).data('file-id');
        swal({
            title: "<?php echo app('translator')->get('messages.sweetAlertTitle'); ?>",
            text: "<?php echo app('translator')->get('messages.confirmation.deleteFile'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<?php echo app('translator')->get('messages.deleteConfirmation'); ?>",
            cancelButtonText: "<?php echo app('translator')->get('messages.confirmNoArchive'); ?>",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {

                var url = "<?php echo e(route('admin.files.destroy',':id')); ?>";
                url = url.replace(':id', id);

                var token = "<?php echo e(csrf_token()); ?>";

                $.easyAjax({
                    type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                    success: function (response) {
                        if (response.status == "success") {
                            $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                            $('#files-list-panel ul.list-group').html(response.html);

                        }
                    }
                });
            }
        });
    });

</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eOsuite\resources\views/admin/lead/show.blade.php ENDPATH**/ ?>