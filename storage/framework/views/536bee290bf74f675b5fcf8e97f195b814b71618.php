<style>
    .thumbnail-img{
        line-break: anywhere;
    }
    </style>
<div class="row">
    <?php $__empty_1 = true; $__currentLoopData = $contract->files; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        <div class="col-md-2 m-b-10">
            <div class="card">
                    <div class="file-bg">
                        <div class="overlay-file-box">
                            <div class="user-content">

                                <img class="card-img-top img-responsive" src="<?php echo e($file->file_url); ?>" alt="Card image cap">
                                
                                    
                                
                            </div>
                        </div>
                    </div>

                <div class="card-block">
                    <h6 class="card-title thumbnail-img"><?php echo e($file->filename); ?></h6>

                        <a target="_blank" href="<?php echo e($file->file_url); ?>"
                           data-toggle="tooltip" data-original-title="View"
                           class="btn btn-info btn-circle"><i
                                    class="fa fa-search"></i></a>

                    <?php if(is_null($file->external_link)): ?>
                    <a href="<?php echo e(route('admin.contract-files.download', $file->id)); ?>"
                       data-toggle="tooltip" data-original-title="Download"
                       class="btn btn-inverse btn-circle"><i
                                class="fa fa-download"></i></a>
                    <a href="javascript:;" data-toggle="tooltip"
                       data-original-title="Delete"
                       data-file-id="<?php echo e($file->id); ?>"
                       class="btn btn-danger btn-circle sa-params" data-pk="thumbnail"><i
                                class="fa fa-times"></i></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        <div class="col-md-12">
            <div class="card">
                <div class="card-block">
                    <?php echo app('translator')->get('messages.noFileUploaded'); ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
</div>
<?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/contracts/contract-files/thumbnail-list.blade.php ENDPATH**/ ?>