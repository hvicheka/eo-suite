<!DOCTYPE html>

<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e($global->favicon_url); ?>">
    
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo e($global->favicon_url); ?>">
    <meta name="theme-color" content="#ffffff">

    <title><?php echo app('translator')->get($pageTitle); ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo e(asset('bootstrap/dist/css/bootstrap.min.css')); ?>" rel="stylesheet">

    <!-- This is a Animation CSS -->
    <link href="<?php echo e(asset('css/animate.css')); ?>" rel="stylesheet">

    <?php echo $__env->yieldPushContent('head-script'); ?>
    <!-- This is a Custom CSS -->
    <link href="<?php echo e(asset('css/style.css')); ?>" rel="stylesheet">
    <!-- color CSS you can use different color css from css/colors folder -->
    <!-- We have chosen the skin-blue (default.css) for this starter
       page. However, you can choose any other skin from folder css / colors .
       -->
    <link href="<?php echo e(asset('css/colors/default.css')); ?>" id="theme" rel="stylesheet">
    <link href="<?php echo e(asset('plugins/froiden-helper/helper.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('plugins/bower_components/toast-master/css/jquery.toast.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/custom-new.css')); ?>" rel="stylesheet">

    <?php if($global->rounded_theme): ?>
    <link href="<?php echo e(asset('css/rounded.css')); ?>" rel="stylesheet">
    <?php endif; ?>
    <style>
        html {
            background: #ffffff;
        }
    </style>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<body>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box p-t-20">
                <?php echo Form::open(['id'=>'createLead','class'=>'ajax-form','method'=>'POST']); ?>

                <div class="form-body">
                    <div class="row">
                        <input type="hidden" name="company_id" value="<?php echo e($leadFormFields[0]->company_id); ?>">

                        <?php $__currentLoopData = $leadFormFields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($item->field_name != 'message'): ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.lead.'.$item->field_name); ?></label>
                                        <input type="text" id="<?php echo e($item->field_name); ?>" name="<?php echo e($item->field_name); ?>"
                                            class="form-control">
                                    </div>
                                </div>
                            <?php else: ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.lead.'.$item->field_name); ?> </label>
                                        <textarea class="form-control"   id="<?php echo e($item->field_name); ?>" name="<?php echo e($item->field_name); ?>"  ></textarea>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php if($global->lead_form_google_captcha == 1  && $superadmin->google_captcha_version=="v2" && $superadmin->google_recaptcha_status): ?>
                            <div class="form-group <?php echo e($errors->has('g-recaptcha-response') ? 'has-error' : ''); ?>">
                                <div class="col-xs-12 m-b-20">
                                    <div class="g-recaptcha"
                                         data-sitekey="<?php echo e($superadmin->google_recaptcha_key); ?>">
                                    </div>
                                    <?php if($errors->has('g-recaptcha-response')): ?>
                                        <div class="help-block with-errors"><?php echo e($errors->first('g-recaptcha-response')); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        <?php endif; ?>
                        <input type='hidden' name='recaptcha_token' id='recaptcha_token'>
                    </div>

                </div>
                <div class="form-actions">
                    <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i>
                        <?php echo app('translator')->get('app.save'); ?></button>
                    <button type="reset" class="btn btn-default"><?php echo app('translator')->get('app.reset'); ?></button>
                </div>
                <?php echo Form::close(); ?>


                <div class="row">
                    <div class="col-xs-12">
                        <div class="alert alert-success" id="success-message" style="display:none"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>


<!-- jQuery -->
<script src="<?php echo e(asset('plugins/bower_components/jquery/dist/jquery.min.js')); ?>"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo e(asset('bootstrap/dist/js/bootstrap.min.js')); ?>"></script>

<!--Wave Effects -->
<script src="<?php echo e(asset('js/waves.js')); ?>"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo e(asset('plugins/froiden-helper/helper.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/toast-master/js/jquery.toast.js')); ?>"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>

<?php if($superadmin->google_recaptcha_status  && $superadmin->google_captcha_version=="v3" && $global->lead_form_google_captcha == 1): ?>
    <script src="https://www.google.com/recaptcha/api.js?render=<?php echo e($superadmin->google_recaptcha_key); ?>"></script>

    <script>
        setTimeout(function(){

            grecaptcha.ready(function() {
                grecaptcha.execute('<?php echo e($superadmin->google_recaptcha_key); ?>', {action: 'submit'}).then(function(token) {
                    document.getElementById("recaptcha_token").value = token;
                });
            });

        }, 3000);

    </script>
<?php endif; ?>
<script>

$('#save-form').click(function () {
            $.easyAjax({
                url: '<?php echo e(route('front.leadStore')); ?>',
                container: '#createLead',
                type: "POST",
                redirect: true,
                disableButton: true,
                data: $('#createLead').serialize(),
                success: function (response) {
                    if (response.status == "success") {
                        $('#createLead')[0].reset();
                        $('#createLead').hide();
                        $('#success-message').html(response.message);
                        $('#success-message').show();
                    }
                }
            })
        });
</script>

</head><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/lead-form.blade.php ENDPATH**/ ?>