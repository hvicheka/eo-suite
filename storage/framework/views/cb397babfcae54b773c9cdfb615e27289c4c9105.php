<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title"><?php echo app('translator')->get('modules.productCategory.productSubCategory'); ?></h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table category-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th><?php echo app('translator')->get('modules.productCategory.subCategory'); ?></th>
                    <th><?php echo app('translator')->get('modules.productCategory.category'); ?></th>
                    <th><?php echo app('translator')->get('app.action'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php $__empty_1 = true; $__currentLoopData = $subCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$subCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <tr id="cat-<?php echo e($subCategory->id); ?>">
                        <td><?php echo e($key+1); ?></td>
                        <td><?php echo e(ucwords($subCategory->category_name)); ?></td>
                        <td><?php echo e(ucwords($subCategory->category->category_name)); ?></td>
                        <td><a href="javascript:;" data-cat-id="<?php echo e($subCategory->id); ?>" class="btn btn-sm btn-danger btn-rounded delete-category"><?php echo app('translator')->get("app.remove"); ?></a></td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <tr>
                        <td colspan="4"><?php echo app('translator')->get('messages.noProjectCategory'); ?></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>

        <?php echo Form::open(['id'=>'createProjectCategory','class'=>'ajax-form','method'=>'POST']); ?>

        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label><?php echo app('translator')->get('modules.productCategory.category'); ?></label>
                        <select class="select2 form-control" name="category" id="category"
                                data-style="form-control">
                            <?php $__empty_1 = true; $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <option <?php if($categoryID == $category->id): ?> selected <?php endif; ?> value="<?php echo e($category->id); ?>"><?php echo e(ucwords($category->category_name)); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                <option value=""><?php echo app('translator')->get('messages.noProductCategory'); ?></option>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label><?php echo app('translator')->get('modules.productCategory.subCategoryName'); ?></label>
                        <input type="text" name="sub_category_name" id="sub_category_name" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> <?php echo app('translator')->get('app.save'); ?></button>
        </div>
        <?php echo Form::close(); ?>

    </div>
</div>
<script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
<script>

    $("#category").select2({
        formatNoMatches: function () {
            return "<?php echo e(__('messages.noRecordFound')); ?>";
        }
    });

    $('body').on('click', '.delete-category', function(e) {
        var id = $(this).data('cat-id');
        var url = "<?php echo e(route('admin.productSubCategory.destroy',':id')); ?>";
        url = url.replace(':id', id);

        var token = "<?php echo e(csrf_token()); ?>";

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token, '_method': 'DELETE'},
            success: function (response) {

                if (response.status == "success") {
                    $.unblockUI();
                    $('#cat-'+id).fadeOut();
                    var options = [];
                    var rData = [];
                    rData = response.data;
                    subCategories = response.data; // using it on product create/edit page
                    var opts = '';
                    var catIds = '<?php echo e($categoryID); ?>';
                    var subCategory = rData.filter(function (item) {
                        return item.category_id == catIds;
                    });
                    subCategory.forEach(project => {
                        opts += `<option value='${project.id}'>${project.category_name}</option>`
                    })

                    $('#sub_category_id').html('<option value="">Select Sub Category...</option>'+opts);

                    $("#sub_category_id").select2({
                        formatNoMatches: function () {
                            return "<?php echo e(__('messages.noRecordFound')); ?>";
                        }
                    });
                }
            }
        });
    });

    $('#save-category').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.productSubCategory.store')); ?>',
            container: '#createProjectCategory',
            type: "POST",
            data: $('#createProjectCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    var options = [];
                    var rData = [];
                    rData = response.data;
                    subCategories = response.data; // using it on product create/edit page

                    var opts = '';
                    var catIds =  '<?php echo e($categoryID); ?>';
                    var subCategory = rData.filter(function (item) {
                        return item.category_id == catIds;
                    });

                    subCategory.forEach(project => {
                        opts += `<option value='${project.id}'>${project.category_name}</option>`
                    })

                    let listData = "";
                    $.each(subCategories, function( index, value ) {
                        listData += '<tr id="cat-' + value.id + '">'+
                            '<td>'+(index+1)+'</td>'+
                            '<td>' + value.category_name + '</td>'+
                            '<td>' + value.category.category_name + '</td>'+
                            '<td><a href="javascript:;" data-cat-id="' + value.id + '" class="btn btn-sm btn-danger btn-rounded delete-category"><?php echo app('translator')->get("app.remove"); ?></a></td>'+
                            '</tr>';
                    });
                    $('.category-table tbody' ).html(listData);
                    $('#sub_category_name').val(' ');

                    $('#sub_category_id').html('<option value="">Select Sub Category...</option>'+opts);

                    $("#sub_category_id").select2({
                        formatNoMatches: function () {
                            return "<?php echo e(__('messages.noRecordFound')); ?>";
                        }
                    });
                }
            }
        })
    });
</script><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/products/create-sub-category.blade.php ENDPATH**/ ?>