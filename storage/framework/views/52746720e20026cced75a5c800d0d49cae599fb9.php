<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title"><?php echo app('translator')->get('modules.frontCms.defaultLanguage'); ?></h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        <div class="alert alert-info ">
            <i class="fa fa-info-circle"></i> <?php echo app('translator')->get('messages.defaultLanguageCompany'); ?>
        </div>
        <?php echo Form::open(['id'=>'setDefaultLanguage','class'=>'ajax-form','method'=>'POST']); ?>

        <div class="form-body">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="address"><?php echo app('translator')->get('modules.frontCms.defaultLanguage'); ?></label>
                        <select name="default_language" id="default_language" class="form-control select2">
                            <option <?php if($global->new_company_locale == "en"): ?> selected <?php endif; ?> value="en">English
                            </option>
                            <?php $__currentLoopData = $languageSettings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($language->language_code); ?>" <?php if($global->new_company_locale == $language->language_code): ?> selected <?php endif; ?> ><?php echo e($language->language_name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> <?php echo app('translator')->get('app.save'); ?></button>
        </div>
        <?php echo Form::close(); ?>

    </div>
</div>

<script>

    $('#save-category').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('super-admin.companies.default-language-save')); ?>',
            container: '#setDefaultLanguage',
            type: "POST",
            data: $('#setDefaultLanguage').serialize(),
            success: function (response) {
                $('#projectCategoryModal').modal('hide');
                window.location.reload();
            }
        })
    });
</script><?php /**PATH D:\WAMP-SERVER\www\eOsuite\resources\views/super-admin/companies/default-language.blade.php ENDPATH**/ ?>