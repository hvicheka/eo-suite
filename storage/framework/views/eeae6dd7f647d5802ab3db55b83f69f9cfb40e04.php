<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li class="active"><?php echo e(__($pageTitle)); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('plugins/daterange-picker/daterangepicker.css')); ?>" />

<style>
    #attendance-report-table_wrapper .dt-buttons{
        display: none !important;
    }
    #attendance-report-table_filter{
        display: none !important;
    }
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>


    <div class="white-box">
        <?php $__env->startSection('filter-section'); ?>
            <div class="row">
                <?php echo Form::open(['id'=>'filter-form','class'=>'ajax-form','method'=>'POST']); ?>

                <div class="col-xs-12">
                    <div class="example">
                        <h5 class="box-title"><?php echo app('translator')->get('app.selectDateRange'); ?></h5>
                        <div class="form-group">
                            <div id="reportrange" class="form-control reportrange">
                                <i class="fa fa-calendar"></i>&nbsp;
                                <span></span> <i class="fa fa-caret-down pull-right"></i>
                            </div>

                            <input type="hidden" class="form-control" id="start-date" placeholder="<?php echo app('translator')->get('app.startDate'); ?>"
                                   value="<?php echo e(\Carbon\Carbon::today()->startOfMonth()->format($global->date_format)); ?>"/>
                            <input type="hidden" class="form-control" id="end-date" placeholder="<?php echo app('translator')->get('app.endDate'); ?>"
                                   value="<?php echo e(\Carbon\Carbon::today()->format($global->date_format)); ?>"/>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12">
                    <h5 class="box-title m-t-30"><?php echo app('translator')->get('app.select'); ?> <?php echo app('translator')->get('app.employee'); ?></h5>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12">
                                <select class="select2 form-control" data-placeholder="<?php echo app('translator')->get('app.all'); ?>" id="employeeID" name="employee_id">
                                    <option value="all"><?php echo app('translator')->get('app.all'); ?></option>
                                    <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option
                                                value="<?php echo e($employee->id); ?>"><?php echo e(ucwords($employee->name)); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="form-group">
                        <button type="button" class="btn btn-success col-md-6" id="filter-results"><i class="fa fa-check"></i> <?php echo app('translator')->get('app.apply'); ?>
                        </button>
                        <button type="button" id="reset-filters" class="btn btn-inverse col-md-5 col-md-offset-1"><i class="fa fa-refresh"></i> <?php echo app('translator')->get('app.reset'); ?></button>
                    </div>

                </div>
                <?php echo Form::close(); ?>


            </div>
        <?php $__env->stopSection(); ?>
    </div>

    <div class="row">

        <div class="col-xs-12">
            <div class="white-box" id="attendanceData">
                <h4 class="dashboard-stats"><span class="text-info" id="totalDays"></span> <span class="font-12 text-muted m-l-5"> <?php echo app('translator')->get('modules.attendance.totalWorkingDays'); ?></span></h4>

                <div class="table-responsive">
                    <?php echo $dataTable->table(['class' => 'table table-bordered table-hover toggle-circle default footable-loaded footable']); ?>

                </div>
            </div>
        </div>
    </div>
    <!-- .row -->


<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<?php if($global->locale == 'en'): ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.<?php echo e($global->locale); ?>-AU.min.js"></script>
<?php elseif($global->locale == 'br'): ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.pt-BR.min.js"></script>
<?php else: ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.<?php echo e($global->locale); ?>.min.js"></script>
<?php endif; ?>

<script src="<?php echo e(asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="https://www.mobicollector.com/DataTable/extensions/Buttons/js/dataTables.buttons.js"></script>
<script src="<?php echo e(asset('js/datatables/buttons.server-side.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/moment/moment.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('plugins/daterange-picker/daterangepicker.js')); ?>"></script>
<?php echo $dataTable->scripts(); ?>

<script>
    $(function() {
        var dateformat = '<?php echo e($global->moment_format); ?>';

        var startDate = '<?php echo e(\Carbon\Carbon::today()->startOfMonth()->format($global->date_format)); ?>';
        var start = moment(startDate, dateformat);

        var endDate = '<?php echo e(\Carbon\Carbon::today()->format($global->date_format)); ?>';
        var end = moment(endDate, dateformat);

        function cb(start, end) {
            $('#start-date').val(start.format(dateformat));
            $('#end-date').val(end.format(dateformat));
            $('#reportrange span').html(start.format(dateformat) + ' - ' + end.format(dateformat));
        }
        moment.locale('<?php echo e($global->locale); ?>');
        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,

            locale: {
                language: '<?php echo e($global->locale); ?>',
                format: '<?php echo e($global->moment_format); ?>',
            },
            linkedCalendars: false,
            ranges: dateRangePickerCustom
        }, cb);

        cb(start, end);

    });
    $(".select2").select2({
        formatNoMatches: function () {
            return "<?php echo e(__('messages.noRecordFound')); ?>";
        }
    });

    $('#attendance-report-table').on('preXhr.dt', function (e, settings, data) {
        var employeeID = $('#employeeID').val();
        var startDate = $('#start-date').val();
        var endDate = $('#end-date').val();

        data['startDate'] = startDate;
        data['endDate'] = endDate;
        data['employee'] = employeeID;
        data['_token'] = '<?php echo e(csrf_token()); ?>';
    });


    var table;
    getTotalData();
    function showTable() {
        getTotalData()
        window.LaravelDataTables["attendance-report-table"].draw();
    }

    function getTotalData(){
        var employeeID = $('#employeeID').val();
        var startDate = $('#start-date').val();
        var endDate = $('#end-date').val();

        var url2 = '<?php echo route('admin.attendance-report.report'); ?>';

        $.easyAjax({
            type: 'POST',
            url: url2,
            data: {'employeeID': employeeID, 'startDate': startDate, 'endDate': endDate, '_token': '<?php echo e(csrf_token()); ?>'},
            success: function (response) {
                $('#totalDays').text(response.data);
            }
        });
    }

    $('#filter-results').click(function () {
        showTable();
    });

    $('#reset-filters').click(function () {
        $('#filter-form')[0].reset();
        $('#status').val('all');
        $('.select2').val('all');
        $('#filter-form').find('select').select2();
        $('#start-date').val('<?php echo e(\Carbon\Carbon::today()->startOfMonth()->format($global->date_format)); ?>');
        $('#end-date').val('<?php echo e(\Carbon\Carbon::today()->format($global->date_format)); ?>');
        $('#reportrange span').html('<?php echo e(\Carbon\Carbon::today()->startOfMonth()->format($global->date_format)); ?>' + ' - ' + '<?php echo e(\Carbon\Carbon::today()->format($global->date_format)); ?>');
        $('#filter-results').trigger("click");
    })

    // $(document).ready(function(){
    //     showTable();
    // });

    $('#export-excel').click(function () {
        var employeeID = $('#employeeID').val();
        var startDate = $('#start-date').val();
        var endDate = $('#end-date').val();


        //refresh datatable
        var url2 = '<?php echo route('admin.attendance-report.reportExport', [':startDate', ':endDate', ':employeeID']); ?>';

        url2 = url2.replace(':startDate', startDate);
        url2 = url2.replace(':endDate', endDate);
        url2 = url2.replace(':employeeID', employeeID);

        window.location = url2;
    })


    // showTable();

</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eOsuite\resources\views/admin/reports/attendance/index.blade.php ENDPATH**/ ?>