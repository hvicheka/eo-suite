<?php $__env->startPush('head-script'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.css')); ?>">


<?php $__env->stopPush(); ?>
<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.contracts.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.addNew'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel panel-inverse">
                    <div class="panel-heading"> <?php echo app('translator')->get('app.add'); ?> <?php echo app('translator')->get('app.menu.contract'); ?></div>

                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <?php echo Form::open(['id'=>'createContract','class'=>'ajax-form','method'=>'POST']); ?>


                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label required" for="relate_to"><?php echo app('translator')->get('modules.contracts.relate_to'); ?></label>
                                            <select class="select2 form-control" name="relate_to" id="relate_to">
                                                <option value="lead">Lead</option>
                                                <option value="customer">Customer</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label required" for="lead_id"><?php echo app('translator')->get('modules.contracts.lead_customer'); ?></label>
                                            <select class="select2 form-control" name="lead_id" id="lead_id">
                                                <option value="">Select Option</option>
                                                <?php $__currentLoopData = $leads; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($item->id); ?>"><?php echo e($item->client_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('modules.contracts.contract_templete'); ?></label>
                                            <select class="form-control select2" name="contract_theme_id" id="contract_theme_id">
                                                <?php $__currentLoopData = $contract_themes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $theme): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($theme->id); ?>"><?php echo e($theme->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('modules.invoices.currency'); ?></label>
                                            <select class="form-control" name="currency_id" id="currency_id">
                                                <?php $__currentLoopData = $currencies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $currency): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($currency->id); ?>" <?php if($global->currency_id == $currency->id): ?> selected <?php endif; ?>><?php echo e($currency->currency_symbol.' ('.$currency->currency_code.')'); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
    
                                    </div>

                                    

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('modules.quotation.company_name'); ?> <span class="text-danger">*</span></label> 
                                            <div class="input-icon">
                                                <input type="text" class="form-control" name="company_name" id="company_name" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('modules.quotation.name'); ?> <span class="text-danger">*</span></label> 
                                            <div class="input-icon">
                                                <input type="text" class="form-control" name="name" id="name" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('modules.quotation.position'); ?> <span class="text-danger">*</span></label> 
                                            <div class="input-icon">
                                                <input type="text" class="form-control" name="position" id="position">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><?php echo app('translator')->get('modules.lead.mobile'); ?></label>
                                            <input type="tel" name="mobile" id="mobile" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3 ">
                                        <div class="form-group">
                                            <label><?php echo app('translator')->get('modules.clients.officePhoneNumber'); ?></label>
                                            <input type="text" name="office_phone" id="office_phone"   class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('modules.quotation.email'); ?> <span class="text-danger">*</span> </label>
                                            <div class="input-icon">
                                                <input type="email" class="form-control" name="email" id="email">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 ">
                                        <div class="form-group">
                                            <label><?php echo app('translator')->get('modules.stripeCustomerAddress.city'); ?></label>
                                            <input type="text" name="city" id="city"  class="form-control">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3 ">
                                        <div class="form-group">
                                            <label><?php echo app('translator')->get('modules.stripeCustomerAddress.state'); ?></label>
                                            <input type="text" name="state" id="state"   class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3 ">
                                        <div class="form-group">
                                            <label><?php echo app('translator')->get('modules.stripeCustomerAddress.country'); ?></label>
                                            <input type="text" name="country" id="country"  class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3 ">
                                        <div class="form-group">
                                            <label><?php echo app('translator')->get('modules.stripeCustomerAddress.postalCode'); ?></label>
                                            <input type="text" name="postal_code" id="postalCode"class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label required"><?php echo app('translator')->get('modules.timeLogs.startDate'); ?></label>
                                            <input id="start_date" name="start_date" type="text"
                                                    class="form-control"
                                                    value="<?php echo e(\Carbon\Carbon::today()->format($global->date_format)); ?>">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('modules.timeLogs.endDate'); ?></label>
                                            <input id="end_date" name="end_date" type="text" class="form-control"
                                                value="<?php echo e(\Carbon\Carbon::today()->format($global->date_format)); ?>">
                                        </div>
                                    </div>

                                    

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('modules.quotation.assistTo'); ?> <span class="text-danger">*</span> </label>
                                            <select class="form-control select2" name="assist_to_id" id="assist_to_id">
                                                <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('modules.quotation.saleBy'); ?> <span class="text-danger">*</span> </label>
                                            <select class="form-control select2" name="sale_by_id" id="sale_by_id">
                                                <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label required" for="company_name"><?php echo app('translator')->get('app.client'); ?></label>
                                            <select class="select2 form-control" data-placeholder="<?php echo app('translator')->get('app.client'); ?>" name="client" id="clientID">
                                                <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option
                                                            value="<?php echo e($client->id); ?>"><?php echo e(ucwords($client->name)); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label required" for="subject"><?php echo app('translator')->get('app.subject'); ?></label>
                                            <input type="text" class="form-control" id="subject" name="subject">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label  class="control-label required"><?php echo app('translator')->get('modules.contracts.contractType'); ?>
                                                <a href="javascript:;"
                                                id="createContractType"
                                                class="btn btn-xs btn-outline btn-success">
                                                    <i class="fa fa-plus"></i> <?php echo app('translator')->get('modules.contracts.addContractType'); ?>
                                                </a>
                                            </label>
                                            <select class="select2 form-control" data-placeholder="<?php echo app('translator')->get('app.client'); ?>" id="contractType" name="contract_type">
                                                <?php $__currentLoopData = $contractType; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option
                                                            value="<?php echo e($type->id); ?>"><?php echo e(ucwords($type->name)); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><?php echo app('translator')->get('modules.contracts.contractName'); ?></label>
                                            <input name="contract_name" type="text"
                                                class="form-control">
                                        </div>
                                    </div>



                                    

                                    
                                

                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo app('translator')->get('modules.contracts.summary'); ?></label>
                                                <textarea class="form-control summernote" id="contract_detail" name="contract_detail" rows="4"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo app('translator')->get('modules.contracts.notes'); ?></label>
                                                <textarea class="form-control summernote" id="description" name="description" rows="4"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <label><?php echo app('translator')->get('modules.contracts.companyLogo'); ?></label>
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img src="http://via.placeholder.com/200x150.png?text=<?php echo app('translator')->get('modules.contracts.companyLogo'); ?>"
                                                            alt=""/>
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                                        style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                    <span class="btn btn-info btn-file">
                                                        <span class="fileinput-new"> <?php echo app('translator')->get('app.selectImage'); ?> </span>
                                                        <span class="fileinput-exists"> <?php echo app('translator')->get('app.change'); ?> </span>
                                                        <input type="file" id="company_logo" name="company_logo"> </span>
                                                        <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                        data-dismiss="fileinput"> <?php echo app('translator')->get('app.remove'); ?> </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                        
                                </div>
                                
                                <hr>
                                <div class="col-xs-12 m-t-5">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('modules.quotation.product'); ?></label>
                                            <select class="form-control select2" name="product_id" id="product_id">
                                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <br>
                                        <button type="button" class="btn btn-info" id="add-item"><i class="fa fa-plus"></i> <?php echo app('translator')->get('modules.invoices.addItem'); ?></button>
                                    </div>
                                </div>
                                
                                <div class="row">
    
                                    <div class="col-xs-12  visible-md visible-lg">
    
                                        <div class="<?php if($invoiceSetting->hsn_sac_code_show): ?> col-md-3 <?php else: ?> col-md-4 <?php endif; ?> font-bold" style="padding: 8px 15px">
                                            <?php echo app('translator')->get('modules.invoices.item'); ?>
                                        </div>
    
                                        
    
                                        <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                            <?php echo app('translator')->get('modules.invoices.qty'); ?>
                                        </div>
    
                                        <div class="col-md-1 font-bold" style="padding: 8px 15px">
                                            <?php echo app('translator')->get('modules.invoices.unitPrice'); ?>
                                        </div>

                                        <div class="col-md-3 font-bold" style="padding: 8px 15px">
                                            <?php echo app('translator')->get('modules.invoices.discount'); ?>
                                        </div>
    
                                        
    
                                        <div class="col-md-2 text-center font-bold" style="padding: 8px 15px">
                                            <?php echo app('translator')->get('modules.invoices.amount'); ?>
                                        </div>
    
                                        <div class="col-md-1" style="padding: 8px 15px">
                                            &nbsp;
                                        </div>
    
                                    </div>
    
                                    <div id="sortable">
                                        <div class="col-xs-12 item-row margin-top-5" id="sortable_item">
    
                                            <div class="col-md-3">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.item'); ?></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>
                                                            <input type="text" class="form-control item_name" readonly name="item_name[]">
                                                            <input type="hidden" class="form-control item_id" name="item_id[]">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <textarea name="item_summary[]" class="form-control item_summary" placeholder="<?php echo app('translator')->get('app.description'); ?>" cols="10" rows="2"></textarea>
    
                                                    </div>
                                                </div>
    
                                            </div>
    
                                                                                       
    
                                            <div class="col-md-1">
    
                                                <div class="form-group">
                                                    <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.qty'); ?></label>
                                                    <input type="text" min="0" class="form-control quantity" value="1" name="quantity[]" >
                                                    
                                                </div>
    
                                            </div>
    
                                            <div class="col-md-1">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.unitPrice'); ?></label>
                                                        <input type="text" min="" class="form-control cost_per_item" readonly name="cost_per_item[]" value="0" >
                                                    </div>
                                                </div>
    
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <select class="form-control line_discount_type" name="line_discount_type[]">
                                                        <option value="percent">%</option>
                                                        <option value="fixed"><?php echo app('translator')->get('modules.invoices.amount'); ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-1" >
                                                <div class="form-group">
                                                    <input type="number" min="0" value="0" class="form-control line_discount_value" name="line_discount_value[]">
                                                </div>
                                            </div>
                                           
    
                                            
    
                                            <div class="col-md-2 border-dark  text-center">
                                                <label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.amount'); ?></label>
    
                                                <p class="form-control-static"><span class="amount-html">0.00</span></p>
                                                <input type="hidden" class="amount" name="amount[]">
                                            </div>
    
                                            <div class="col-md-1 text-right visible-md visible-lg">
                                                <button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>
                                            </div>
                                            <div class="col-md-1 hidden-md hidden-lg">
                                                <div class="row">
                                                    <button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i> <?php echo app('translator')->get('app.remove'); ?></button>
                                                </div>
                                            </div>
    
                                        </div>
                                    </div>
    
                                    
                                    <div id="item-list">
    
                                    </div>
    
                                
                                    <div class="col-xs-12 ">
                                        <div class="row">
                                            <div class="col-md-offset-9 col-xs-6 col-md-1 text-right p-t-10" ><?php echo app('translator')->get('modules.invoices.subTotal'); ?></div>
    
                                            <p class="form-control-static col-xs-6 col-md-2" >
                                                <span class="sub-total">0.00</span>
                                            </p>

                                            <input type="hidden" class="sub-total-field" name="sub_total" value="0">
                                        </div>
    
                                        <div class="row">
                                            <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                                <?php echo app('translator')->get('modules.invoices.discount'); ?>
                                            </div>
                                            <div class="form-group col-xs-6 col-md-1" >
                                                <input type="number" min="0" value="0" name="discount_value" class="form-control discount_value">
                                            </div>
                                            <div class="form-group col-xs-6 col-md-1" >
                                                <select class="form-control" name="discount_type" id="discount_type">
                                                    <option value="percent">%</option>
                                                    <option value="fixed"><?php echo app('translator')->get('modules.invoices.amount'); ?></option>
                                                </select>
                                            </div>
                                        </div>
    
                                        <div class="row m-t-5" id="invoice-taxes">
                                            <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                                <?php echo app('translator')->get('modules.invoices.tax'); ?>
                                            </div>
    
                                            <p class="form-control-static col-xs-6 col-md-2" >
                                                <span class="tax-percent">0.00</span>
                                            </p>
                                        </div>
    
                                        <div class="row m-t-5 font-bold">
                                            <div class="col-md-offset-9 col-md-1 col-xs-6 text-right p-t-10" ><?php echo app('translator')->get('modules.invoices.total'); ?></div>
    
                                            <p class="form-control-static col-xs-6 col-md-2" >
                                                <span class="total">0.00</span>
                                            </p>
                                            <input type="hidden" class="total-field" name="total" value="0">
                                        </div>
    
                                    </div>
    
                                </div> 
                                
                                <hr>

                                <div class="col-md-12">
                                    <button type="submit" id="save-form" class="btn btn-success waves-effect waves-light m-r-10">
                                        <?php echo app('translator')->get('app.save'); ?>
                                    </button>
                                    <button type="reset" class="btn btn-inverse waves-effect waves-light"><?php echo app('translator')->get('app.reset'); ?></button>
                                    
                                </div>
                
                            <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->
    
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    
<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
    <script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.min.js')); ?>"></script>
    

    <script>


        $(function () {
            $( "#sortable" ).sortable();
        });

        $(".select2").select2({
            formatNoMatches: function () {
                return "<?php echo e(__('messages.noRecordFound')); ?>";
            }
        });
       

        jQuery('#end_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart: '<?php echo e($global->week_start); ?>',
            format: '<?php echo e($global->date_picker_format); ?>',
        });

        $("#start_date").datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'<?php echo e($global->week_start); ?>',
            format: '<?php echo e($global->date_picker_format); ?>',
        }).on('changeDate', function (selected) {
            var maxDate = new Date(selected.date.valueOf());
            $('#end_date').datepicker('setStartDate', maxDate);
        });

        
        $('#createContractType').click(function(){
            var url = '<?php echo e(route('admin.contract-type.create-contract-type')); ?>';
            $('#modelHeading').html("<?php echo app('translator')->get('modules.contracts.manageContractType'); ?>");
            $.ajaxModal('#taskCategoryModal', url);
        })
        function setAmount() {
            let no_amount = document.getElementById("check_amount").checked;
            if(no_amount == true){
                document.getElementById("amount").value = "0";
            }else{
                document.getElementById("amount").value = "";
            }
        
        }

        function setEndDate() {
            let no_amount = document.getElementById("no_enddate").checked;
            if(no_amount == true){
                document.getElementById("end_date").value = "";
            }else{
                document.getElementById("end_date").value = "<?php echo e(\Carbon\Carbon::today()->format($global->date_format)); ?>";
            }
        
        }
        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        }   );


    // *****Calculoate Item working
    $('#product_id').change(function () {
        var value = $(this).val();
        var products = <?php echo collect($products); ?>;

        if($('div#sortable_item:last').attr('data-id') == value){

            // $('div#sortable_item').attr('data-id',value);
            
        }else{
            $('div#sortable_item:last').attr('data-id',value);
        };

        $.each(products, (i, v) => {
            if(v.id == value){
                $(`div#sortable_item[data-id="${value}"]:last`).find('.item_name').val(`${v.name}`);
                $(`div#sortable_item[data-id="${value}"]:last`).find('.item_summary').val(`${v.description}`);
                $(`div#sortable_item[data-id="${value}"]:last`).find('.item_id').val(`${v.id}`);
                $(`div#sortable_item[data-id="${value}"]:last`).find('.cost_per_item').val(`${v.price}`);

                var quantity = 1;

                var perItemCost = v.price;

                var amount = (quantity*perItemCost);

                $(`div#sortable_item[data-id="${value}"]:last`).find('.amount').val(decimalupto2(amount));
                $(`div#sortable_item[data-id="${value}"]:last`).find('.amount-html').html(decimalupto2(amount));
                
                calculateTotal();

            }
        });
    })

    $('#add-item').click(function () {

    var i = $(document).find('.item_name').length;
    var item = '<div class="col-xs-12 item-row margin-top-5" id="sortable_item">'

        +'<div class="col-md-3">'
        +'<div class="row">'
        +'<div class="form-group">'
        +'<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.item'); ?></label>'
        +'<div class="input-group">'
        +'<div class="input-group-addon"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>'
        +'<input type="text" class="form-control item_name " readonly name="item_name[]" >'
        +'<input type="hidden" class="item_id" name="item_id[]">'
        +'</div>'
        +'</div>'

        +'<div class="form-group">'
        +'<textarea name="item_summary[]" class="form-control item_summary" placeholder="<?php echo app('translator')->get('app.description'); ?>" rows="2"></textarea>'
        +'</div>'
        +'</div>'
        +'</div>'

        // +'<div class="col-md-1">'
        // +'<div class="form-group">'
        // +'<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.hsnSacCode'); ?></label>'
        // +'<input type="text"  class="form-control" name="hsn_sac_code[]" >'
        // +'</div>'
        // +'</div>'
        +'<div class="col-md-1">'

        +'<div class="form-group">'
        +'<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.qty'); ?></label>'
        +'<input type="number" min="1" class="form-control quantity" value="1" name="quantity[]" >'
        +'</div>'
        +'</div>'

        +'<div class="col-md-1">'
        +'<div class="row">'
        +'<div class="form-group">'
        +'<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.unitPrice'); ?></label>'
        +'<input type="text" min="0" class="form-control cost_per_item" readonly value="0" name="cost_per_item[]">'
        +'</div>'
        +'</div>'

        +'</div>'

        +'<div class="col-md-2">'
        +'<div class="form-group">'
        +'<select class="form-control line_discount_type" name="line_discount_type[]">'
        +'<option value="percent">%</option>'
        +'<option value="fixed"><?php echo app('translator')->get('modules.invoices.amount'); ?></option>'
        +'</select>'
        +'</div>'
        +'</div>'

        +'<div class="col-md-1">'
        +'<div class="form-group">'
        +'<input type="number" min="0" value="0" name="line_discount_value[]" class="form-control line_discount_value">'
        +'</div>'
        +'</div>'

        // +'<div class="col-md-2">'
        // +'<div class="form-group">'
        // +'<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.tax'); ?></label>'
        // +'<select id="multiselect'+i+'" name="taxes['+i+'][]"  multiple="multiple" class="selectpicker form-control type">'
        //     <?php $__currentLoopData = $taxes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tax): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        // +'<option data-rate="<?php echo e($tax->rate_percent); ?>" value="<?php echo e($tax->id); ?>"><?php echo e($tax->tax_name.': '.$tax->rate_percent); ?>%</option>'
        //     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        // +'</select>'
        // +'</div>'
        // +'</div>'

        +'<div class="col-md-2 text-center">'
        +'<label class="control-label hidden-md hidden-lg"><?php echo app('translator')->get('modules.invoices.amount'); ?></label>'
        +'<p class="form-control-static"><span class="amount-html">0.00</span></p>'
        +'<input type="hidden" class="amount" name="amount[]">'
        +'</div>'

        +'<div class="col-md-1 text-right visible-md visible-lg">'
        +'<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'
        +'</div>'

        +'<div class="col-md-1 hidden-md hidden-lg">'
        +'<div class="row">'
        +'<button type="button" class="btn remove-item btn-danger"><i class="fa fa-remove"></i> <?php echo app('translator')->get('app.remove'); ?></button>'
        +'</div>'
        +'</div>'

        +'</div>';

        $(item).hide().appendTo("#sortable").fadeIn(500);
        $('#multiselect'+i).selectpicker();
        hsnSacColumn();
    });


    hsnSacColumn();
    function hsnSacColumn(){
        <?php if($invoiceSetting->hsn_sac_code_show): ?>
        $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').removeClass( "col-md-4");
        $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').addClass( "col-md-3");
        $('input[name="hsn_sac_code[]"]').parent("div").parent('div').show();
        <?php else: ?>
        $('input[name="hsn_sac_code[]"]').parent("div").parent('div').hide();
        $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').removeClass( "col-md-3");
        $('input[name="item_name[]"]').parent("div").parent('div').parent('div').parent('div').addClass( "col-md-4");
        <?php endif; ?>
    }

    $('#createContract').on('click','.remove-item', function () {
        $(this).closest('.item-row').fadeOut(300, function() {
            $(this).remove();
            calculateTotal();
        });
    });

    $('#createContract').on('keyup change','.quantity,.cost_per_item,.item_name, .discount_value, .line_discount_value', function () {
          
          var quantity = $(this).closest('.item-row').find('.quantity').val();

          var line_discount_value = $(this).closest('.item-row').find('.line_discount_value').val();

          var line_discount_type = $(this).closest('.item-row').find('.line_discount_type').val();

          var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

          if(line_discount_type == 'percent'){
              line_discounted_amount = parseFloat(perItemCost)*parseFloat(line_discount_value?line_discount_value:0) / 100;
          }
          else{
              line_discounted_amount = parseFloat(line_discount_value?line_discount_value:0);
          }

          var amount = (quantity* (perItemCost - line_discounted_amount));

          $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
          $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));

          calculateTotal();
      });

      $('#createContract').on('change','.line_discount_type', function () {
          var quantity = $(this).closest('.item-row').find('.quantity').val();

          var line_discount_value = $(this).closest('.item-row').find('.line_discount_value').val();

          var line_discount_type = $(this).closest('.item-row').find('.line_discount_type').val();

          var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

          if(line_discount_type == 'percent'){
              line_discounted_amount = parseFloat(perItemCost)*parseFloat(line_discount_value?line_discount_value:0) / 100;
          }
          else{
              line_discounted_amount = parseFloat(line_discount_value?line_discount_value:0);
          }

          var amount = (quantity* (perItemCost - line_discounted_amount));

          $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
          $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));

          calculateTotal();

      });


    $('#createContract').on('change','.type, #discount_type', function () {
        var quantity = $(this).closest('.item-row').find('.quantity').val();

        var perItemCost = $(this).closest('.item-row').find('.cost_per_item').val();

        var amount = (quantity*perItemCost);

        $(this).closest('.item-row').find('.amount').val(decimalupto2(amount));
        $(this).closest('.item-row').find('.amount-html').html(decimalupto2(amount));

        calculateTotal();


    });

    function calculateTotal()
    {
        var subtotal = 0;
        var discount = 0;
        var tax = '';
        var taxList = new Object();
        var taxTotal = 0;
        var discountType = $('#discount_type').val();
        var discountValue = $('.discount_value').val();

        $(".quantity").each(function (index, element) {
            var itemTax = [];
            var itemTaxName = [];
            var discountedAmount = 0;

            $(this).closest('.item-row').find('select.type option:selected').each(function (index) {
                itemTax[index] = $(this).data('rate');
                itemTaxName[index] = $(this).text();
            });
            var itemTaxId = $(this).closest('.item-row').find('select.type').val();

            var amount = parseFloat($(this).closest('.item-row').find('.amount').val());
            if(discountType == 'percent' && discountValue != ''){
                discountedAmount = parseFloat(amount - ((parseFloat(amount)/100)*parseFloat(discountValue)));
            }
            else{
                discountedAmount = parseFloat(amount - (parseFloat(discountValue)));
            }

            if(isNaN(amount)){ amount = 0; }

            subtotal = (parseFloat(subtotal)+parseFloat(amount)).toFixed(2);

            if(itemTaxId != ''){
                for(var i = 0; i<=itemTaxName.length; i++)
                {
                    if(typeof (taxList[itemTaxName[i]]) === 'undefined'){
                        if (discountedAmount > 0) {
                            taxList[itemTaxName[i]] = ((parseFloat(itemTax[i])/100)*parseFloat(discountedAmount));                         
                        } else {
                            taxList[itemTaxName[i]] = ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                        }
                    }
                    else{
                        if (discountedAmount > 0) {
                            taxList[itemTaxName[i]] = parseFloat(taxList[itemTaxName[i]]) + ((parseFloat(itemTax[i])/100)*parseFloat(discountedAmount));   
                            console.log(taxList[itemTaxName[i]]);
                         
                        } else {
                            taxList[itemTaxName[i]] = parseFloat(taxList[itemTaxName[i]]) + ((parseFloat(itemTax[i])/100)*parseFloat(amount));
                        }
                    }
                }
            }
        });


        $.each( taxList, function( key, value ) {
            if(!isNaN(value)){
                tax = tax+'<div class="col-md-offset-8 col-md-2 text-right p-t-10">'
                    +key
                    +'</div>'
                    +'<p class="form-control-static col-xs-6 col-md-2" >'
                    +'<span class="tax-percent">'+(decimalupto2(value)).toFixed(2)+'</span>'
                    +'</p>';
                taxTotal = taxTotal+decimalupto2(value);
            }
        });

        if(isNaN(subtotal)){  subtotal = 0; }

        $('.sub-total').html(decimalupto2(subtotal).toFixed(2));
        $('.sub-total-field').val(decimalupto2(subtotal));

        

        if(discountValue != ''){
            if(discountType == 'percent'){
                discount = ((parseFloat(subtotal)/100)*parseFloat(discountValue));
            }
            else{
                discount = parseFloat(discountValue);
            }

        }

        $('#invoice-taxes').html(tax);

        var totalAfterDiscount = decimalupto2(subtotal-discount);

        totalAfterDiscount = (totalAfterDiscount < 0) ? 0 : totalAfterDiscount;

        var total = decimalupto2(totalAfterDiscount+taxTotal);

        $('.total').html(total.toFixed(2));
        $('.total-field').val(total.toFixed(2));

    }

    $('#tax-settings').click(function () {
        var url = '<?php echo e(route('admin.taxes.create')); ?>';
        $('#modelHeading').html('Manage Project Category');
        $.ajaxModal('#taxModal', url);
    })

    function decimalupto2(num) {
        var amt =  Math.round(num * 100) / 100;
        return parseFloat(amt.toFixed(2));
    }

    $('#save-form').click(function () {
            $.easyAjax({
                url: '<?php echo e(route('admin.contracts.store')); ?>',
                container: '#createContract',
                type: "POST",
                file: (document.getElementById("company_logo").files.length == 0) ? false : true,
                redirect: true,
                data: $('#createContract').serialize()
            })
        });

    </script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/contracts/create.blade.php ENDPATH**/ ?>