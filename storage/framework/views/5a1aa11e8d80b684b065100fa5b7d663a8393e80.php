<?php $__empty_1 = true; $__currentLoopData = $project->invoices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $invoice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
    <li class="list-group-item">
        <div class="row">
            <div class="col-md-5">
                <?php echo e($invoice->invoice_number); ?>

            </div>
            <div class="col-md-2">
                <?php echo e(currency_position($invoice->total, $invoice->currency->currency_symbol)); ?>

            </div>
            <div class="col-md-2">
                <?php if($invoice->status == 'unpaid'): ?>
                    <label class="label label-danger"><?php echo app('translator')->get('modules.invoices.unpaid'); ?></label>
                <?php else: ?>
                    <label class="label label-success"><?php echo app('translator')->get('modules.invoices.paid'); ?></label>
                <?php endif; ?>
            </div>
            <div class="col-md-3">
                <a href="<?php echo e(route('admin.invoices.download', $invoice->id)); ?>" data-toggle="tooltip" data-original-title="Download" class="btn btn-default btn-circle"><i class="fa fa-download"></i></a>
                &nbsp;&nbsp;
                <a href="javascript:;" data-toggle="tooltip" data-original-title="Delete" data-invoice-id="<?php echo e($invoice->id); ?>" class="btn btn-danger btn-circle sa-params"><i class="fa fa-times"></i></a>
                <span class="m-l-10"><?php echo e($invoice->issue_date->format($global->date_format)); ?></span>
            </div>
        </div>
    </li>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
    <li class="list-group-item">
        <div class="row">
            <div class="col-md-7">
                <?php echo app('translator')->get('messages.noInvoice'); ?>
            </div>
        </div>
    </li>
<?php endif; ?>
<?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/projects/invoices/invoice-ajax.blade.php ENDPATH**/ ?>