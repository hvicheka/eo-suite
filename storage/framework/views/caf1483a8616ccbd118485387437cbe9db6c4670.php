

<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.leads.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.edit'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.css')); ?>">

<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> <?php echo app('translator')->get('modules.lead.updateTitle'); ?></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <?php echo Form::open(['id'=>'updateLead','class'=>'ajax-form','method'=>'PUT']); ?>

                        <div class="form-body">
                            <h3 class="box-title required"><?php echo app('translator')->get('modules.lead.companyDetails'); ?></h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo app('translator')->get('modules.lead.companyName'); ?></label>
                                                <input type="text" id="company_name" name="company_name" class="form-control"  value="<?php echo e($lead->company_name ?? ''); ?>">
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo app('translator')->get('modules.lead.website'); ?></label>
                                                <input type="text" id="website" name="website" class="form-control" value="<?php echo e($lead->website ?? ''); ?>" >
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
        
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo app('translator')->get('app.address'); ?></label>
                                                <textarea name="address"  id="address"  rows="5" class="form-control"><?php echo e($lead->address ?? ''); ?></textarea>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label><?php echo app('translator')->get('app.mobile'); ?></label>
                                            <?php $code = explode(" ",$lead->mobile); ?>
                                            <div class="form-group">
                                                <select class="select2 phone_country_code form-control" name="phone_code">
                                                    <option value="">--</option>
                                                    <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option
                                                                <?php if($item->phonecode == $code[0]): ?>
                                                                selected
                                                                <?php endif; ?>
                                                                value="<?php echo e($item->phonecode); ?>">+ <?php echo e($item->phonecode.' ('.$item->iso.')'); ?> </option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                                
                                                <input type="tel" name="mobile" id="mobile" class="mobile" autocomplete="nope" value="<?php echo e($lead->mobile); ?>">

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><?php echo app('translator')->get('modules.clients.officePhoneNumber'); ?></label>
                                                <input type="text" name="office_phone" id="office_phone" value="<?php echo e($lead->office_phone); ?>"  class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><?php echo app('translator')->get('modules.stripeCustomerAddress.city'); ?></label>
                                                <input type="text" name="city" id="city"  value="<?php echo e($lead->city); ?>" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><?php echo app('translator')->get('modules.stripeCustomerAddress.state'); ?></label>
                                                <input type="text" name="state" id="state"  value="<?php echo e($lead->state); ?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><?php echo app('translator')->get('modules.stripeCustomerAddress.country'); ?></label>
                                                <input type="text" name="country" id="country" value="<?php echo e($lead->country); ?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><?php echo app('translator')->get('modules.stripeCustomerAddress.postalCode'); ?></label>
                                                <input type="text" name="postal_code" id="postalCode" value="<?php echo e($lead->postal_code); ?>"class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-12 text-center" >
                                            
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px;">
                                                        <?php if($lead->image): ?>
                                                        <img src="<?php echo e($lead->image_url); ?>" alt=""/>                                    
                                                        <?php else: ?>
                                                        <img src="http://via.placeholder.com/200x150.png?text=<?php echo app('translator')->get('modules.contracts.companyLogo'); ?>"
                                                        alt=""/>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                                        style="max-width: 200px; max-height: 150px;">
                                                    </div>
                                                    <div>
                                                    <span class="btn btn-info btn-file">
                                                        <span class="fileinput-new"> <?php echo app('translator')->get('app.selectImage'); ?> </span>
                                                        <span class="fileinput-exists"> <?php echo app('translator')->get('app.change'); ?> </span>
                                                    <input type="file" name="image" id="company_logo"> </span>
                                                        <a href="javascript:;" class="btn btn-danger fileinput-exists"
                                                        data-dismiss="fileinput"> <?php echo app('translator')->get('app.remove'); ?> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                            
                                    </div> 
                                </div>
                            </div>


                            <h3 class="box-title m-t-40"><?php echo app('translator')->get('modules.lead.leadDetails'); ?></h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label required><?php echo app('translator')->get('modules.lead.clientName'); ?></label>
                                        <input type="text" name="client_name" id="client_name" class="form-control" value="<?php echo e($lead->client_name); ?>">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label required><?php echo app('translator')->get('modules.lead.clientEmail'); ?></label>
                                        <input type="email" name="email" id="client_email" class="form-control" value="<?php echo e($lead->client_email); ?>">
                                    </div>
                                </div>

                                

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="required"><?php echo app('translator')->get('app.position'); ?></label>
                                        <input type="text" id="position" name="position" class="form-control" value="<?php echo e($lead->position); ?>">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->get('app.lead'); ?> <?php echo app('translator')->get('app.value'); ?></label>
                                        <input type="number" min="0" value="<?php echo e($lead->value); ?>" name="value" id="value"  class="form-control">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for=""><?php echo app('translator')->get('app.lead_type'); ?></label>
                                        <select class="select2 form-control" id="lead_type" name="lead_type">
                                            <option value="new_lead" <?php echo e($lead->lead_type=='new_lead'?'selected':''); ?>>New Lead</option>
                                        </select>
                                    </div>
                                </div>

                    
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for=""><?php echo app('translator')->get('app.star_rating'); ?></label>
                                        <select class="select2 form-control" id="star_rating" name="star_rating">
                                            <option value="">Select Option</option>
                                            <?php $__currentLoopData = $starRating; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($key); ?>" <?php echo e($lead->star_rating == $key?'selected':''); ?>><?php echo e($item); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for=""><?php echo app('translator')->get('app.handle_by'); ?></label>
                                        <select class="select2 form-control" id="handly_by_id" name="handly_by_id">
                                            <option value="">Select Option</option>
                                            <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($employee->id); ?>" <?php echo e($lead->handly_by_id == $employee->id?'selected':''); ?>><?php echo e($employee->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Probability</label>
                                        <input type="number" min="0" value="<?php echo e($lead->probability); ?>" name="probability" id="probability"  class="form-control">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Lead Forecast</label>
                                        <select class="select2 form-control" id="lead_forecast" name="lead_forecast">
                                            <option value="">Select Option</option>
                                            <?php $__currentLoopData = $leadForecast; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($key); ?>" <?php echo e($lead->lead_forecast == $key?'selected':''); ?>><?php echo e($item); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->get('app.next_follow_up'); ?></label>
                                        <select name="next_follow_up" id="next_follow_up" class="form-control">
                                            <option <?php if($lead->next_follow_up == 'yes'): ?> selected
                                                    <?php endif; ?> value="yes"> <?php echo app('translator')->get('app.yes'); ?></option>
                                           <option <?php if($lead->next_follow_up == 'no'): ?> selected
                                                    <?php endif; ?> value="no"> <?php echo app('translator')->get('app.no'); ?></option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->get('app.source'); ?></label>
                                        <select name="source" id="source" class="form-control">
                                            <?php $__empty_1 = true; $__currentLoopData = $sources; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $source): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                <option <?php if($lead->source_id == $source->id): ?> selected
                                                        <?php endif; ?> value="<?php echo e($source->id); ?>"> <?php echo e(ucfirst($source->type)); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>

                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for=""><?php echo app('translator')->get('modules.tickets.chooseAgents'); ?> <a href="javascript:;"
                                                                                              id="addLeadAgent"
                                                                                              class="btn btn-sm btn-outline btn-success"><i
                                                        class="fa fa-plus"></i> <?php echo app('translator')->get('app.add'); ?> <?php echo app('translator')->get('app.leadAgent'); ?></a></label>
                                        <select class="select2 form-control" data-placeholder="<?php echo app('translator')->get('modules.tickets.chooseAgents'); ?>" id="agent_id" name="agent_id">
                                            <option value=""><?php echo app('translator')->get('modules.tickets.chooseAgents'); ?></option>
                                            <?php $__currentLoopData = $leadAgents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option  <?php if($emp->id == $lead->agent_id): ?> selected <?php endif; ?>  value="<?php echo e($emp->id); ?>"><?php echo e(ucwords($emp->user->name)); ?> <?php if($emp->user->id == $user->id): ?>
                                                        (YOU) <?php endif; ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                    
                                </div>
                                <div class="col-md-3">
                                             <div class="form-group" style="margin-top: 7px;">
                                              <label ><?php echo app('translator')->get('modules.lead.leadCategory'); ?>
                                                <a href="javascript:;" id="addLeadCategory" class="btn btn-xs btn-success btn-outline"><i class="fa fa-plus"></i></a>
                                                 </label>
                                            <select class="select2 form-control" name="category_id" id="category_id"
                                                data-style="form-control">
                                                <?php $__empty_1 = true; $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                            <option value="<?php echo e($category->id); ?>"
                                                    <?php if($lead->category_id == $category->id): ?>
                                                    selected
                                                    <?php endif; ?>
                                            ><?php echo e(ucwords($category->category_name)); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                            <option value=""><?php echo app('translator')->get('messages.noCategoryAdded'); ?></option>
                                        <?php endif; ?>
                                             </select>
                                         </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->get('app.status'); ?></label>
                                        <select name="status" id="status" class="form-control">
                                            <?php $__empty_1 = true; $__currentLoopData = $status; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sts): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                <option <?php if($lead->status_id == $sts->id): ?> selected
                                                        <?php endif; ?> value="<?php echo e($sts->id); ?>"> <?php echo e(ucfirst($sts->type)); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>

                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Repeat Order</label>
                                        <br>
                                        <label><input type="radio" name="repeat_order" <?php echo e($lead->repeat_order==0?'checked':''); ?>>No</label>
                                        |
                                        <label><input type="radio" name="repeat_order" <?php echo e($lead->repeat_order==1?'checked':''); ?>>Yes</label>
                                    </div>
                                </div>

                                <!--/span-->
                            </div>
                    
                            <!--/row-->

                            <div class="row">
                                <?php $__currentLoopData = $fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="col-md-4">
                                        <label><?php echo e(ucfirst($field->label)); ?></label>
                                        <div class="form-group">
                                            <?php if( $field->type == 'text'): ?>
                                                <input type="text" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" placeholder="<?php echo e($field->label); ?>" value="<?php echo e($lead->custom_fields_data['field_'.$field->id] ?? ''); ?>">
                                            <?php elseif($field->type == 'password'): ?>
                                                <input type="password" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" placeholder="<?php echo e($field->label); ?>" value="<?php echo e($lead->custom_fields_data['field_'.$field->id] ?? ''); ?>">
                                            <?php elseif($field->type == 'number'): ?>
                                                <input type="number" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" placeholder="<?php echo e($field->label); ?>" value="<?php echo e($lead->custom_fields_data['field_'.$field->id] ?? ''); ?>">

                                            <?php elseif($field->type == 'textarea'): ?>
                                                <textarea name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" id="<?php echo e($field->name); ?>" cols="3"><?php echo e($lead->custom_fields_data['field_'.$field->id] ?? ''); ?></textarea>

                                            <?php elseif($field->type == 'radio'): ?>
                                                <div class="radio-list">
                                                    <?php $__currentLoopData = $field->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <label class="radio-inline <?php if($key == 0): ?> p-0 <?php endif; ?>">
                                                            <div class="radio radio-info">
                                                                <input type="radio" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" id="optionsRadios<?php echo e($key.$field->id); ?>" value="<?php echo e($value); ?>" <?php if(isset($lead) && $lead->custom_fields_data['field_'.$field->id] == $value): ?> checked <?php elseif($key==0): ?> checked <?php endif; ?>>>
                                                                <label for="optionsRadios<?php echo e($key.$field->id); ?>"><?php echo e($value); ?></label>
                                                            </div>
                                                        </label>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </div>
                                            <?php elseif($field->type == 'select'): ?>
                                                <?php echo Form::select('custom_fields_data['.$field->name.'_'.$field->id.']',
                                                        $field->values,
                                                         isset($lead)?$lead->custom_fields_data['field_'.$field->id]:'',['class' => 'form-control gender']); ?>


                                            <?php elseif($field->type == 'checkbox'): ?>
                                            <div class="mt-checkbox-inline custom-checkbox checkbox-<?php echo e($field->id); ?>">
                                                <input type="hidden" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" 
                                                id="<?php echo e($field->name.'_'.$field->id); ?>" value="<?php echo e($lead->custom_fields_data['field_'.$field->id]); ?>">
                                                <?php $__currentLoopData = $field->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <label class="mt-checkbox mt-checkbox-outline">
                                                        <input name="<?php echo e($field->name.'_'.$field->id); ?>[]" class="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]"
                                                               type="checkbox" value="<?php echo e($value); ?>" onchange="checkboxChange('checkbox-<?php echo e($field->id); ?>', '<?php echo e($field->name.'_'.$field->id); ?>')"
                                                               <?php if($lead->custom_fields_data['field_'.$field->id] != '' && in_array($value ,explode(', ', $lead->custom_fields_data['field_'.$field->id]))): ?> checked <?php endif; ?> > <?php echo e($value); ?>

                                                        <span></span>
                                                    </label>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>
                                            <?php elseif($field->type == 'date'): ?>
                                            <input type="text" class="form-control date-picker" size="16" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]"
                                            value="<?php echo e(($lead->custom_fields_data['field_'.$field->id] != '') ? \Carbon\Carbon::parse($lead->custom_fields_data['field_'.$field->id])->format($global->date_format) : \Carbon\Carbon::now()->format($global->date_format)); ?>">
                                            <?php endif; ?>
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block"></span>

                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <label><?php echo app('translator')->get('app.note'); ?></label>
                                    <div class="form-group">
                                        <textarea name="note" id="note" class="form-control summernote" rows="5"><?php echo e($lead->note ?? ''); ?></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> <?php echo app('translator')->get('app.update'); ?></button>
                            <a href="<?php echo e(route('admin.leads.index')); ?>" class="btn btn-default"><?php echo app('translator')->get('app.back'); ?></a>
                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->
    
    <div class="modal fade bs-modal-md in" id="projectCategoryModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    
<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.min.js')); ?>"></script>

<script type="text/javascript">
    function checkboxChange(parentClass, id){
        var checkedData = '';
        $('.'+parentClass).find("input[type= 'checkbox']:checked").each(function () {
            if(checkedData !== ''){
                checkedData = checkedData+', '+$(this).val();
            }
            else{
                checkedData = $(this).val();
            }
        });
        $('#'+id).val(checkedData);
    }

    $(".select2").select2({
        formatNoMatches: function () {
            return "<?php echo e(__('messages.noRecordFound')); ?>";
        }
    });
    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });
    $(".date-picker").datepicker({
        todayHighlight: true,
        autoclose: true,
        weekStart:'<?php echo e($global->week_start); ?>',
    });

    $('#updateLead').on('click', '#addLeadAgent', function () {
        var url = '<?php echo e(route('admin.lead-agent-settings.create')); ?>';
        $('#modelHeading').html('Manage Lead Agent');
        $.ajaxModal('#projectCategoryModal', url);
    })

    $('#save-form').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.leads.update', [$lead->id])); ?>',
            container: '#updateLead',
            type: "POST",
            redirect: true,
            file: (document.getElementById("company_logo").files.length == 0) ? false : true,
            data: $('#updateLead').serialize()
        })
    });
    $('#addLeadCategory').click(function () {
        var url = '<?php echo e(route('admin.leadCategory.create')); ?>';
        $('#modelHeading').html('...');
        $.ajaxModal('#projectCategoryModal', url);
    })
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/lead/edit.blade.php ENDPATH**/ ?>