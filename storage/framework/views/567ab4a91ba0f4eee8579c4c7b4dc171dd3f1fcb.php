<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right bg-title-right">
            <a href="<?php echo e(route('admin.currency.create')); ?>"
               class="btn btn-outline btn-success btn-sm"><?php echo app('translator')->get('modules.currencySettings.addNewCurrency'); ?> <i
                        class="fa fa-plus" aria-hidden="true"></i></a>
            <a href="javascript:;" id="update-exchange-rates"
               class="btn btn-outline btn-info btn-sm"><?php echo app('translator')->get('app.update'); ?> <?php echo app('translator')->get('modules.currencySettings.exchangeRate'); ?>
                <i class="fa fa-refresh" aria-hidden="true"></i></a>

            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('super-admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li class="active"><?php echo e(__($pageTitle)); ?></li>
            </ol>
        </div>

        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li class="active"><?php echo e(__($pageTitle)); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel">

                <div class=" sttabs tabs-style-line">
                    <?php echo $__env->make('admin.currencies.tabs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                    <?php echo $__env->make('sections.admin_setting_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                    <div class="tab-content">
                        <div id="vhome3" class="tab-pane active">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="white-box">
                                        <h3 class="box-title m-b-0"><?php echo app('translator')->get('modules.currencySettings.currencies'); ?></h3>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="alert alert-info"><i
                                                            class="fa fa-info-circle"></i> <?php echo app('translator')->get('messages.exchangeRateNote'); ?>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th><?php echo app('translator')->get('modules.currencySettings.currencyName'); ?></th>
                                                    <th><?php echo app('translator')->get('modules.currencySettings.currencySymbol'); ?></th>
                                                    <th><?php echo app('translator')->get('modules.currencySettings.currencyPosition'); ?></th>
                                                    <th><?php echo app('translator')->get('modules.currencySettings.currencyCode'); ?></th>
                                                    <th><?php echo app('translator')->get('modules.currencySettings.exchangeRate'); ?></th>
                                                    <th><?php echo app('translator')->get('app.status'); ?></th>
                                                    <th class="text-nowrap"><?php echo app('translator')->get('app.action'); ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $__currentLoopData = $currencies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $currency): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><?php echo e(ucwords($currency->currency_name)); ?> <?php echo ($global->currency_id == $currency->id) ? "<label class='label label-success'>DEFAULT</label>" : ""; ?></td>
                                                        <td><?php echo e($currency->currency_symbol); ?></td>
                                                        <td><?php echo app('translator')->get('modules.currencySettings.'.$currency->currency_position); ?></td>
                                                        <td><?php echo e($currency->currency_code); ?></td>
                                                        <td><?php echo e($currency->exchange_rate); ?></td>
                                                        <td>
                                                            <?php
                                                                $class = ($currency->status == 'enable') ? 'label-custom' : 'label-danger';
                                                            ?>
                                                            <span class="label <?php echo e($class); ?>"><?php echo e(ucfirst($currency->status)); ?></span>
                                                        </td>
                                                        <td class="text-nowrap">
                                                            <a href="<?php echo e(route('admin.currency.edit', [$currency->id])); ?>"
                                                               class="btn btn-info btn-circle"
                                                               data-toggle="tooltip" data-original-title="Edit"><i
                                                                        class="fa fa-pencil" aria-hidden="true"></i></a>

                                                            
                                                            
                                                        </td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>    <!-- .row -->

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>
    <!-- .row -->
    
    <div class="modal fade bs-modal-md in" id="projectCategoryModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
    <script>
        $('body').on('click', '.sa-params', function () {
            var id = $(this).data('currency-id');
            swal({
                title: "<?php echo app('translator')->get('messages.sweetAlertTitle'); ?>",
                text: "<?php echo app('translator')->get('messages.confirmation.deleteCurrancy'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo app('translator')->get('messages.deleteConfirmation'); ?>",
                cancelButtonText: "<?php echo app('translator')->get('messages.confirmNoArchive'); ?>",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {

                    var url = "<?php echo e(route('admin.currency.destroy',':id')); ?>";
                    url = url.replace(':id', id);

                    var token = "<?php echo e(csrf_token()); ?>";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                window.location.reload();
                            }
                        }
                    });
                }
            });
        });
        $('ul.showClientTabs .currencySetting').addClass('tab-current');

        $('#update-exchange-rates').click(function () {
            var url = '<?php echo e(route('admin.currency.update-exchange-rates')); ?>';
            $.easyAjax({
                url: url,
                type: "GET",
                success: function (response) {
                    if (response.status == "success") {
                        $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                        window.location.reload();
                    }
                }
            })
        });

    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/currencies/index.blade.php ENDPATH**/ ?>