<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Template CSS -->
    <!-- <link type="text/css" rel="stylesheet" media="all" href="css/main.css"> -->

    <title>Proposal # <?php echo e($proposal->id); ?></title>
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <style>

body {
            margin: 0;
            font-family: 'Open Sans', sans-serif !important;
            font-weight: 'Light' 300;
        }

        .bg-grey {
            background-color: #F2F4F7;
        }

        .bg-white {
            background-color: #fff;
        }

       
        .text-black {
            color: #28313c;
        }

        .text-grey {
            color: #616e80;
        }

       

        .text-uppercase {
            text-transform: uppercase;
        }

        .text-capitalize {
            text-transform: capitalize;
        }

        /* .line-height {
            line-height: 24px;
        } */

        .mt-1 {
            margin-top: 1rem;
        }

        .mb-0 {
            margin-bottom: 0px;
        }

        .b-collapse {
            border-collapse: collapse;
            border: 1px solid #2b2a2a;
        }

        .heading-table-left {
            padding: 6px;
             border: 1px solid #2b2a2a;
            font-weight: bold;
            background-color: #f1f1f3;
            border-right: 0;
        }

        .heading-table-right {
            padding: 6px;
             border: 1px solid #2b2a2a;
            border-left: 0;
        }

        .unpaid {
            color: #000000;
            border: 1px solid #000000;
            position: relative;
            padding: 11px 22px;
            
            border-radius: 0.25rem;
            width: 100px;
            text-align: center;
        }

        .main-table-heading {
             border: 1px solid #2b2a2a;
            background-color: #f1f1f3;
            font-weight: 700;
            white-space: nowrap;
        }

        .main-table-heading td {
            padding: 11px 10px;
             border: 1px solid #2b2a2a;
            font-size: 12px !important;
            
        }

        .main-table-items td {
            padding: 11px 10px;
             border: 1px solid #2b2a2a;
            font-size: 12px !important;
        }

        .total-box {
             border: 1px solid #2b2a2a;
            padding: 0px;
            border-bottom: 0px;
            font-size: 12px !important;
        }

        .subtotal {
            padding: 9px 10px;
             border: 1px solid #2b2a2a;
            border-top: 0;
            border-left: 0;
            font-size: 12px !important;
        }

        .subtotal-amt {
            padding: 9px 10px;
             border: 1px solid #2b2a2a;
            border-top: 0;
            border-right: 0;
            font-size: 12px !important;
        }

        .total {
            padding: 9px 10px;
             border: 1px solid #2b2a2a;
            border-top: 0;
            font-weight: 700;
            border-left: 0;
            font-size: 12px !important;
        }

        .total-amt {
            padding: 9px 10px;
             border: 1px solid #2b2a2a;
            border-top: 0;
            border-right: 0;
            font-weight: 700;
            font-size: 12px !important;
        }

        .balance {
            
            font-weight: bold;
            background-color: #f1f1f3;
        }

        .balance-left {
            padding: 9px 10px;
             border: 1px solid #2b2a2a;
            border-top: 0;
            border-left: 0;
        }

        .balance-right {
            padding: 9px 10px;
             border: 1px solid #2b2a2a;
            border-top: 0;
            border-right: 0;
        }

        .centered {
            margin: 0 auto;
        }

        .rightaligned {
            margin-right: 0;
            margin-left: auto;
        }

        .leftaligned {
            margin-left: 0;
            margin-right: auto;
        }

        .page_break {
            page-break-before: always;
        }
        #logo {
            height: 60px;
        }
        #contract_sign{
            height: 60px;
        }
        .text-center{
            text-align: center;
        }
        .text-right{
            text-align: right;
        }
        hr{
            border: 0.1px solid gray;
        }
        
        div table {
            /* font-size: 8px !important; */
            border-collapse: collapse !important;
            width: 100%;
            font-family: 'Source Sans Pro', sans-serif;
            font-weight: normal !important;
            /* margin: 0 5px; */
        }
        div table tr td{
            font-size: 11px !important;
            padding: 5px !important;
        }
        ul li {
            font-size: 14px !important;
            font-family: 'Source Sans Pro', sans-serif;
            font-weight: normal !important;
            color: #404040;
            padding: 2px 0;
        }
    </style>

</head>

<body class="content-wrapper">
<br />
<?php
    $colspan = ($invoiceSetting->hsn_sac_code_show) ? 4 : 3;
?>



<?php
    if(invoice_setting()->logo_url) {
        $url =  invoice_setting()->logo_url;
    }else{
        $url =  '';
    }
    $url =  invoice_setting()->logo_url;
    $company_logo = "<img src='$url' alt='' id='logo'/>";
    if ($proposal->discount > 0) {
        if ($proposal->discount_type == 'percent') {
            $discount = number_format((float) $proposal->discount, 2, '.', '').'%';
        } else {
            $discount = currency_formatter($proposal->discount,$proposal->currency->currency_symbol);
        }
    } else {
        $discount = 0;
    }
    $product_list = '<table width="100%" class="f-14 b-collapse">';
    $product_list .= '<tr class="main-table-heading">';
    $product_list .= '<td class="text-center">'.__("modules.invoices.no").'</td>';
    $product_list .= '<td class="text-center">'.__("modules.invoices.item").'</td>';
    $product_list .= '<td class="text-center">'.__("app.description").'</td>';
    $product_list .= '<td class="text-center">'.__("modules.invoices.qty").'</td>';
    $product_list .= '<td class="text-center">'.__("modules.invoices.unitPrice").'</td>';
    $product_list .= '<td class="text-center">'.__("modules.invoices.discount").'</td>';
    $product_list .= '<td class="text-center">'.__("modules.invoices.amount").'</td>';
    $product_list .= '<tbody>';
        
        foreach($proposal->items as $key => $item){

            if($item->discount_type == 'percent'){
                $line_discount_amount = number_format((float) $item->discount, 0, '.', '').'%';
            }else{
                $line_discount_amount = currency_formatter($item->discount,$proposal->currency->currency_symbol);
            }

            $product_list .= '<tr class="main-table-items" style="font-size: 12px;">';
            $product_list .= '<td>'.++$key.'</td>';
            $product_list .= '<td>'.$item->item_name.'</td>';
            $product_list .= '<td>'.$item->item_summary.'</td>';
            $product_list .= '<td class="text-center">'.$item->quantity.'</td>';
            $product_list .= '<td class="text-center">'.currency_formatter($item->unit_price,$proposal->currency->currency_symbol).'</td>';
            $product_list .= '<td class="text-center">'.$line_discount_amount.'</td>';
            $product_list .= '<td class="text-center">'.currency_formatter($item->amount,$proposal->currency->currency_symbol).'</td>';
            $product_list .= '</tr>';
        }

    $product_list .= '<tr align="right" class="">
                        <td colspan="6" class="subtotal">'.__("modules.invoices.subTotal").'</td>
                        <td class="subtotal-amt">'.currency_formatter($proposal->sub_total,$proposal->currency->currency_symbol).'</td>
                        </tr>';
          
    $product_list .= '  <tr align="right" class="">
                        <td colspan="6" class="subtotal">'.__("modules.invoices.discount").'</td>
                        <td class="subtotal-amt">'.
                            $discount
                            .'</td>
                        </tr>';
       
    foreach ($taxes as $key => $tax){
        $product_list .='<tr align="right" class="">
                            <td colspan="6" class="subtotal">'.strtoupper($key).'</td>
                            <td class="subtotal-amt">'.currency_formatter($tax,$proposal->currency->currency_symbol).'</td>
                        </tr>';
    }
    
    $product_list .= '<tr align="right" class="">
                <td colspan="6" class="total">'.__("modules.invoices.total").'</td>
                <td class="total-amt f-15">'.currency_formatter($proposal->total,$proposal->currency->currency_symbol).'</td>
             </tr>';

    $product_list .= '</tbody></table>';
    // *****************************************************
            
    $product_features = '';
    foreach ($products as $key => $product) {
        $product_features .= $product->feature;
    }
?>

<?php
$content = $quotation_theme->description;
$content = str_replace("{{company-logo}}",$company_logo,$content);
$content = str_replace(" {{quotation-number}}",$proposal->id,$content);
$content = str_replace("{{created-date}}",date('d-M-Y',strtotime($proposal->created_at)),$content);
$content = str_replace("{{company-name}}",ucwords($global->company_name),$content);
$content = str_replace("{{customer-name}}",$proposal->company_name,$content);
$content = str_replace("{{valid-from}}",date('d-M-Y',strtotime($proposal->valid_from)),$content);
$content = str_replace("{{valid-to}}",date('d-M-Y',strtotime($proposal->valid_till)),$content);
$content = str_replace("{{product-list}}",$product_list,$content);
$content = str_replace("{{product-feature}}",$product_features,$content);

?>

    <?php echo $content; ?>


    
     
</body>

</html>
<script>

    window.print();
    window.onafterprint = function(){
        window.close();
    }

</script>
<?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/proposals/proposal-pdfnew.blade.php ENDPATH**/ ?>