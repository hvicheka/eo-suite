<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo app('translator')->get($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.expenses-recurring.index')); ?>"><?php echo app('translator')->get($pageTitle); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.addNew'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.css')); ?>">

<style>
    .recurringPayment {
        display: none;
    }
    .label-font{
        font-weight: 500 !important;
    }
</style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> <?php echo app('translator')->get('modules.expenses.addExpense'); ?></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <?php echo Form::open(['id'=>'createExpense','class'=>'ajax-form','method'=>'POST']); ?>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="required"><?php echo app('translator')->get('modules.expenses.itemName'); ?></label>
                                        <input type="text" name="item_name" id="item_name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.expenses.expenseCategory'); ?>
                                            <a href="javascript:;" id="addExpenseCategory" class="btn btn-xs btn-success btn-outline"><i class="fa fa-plus"></i></a>
                                        </label>
                                        <select class="select2 form-control drop_down" name="category_id" id="category_id"
                                                data-style="form-control">
                                            <?php $__empty_1 = true; $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                <option value="<?php echo e($category->id); ?>"><?php echo e(ucwords($category->category_name)); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                <option value=""><?php echo app('translator')->get('messages.noProjectCategoryAdded'); ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->get('app.description'); ?></label>
                                        <textarea type="text" name="description" class="form-control summernote" id="SummernoteText"> </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label><?php echo app('translator')->get('modules.messages.chooseMember'); ?></label>
                                        <select id="user_id" class="select2 form-control drop_down" data-placeholder="<?php echo app('translator')->get('modules.messages.chooseMember'); ?>" name="user_id">
                                            <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($employee['user']['id']); ?>"><?php echo e(ucwords($employee['user']['name'])); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div>

                                <!--/span-->

                                <div class="col-sm-12 col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="project_id"><?php echo app('translator')->get('modules.invoices.project'); ?></label>
                                        <select class="select2 form-control drop_down" id="project_id" name="project_id">
                                            <option value="0"><?php echo app('translator')->get('app.selectProject'); ?></option>
                                            <?php $__empty_1 = true; $__currentLoopData = $employees[0]['user']['projects']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                <option value="<?php echo e($project['id']); ?>">
                                                    <?php echo e($project['project_name']); ?>

                                                </option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="required"><?php echo app('translator')->get('app.price'); ?></label>
                                        <input type="text" name="price" id="price" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->

                                <div class="col-sm-12 col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="company_phone"><?php echo app('translator')->get('modules.invoices.currency'); ?></label>
                                        <select class="form-control select2 drop_down" id="currency_id" name="currency_id">
                                            <?php $__empty_1 = true; $__currentLoopData = $currencies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $currency): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                <option <?php if($currency->id == $global->currency_id): ?> selected <?php endif; ?> value="<?php echo e($currency->id); ?>">
                                                    <?php echo e($currency->currency_name); ?> - (<?php echo e($currency->currency_symbol); ?>)
                                                </option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>

                                <!--/span-->
                            </div>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.invoices.billingFrequency'); ?></label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select class="form-control" onchange="changeRotation(this.value);" name="rotation" id="rotation">
                                                    <option value="daily"><?php echo app('translator')->get('app.daily'); ?></option>
                                                    <option value="weekly"><?php echo app('translator')->get('app.weekly'); ?></option>
                                                    <option value="bi-weekly"><?php echo app('translator')->get('app.bi-weekly'); ?></option>
                                                    <option value="monthly"><?php echo app('translator')->get('app.monthly'); ?></option>
                                                    <option value="quarterly"><?php echo app('translator')->get('app.quarterly'); ?></option>
                                                    <option value="half-yearly"><?php echo app('translator')->get('app.half-yearly'); ?></option>
                                                    <option value="annually"><?php echo app('translator')->get('app.annually'); ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 dayOfWeek">
                                    <div class="form-group">
                                        <label class="control-label required"><?php echo app('translator')->get('modules.expensesRecurring.dayOfWeek'); ?></label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select class="form-control required"  name="day_of_week" id="dayOfWeek">
                                                    <option value="1"><?php echo app('translator')->get('app.sunday'); ?></option>
                                                    <option value="2"><?php echo app('translator')->get('app.monday'); ?></option>
                                                    <option value="3"><?php echo app('translator')->get('app.tuesday'); ?></option>
                                                    <option value="4"><?php echo app('translator')->get('app.wednesday'); ?></option>
                                                    <option value="5"><?php echo app('translator')->get('app.thursday'); ?></option>
                                                    <option value="6"><?php echo app('translator')->get('app.friday'); ?></option>
                                                    <option value="7"><?php echo app('translator')->get('app.saturday'); ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 dayOfMonth">
                                    <div class="form-group">
                                        <label class="control-label required"><?php echo app('translator')->get('modules.expensesRecurring.dayOfMonth'); ?></label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select class="form-control"  name="day_of_month" id="dayOfMonth">
                                                    <?php for($m=1; $m<=31; ++$m): ?>
                                                        <option value="<?php echo e($m); ?>"><?php echo e($m); ?></option>
                                                    <?php endfor; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6 billingInterval">
                                    <div class="form-group">
                                        <label class="control-label "><?php echo app('translator')->get('modules.invoices.billingCycle'); ?></label>
                                        <div class="input-icon">
                                            <input type="number" class="form-control" name="billing_cycle" id="billing_cycle" value="">
                                        </div>
                                        <p class="text-bold"><?php echo app('translator')->get('messages.setForInfinite'); ?></p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('app.invoice'); ?></label>
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                            <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new"><?php echo app('translator')->get('app.selectFile'); ?></span> <span class="fileinput-exists"><?php echo app('translator')->get('app.change'); ?></span>
                                            <input type="file" name="bill" id="bill">
                                            </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput"><?php echo app('translator')->get('app.remove'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form-2" class="btn btn-success"><i class="fa fa-check"></i>
                                <?php echo app('translator')->get('app.save'); ?>
                            </button>
                            <button type="reset" class="btn btn-default reset-form"><?php echo app('translator')->get('app.reset'); ?></button>
                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->
    
    <div class="modal fade bs-modal-md in" id="expenseCategoryModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    
<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
<script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/switchery/dist/switchery.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/summernote/dist/summernote.min.js')); ?>"></script>

<script>
    var employees = <?php echo json_encode($employees, 15, 512) ?>;
    // Switchery
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function () {
        new Switchery($(this)[0], $(this).data());
    });

    changeRotation($('#rotation').val());

    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });

    function changeRotation (rotationValue){
        if(rotationValue == 'weekly' || rotationValue == 'bi-weekly'){
            $('.dayOfWeek').show().fadeIn(300);
            $('.dayOfMonth').hide().fadeOut(300);
        }
        else if(rotationValue == 'monthly' || rotationValue == 'quarterly' || rotationValue == 'half-yearly' || rotationValue == 'annually'){
            $('.dayOfWeek').hide().fadeOut(300);
            $('.dayOfMonth').show().fadeIn(300);
        }
        else{
            $('.dayOfWeek').hide().fadeOut(300);
            $('.dayOfMonth').hide().fadeOut(300);
        }
    }

    $('#user_id').change(function (e) {
        // get projects of selected users
        var opts = '';

        var employee = employees.filter(function (item) {
            return item.user_id == e.target.value
        });

        employee[0].user.projects.forEach(project => {
            opts += `<option value='${project.id}'>${project.project_name}</option>`
        })

        $('#project_id').html('<option value="0">Select Project...</option>'+opts)
    });
    

    $(".select2").select2({
        formatNoMatches: function () {
            return "<?php echo e(__('messages.noRecordFound')); ?>";
        }
    });

    $(".reset-form").click(function() {
        $('#SummernoteText').summernote('reset');
             $("#createExpense").trigger("reset");
                $('select.drop_down').select2({
                    allowClear: true 
                });
     });
    $('#addExpenseCategory').click(function () {
        var url = '<?php echo e(route('admin.expenseCategory.create-cat')); ?>';
        $('#modelHeading').html('...');
        $.ajaxModal('#expenseCategoryModal', url);
    });

    $('#infinite-expenses').change(function () {
        if($(this).is(':checked')){
            $('.billingInterval').hide();
        }
        else{
            $('.billingInterval').show();
        }
    });

    jQuery('#purchase_date').datepicker({
        format: '<?php echo e($global->date_picker_format); ?>',
        autoclose: true,
        todayHighlight: true
    });

    $('#save-form-2').click(function () {
        $.easyAjax({
            url: '<?php echo e(route('admin.expenses-recurring.store')); ?>',
            container: '#createExpense',
            type: "POST",
            redirect: true,
            file: (document.getElementById("bill").files.length == 0) ? false : true,
            data: $('#createExpense').serialize()
        })
    });
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/expenses-recurring/create.blade.php ENDPATH**/ ?>