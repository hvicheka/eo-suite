

<?php $__env->startSection('page-title'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 bg-title-left">
            <h4 class="page-title"><i class="<?php echo e($pageIcon); ?>"></i> <?php echo e(__($pageTitle)); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 bg-title-right">
            <ol class="breadcrumb">
                <li><a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo app('translator')->get('app.menu.home'); ?></a></li>
                <li><a href="<?php echo e(route('admin.products.index')); ?>"><?php echo e(__($pageTitle)); ?></a></li>
                <li class="active"><?php echo app('translator')->get('app.update'); ?> <?php echo app('translator')->get('app.menu.products'); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('head-script'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/bower_components/html5-editor/bootstrap-wysihtml5.css')); ?>">

<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> <?php echo app('translator')->get('app.update'); ?> <?php echo app('translator')->get('app.menu.products'); ?></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <?php echo Form::open(['id'=>'updateProduct','class'=>'ajax-form']); ?>

                        <input name="_method" value="PUT" type="hidden">
                        <div class="form-body">
                            <h3 class="box-title"><?php echo app('translator')->get('app.menu.products'); ?> <?php echo app('translator')->get('app.details'); ?></h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('app.name'); ?></label>
                                            <input type="text" id="name" name="name" class="form-control" value="<?php echo e($product->name); ?>">
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('app.price'); ?></label>
                                            <input type="number" min="0" id="price" name="price" class="form-control" value="<?php echo e($product->price); ?>">
                                            <span class="help-block"> <?php echo app('translator')->get('messages.productPrice'); ?></span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-xs-12 col-md-6 ">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('modules.productCategory.productCategory'); ?> <a href="javascript:;" id="addProjectCategory" class="text-info"><i class="ti-settings text-info"></i></a>
                                            </label>
                                            <select class="selectpicker form-control" name="category_id" id="category_id"
                                                    data-style="form-control">
                                                <option value=""><?php echo app('translator')->get('messages.pleaseSelectCategory'); ?></option>
                                                <?php $__empty_1 = true; $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                                    <option <?php if( $category->id == $product->category_id): ?> selected <?php endif; ?> value="<?php echo e($category->id); ?>"><?php echo e(ucwords($category->category_name)); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                                    <option value=""><?php echo app('translator')->get('messages.noProductCategory'); ?></option>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6 ">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('modules.productCategory.productSubCategory'); ?> <a href="javascript:;" id="addProductSubCategory" class="text-info"><i class="ti-settings text-info"></i></a>
                                            </label>
                                            <select class="select2 form-control" name="sub_category_id" id="sub_category_id"
                                                    data-style="form-control">
                                                <option value=""><?php echo app('translator')->get('messages.selectSubCategory'); ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <br>
                                    <div class="wrap-image text-center">
                                        <input type="file" id="image" name="image" style="display: none;"/>
                                        <a href="javascript:changeProfile()" id="upload" class="">
                                            <img class="preveiw_image"  id="preview" src="<?php echo e($product->img_name?asset('user-uploads/product/'.$product->img_name):asset('img/default-image.png')); ?>" alt="" style="width: 176px;height: 180px;object-fit: cover;">
                                        </a>
                                        <div class="btn-remove">
                                            <a href="javascript:removeImage()">Remove</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--/row-->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('modules.invoices.tax'); ?> <a href="javascript:;" id="tax-settings" ><i class="ti-settings text-info"></i></a></label>
                                        <select id="multiselect" name="tax[]"  multiple="multiple" class="selectpicker form-control type">
                                            <?php $__currentLoopData = $taxes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tax): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option <?php if(isset($product->taxes) && $product->taxes != "null"  && array_search($tax->id, json_decode($product->taxes)) !== false): ?>
                                                        selected
                                                        <?php endif; ?>
                                                        value="<?php echo e($tax->id); ?>"><?php echo e($tax->tax_name); ?>: <?php echo e($tax->rate_percent); ?>%</option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div>
                                <?php if($invoiceSetting->hsn_sac_code_show): ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo app('translator')->get('app.hsnSacCode'); ?></label>
                                            <input type="text" id="hsn_sac_code" value="<?php echo e($product->hsn_sac_code); ?>" name="hsn_sac_code" class="form-control" >
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('app.description'); ?></label>
                                        <textarea name="description" id="" cols="30" rows="10" class="form-control summernote"><?php echo e($product->description); ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo app('translator')->get('app.feature'); ?></label>
                                        <textarea name="feature" id="" cols="30" rows="20" class="form-control summernote"><?php echo e($product->feature); ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">

                                        <div class="checkbox checkbox-info">
                                            <input id="purchase_allow" name="purchase_allow" value="no"
                                                   type="checkbox" <?php if($product->allow_purchase == 1): ?> checked <?php endif; ?>>
                                            <label for="purchase_allow"><?php echo app('translator')->get('app.purchaseAllow'); ?></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> <?php echo app('translator')->get('app.save'); ?></button>

                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="modal fade bs-modal-md in" id="taxModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    

<?php $__env->stopSection(); ?>

<?php $__env->startPush('footer-script'); ?>
    <script src="<?php echo e(asset('plugins/bower_components/custom-select/custom-select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/html5-editor/wysihtml5-0.3.0.js')); ?>"></script>
    <script src="<?php echo e(asset('plugins/bower_components/html5-editor/bootstrap-wysihtml5.js')); ?>"></script>
    <script>
        $('.textarea_editor').wysihtml5();
        $('.textarea_feature').wysihtml5();
        function changeProfile() {
            $('#image').click();
        }
        $('#image').change(function () {
            var imgPath = this.value;
            var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
                readURL(this);
            else
                alert("Please select image file (jpg, jpeg, png).")
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.readAsDataURL(input.files[0]);
                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                    // $('#preview_a').attr('src', e.target.result);
                };
                $("#remove").val(0);
            }
        }
        function removeImage() {
            $('#preview').attr('src', '<?php echo e(asset('img/default-image.png')); ?>');
            $("#remove").val(1);
        }
        var subCategories = <?php echo json_encode($subCategories, 15, 512) ?>;
        var product = <?php echo json_encode($product, 15, 512) ?>;
        var defaultOpt = '<option <?php if(is_null($product->sub_category_id)): ?> selected <?php endif; ?> value="">Select Sub Category...</option>'

        var subCategory = subCategories.filter(function (item) {
            return item.id == product.sub_category_id
        })

        var options =  '';

        subCategory.forEach(project => {
            options += `<option ${project.id === product.sub_category_id ? 'selected' : ''} value='${project.id}'>${project.category_name}</option>`
        })

        $('#sub_category_id').html(defaultOpt+options);

        $('#category_id').change(function (e) {
            // get projects of selected users
            var opts = '';

            var subCategory = subCategories.filter(function (item) {
                return item.category_id == e.target.value
            });
            subCategory.forEach(project => {
                console.log(project);
                opts += `<option value='${project.id}'>${project.category_name}</option>`
            })

            $('#sub_category_id').html('<option value="0">Select Sub Category...</option>'+opts)
            $("#sub_category_id").select2({
                formatNoMatches: function () {
                    return "<?php echo e(__('messages.noRecordFound')); ?>";
                }
            });
        });

        $('#updateProduct').on('click', '#addProjectCategory', function () {

            var url = '<?php echo e(route('admin.productCategory.create')); ?>';
            $('#modelHeading').html('Manage Project Category');
            $.ajaxModal('#taxModal', url);
        });

        $('#updateProduct').on('click', '#addProductSubCategory', function () {
            var catID = $('#category_id').val();
            var url = '<?php echo e(route('admin.productSubCategory.create')); ?>?catID='+catID;
            $('#modelHeading').html('Manage Project Sub Category');
            $.ajaxModal('#taxModal', url);
        });

        $(".select2").select2({
            formatNoMatches: function () {
                return "<?php echo e(__('messages.noRecordFound')); ?>";
            }
        });

        $('#tax-settings').on('click', function (event) {
            event.preventDefault();
            var url = '<?php echo e(route('admin.taxes.create')); ?>';
            $('#modelHeading').html('Manage Project Category');
            $.ajaxModal('#taxModal', url);
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '<?php echo e(route('admin.products.update', [$product->id])); ?>',
                container: '#updateProduct',
                type: "POST",
                file: true,
                redirect: true,
                data: $('#updateProduct').serialize()
            })
        });
    </script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/products/edit.blade.php ENDPATH**/ ?>