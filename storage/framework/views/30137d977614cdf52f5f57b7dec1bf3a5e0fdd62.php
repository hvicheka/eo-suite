<?php $__env->startPush('head-script'); ?>
    <?php if(count($packages) > 0): ?>
    <style>
        .package-column {
            max-width: <?php echo e(100/count($packages)); ?>%;
            flex: 0 0 <?php echo e(100/count($packages)); ?>%
        }
    </style>
    <?php endif; ?>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <!-- START Pricing Section -->
    <section class="pricing-section bg-white sp-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="sec-title mb-5">
                        <h3><?php echo e($trFrontDetail->price_title ?: $defaultTrFrontDetail->price_title); ?></h3>
                        <p><?php echo e($trFrontDetail->price_description ?: $defaultTrFrontDetail->price_description); ?></p>
                    </div>
                    
                        
                    
                </div>
            </div>
            <div class="text-center mb-5">
                <div class="nav price-tabs justify-content-center" role="tablist">
                    <?php if($monthlyPlan > 0): ?>
                    <a class="nav-link active" href="#monthly" role="tab" data-toggle="tab"><?php echo app('translator')->get('app.monthly'); ?></a>
                    <?php endif; ?>
                    <?php if($annualPlan > 0): ?>
                    <a class="nav-link annual_package " href="#yearly"  role="tab" data-toggle="tab"><?php echo app('translator')->get('app.annual'); ?> <span  class="" style="font-size: 14px;">(free 2 months)</span></a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="tab-content wow fadeIn">
                <div role="tabpanel" class="tab-pane " id="yearly">
                    <div class="container">
                        <div class="price-wrap border row no-gutters">
                            <div class="diff-table col-6 col-md-3">
                                <div class="price-top">
                                    <div class="price-top title">
                                        <h3><?php echo app('translator')->get('app.pickUp'); ?> <br> <?php echo app('translator')->get('app.yourPlan'); ?></h3>
                                        
                                    </div>
                                    <div class="price-content">

                                        <ul>
                                            <li>
                                                <?php echo app('translator')->get('app.max'); ?> <?php echo app('translator')->get('app.menu.employees'); ?>
                                            </li>
                                            <li>
                                                <?php echo app('translator')->get('app.menu.fileStorage'); ?>
                                            </li>
                                            <?php $__currentLoopData = $packageFeatures; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $packageFeature): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if(in_array($packageFeature, $activeModule)): ?>
                                                    <li>
                                                        <?php echo e(__('modules.module.'.$packageFeature)); ?>

                                                    </li>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                                <div class="all-plans col-6 col-md-9">
                                <div class="row no-gutters flex-nowrap flex-wrap overflow-x-auto row-scroll">
                                    <?php $__currentLoopData = $packages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($item->annual_status == '1'): ?>
                                        <div class="col-md-3 package-column">
                                            <div class="pricing-table price-<?php if($item->is_recommended == 1): ?>pro <?php endif; ?>">
                                                <div class="price-top">
                                                    <div class="price-head text-center">
                                                        <h5 class="mb-0"><?php echo e(($item->name)); ?></h5>
                                                    </div>
                                                    <div class="rate">
                                                        <h2 class="mb-2">
                                                            <?php if(currency_position(null,$global->currency->currency_symbol) =='front'): ?>
                                                                <sup><?php echo e($global->currency->currency_symbol); ?></sup>
                                                            <?php endif; ?>
                                                            <span class="font-weight-bolder"><?php echo e($item->annual_price); ?></span>
                                                            <?php if(currency_position(null,$global->currency->currency_symbol) !=='front'): ?>
                                                                <sup><?php echo e($global->currency->currency_symbol); ?></sup>
                                                            <?php endif; ?>

                                                        </h2>
                                                        <p class="mb-0"><?php echo app('translator')->get('app.billedAnnually'); ?></p>
                                                    </div>
                                                </div>
                                                <div class="price-content">
                                                    <ul>
                                                        <li>
                                                            <?php echo e($item->max_employees); ?>

                                                        </li>
                                                        <?php if($item->max_storage_size == -1): ?>
                                                            <li>
                                                                <?php echo app('translator')->get('app.unlimited'); ?>
                                                            </li>
                                                        <?php else: ?>
                                                            <li>
                                                                <?php echo e($item->max_storage_size); ?> <?php echo e(strtoupper($item->storage_unit)); ?>

                                                            </li>
                                                        <?php endif; ?>
                                                        <?php
                                                            $packageModules = (array)json_decode($item->module_in_package);
                                                        ?>
                                                        <?php $__currentLoopData = $packageFeatures; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $packageFeature): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if(in_array($packageFeature, $activeModule)): ?>
                                                                <li>
                                                                    <?php if(in_array($packageFeature, $packageModules)): ?>
                                                                        <i class="zmdi zmdi-check-circle blue"></i>
                                                                    <?php else: ?>
                                                                        <i class="zmdi zmdi-close-circle"></i>
                                                                    <?php endif; ?>
                                                                    &nbsp;
                                                                </li>
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </ul>
                                                </div>
                                                
                                                    
                                                
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane " id="monthly">
                        <div class="container">
                            <div class="price-wrap border row no-gutters">
                                <div class="diff-table col-6 col-md-3">
                                    <div class="price-top">
                                        <div class="price-top title">
                                            <h3><?php echo app('translator')->get('app.pickUp'); ?> <br> <?php echo app('translator')->get('app.yourPlan'); ?></h3>
                                            
                                        </div>
                                        <div class="price-content">

                                            <ul>
                                                <li>
                                                    <?php echo app('translator')->get('app.max'); ?> <?php echo app('translator')->get('app.menu.employees'); ?>
                                                </li>
                                                <li>
                                                    <?php echo app('translator')->get('app.menu.fileStorage'); ?>
                                                </li>
                                                <?php $__currentLoopData = $packageFeatures; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $packageFeature): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if(in_array($packageFeature, $activeModule)): ?>
                                                        <li>
                                                            <?php echo e(__('modules.module.'.$packageFeature)); ?>

                                                        </li>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                    <div class="all-plans col-6 col-md-9">
                                    <div class="row no-gutters flex-nowrap flex-wrap overflow-x-auto row-scroll">
                                        <?php $__currentLoopData = $packages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($item->monthly_status == '1'): ?>
                                            <div class="col-md-3 package-column">
                                                <div class="pricing-table price-<?php if($item->is_recommended == 1): ?>pro <?php endif; ?> ">
                                                    <div class="price-top">
                                                        <div class="price-head text-center">
                                                            <h5 class="mb-0"><?php echo e(($item->name)); ?></h5>
                                                        </div>
                                                        <div class="rate">
                                                            <h2 class="mb-2">
                                                                <?php if(currency_position(null,$global->currency->currency_symbol) =='front'): ?>
                                                                    <sup><?php echo e($global->currency->currency_symbol); ?></sup>
                                                                <?php endif; ?>
                                                                <span class="font-weight-bolder"><?php echo e($item->monthly_price); ?></span>
                                                                <?php if(currency_position(null,$global->currency->currency_symbol) !=='front'): ?>
                                                                    <sup><?php echo e($global->currency->currency_symbol); ?></sup>
                                                                <?php endif; ?>
                                                            </h2>
                                                            <p class="mb-0"><?php echo app('translator')->get('app.billedMonthly'); ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="price-content">
                                                        <ul>
                                                            <li>
                                                                <?php echo e($item->max_employees); ?>

                                                            </li>

                                                            <?php if($item->max_storage_size == -1): ?>
                                                                <li>
                                                                    <?php echo app('translator')->get('app.unlimited'); ?>
                                                                </li>
                                                            <?php else: ?>
                                                                <li>
                                                                    <?php echo e($item->max_storage_size); ?> <?php echo e(strtoupper($item->storage_unit)); ?>

                                                                </li>
                                                            <?php endif; ?>

                                                            <?php
                                                                $packageModules = (array)json_decode($item->module_in_package);
                                                            ?>
                                                            <?php $__currentLoopData = $packageFeatures; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $packageFeature): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <?php if(in_array($packageFeature, $activeModule)): ?>
                                                                    <li>
                                                                        <?php if(in_array($packageFeature, $packageModules)): ?>
                                                                            <i class="zmdi zmdi-check-circle blue"></i>
                                                                        <?php else: ?>
                                                                            <i class="zmdi zmdi-close-circle"></i>
                                                                        <?php endif; ?>
                                                                        &nbsp;
                                                                    </li>
                                                                <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </ul>
                                                    </div>
                                                    
                                                        
                                                    
                                                </div>
                                            </div>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </section>
    <!-- END Pricing Section -->

    <!-- START Section FAQ -->
    <section class="bg-white sp-100-70 pt-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="sec-title mb-60">
                        <h3><?php echo e($trFrontDetail->faq_title ?: $defaultTrFrontDetail->faq_title); ?></h3>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-9">
                    <div id="accordion" class="theme-accordion">
                        <?php $__empty_1 = true; $__currentLoopData = $frontFaqs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $frontFaq): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                            <div class="card border-0 mb-30">
                                <div class="card-header border-bottom-0 p-0" id="acc<?php echo e($frontFaq->id); ?>">
                                    <h5 class="mb-0">
                                        <button class="position-relative text-decoration-none w-100 text-left collapsed"
                                                data-toggle="collapse" data-target="#collapse<?php echo e($frontFaq->id); ?>"
                                                aria-controls="collapse<?php echo e($frontFaq->id); ?>">
                                           <?php echo e($frontFaq->question); ?>

                                        </button>
                                    </h5>
                                </div>

                                <div id="collapse<?php echo e($frontFaq->id); ?>" class="collapse" aria-labelledby="acc<?php echo e($frontFaq->id); ?>" data-parent="#accordion">
                                    <div class="card-body">
                                        <p><?php echo $frontFaq->answer; ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END Section FAQ -->

<?php $__env->stopSection(); ?>
<?php $__env->startPush('footer-script'); ?>
<script>
   <?php if($monthlyPlan <= 0): ?>
        $('.annual_package').removeClass('inactive').addClass('active');
        $('#yearly').removeClass('inactive').addClass('active');
    <?php else: ?>
        $('#monthly').removeClass('inactive').addClass('active');
    <?php endif; ?>

</script>

<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.sass-app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/saas/pricing.blade.php ENDPATH**/ ?>