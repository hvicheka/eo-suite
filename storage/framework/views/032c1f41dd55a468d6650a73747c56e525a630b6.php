<div class="col-xs-12">
    <div class="white-box">

        <div class="row m-t-20">
            <div class="col-xs-12">
                <div class="panel">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body b-all border-radius">
                            <div class="row">
                                
                                <div class="col-md-5">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <img src="<?php echo e($client->image_url); ?>" width="75" height="75" class="img-circle" alt="">
                                        </div>
                                        <div class="col-xs-9">
                                            <p>
                                                <span class="font-medium text-info font-semi-bold"><?php echo e(ucwords($client->name)); ?></span>
                                                <br>

                                                <?php if(!empty($client->client_details) && $client->client_details->company_name != ''): ?>
                                                   <span class="text-muted"><?php echo e($client->client_details->company_name); ?></span>  
                                                <?php endif; ?>
                                            </p>
                                            
                                            
            
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-7 b-l">
                                    <div class="row project-top-stats">

                                        <div class="col-md-6 m-b-20 m-t-10 text-center">
                                            <span class="text-primary">
                                                <?php echo e($clientStats->totalProjects); ?>

                                            </span> <span class="font-12 text-muted m-l-5"> <?php echo app('translator')->get('modules.dashboard.totalProjects'); ?></span>
                                        </div>
        
                                        <div class="col-md-4 m-b-20 m-t-10 text-center">
                                            <span class="text-danger">
                                                <?php echo e($clientStats->totalUnpaidInvoices); ?>

                                            </span> <span class="font-12 text-muted m-l-5"> <?php echo app('translator')->get('modules.dashboard.totalUnpaidInvoices'); ?></span>
                                        </div>
        
                                    </div>
                                    
                                    <div class="row project-top-stats">

                                        <div class="col-md-6 m-b-20 m-t-10 text-center">
                                            <span class="text-success">
                                                <?php echo e($clientStats->projectPayments); ?>

                                            </span> <span class="font-12 text-muted m-l-5"> <?php echo app('translator')->get('app.earnings'); ?></span>
                                        </div>
        
                                        <div class="col-md-4 m-b-20 m-t-10 text-center">
                                            <span class="text-primary">
                                                <?php echo e($clientStats->totalContracts); ?>

                                            </span> <span class="font-12 text-muted m-l-5"> <?php echo app('translator')->get('modules.contracts.totalContracts'); ?></span>
                                        </div>
                        
                                    </div>

                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div><?php /**PATH D:\WAMP-SERVER\www\eosuite\resources\views/admin/clients/client_header.blade.php ENDPATH**/ ?>